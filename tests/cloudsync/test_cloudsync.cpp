#include <QtTest>
#include "../common/common.h"
#include "sql_class.h"
#include "accesscontrol.h"
#include "cloud/cloud_controller.h"

class test_cloudsync : public QObject
{
    Q_OBJECT

public:
    test_cloudsync();
    ~test_cloudsync();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_AccessControl_NotLogged();

};

test_cloudsync::test_cloudsync()
{

}

test_cloudsync::~test_cloudsync()
{

}

void test_cloudsync::initTestCase()
{
    common::initDataBase();
}

void test_cloudsync::cleanupTestCase()
{
    common::clearDatabase();
}

void test_cloudsync::test_AccessControl_NotLogged()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    QSharedPointer<cloud_controller> cloud(new cloud_controller());
    cloud->initAccessControl();
    auto user = ac->user();
    QVERIFY(user);
    // All roles except administrator
    for (auto role : ac->roles()) {
        qDebug() << role.id();
        if (role.id() != Permission::RoleId::Administrator)
            QVERIFY(user->hasRole(role));
    }
}

QTEST_APPLESS_MAIN(test_cloudsync)

#include "test_cloudsync.moc"
