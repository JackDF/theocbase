#include <QtTest>
#include <QDebug>
#include <QDir>
#include "../common/common.h"
#include "slipscanner.h"

class test_scan : public QObject
{
    Q_OBJECT

public:
    test_scan();
    ~test_scan();

private slots:
    void initTestCase();
    void test_case1();
    void cleanupTestCase();
};

test_scan::test_scan()
{

}

test_scan::~test_scan()
{

}

void test_scan::initTestCase()
{
    common::initDataBase();
}

void test_scan::cleanupTestCase()
{
    common::clearDatabase();
}

void test_scan::test_case1()
{
    QString folder = SRCDIR;
    QDir dir(folder);
    QVERIFY2(dir.exists(), qPrintable(QString("Directory '%1' does not exists.").arg(folder)));

    auto fileList = dir.entryList({"*S-89*.jpg"});

    QVERIFY2(fileList.count() > 0, qPrintable(QString("Directory '%1' does not contains any S-89*.jpg file").arg(folder)));

    for (const QString &filename : fileList) {
        SlipScanner scanner(nullptr, dir.path() + QDir::separator() + filename, true);
        if (scanner.needFinalized) {
            scanner.FinalizeBoxes(10);
        }

        int slipsPerPage = scanner.is4up ? 4 : 1;

        QList<int> layouts2019 = { 703 };
        QList<int> layouts2020 = { 604, 802, 406, 208, 505 };
        QList<int> layouts2021 = { 603, 306 };

        int textBoxes = 7;
        int checkBoxes = 10;
        if (layouts2019.contains(scanner.layout)) {
            // slip type 2019
            textBoxes = 4;
        } else if (layouts2020.contains(scanner.layout)) {
            // slip type 2020
            textBoxes = 7;
        } else if (layouts2021.contains(scanner.layout)) {
            // slip type 2021
            textBoxes = 6;
            checkBoxes = 9;
        } else {
            QFAIL(qPrintable(QString("Unknown layout: %1").arg(scanner.layout)));
        }

        int textBoxesPerPage = textBoxes * slipsPerPage;
        int checkBoxesPerPage = checkBoxes * slipsPerPage;
        QCOMPARE(scanner.textBoxes.length(), textBoxesPerPage);
        QCOMPARE(scanner.checkBoxes.length(), checkBoxesPerPage);

        for (int i = 0; i < slipsPerPage; i++) {
            int lastY = 0;
            for (int j = 0; j < 3; j++) {
                QVERIFY(scanner.textBoxes[j + textBoxes * i]->y1 > lastY);
                lastY = scanner.textBoxes[j + textBoxes * i]->y1;
            }
        }
    }
}

QTEST_MAIN(test_scan)

#include "test_scan.moc"
