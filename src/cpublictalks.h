/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPUBLICTALKS_H
#define CPUBLICTALKS_H

#include <QObject>
#include <QDate>
#include <QStandardItemModel>
#include "cpersons.h"
#include "person.h"
#include "ccongregation.h"
#include "sql_class.h"

struct ThemeListItem {
    int id;
    int number;
    QString theme;
    QDate last;
    QString revision;
};

struct cpttheme {
    QString theme = "";
    int number = -1;
    int id = -1;
    QString themeInLanguage(int lang_id, QDate date = QDate::currentDate());
};

class cpoutgoing
{
public:
    cpoutgoing(int id, int congregation_id, QDate weekof);
    ~cpoutgoing() = default;
    cpttheme getTheme() const;
    void setTheme(const cpttheme &t);
    person *getSpeaker() const;
    void setSpeaker(person *p);
    ccongregation::congregation const &getCongregation() const;
    void setCongregation(const int id);
    int getId() const;
    QDate weekof() const;
    QDate date() const;
    QString time() const;

    bool save();

private:
    cpttheme _ctheme;
    QSharedPointer<person> _speaker;
    ccongregation::congregation _congregation;
    int _id;
    QDate _weekof;
    sql_class *sql;
};

class cptmeeting : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date NOTIFY meetingChanged)
    Q_PROPERTY(person *chairman READ chairman WRITE setChairman NOTIFY meetingChanged)
    Q_PROPERTY(int themeNumber READ themeNumber NOTIFY meetingChanged)
    Q_PROPERTY(QString theme READ themeName NOTIFY meetingChanged)
    Q_PROPERTY(person *speaker READ speaker WRITE setSpeaker NOTIFY meetingChanged)
    Q_PROPERTY(QString wtTheme MEMBER wttheme NOTIFY meetingChanged)
    Q_PROPERTY(person *wtConductor READ wtConductor WRITE setWtConductor NOTIFY meetingChanged)
    Q_PROPERTY(person *wtReader READ wtReader WRITE setWtReader NOTIFY meetingChanged)
    Q_PROPERTY(int songTalk MEMBER song_talk NOTIFY meetingChanged)
    Q_PROPERTY(int songWtStart MEMBER song_wt_start NOTIFY meetingChanged)
    Q_PROPERTY(int songWtEnd MEMBER song_wt_end NOTIFY meetingChanged)
    Q_PROPERTY(QString songTalkTitle READ getSong1Title NOTIFY meetingChanged)
    Q_PROPERTY(QString songWtStartTitle READ getSong2Title NOTIFY meetingChanged)
    Q_PROPERTY(QString songWtEndTitle READ getSong3Title NOTIFY meetingChanged)
    Q_PROPERTY(int wtTime MEMBER wt_time NOTIFY meetingChanged)
    Q_PROPERTY(QString wtSource MEMBER wtsource NOTIFY meetingChanged)
    Q_PROPERTY(QString wtIssue MEMBER wtissue NOTIFY meetingChanged)
    Q_PROPERTY(QString finalTalk READ getFinalTalk WRITE setFinalTalk NOTIFY meetingChanged)
    Q_PROPERTY(QString finalTalkSpeakerName MEMBER final_talk_speaker NOTIFY meetingChanged)
    Q_PROPERTY(QString notes MEMBER notes NOTIFY meetingChanged)
    Q_PROPERTY(QDateTime startTime MEMBER start_time NOTIFY meetingChanged)
    Q_PROPERTY(person *finalPrayer READ finalPrayer WRITE setFinalPrayer NOTIFY meetingChanged)
    Q_PROPERTY(person *hospitalityHost READ getHospitalityhost WRITE setHospitalityhost NOTIFY meetingChanged)

public:
    cptmeeting(QDate date = QDate(), QObject *parent = nullptr);
    ~cptmeeting() = default;

    cpttheme theme;
    QString wtsource;
    QString wtissue;
    QString wttheme;
    int song_talk;
    int song_wt_start;
    int song_wt_end;
    int wt_time;
    QString notes;
    QDateTime start_time;
    QString final_talk_speaker;
    int id;

    QDate date() const;

    person *speaker();
    void setSpeaker(person *s);

    person *chairman();
    void setChairman(person *c);

    person *wtConductor();
    void setWtConductor(person *c);

    person *wtReader();
    void setWtReader(person *r);

    int themeNumber() const;

    QString themeName() const;
    Q_INVOKABLE void setTheme(int id);

    person *getHospitalityhost() const;
    void setHospitalityhost(person *hospitalityhost);

    QString getSong1Title() const;
    QString getSong2Title() const;
    QString getSong3Title() const;
    QString getFinalTalk() const;
    void setFinalTalk(const QString &talktheme);

    QColor getWtIssueColor();
    QColor getWtIssueLightColor();

    person *finalPrayer() const;
    void setFinalPrayer(person *prayer);

    Q_INVOKABLE void save();
    Q_INVOKABLE void saveNotes();

signals:
    void meetingChanged();

private:
    QDate _date;
    sql_class *sql;
    QString getSongTitle(int song) const;
    QString m_final_talk;
    person *m_speaker;
    person *m_chairman;
    person *m_wtconductor;
    person *m_wtreader;
    person *m_hospitalityhost;
    person *m_finalprayer;
};

class cpublictalks
{
public:
    cpublictalks();
    cptmeeting *getMeeting(QDate date);
    QList<cpoutgoing *> getOutgoingSpeakers(const QDate d);
    QList<cpoutgoing *> getOutgoingBySpeaker(QDate d, int speakerid, bool onlythismonth = false);
    QList<cpoutgoing *> getOutgoingBySpeaker(int speakerid, QDate fromdate, QDate todate);
    cpoutgoing *getOutgoingByCongregation(const QDate d, const QString &congregationName);
    cpoutgoing *addOutgoingSpeaker(const QDate d, int speakerid, int themeid, int congregationid);
    bool removeOutgoingSpeaker(int id);
    cpttheme getThemeById(int id);
    cpttheme getThemeByNumber(int number, QDate date = QDate::currentDate());
    std::vector<ThemeListItem> getThemeList(const QString &customFilter, person *speaker = nullptr, QDate date = QDate::currentDate(), bool addBlankRow = false);
    QStandardItemModel *getThemesTable(const QString &customFilter, person *speaker = nullptr, QDate date = QDate::currentDate(), bool addBlankRow = false);
    QStandardItemModel *getSpeakers(int themeid = 0, int congregationid = 0, const QString &customFilter = "", bool onlyOwnCongregation = false, bool addBlankRow = true, QDate date = QDate());
    person *defaultWtConductor();

private:
    QString langId;
    sql_class *sql;
};

#endif // CPUBLICTALKS_H
