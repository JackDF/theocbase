#include "lmm_cbs.h"

LMM_CBS::LMM_CBS(QObject *parent) : LMM_Assignment(parent)
{
    m_talkid = LMM_Schedule::TalkType_CBS;
}

LMM_CBS::LMM_CBS(int scheduleDbId, int assignmentDbId, QObject *parent) : LMM_Assignment(LMM_Schedule::TalkType_CBS,scheduleDbId,0,assignmentDbId,parent)
{
}

person *LMM_CBS::reader() const
{
    return m_reader;
}

void LMM_CBS::setReader(person *reader)
{
    m_reader = reader;
    if (m_reader) m_reader->setParent(this);
    emit readerChanged();
}

SortFilterProxyModel *LMM_CBS::getReaderList()
{
    enum MyRoles {
        id = Qt::UserRole+1,
        name,
        date,
        icon
    };
    QHash<int,QByteArray> roles;
    roles[MyRoles::id] = "id";
    roles[MyRoles::name] = "name";
    roles[MyRoles::date] = "date";
    roles[MyRoles::icon] = "icon";

    QStandardItemModel *stdmodel = new QStandardItemModel(0,3,this);
    stdmodel->setItemRoleNames(roles);
    stdmodel->setHorizontalHeaderItem(0,new QStandardItem("Id"));
    stdmodel->setHorizontalHeaderItem(1,new QStandardItem("Name"));
    stdmodel->setHorizontalHeaderItem(2,new QStandardItem("Date"));

    sql_class *sql = &Singleton<sql_class>::Instance();
    int typeValue = person::CBSReader;
    // query should change to use LMM tables !!!!!
    QString str = "SELECT biblestudyMeeting.date, persons.id, persons.lastname, persons.firstname, persons.congregation_id,mmp.multiused_person "
                  "FROM persons "
                  "left join (select distinct 1 multiused_person,person_id,date "
                  "from (select distinct person_id,date,mtype "
                  "from personmidweek where mtype <> 'PRAYER' and date = '%1') "
                  "group by date,person_id ) mmp "
                  "ON (persons.id = mmp.person_id ) "
                  "LEFT JOIN biblestudyMeeting ON persons.id = biblestudyMeeting.";
    //person_id";
    if (typeValue == person::CBSReader){
        str.append("reader_id");
    }else{
        str.append("person_id");
    }
    str.append(" WHERE usefor & " + QString::number(typeValue) +
               " AND congregation_id = " + sql->getSetting("congregation_id") +
               " AND persons.active = 1"
               " GROUP BY persons.id");
    str = str.arg(LMM_Assignment::date().toString(Qt::ISODate));
    sql_items items = sql->selectSql(str);

    // empty row
    stdmodel->setRowCount(1);

    for(unsigned int i = 0;i<items.size();i++){
        QStandardItem *item = new QStandardItem();
        item->setData(items[i].value("id").toInt(),MyRoles::id);
        item->setData(items[i].value("firstname").toString() + " " +
                      items[i].value("lastname").toString(), MyRoles::name);
        item->setData(items[i].value("date").toDate().toString(Qt::ISODate), MyRoles::date);
        item->setData("qrc:///icons/warning-already-assigned.png", MyRoles::icon);
        stdmodel->setRowCount(stdmodel->rowCount()+1);
        stdmodel->setItem(i+1,item);
    }
    SortFilterProxyModel *sortmodel = new SortFilterProxyModel(this, stdmodel, MyRoles::name, "cbsreader");
    return sortmodel;
}

bool LMM_CBS::save()
{
    m_assistant_id = reader() ? reader()->id() : -1;
    m_volunteer_id = -1;
    return LMM_Assignment::save();
}

