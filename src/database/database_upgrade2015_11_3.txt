﻿delete from lmm_workbookregex;
INSERT INTO lmm_workbookregex VALUES ("en","date","^(?<month>\D+)\s(?<fromday>\d+)\D*\s*(?<thruday>\d+)$",null);
INSERT INTO lmm_workbookregex VALUES ("en","song","Song\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("en","timing","[(](?<timing>\d+)\smin.(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("en","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("af","date","^(?<fromday>\d+)\s*(?<month1>[^–]*)\D*(?<thruday>\d+)\s*(?<month2>\D*)$",null);
INSERT INTO lmm_workbookregex VALUES ("af","song","Lied\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("af","timing","[(](?<timing>\d+)\smin.(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("af","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("fi","date","^(?<fromday>\d+)[.]\s*(?<month1>[^\s–]*)\s*–\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("fi","song","Laulu\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("fi","timing","[(](?<timingextra>\D*)(?<timing>\d+)\smin\.*[)]",null);
INSERT INTO lmm_workbookregex VALUES ("fi","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("de","date","^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*–\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("de","song","Lied\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("de","timing","[(](?<timingextra>[^[)\d]*)(?<timing>\d+)\sMin.[)]",null);
INSERT INTO lmm_workbookregex VALUES ("de","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("fr","date","^(?<fromday>\d+)[er\s]*(?<month1>[^-–]*)[-–](?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("fr","song","Cantique\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("fr","timing","[(]•*\s*(?<timing>\d+)\smin(?<timingextra>[^)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("fr","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("el","date","^(?<fromday>\d+)\s*(?<month1>[^-]*)-(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("el","song","Ύμνος\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("el","timing","[(](?<timing>\d+)\sλεπτά(?<timingextra>[^[)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("el","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("hu","date","^(?<month>[^\s]+)\s+(?<fromday>\d+)\D+(?<thruday>\d+)[.]$",null);
INSERT INTO lmm_workbookregex VALUES ("hu","song","(\d+).\sének(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("hu","timing","[(](?<timingextra>\D*)(?<timing>\d+)\s*perc\s*[)]",null);
INSERT INTO lmm_workbookregex VALUES ("hu","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("es","date","^(?<fromday>\d+)\s*d*e*\s*(?<month1>[^\s]*)\s*[a-]\s*(?<thruday>\d+)\sde\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("es","song","Canción\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("es","timing","[(](?<timing>\d+)\smins.(?<timingextra>[^)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("es","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("nl","date","^(?<fromday>\d+)\s*(?<month1>\D*)[-–](?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("nl","song","Lied\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("nl","timing","[(](?<timing>\d+)\smin.(?<timingextra>[^[)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("nl","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("pt","date","^(?<fromday>\d+)\s*d*e*\s*(?<month1>[^\s]*)\s*[–-]\s*(?<thruday>\d+)\sde\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("pt","song","Cântico\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("pt","timing","[(](?<timing>\d+)\smin[.]*(?<timingextra>[^)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("pt","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("ru","date","^(?<fromday>\d+)\s*(?<month1>\D*)\s*—\s*(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("ru","song","Песня\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("ru","timing","[(](?<timing>\d+)\sмин(?<timingextra>[^)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("ru","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("da","date","^(?<fromday>\d+)[.]\s*(?<month>\D*)\s*[–-]\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("da","song","Sang nr.\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("da","timing","[(](?<timingextra>[^[)\d]*)(?<timing>\d+)\smin[.]*[)]",null);
INSERT INTO lmm_workbookregex VALUES ("da","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("it","date","^(?<fromday>\d+)\s*(?<month1>\D*)[–-]\s*(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("it","song","Cantico\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("it","timing","[(](?<timing>\d+)\smin(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("it","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("sv","date","^(?<fromday>\d+)\s*(?<month1>\D*)–(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("sv","song","Sång\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("sv","timing","[(](?<timingextra>[^[)\d]*)(?<timing>\d+)\smin[.]*[)]",null);
INSERT INTO lmm_workbookregex VALUES ("sv","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("no","date","^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*[––]\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("no","song","Sang nr.\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("no","timing","[(](?<timing>\d+)\smin(?<timingextra>[^[)]*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("no","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("cs","date","^(?<fromday>\d+)[.]\s*(?<month1>[^\s–]*)–(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("cs","song","Píseň č.\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("cs","timing","[(](?<timing>\d+)\smin[.]*(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("cs","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("et","date","^(?<fromday>\d+)[.]\s*(?<month1>[^\s–]*)\s*–\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("et","song","Laul\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("et","timing","[(](?<timingextra>\D*)(?<timing>\d+)\smin\.*[)]",null);
INSERT INTO lmm_workbookregex VALUES ("et","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("hr","date","^(?<fromday>\d+)[.]*\s*(?<month1>[^\s–]*)\s*[—-]\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("hr","song","Pjesma\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("hr","timing","[(](?<timing>\d+)\smin\.*(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("hr","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("ka","date","^(?<fromday>\d+)\s*(?<month1>[^\s–]*)\s*—\s*(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("ka","song","სიმღერა\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("ka","timing","[(](?<timing>\d+)\sწთ.(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("ka","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("lt","date","^(?<month1>\D+)\s+(?<fromday>\d+)–(?<month2>\D*)\s*(?<thruday>\d+)\sd[.]$",null);
INSERT INTO lmm_workbookregex VALUES ("lt","song","(\d+)\sgiesmė",null);
INSERT INTO lmm_workbookregex VALUES ("lt","timing","[(](?<timing>\d+)\smin\.*(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("lt","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("pl","date","^(?<fromday>\d+)\s*(?<month1>[^\s-]*)\s*[do-]+\s*(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("pl","song","Pieśń\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("pl","timing","[(](?<timing>\d+)\smin(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("pl","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("ro","date","^(?<fromday>\d+)\s*(?<month1>\D*)\s*[–-]\s*(?<thruday>\d+)\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("ro","song","Cântarea\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("ro","timing","[(](?<timingextra>\D*)(?<timing>\d+)\smin\.*[)]",null);
INSERT INTO lmm_workbookregex VALUES ("ro","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
INSERT INTO lmm_workbookregex VALUES ("sk","date","^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*–\s*(?<thruday>\d+)[.]\s(?<month2>\D+)$",null);
INSERT INTO lmm_workbookregex VALUES ("sk","song","^Pieseň č.\s(\d+)",null);
INSERT INTO lmm_workbookregex VALUES ("sk","timing","[(](?<timing>\d+)\smin\.*(?<timingextra>\D*)[)]",null);
INSERT INTO lmm_workbookregex VALUES ("sk","assignment1","•(?<theme>[^•]+)•*(?<source>.*)",null);
insert into lmm_workbookregex (lang, key, value) select 'bg', 'date', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*[–—]\s*(?<thruday>\d+)\s*(?<month2>[\D]+)'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'song', 'Песен\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'timing', '[(](?<timingextra>\D*)\s*(?<timing>\d+)\sмин[)]'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
insert into lmm_workbookregex (lang, key, value) select 'NV', 'date', '^(?<month>\D+)\s(?<fromday>\d+)\D*\s*(?<thruday>\d+)$'
insert into lmm_workbookregex (lang, key, value) select 'NV', 'song', '\s*Sin\s*(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'NV', 'timing', '[(](?<timing>\d+)\smin[.]*(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'NV', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
insert into lmm_workbookregex (lang, key, value) select 'vi', 'date', '^Ngày (?<fromday>\d+)[\s-\d]+(?<month1>tháng\s\d+)'
insert into lmm_workbookregex (lang, key, value) select 'vi', 'song', 'Bài hát\s*(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'vi', 'timing', '[(](?<timing>\d+)\sphút(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'vi', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'date', '^(?<fromday>\d+)\s*(?<month1>[^\s]*)[\s-–]*(?<thruday>\d+)\s*(?<month2>\D+)$'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'song', 'Kantik\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'timing', '[(](?<timing>\d+)\smin\D+(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'date', '^(?<fromday>\d+)\s*(?<month1>[^\s]*)[\s-–]*(?<thruday>\d+)\s*(?<month2>\D+)$'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'song', 'Lagu\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'timing', '[(](?<timing>\d+)\smin\D+(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
insert into lmm_workbookregex (lang, key, value) select 'TL', 'date', '^(?<month1>[^\s]*)\s*(?<fromday>\d+)\s*[\s-–]*(?<month2>\D+)\s*(?<thruday>\d+)$'
insert into lmm_workbookregex (lang, key, value) select 'TL', 'song', 'Awit\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'TL', 'timing', '[(](?<timing>\d+)\smin[.](?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'TL', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
INSERT INTO settings (name, value, active, time_stamp) select "ht_months","janvye;fevriye;mas;avril;me;jen;jiyè;out;septanm;oktòb;novanm;desanm", 1, strftime('%s','now');
INSERT INTO settings (name, value, active, time_stamp) select "slip1_e_ckboxnames","1;2;3;4;5;6;7;8", 1, strftime('%s','now');
INSERT INTO settings (name, value, active, time_stamp) select "slip1_e_boxnames","1;2;3;4;5", 1, strftime('%s','now');
INSERT INTO settings (name, value, active, time_stamp) select "slip4_e_ckboxnames","5;6;7;8;9;11;12;13;18;19;20;21;22;24;25;26;31;32;33;34;35;37;38;39;44;45;46;47;48;50;51;52", 1, strftime('%s','now');
INSERT INTO settings (name, value, active, time_stamp) select "slip4_e_boxnames","1;2;3;4;10;14;15;16;17;23;27;28;29;30;36;40;41;42;43;49", 1, strftime('%s','now');
update lmm_schedule set time_stamp = strftime('%s','now')
update lmm_meeting set time_stamp = strftime('%s','now')
update lmm_assignment set time_stamp = strftime('%s','now')
