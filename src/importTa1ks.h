/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMPORTTA1KS_H
#define IMPORTTA1KS_H

#include <QString>
#include <QDebug>
#include <QMessageBox>
#include <QTextCodec>
#include <cstdint>
#include "ccongregation.h"
#include "person.h"
#include "cpublictalks.h"

class importTa1ks
{
public:
    importTa1ks(QString directory);
    void Go();

private:
    bool TestBit(char A[], int k);
    bool importCircuits(QString filename);
    bool importCongregations(QString filename);
    bool importSpeakers(QString filename);
    bool importSpeakersXtend(QString filename);
    bool importTalks(QString filename);
    bool importOutSpeaker(QDate date, uint16_t spkrID, uint16_t talkID, uint16_t congID);
    bool importHistory(QString filename);
    QTime time2QTime(char* time);
    QString win1252toUnicode(std::string str);

    sql_class *sql;

    /**
     * @brief m_directory - directory of Ta1ks data selected by user
     */
    QDir m_directory;

    /**
     * @brief The Ta1ks - circuit struct
     */
    struct CircuitData{
        char name[30];
        uint16_t circuitID;
    };
    QHash<uint16_t, QString> circuitList;

    /**
     * @brief congOldIdToNewID - key is Ta1ks congregation ID, value is our congregation ID
     */
    QHash<int, int> congOldIdToNewID;

    QList<int> coordinatorList;
    QList<int> secretaryList;
    QList<int> talkCoordinatorList;

    /**
     * @brief The Ta1ks - congregation struct
     */
    struct CongData{
        char name[50];
        char address1[41];
        char address2[41];
        char tel1[16];
        char tel2[16];
        char oddTime[10];
        uint16_t congregationID;
        uint16_t unknown_1;
        uint16_t circuitID;
        uint16_t unknown_2;
        uint16_t coordinatorOfBOE;
        uint16_t secretary;
        uint16_t talkCoordinator;
        char evenTime[10];
        char unknown_3[28];
        char oddWeekDay;
        char evenWeekDay;
        char rest4[28];
    };

    /**
     * @brief congOldIdToNewID - key is Ta1ks speaker/brother ID, value is our person ID
     */
    QHash<int, int> speakerOldIdToNewID;

//    struct
//    {
//        unsigned short talkNr : 1;
//    } publicTalks[200];

    /**
     * @brief The Ta1ks - speaker struct
     */
    struct SpeakerData{
        char firstName[20];
        char lastName[20];
        char address1[40];
        char address2[40];
        char phone1[16];
        char talks[32];         // bitfield, 200 public talks, 56 other talks (memorial, funeral, ..)
        char unknown_1[32];
        char servant;           // 1 = ministerial servant; 2 = elder; 32 = suspended
        char privileges;        // 2 = outgoing speaker; 4 = chairman; 8 = ?; 16 = wtreader
        uint16_t congregationID;
        uint16_t speakerID;
        char unknown_3[50];
    };    

    /**
     * @brief The Ta1ks - extended speaker struct
     */
    struct SpeakerXtendData{
        uint16_t speakerID;
        char phone2[16];
        char phone3[16];
        char email[48];
        char remark[46];
    };

    /**
     * @brief congOldIdToNewID - key is Ta1ks public talk ID, value is our public talk ID
     */
    QHash<int, int> talkOldIdToNewID;

    /**
     * @brief The Ta1ks - talk struct
     */
    struct TalkData{
        char theme[93];
        char number[7];
        uint16_t talkID;
        uint16_t topics[26];
    };

    /**
     * @brief The Ta1ks - blackout (speaker's availability) struct
     */
    struct BlackoutData{
        uint16_t speakerID;
        uint16_t Week;
        char unknown_1[4];
    };

    /**
     * @brief The Ta1ks - history struct
     */
    struct HistoryData{
        uint16_t date;
        uint16_t inSpeakerID;
        uint16_t inTalkID;
        uint16_t inCongID;
        uint16_t requestedDate;
        uint16_t confirmedDate;
        uint16_t remindedDate;
        uint16_t chairmanID;
        uint16_t wtReaderID;
        uint16_t hospitalityID;
        char unknown_1[8];
        uint16_t outSpkr1ID;
        uint16_t outSpkr2ID;
        uint16_t outSpkr3ID;
        uint16_t outSpkr4ID;
        uint16_t outSpkr5ID;
        uint16_t outSpkr6ID;
        uint16_t outCong1ID;
        uint16_t outCong2ID;
        uint16_t outCong3ID;
        uint16_t outCong4ID;
        uint16_t outCong5ID;
        uint16_t outCong6ID;
        uint16_t outTalk1ID;
        uint16_t outTalk2ID;
        uint16_t outTalk3ID;
        uint16_t outTalk4ID;
        uint16_t outTalk5ID;
        uint16_t outTalk6ID;
    };

    /**
     * @brief The Scheduling enum
     */
    enum Scheduling {
        // use for bit values
        Unknown = 1 << 0,       //                                       1
        Assembly = 1 << 1,      //                                       2
        COVisit = 1 << 2,       //  circuit overseer's visit             4
        Other = 1 << 3,         //                                       8
        Chairman = 1 << 4,      //  public meeting chairman             16
        Hospitality = 1 << 5,   //  hospitality for public speaker      32
        WtReader = 1 << 6,      //  watchtower reader                   64
    };

    /**
     * @brief The Ta1ks - special events struct
     */
    struct SpecialData{
        uint16_t Week;
        uint16_t scheduling;
        char speakerOrEvent[28];
        char talkTitle[64];
    };
};

#endif // IMPORTTA1KS_H
