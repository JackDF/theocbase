/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printweekendschedule.h"

PrintWeekendSchedule::PrintWeekendSchedule()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::PublicMeetingTemplate, "publicmeetingTemplate", "WE-Schedule_1.htm", "we-schedule"));
    qrCodeSupport = true;
}

QVariant PrintWeekendSchedule::fillTemplate()
{
    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);

    fillWeekList(publicmeetingday - 1);

    str.replace("!TITLE!", sql->getSetting("print_we_title", tr("Weekend Meeting")));
    str.replace("!PM!", tr("Public Meeting"));

    // titles for all meetings
    str.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    str.replace("!CONGREGATION!", tr("Congregation"));
    str.replace("!CONGREGATION_NAME!", myCongregation.name);
    str.replace("!CONDUCTOR!", tr("Conductor"));
    str.replace("!CHAIRMAN!", tr("Chairman"));
    str.replace("!SPEAKER!", tr("Speaker", "Public talk speaker"));
    str.replace("!READER!", tr("Reader"));
    str.replace("!PT!", tr("Public Talk"));
    str.replace("!WT!", tr("Watchtower Study"));
    str.replace("!CBS!", tr("Congregation Bible Study"));
    str.replace("!LM!", tr("Christian Life and Ministry Meeting"));
    str.replace("!DATE_TITLE!", tr("Date"));
    str.replace("!PHONE!", tr("Phone", "Phone number title"));
    str.replace("!HOST!", tr("Host", "Host for incoming public speaker"));

    QPair<QString, QString> sec = templateGetSection(str, "!REPEAT_START!", "!REPEAT_END!", "publicmeeting");
    QString basepage = sec.first;
    QString section = sec.second;

    // titles
    basepage.replace("!PT_SPEAKER!", tr("Speaker"));
    basepage.replace("!PT_CHAIRMAN!", tr("Chairman"));

    basepage.replace("!READER!", tr("Reader"));
    basepage.replace("!WT_READER!", tr("WT Reader"));

    basepage.replace("!DATE_TITLE!", tr("Date"));
    basepage.replace("!PT_THEME_TITLE!", tr("Theme"));

    QString weekContext = "";

    for (weekInfo *w : weekList) {
        currentWeek = w;
        QString oneweek(section);
        oneweek = fillTemplateBlockPM(oneweek);
        weekContext.append(oneweek);
    }

    basepage.replace("<div class=\"publicmeeting\"></div>", "<div>" + weekContext + "</div>");
    // Add QR Code
    // TODO: ??basepage = addQrCode(basepage);

    return ifthen.compute(basepage);
}

QString PrintWeekendSchedule::fillTemplateBlockPM(QString context)
{
    QSharedPointer<cpublictalks> cpt(new cpublictalks());
    QSharedPointer<cptmeeting> meeting(cpt->getMeeting(currentWeek->week));
    ccongregation::exceptions ex = c.isException(currentWeek->week);
    int md = c.getMeetingDay(currentWeek->week, ccongregation::pm);

    bool showSongTitles = currentTemplateData->optionValue("song-titles") == "yes";
    bool showWatchtowerNumber = currentTemplateData->optionValue("wt-number") == "yes";
    bool useDuration = currentTemplateData->optionValue("duration") == "duration";

    context = replaceDateTag(context, "DATE", currentWeek->week.addDays(md == 0 ? publicmeetingday - 1 : md - 1));
    context = fillCommonItems(context, md == 0, ex);

    // title
    context.replace("!PT!", tr("Public Talk"));
    context.replace("!PT_TIME!", "30");
    context.replace("!WT!", tr("Watchtower Study"));
    context.replace("!CO!", tr("Circuit Overseer"));
    context.replace("!WT_TIME!", ex == ccongregation::CircuitOverseersVisit ? "30" : "60");
    // notes
    context.replace("!NOTES!", tr("Notes"));
    context.replace("!PM_NOTES!", meeting->notes);

    QTime starttime = QTime::fromString(myCongregation.getPublicmeeting(currentWeek->week).getMeetingtime(), "hh:mm");
    context = replaceTimeTag(context, "PM_STARTTIME", starttime);
    context = replaceTimeTag(context, "SONG1_STARTTIME", starttime);
    context = replaceTimeTag(context, "PT_STARTTIME", starttime.addSecs(60 * 5));
    context = replaceTimeTag(context, "SONG2_STARTTIME", starttime.addSecs(60 * 35));
    context = replaceTimeTag(context, "WT_STARTTIME", starttime.addSecs(60 * 40));
    context = replaceTimeTag(context, "SONG3_STARTTIME", starttime.addSecs(60 * 100));

    // prayer title
    context.replace("!PRAYER!", tr("Prayer"));

    // song title
    context.replace("!SONG!", tr("Song"));

    if (useDuration)
        context.replace("!DURATION!", "Yes");
    else
        context.replace("!DURATION!", "");

    // pt song
    if (meeting->song_talk > 0)
        context = replaceIntTag(context, "SONG1_NO", meeting->song_talk);
    else
        context.replace("!SONG1_NO!", "");
    context.replace("!SONG1_NAME!", showSongTitles ? meeting->getSong1Title() : "");

    // wt song1
    if (meeting->song_wt_start > 0)
        context = replaceIntTag(context, "SONG2_NO", meeting->song_wt_start);
    else
        context.replace("!SONG2_NO!", "");
    context.replace("!SONG2_NAME!", showSongTitles ? meeting->getSong2Title() : "");

    // wt song2
    if (meeting->song_wt_end > 0)
        context = replaceIntTag(context, "SONG3_NO", meeting->song_wt_end);
    else
        context.replace("!SONG3_NO!", "");
    context.replace("!SONG3_NAME!", showSongTitles ? meeting->getSong3Title() : "");

    // theme
    context.replace("!PT_THEME!", meeting->theme.themeInLanguage(sql->getLanguageDefaultId(), currentWeek->week));
    context = replaceIntTag(context, "PT_NUMBER", meeting->theme.number);

    QRegularExpression rx("!PT_GRP_THEME_[A-Z|a-z]*!");
    int grppos = context.indexOf(rx);
    if (grppos > -1) {
        QRegularExpressionMatch match = rx.match(context);
        QStringList grpTexts = match.capturedTexts();
        context.replace(rx, meeting->theme.themeInLanguage(sql->getLanguageId(grpTexts.at(0).right(3).left(2).toLower()), currentWeek->week));
    }

    context.replace("!CONGREGATION_TITLE!", tr("%1 Congregation", "Congregation_Title Tag. %1 is Congregation Name").arg(myCongregation.name));
    context.replace("!CONGREGATION!", tr("Congregation"));
    context.replace("!CONGREGATION_NAME!", myCongregation.name);

    // speaker
    context.replace("!PT_SPEAKER!", meeting->speaker() ? meeting->speaker()->fullname() : "");
    context.replace("!PT_SPEAKER_PHONE!", meeting->speaker() ? meeting->speaker()->phone() : "");
    context.replace("!PT_SPEAKER_MOBILE!", meeting->speaker() ? meeting->speaker()->mobile() : "");
    sql_item congregationInfo;
    if (meeting->speaker())
        congregationInfo = meeting->speaker()->congregationInfo()[0];
    context.replace("!PT_SPEAKER_CONGREGATION!", meeting->speaker() ? congregationInfo.value("name").toString() : "");
    context.replace("!PT_SPEAKER_CONGREGATION_ADDRESS!", meeting->speaker() ? congregationInfo.value("address").toString() : "");
    context.replace("!PT_SPEAKER_CONGREGATION_CIRCUIT!", meeting->speaker() ? congregationInfo.value("circuit").toString() : "");
    context.replace("!PT_SPEAKER_CONGREGATION_INFO!", meeting->speaker() ? congregationInfo.value("info").toString() : "");

    // hospitality
    context.replace("!PT_SPEAKER_HOST!", meeting->getHospitalityhost() ? meeting->getHospitalityhost()->fullname() : "");

    // chairman
    context.replace("!PM_CHAIRMAN!", meeting->chairman() ? meeting->chairman()->fullname() : "");

    // reader
    context.replace("!WT_READER!", meeting->wtReader() ? meeting->wtReader()->fullname() : "");

    // wt details
    context.replace("!WT_ARTICLE!", meeting->wtsource);
    context.replace("!WT_COLOR!", meeting->getWtIssueColor().name());
    context.replace("!WT_LIGHT_COLOR!", meeting->getWtIssueLightColor().name());

    if (showWatchtowerNumber) {
        context.replace("!WT_NO!", meeting->wtissue);
    } else {
        context.replace("!WT_NO!", "");
    }
    context.replace("!WT_THEME!", meeting->wttheme);

    // wt conductor
    context.replace("!WT_CONDUCTOR_TITLE!", tr("Watchtower Conductor"));
    context.replace("!WT_CONDUCTOR!", meeting->wtConductor() ? meeting->wtConductor()->fullname() : "");

    context.replace("!NO_MEETING!", tr("No regular meeting"));
    context.replace("!CO_THEME!", meeting->getFinalTalk());
    context.replace("!CO_TIME!", QVariant(ex == ccongregation::CircuitOverseersVisit ? 30 : 0).toString());
    context = replaceTimeTag(context, "CO_STARTTIME", starttime.addSecs(60 * (ex == ccongregation::CircuitOverseersVisit ? 70 : 0)));

    context.replace("!PRAYER2_NAME!", meeting->finalPrayer() ? meeting->finalPrayer()->fullname() : "");

    context.remove("!PT_TITLE!");
    context.remove("!PT_THEME!");
    context.remove("!PT_NO!");
    context.remove("!PT_SPEAKER!");
    context.remove("!PT_SPEAKER_PHONE!");
    context.remove("!PT_SPEAKER_CONGREGATION!");
    context.remove("!PT_CHAIRMAN!");
    context.remove("!PT_CHAIRMAN_NAME!");
    context.remove("!PT_CONDUCTOR_NAME!");
    context.remove("!WT_ARTICLE");
    context.remove("!WT_THEME!");
    context.remove("!WT_CONDUCTOR_TITLE!");
    context.remove("!WT_READER!");
    context.remove("!WT_READER_NAME!");
    context.remove("!PRAYER2_NAME!");
    return QString(context);
}
