/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOLREMINDER_H
#define SCHOOLREMINDER_H

#include <QString>
#include <QFile>
#include <QTextStream>
#include "sql_class.h"
#include "cpersons.h"
#include "ccongregation.h"
#include "lmm_meeting.h"
#include "lmm_schedule.h"
#include "lmm_assignment.h"
#include "lmm_assignment_ex.h"
#include "ical.h"

class schoolreminder : QObject
{
    Q_OBJECT
public:
    schoolreminder(QObject *parent = nullptr);
    ~schoolreminder();
    QString reminderSubject;
    QString theme;
    QString reminderText;
    QString toName;
    QString toEmail;
    QDate date;
    int id;
    int userId;
    int objectId;
    int objectType;
    int counter;
    bool cancelled;

    /**
     * @brief getEmailReminders - Get all unsent email reminders of school assignment
     * @return - The list of email reminders
     */
    QList<schoolreminder *> getRemindersList(QDate from = QDate::currentDate(), QDate to = QDate());

    /**
     * @brief save - Save current reminder
     */
    void save();

    /**
     * @brief create_ical_file - Create ical file content
     * @return  - Content of file in QString
     */
    QString get_iCal();

private:
    ccongregation c;
    void fillReminder(QList<schoolreminder *> &list, const sql_item sitem, const QDate d, const QString field, QString assignmentName, bool cancellation = false);
};

#endif // SCHOOLREMINDER_H
