#ifndef GENERAL_H
#define GENERAL_H

#include <QProcess>
#include <QFileInfo>
#include <QDir>
#include <QDate>
#include <QLocale>
#include <QDesktopServices>
#include <QUrl>
#include <QPixmap>
#include <QPainter>
#include <QIcon>
#include <QToolButton>
#include <QPixmap>
#include <QBitmap>
#include <QListWidgetItem>

class general
{
public:
    static QDate TextToDate(QString text);
    static QString RemoveAccents(QString const& s);
    static void ShowInGraphicalShell(QWidget *parent, const QString &location);
    static QIcon changeIconColor(QIcon icon, QColor color);

    static void changeButtonColors(QList<QToolButton *> items, QColor paletteBackgroundColor);
    static void changeListWidgetItemColor(QList<QListWidgetItem*> items, QColor backgroundColor);
};

#endif // GENERAL_H
