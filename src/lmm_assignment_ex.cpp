#include "lmm_assignment_ex.h"
#include "sqlcombo.h"

LMM_Assignment_ex::LMM_Assignment_ex(QObject *parent)
    : LMM_Assignment(parent)
{
}

LMM_Assignment_ex::LMM_Assignment_ex(int talkId, int sequence, int scheduleDbId, int assignmentDbId, QObject *parent)
    : LMM_Assignment(talkId, sequence, scheduleDbId, assignmentDbId, parent)
{
}

person *LMM_Assignment_ex::assistant() const
{
    return m_assistant;
}

void LMM_Assignment_ex::setAssistant(person *assistant)
{
    m_assistant = assistant;
    if (m_assistant)
        m_assistant->setParent(this);
    emit assistantChanged();
}

person *LMM_Assignment_ex::volunteer() const
{
    return m_volunteer;
}

void LMM_Assignment_ex::setVolunteer(person *volunteer)
{
    m_volunteer = volunteer;
    if (m_volunteer)
        m_volunteer->setParent(this);
    emit volunteerChanged();
}

QString LMM_Assignment_ex::timing() const
{
    return m_timing;
}

void LMM_Assignment_ex::setTiming(QString time)
{
    m_timing = time;
    emit timingChanged(time);
}

bool LMM_Assignment_ex::completed() const
{
    return m_completed;
}

void LMM_Assignment_ex::setCompleted(bool completed)
{
    m_completed = completed;
    emit completedChanged(completed);
}

QString LMM_Assignment_ex::setting() const
{
    return m_setting;
}

void LMM_Assignment_ex::setSetting(QString setting)
{
    m_setting = setting;
}

SortFilterProxyModel *LMM_Assignment_ex::getAssistantList()
{
    QHash<int, QByteArray> roles;
    roles[MySortFilterProxyModel::MyRoles::id] = "id";
    roles[MySortFilterProxyModel::MyRoles::name] = "name";
    roles[MySortFilterProxyModel::MyRoles::date] = "date";
    roles[MySortFilterProxyModel::MyRoles::date_val] = "date_val";
    roles[MySortFilterProxyModel::MyRoles::date2] = "date2";
    roles[MySortFilterProxyModel::MyRoles::date2_val] = "date2_val";
    roles[MySortFilterProxyModel::MyRoles::icon] = "icon";

    QStandardItemModel *itemmodel = new QStandardItemModel(0, 5, this);
    itemmodel->setItemRoleNames(roles);
    itemmodel->setHorizontalHeaderItem(0, new QStandardItem("Id"));
    itemmodel->setHorizontalHeaderItem(1, new QStandardItem("Name"));
    itemmodel->setHorizontalHeaderItem(2, new QStandardItem("Date"));
    itemmodel->setHorizontalHeaderItem(3, new QStandardItem("Date2"));
    itemmodel->setHorizontalHeaderItem(4, new QStandardItem("Icon"));

    sql_class *sql = &Singleton<sql_class>::Instance();

    SqlComboHelper sch;

    sch.set_talk_conductor(static_cast<LMM_Schedule::TalkTypes>(talkId()));
    sch.set_classnumber(static_cast<short>(m_classnumber));
    sch.set_date(LMM_Schedule::date());
    sch.set_assisting_id(this->speaker() ? this->speaker()->id() : -1);
    sql_items items = sql->selectSql(sch.get_sql());

    // empty row
    itemmodel->setRowCount(1);

    QVector<QList<int>> lists;
    lists.resize(3);

    loadPersonsOnBreak(lists[0]);
    loadMultiAssignedPersons(lists[1]);
    loadFamilyMembers(lists[2]);
    QVector<int> person_ids;
    QVector<short> pris;
    getPersonIdsFromQryResult(person_ids, items, "id");
    getPersonsIconAlertValues(pris, person_ids, lists);
    ccongregation c;

    QString nameFormat = sql->getSetting("nameFormat", "%2, %1");

    for (unsigned int i = 0; i < items.size(); i++) {
        QStandardItem *item = new QStandardItem();
        item->setData(items[i].value("id").toInt(), MySortFilterProxyModel::MyRoles::id);
        item->setData(nameFormat.arg(items[i].value("firstname").toString(),
                                     items[i].value("lastname").toString()),
                      MySortFilterProxyModel::MyRoles::name);
        QDate dateany = items[i].value("date_any").toDate();
        dateany = dateany.addDays(c.getMeetingDay(dateany, ccongregation::tms) - 1);
        item->setData(dateany, MySortFilterProxyModel::MyRoles::date2_val);

        QString str = dateany.toString("yyyy-MM-dd");
        if (str != "") {
            str += " " + LMM_Schedule::getStringTalkType(items[i].value("talk_id").toString().split(" ")[0].toInt());
            str += "c";
            str += items[i].value("classnumber_any").toString();
            str += (items[i].value("assignee_id").toInt() == 0 ? "S" : "A");
        }
        item->setData(str, MySortFilterProxyModel::MyRoles::date2);

        QDate datespecific = items[i].value("date_specific").toDate();
        datespecific = datespecific.addDays(c.getMeetingDay(datespecific, ccongregation::tms) - 1);
        item->setData(datespecific, MySortFilterProxyModel::MyRoles::date_val);
        str = datespecific.toString("yyyy-MM-dd");
        if (str != "") {
            str += " c";
            str += items[i].value("classnumber_specific").toString();
        }
        item->setData(str, MySortFilterProxyModel::MyRoles::date);
        item->setData(items[i].value("firstname"), MySortFilterProxyModel::MyRoles::h_firstname);
        item->setData(items[i].value("lastname"), MySortFilterProxyModel::MyRoles::h_lastname);
        item->setData(items[i].value("date_offset_any"), MySortFilterProxyModel::MyRoles::h_offset_any);
        item->setData(items[i].value("date_offset_specific"), MySortFilterProxyModel::MyRoles::h_offset_specific);

        switch (pris[static_cast<int>(i)]) {
        case 0:
            break;
        case 1:
            item->setData("qrc:///icons/warning_inactive.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        case 2:
            item->setData("qrc:///icons/warning_busy.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        case 3:
            item->setData("qrc:///icons/warning_family_member.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        }

        itemmodel->setRowCount(itemmodel->rowCount() + 1);
        itemmodel->setItem(static_cast<int>(i) + 1, item);
    }
    SortFilterProxyModel *sortmodel = new MySortFilterProxyModel(this, itemmodel, MySortFilterProxyModel::MyRoles::name, "assistant");
    return sortmodel;
}

QString LMM_Assignment_ex::getReminderText() const
{
    QDate d = date();
    ccongregation c;
    d = d.addDays(c.getMeetingDay(d, ccongregation::tms) - 1);

    QString rowTemplate("%1 : %2\n");
    QString text(tr("Please find below details of your assignment:") + "\n\n");
    text.append(rowTemplate.arg(tr("Date"), d.toString(Qt::DefaultLocaleShortDate)));
    text.append(rowTemplate.arg(tr("Name"), (speaker() ? speakerFullName() : "")));

    if (talkId() == LMM_Schedule::TalkType_CBS) {
        text.append(rowTemplate.arg(tr("Reader"), (assistant() ? assistant()->fullname() : "")));
    } else {
        text.append(rowTemplate.arg(tr("Assistant"), (assistant() ? assistant()->fullname() : "")));
    }
    text.append(rowTemplate.arg(tr("Assignment"), LMM_Schedule::getFullStringTalkType(talkId())));

    if (!(talkId() == LMM_Schedule::TalkType_CBS)) {
        text.append(rowTemplate.arg(tr("Study"), QString::number(study_number())) + "\n");
    }
    text.append(rowTemplate.arg(tr("Theme"), theme()) + "\n");
    text.append(rowTemplate.arg(tr("Source material"), source()));
    if (talkId() != LMM_Schedule::TalkType_CBS) {
        QString classText = "";
        switch (classnumber()) {
        case 1:
            classText = tr("Main hall");
            break;
        case 2:
            classText = tr("Auxiliary classroom 1");
            break;
        case 3:
            classText = tr("Auxiliary classroom 2");
            break;
        }
        text.append(rowTemplate.arg(tr("To be given in", "Refer to main hall or aux. classroom. See S-89"), classText));
    }
    return text;
}

bool LMM_Assignment_ex::save()
{
    m_assistant_id = assistant() ? assistant()->id() : -1;
    m_volunteer_id = volunteer() ? volunteer()->id() : -1;
    m_completed = completed();
    m_timing = timing();
    m_setting = setting();
    m_completed = completed();
    m_classnumber = classnumber();
    return LMM_Assignment::save();
}

schoolStudentStudy *LMM_Assignment_ex::getCounselPoint(QString &notice)
{
    if (!speaker())
        return nullptr;
    school s;
    schoolStudentStudy *study(nullptr);
    if (date().year() > 2018) {
        study = s.getStudyUsed(this->speaker()->id(), date(), school_item::AssignmentType::Reading);
    } else {
        if (completed()) {
            study = s.getStudyUsed(this->speaker()->id(),
                                   date(),
                                   this->talkId() == LMM_Schedule::TalkType_BibleReading ? school_item::Reading : (canHaveAssistant() ? school_item::Demonstration : school_item::Discource));
        } else {
            study = s.getActiveStudyPoint(speaker()->id(),
                                          talkId() == LMM_Schedule::TalkType_BibleReading ? school_item::Reading : (canHaveAssistant() ? school_item::Demonstration : school_item::Discource), false,
                                          date());
            sql_class *sql = &Singleton<sql_class>::Instance();
            sql_item queryitems;
            queryitems.insert(":id", speaker()->id());
            queryitems.insert(":date", date());
            bool previousCompleted = sql->selectScalar("SELECT IFNULL((SELECT lmm_assignment.completed FROM lmm_assignment "
                                                       "LEFT JOIN lmm_schedule ON lmm_assignment.lmm_schedule_id = lmm_schedule.id "
                                                       "WHERE assignee_id = :id AND volunteer_id < 1 AND lmm_assignment.date >= date('now') and lmm_assignment.date < :date AND lmm_schedule.talk_id IN (4,6,7,8,9,10,11,27) "
                                                       "ORDER BY lmm_schedule.date LIMIT 1),1)",
                                                       &queryitems)
                                             .toBool();
            if (!previousCompleted)
                notice = tr("See your be book", "Counsel point is not known yet. See your 'Ministry School' book.");
        }
    }
    if (study)
        study->setParent(this);
    return study;
}
