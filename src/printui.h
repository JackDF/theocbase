/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRINTUI_H
#define PRINTUI_H

#include <QDialog>
#include <QtGui>
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include <QMenu>
#include <QTextEdit>
#include <QPrintPreviewWidget>
#include <QPrintDialog>
#include <QPrinterInfo>
#include <QThread>
#include <QLocale>
#include <QList>
#include <QRegularExpression>
#include "school.h"
#include "ccongregation.h"
#include "cpublictalks.h"
#include "ccloud.h"

#include "printingconditionals.h"
#include "slipscanner.h"
#include "printchannel.h"
#include "cloud/cloud_controller.h"

#include "print/printdocument.h"
#include "print/printmidweekschedule.h"
#include "print/printmidweekworksheet.h"
#include "print/printmidweekslip.h"
#include "print/printweekendschedule.h"
#include "print/printhospitality.h"
#include "print/printoutgoingschedule.h"
#include "print/printoutgoingassignment.h"
#include "print/printweekendworksheet.h"
#include "print/printtalksofspeakerslist.h"
#include "print/printcombination.h"
#include "print/printterritoryassignmentrecord.h"
#include "print/printterritorycard.h"
#include "print/printterritorymapcard.h"
#include "general.h"

#include <QWebEnginePage>
#include <QWebEngineView>
#include <QtWebChannel>
#include <QWebEngineSettings>
#include <QWebEngineProfile>
#include <QPdfWriter>

namespace Ui {
class printui;
}

class printui : public QDialog
{
    Q_OBJECT
public:
    printui(QDate date, QWidget *parent = nullptr);
    ~printui();

    void setCloud(cloud_controller *c);

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);

private:
    Ui::printui *ui;
    sql_class *sql;
    AccessControl *ac;
    QString territoryNumberList = "1";
    //QPrintPreviewWidget *pv;
    QSharedPointer<QPrinter> pr;
    QString templatePath1;
    QString templatePath2;
    QString activeHtml;
    QAction *acsv;
    QPoint origo;
    int pagenumber;
    QMarginsF pmlist;
    QStringList paperSizes;
    QString qrcodepath;
    QList<QString> slipSettings;
    QSize customsize;
    int textSizeFactorySlips;
    int mapSizeFactory = 1;
    ccongregation c;
    ccongregation::congregation myCongregation;
    bool isLoaded;
    QString slipWorkingPath;
    bool individualSlips;
    QByteArray bContent;
    QString sharedLink = "";
    QWebEnginePage *mPage;
    QWebEngineView *mWebView;
    QTemporaryFile tmpHtmlFile;
    QWebChannel *channel;
    PrintChannel printChannel;
    cloud_controller *cloud;
    QSharedPointer<PrintDocument> currentTemplate;

    void applyAuthorizationRules();

    void setHTML(QString html);
    QString addQrCode(QString context);
    QString addHtmlImage(QString path, QString tagname);
    QString uploadSharedFile();
    QString getSharedFilePath();
    void showCurrentTemplateOptions();
    void resetZooming();
    QString intListJoin(QList<int> list, QString delim);
    bool generateSlipPdfToJpg(QString pdfpath, QString jpgpath);
    void fillCustomPaperSizes();
    QStringList templateList;
    int NoUIEvents;
    void on_resizeTimer_triggered();
    void setBusy();
    void clearBusy();

private slots:
    //void allReady();
    void printRequest();
    void pickTemplateEditor();
    void radioButtonClicked();
    void on_toolButtonCopy_clicked();
    void toolButtonPdf_clicked();
    void toolButtonHtml_clicked();
    void on_toolButtonPrint_clicked();
    void toolButtonOdf_clicked();
    void on_checkBoxQRCode_clicked(bool checked);
    void on_editDateRangeFrom_dateChanged(const QDate &date);
    void on_editDateRangeThru_dateChanged(const QDate &date);
    void on_lineEditTerritoryNumber_returnPressed();
    void increaseTextSize();
    void decreaseTextSize();
    void resetTextSize();
    void on_cboTemplate_activated(const QString &arg);
    void on_cboPaperSize_activated(int index);
    void on_ckOptionShowSongTitles_clicked(bool checked);
    void on_ckOptionShowWorkbookNumber_clicked(bool checked);
    void on_ckOptionShowWatchtowerNumber_clicked(bool checked);
    void on_ckOptionIncludeCounselText_clicked(bool checked);
    void on_ckOptionSectionTitles_clicked(bool checked);
    void on_rbOptionShowTime_clicked(bool checked);
    void on_rbOptionShowDuration_clicked(bool checked);
    void on_ckOptionOwnCongregation_clicked(bool checked);
    void on_ckOptionShowTalkRevision_clicked(bool checked);
    void on_btnAdditionalOptions_clicked(bool checked);
    void on_txtMWTitle_editingFinished();
    void on_txtWETitle_editingFinished();
    void on_txtMW_OC_Title_editingFinished();
    void on_txtMW_CC_Title_editingFinished();
    void htmlDownloadRequest(QWebEngineDownloadItem *item);
};

#endif // PRINTUI_H
