#ifndef FAMILY_H
#define FAMILY_H

#include <QObject>
#include <QString>
#include <QList>
#include "cpersons.h"
#include "person.h"

class family : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int headId READ getHeadId WRITE setHeadId NOTIFY headIdChanged)

public:
    family(QObject *parent = 0);

    QString const& getName() const;
    void setName(QString const& name);
    void setName(QString const& firstName, QString const& lastName);

    int getHeadId();
    void setHeadId(int headId);

    /**
     * @brief addMember - Add a new member to family
     * @param person_id - Id number in person table
     * @return - true or false
     */
    Q_INVOKABLE bool addMember(int person_id);

    /**
     * @brief removeMember - Remove member from family
     * @param person_id - Id number in person table
     * @return - true or false
     */
    Q_INVOKABLE bool removeMember(int person_id);

    /**
     * @brief removeFamily - Remove family (this clears family head id from members)
     * @return - true or false
     */
    Q_INVOKABLE bool removeFamily();

    /**
     * @brief getMembers - Get memebers of family
     * @return - List of person objects
     */
    QList<person *> getMembers();

    /**
     * @brief getFamilies - Get all families
     * @return - list of family objects
     */
    Q_INVOKABLE static QList<family *> getFamilies();
    Q_INVOKABLE QVariant getFamiliesVariantList();

    /**
     * @brief getPersonFamily - Get publisher's family
     * @param person_id - Publisher's id in database
     * @return - family object or 0 if not exist
     */
    Q_INVOKABLE static family *getPersonFamily(int person_id);

    /**
     * @brief getOrAddFamily - Get existing family or add a new
     * @param headId - Id number of family head
     * @return - family object
     */
    Q_INVOKABLE static family *getOrAddFamily(int headId);

signals:
    void nameChanged(QString name);
    void headIdChanged(int id);

private:
    int mId;
    int mHeadId;
    QString mName;

    sql_class *sql;
};

#endif // FAMILY_H
