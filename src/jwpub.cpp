#include "jwpub.h"

jwpub::jwpub()
{
    tbSql = &Singleton<sql_class>::Instance();
    zipr = &Singleton<zipper>::Instance();
}

jwpub::~jwpub()
{
    if (jwpubDb) {
        jwpubDb->close();
        jwpubDb = nullptr;
    }
}

bool jwpub::Prepare(QString filename)
{
    // check if filename is in url format
    const QUrl fileUrl(filename);
    if (fileUrl.isLocalFile())
        filename = fileUrl.toLocalFile();
    jwPubfilename = filename;

    extractedPath = zipr->UnZip(filename);
    lastErr = zipr->errMessage;
    if (!lastErr.isNull() && !lastErr.isEmpty()) {
        return true;
    }

    sqlDb = zipr->UnZip(extractedPath + "/contents");
    lastErr = zipr->errMessage;
    if (!lastErr.isNull() && !lastErr.isEmpty()) {
        return true;
    }

    jwpubDb = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE","jwpub"));
    jwpubDb->setHostName("localhost");
    jwpubDb->setConnectOptions();
    jwpubDb->setDatabaseName(sqlDb);

    return extractedPath.isEmpty();
}
