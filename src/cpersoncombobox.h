/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPERSONCOMBOBOX_H
#define CPERSONCOMBOBOX_H

#include <QComboBox>
#include <QStyledItemDelegate>
#include "ccongregation.h"
#include "person.h"
#include <QHeaderView>
#include <QTableWidget>

#ifndef AVAILABILITYCHECKER_H
#include "availability/availabilitychecker.h"
#endif

class cPersonComboBox : public QComboBox
{
    Q_OBJECT
public:
    cPersonComboBox(QWidget *parent = 0);
    virtual void hidePopup();
    virtual void showPopup();
    virtual void setCurrentText(QString value);
    int personType();
    void setPersonType(int typeNumber, ccongregation::meetings meetingtype, const QDate &weekCommencingDate = QDate());
    void setDate(const QDate &weekCommencingDate);

protected:
    void focusOutEvent(QFocusEvent *e);

private:
    int personType_;
    ccongregation cong_;
    QTableWidget *tableWidget_;
    QDate weekCommencingDate_;
    ccongregation::meetings meetingtype_;

    void AddPersons(int typeValue, QString currentText);
    void InitTableWidget();
    void PopulateModel(tbAvailability::AvailabilityChecker &checker, QString currentText, person::UseFor roleNeeded);
    void PopulateModelRow(tbAvailability::AvailabilityItem *item, int row, QString currentText, person::UseFor roleNeeded);
    void ShowPossibleConflicts(tbAvailability::AvailabilityItem *item, int row, person::UseFor roleNeeded);
    QTableWidgetItem *CreateStatusImageWidget(QString imageResPath, QString tooltipText);
    QDate CalculateMeetingDate();
    QDate GetMeetingDate(const QDate &weekCommencingDate, ccongregation::meetings meetingType);

signals:
    void beforeShowPopup(QString objectname, cPersonComboBox *object);
    void popupClosed(cPersonComboBox *object);
    void focusOut(cPersonComboBox *object);

public slots:
};

class DateDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    DateDelegate(QObject *parent = 0)
        : QStyledItemDelegate(parent) {}

    QString displayText(const QVariant &value, const QLocale &locale) const
    {
        if (value.type() == QVariant::Date) {
            return value.toDate().toString(Qt::DefaultLocaleShortDate);
        }

        return QStyledItemDelegate::displayText(value, locale);
    }
};

#endif // CPERSONCOMBOBOX_H
