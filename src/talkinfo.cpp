#include "talkinfo.h"

talkinfo::talkinfo(int meeting, MeetingSection meetingSection, bool canCounsel)
{
    _meeting = meeting;
    _meetingSection = meetingSection;
    _canCounsel = canCounsel;
}

int talkinfo::meeting() const
{
    return _meeting;
}; // 0=midweek, 1=weekend
MeetingSection talkinfo::meetingSection() const
{
    return _meetingSection;
};
bool talkinfo::canCounsel() const
{
    return _canCounsel;
};
