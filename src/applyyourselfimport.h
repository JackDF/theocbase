#ifndef APPLYYOURSELFIMPORT_H
#define APPLYYOURSELFIMPORT_H

#include "epub.h"
#include "xml_reader.h"

class applyyourselfimport : public QObject
{
    Q_OBJECT

public:
    explicit applyyourselfimport(QString fileName);
    bool Import();

    enum xmlPartsContexts {
        link,
        strong
    };

    epub epb;
    bool prepared;
    QString lastError;

signals:

public slots:
    void ProcessTOCEntry(QString href, QString chapter);
    void xmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth);

private:

    sql_class *sql;
    int studiesFound;
    int queueStudyNumber;
    QString queueStudy;
};

#endif // APPLYYOURSELFIMPORT_H
