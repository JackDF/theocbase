<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt-BR">
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="46"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="67"/>
        <source>Source</source>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="89"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="89"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="100"/>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Selecionado</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="101"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="120"/>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="39"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="51"/>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="66"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="72"/>
        <source>CBS conductor</source>
        <comment>Dropdown column title</comment>
        <translation>Dirigente EBC</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="73"/>
        <source>Any CL assignment</source>
        <comment>Dropdown column title</comment>
        <translation>Qualquer designação NVC</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="86"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="105"/>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
</context>
<context>
    <name>ComboBoxTable</name>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="192"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="198"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>Mostrar endereço da congregação</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="76"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>Tem certeza que deseja apagar permanentemente os dados da nuvem?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="214"/>
        <source>Last synchronized: %1</source>
        <translation>Última sincronização: %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="218"/>
        <source>Synchronize</source>
        <translation>Sincronizar</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="230"/>
        <source>Delete Cloud Data</source>
        <translation>Apagar dados da nuvem</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>Ajuda do Theocbase</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>Não foi possivel abrir o help viewer (%1)</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="42"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Please find below details of your assignment:</source>
        <translation>Abaixo encontrará detalhes sobre sua designação:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Assignment</source>
        <translation>Designação</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="184"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="185"/>
        <source>Source material</source>
        <translation>Fonte de matéria</translation>
    </message>
</context>
<context>
    <name>LMM_AssignmentContoller</name>
    <message>
        <location filename="../lmm_assignmentcontoller.cpp" line="54"/>
        <source>Do not assign the next study</source>
        <translation>Não designar nova característica</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="179"/>
        <source>Please find below details of your assignment:</source>
        <translation>Abaixo encontrará detalhes sobre sua designação:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="180"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="181"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="188"/>
        <source>Assignment</source>
        <translation>Designação</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Study</source>
        <translation>Lição</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="194"/>
        <source>Source material</source>
        <translation>Fonte de matéria</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="199"/>
        <source>Main hall</source>
        <translation>Salão Principal</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="202"/>
        <source>Auxiliary classroom 1</source>
        <translation>Sala Auxiliar 1</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="205"/>
        <source>Auxiliary classroom 2</source>
        <translation>Sala Auxiliar 2</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="208"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>Local da parte:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="253"/>
        <source>See your be book</source>
        <comment>Counsel point is not known yet. See your &apos;Ministry School&apos; book.</comment>
        <translation>Veja seu livro Beneficie-se</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="607"/>
        <source>Enter source material here</source>
        <translation>Adicione Fonte de matéria aqui</translation>
    </message>
</context>
<context>
    <name>LMM_Schedule</name>
    <message>
        <location filename="../lmm_schedule.cpp" line="97"/>
        <location filename="../lmm_schedule.cpp" line="122"/>
        <source>Chairman</source>
        <comment>talk title</comment>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="98"/>
        <location filename="../lmm_schedule.cpp" line="123"/>
        <source>Treasures From God’s Word</source>
        <comment>talk title</comment>
        <translation>Tesouros da Palavra de Deus</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="99"/>
        <location filename="../lmm_schedule.cpp" line="124"/>
        <source>Digging for Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>Encontre Jóias Espirituais</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="100"/>
        <location filename="../lmm_schedule.cpp" line="125"/>
        <source>Bible Reading</source>
        <comment>talk title</comment>
        <translation>Leitura da Bíblia</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="103"/>
        <location filename="../lmm_schedule.cpp" line="128"/>
        <source>Initial Call</source>
        <comment>talk title</comment>
        <translation>Primeira conversa</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="101"/>
        <location filename="../lmm_schedule.cpp" line="126"/>
        <source>Sample Conversation Video</source>
        <comment>talk title</comment>
        <translation>Vídeo das Conversas sobre a Bíblia</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="104"/>
        <location filename="../lmm_schedule.cpp" line="129"/>
        <source>First Return Visit</source>
        <comment>talk title</comment>
        <translation>Primeira Revisita</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="105"/>
        <location filename="../lmm_schedule.cpp" line="130"/>
        <source>Second Return Visit</source>
        <comment>talk title</comment>
        <translation>Segunda Revisita</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="106"/>
        <location filename="../lmm_schedule.cpp" line="131"/>
        <source>Third Return Visit</source>
        <comment>talk title</comment>
        <translation>Terceira Revisita</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="107"/>
        <location filename="../lmm_schedule.cpp" line="132"/>
        <source>Bible Study</source>
        <comment>talk title</comment>
        <translation>Estudo Bíblico</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="108"/>
        <location filename="../lmm_schedule.cpp" line="133"/>
        <source>Talk</source>
        <comment>talk title</comment>
        <translation>Discurso</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="109"/>
        <location filename="../lmm_schedule.cpp" line="134"/>
        <source>Living as Christians Talk 1</source>
        <comment>talk title</comment>
        <translation>Nossa vida cristã 1 parte</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="110"/>
        <location filename="../lmm_schedule.cpp" line="135"/>
        <source>Living as Christians Talk 2</source>
        <comment>talk title</comment>
        <translation>Nossa vida cristã 2 parte</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="111"/>
        <location filename="../lmm_schedule.cpp" line="136"/>
        <source>Living as Christians Talk 3</source>
        <comment>talk title</comment>
        <translation>Nossa vida cristã 3 parte</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="112"/>
        <location filename="../lmm_schedule.cpp" line="137"/>
        <source>Congregation Bible Study</source>
        <comment>talk title</comment>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="113"/>
        <location filename="../lmm_schedule.cpp" line="138"/>
        <source>Circuit Overseer&#x27;s Talk</source>
        <comment>talk title</comment>
        <translation>Discurso Superintendente de circuito</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="127"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <comment>talk title</comment>
        <translation>Melhore Sua Leitura e Seu Ensino</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>Nome de usuário ou Email</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>Esqueceu a senha</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>Criar Conta</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>Services</source>
        <translation>Serviços</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Hide %1</source>
        <translation>Ocultar %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide Others</source>
        <translation>Ocultar Outros</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Show All</source>
        <translation>Visualisar Todos</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Preferences...</source>
        <translation>Preferências…</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Quit %1</source>
        <translation>Sair %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>About %1</source>
        <translation>Sobre o %1</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="76"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="95"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>Conselheiro Auxiliar de Sala II</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="115"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>Conselheiro Auxiliar de Sala III</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="219"/>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TESOUROS DA PALAVRA DE DEUS</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="227"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>FAÇA SEU MELHOR NO MINISTÉRIO</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="232"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>NOSSA VIDA CRISTÃ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="245"/>
        <source>Midweek Meeting</source>
        <translation>Reunião de Semana</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="257"/>
        <source>Import Schedule...</source>
        <translation>Importar Programa...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="305"/>
        <location filename="../qml/MWMeetingModule.qml" line="500"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="306"/>
        <location filename="../qml/MWMeetingModule.qml" line="501"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>S1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="307"/>
        <location filename="../qml/MWMeetingModule.qml" line="502"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>S2</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="316"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="316"/>
        <source>Counselor</source>
        <translation>Conselheiro</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="342"/>
        <location filename="../qml/MWMeetingModule.qml" line="389"/>
        <source>Song %1 and Prayer</source>
        <translation>Cântico %1 e Oração</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="356"/>
        <source>Opening Comments</source>
        <translation>Comentários iniciais</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="373"/>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="465"/>
        <source>Song %1</source>
        <translation>Cântico %1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="486"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="488"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="44"/>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="422"/>
        <source>No meeting</source>
        <translation>Nenhuma reunião programada</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Conductor:</source>
        <translation>Dirigente:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Reader:</source>
        <translation>Leitor:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>Copyright</source>
        <translation>Direitos autorais</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="475"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Qt libraries licenciadas sob GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>TheocBase Team</source>
        <translation>Equipe TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="280"/>
        <source>Last synchronized</source>
        <translation>Última sincronização</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Licensed under GPLv3.</source>
        <translation>Licenciado sob a norma GPLv3</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Versions of Qt libraries </source>
        <translation>Versão da Qt libraries </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1249"/>
        <source>TheocBase data exchange</source>
        <translation>troca de dados TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="539"/>
        <source>New update available. Do you want to install?</source>
        <translation>Nova atualização disponível. Deseja instalar?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="543"/>
        <source>No new update available</source>
        <translation>Nenhuma atualização disponível</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="569"/>
        <source>Save file</source>
        <translation>Salvar arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1625"/>
        <source>Select ePub file</source>
        <translation>Selecionar epub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1538"/>
        <source>Send e-mail reminders?</source>
        <translation>Enviar notificações por email?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1572"/>
        <source>Updates available...</source>
        <translation>Há novas atualizações.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1152"/>
        <source>Error sending e-mail</source>
        <translation>E-mail não enviado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="393"/>
        <source>WEEK STARTING %1</source>
        <translation>Semana de %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="558"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>Exportando orador que sai não está pronto ainda , desculpe</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="562"/>
        <source>Exporting study history to iCal is not supported</source>
        <translation>exportando histórico para iCal não é suportado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="573"/>
        <source>Save folder</source>
        <translation>Salvar arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1154"/>
        <source>E-mail sent successfully</source>
        <translation>E-mail enviado com sucesso</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="586"/>
        <source>Saved successfully</source>
        <translation>Salvo com Sucesso</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="646"/>
        <location filename="../mainwindow.cpp" line="651"/>
        <source>Counselor-Class II</source>
        <translation>Conselheiro-Sala II</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="647"/>
        <location filename="../mainwindow.cpp" line="652"/>
        <source>Counselor-Class III</source>
        <translation>Conselheiro-Sala III</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="667"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Ajudante para %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="734"/>
        <location filename="../mainwindow.cpp" line="949"/>
        <location filename="../mainwindow.cpp" line="974"/>
        <source>Kingdom Hall</source>
        <translation>Salão do Reino</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="645"/>
        <location filename="../mainwindow.cpp" line="650"/>
        <location filename="../mainwindow.cpp" line="740"/>
        <location filename="../mainwindow.cpp" line="751"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="674"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Leitor para o Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="659"/>
        <location filename="../mainwindow.cpp" line="675"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="648"/>
        <location filename="../mainwindow.cpp" line="653"/>
        <location filename="../mainwindow.cpp" line="698"/>
        <location filename="../mainwindow.cpp" line="700"/>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="659"/>
        <source>Timing</source>
        <translation>Côntrole de tempo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="740"/>
        <location filename="../mainwindow.cpp" line="905"/>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="740"/>
        <source>Watchtower Study</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="757"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="762"/>
        <source>Watchtower Study Conductor</source>
        <translation>Dirigente Estudo Sentinela</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="764"/>
        <source>Watchtower reader</source>
        <translation>Leitor da Sentinela</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1010"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation>As mesmas mudanças podem ser encontradas localmente e na Nuvem (%1 rows). Voce quer manter as mudanças locais?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1051"/>
        <source>The cloud data has now been deleted.</source>
        <translation>Os dados da nuvem foram apagados.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1055"/>
        <source>Synchronize</source>
        <translation>Sincronizar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1057"/>
        <source>Sign Out</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1067"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>Os dados da nuvem foram excluídos. Os seus dados locais serão substituídos. Continuar?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1163"/>
        <source>Open file</source>
        <translation>Abrir Arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1190"/>
        <source>Open directory</source>
        <translation>Abrir diretorio</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1221"/>
        <source>Import Error</source>
        <translation>Importar Erro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1221"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>Não foi possível importar a partir Ta1ks . Arquivos estão ausentes:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1236"/>
        <source>Save unsaved data?</source>
        <translation>Salvar os Dados?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1245"/>
        <source>Import file?</source>
        <translation>Importar dados?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>School assignment</source>
        <translation>Designação da escola</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Congregation Bible Study:</source>
        <translation>Estudo Bíblico de Congregação:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>Theocratic Ministry School:</source>
        <translation>Escola Ministério Teocrático :</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Service Meeting:</source>
        <translation>Reunião de Serviço:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="806"/>
        <source>Congregation Bible Study</source>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="833"/>
        <source>Theocratic Ministry School</source>
        <translation>Escola do Ministério Teocrático</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="860"/>
        <source>Service Meeting</source>
        <translation>Reunião de Serviço</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1368"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1280"/>
        <source>Public talks</source>
        <translation>Discursos Públicos</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1398"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1432"/>
        <source>info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2007"/>
        <source>Data exhange</source>
        <translation>troca de dados</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2060"/>
        <source>TheocBase Cloud</source>
        <translation>Nuvem da TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1591"/>
        <location filename="../mainwindow.ui" line="1624"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1823"/>
        <source>Timeline</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1884"/>
        <source>Print...</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1887"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1896"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Configuração...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1908"/>
        <source>Publishers...</source>
        <translation>Publicadores...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1223"/>
        <location filename="../mainwindow.ui" line="1911"/>
        <source>Publishers</source>
        <translation>Publicadores</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1977"/>
        <source>Speakers...</source>
        <translation>Oradores...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1260"/>
        <location filename="../mainwindow.ui" line="1980"/>
        <source>Speakers</source>
        <translation>Oradores</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2004"/>
        <source>Data exhange...</source>
        <translation>Troca de dados</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2016"/>
        <source>TheocBase help...</source>
        <translation>Ajuda TheocBase...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2031"/>
        <source>History</source>
        <translation>Histórico</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2043"/>
        <location filename="../mainwindow.ui" line="2046"/>
        <source>Full Screen</source>
        <translation>Tela cheia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2051"/>
        <source>Startup Screen</source>
        <translation>Página inicial</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2090"/>
        <source>Reminders...</source>
        <translation>Notificações</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Theme:</source>
        <translation>Tema:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="573"/>
        <source>Speaker:</source>
        <translation>Orador:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Chairman:</source>
        <translation>Presidente:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <source>Watchtower Study:</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="512"/>
        <source>Data exchange</source>
        <translation>Troca de dados</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1114"/>
        <source>Export Format</source>
        <translation>Formato de exportação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1120"/>
        <source>For sending data to another user</source>
        <translation>Para mandar data para outro usuário</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1139"/>
        <source>For easy import to Calendar programs</source>
        <translation>Facilmente importar para programas de calendário</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1171"/>
        <source>Export Method</source>
        <translation>Exportar Método</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1184"/>
        <source>Events grouped by date</source>
        <translation>Eventos agrupados por data</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1177"/>
        <source>All day events</source>
        <translation>Todos os eventos do dia</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1213"/>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1236"/>
        <source>Study History</source>
        <translation>Histórico Características Oratórias</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1287"/>
        <source>Outgoing Talks</source>
        <translation>Orador fora</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1298"/>
        <source>Date Range</source>
        <translation>Período</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1305"/>
        <source>Previous Weeks</source>
        <translation>semanas anteriores</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1319"/>
        <source>From Date</source>
        <translation>Desde</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1336"/>
        <source>Thru Date</source>
        <translation>Até</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1777"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1783"/>
        <source>Tools</source>
        <translation>Ferramentas</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1796"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1872"/>
        <source>Today</source>
        <translation>Hoje</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1920"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1932"/>
        <source>Report bug...</source>
        <translation>Relatar erro...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1944"/>
        <source>Send feedback...</source>
        <translation>Enviar opinião</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1962"/>
        <source>About TheocBase...</source>
        <translation>Sobre o TheocBase...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1989"/>
        <source>Check updates...</source>
        <translation>Verificar Atualizações...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2019"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2078"/>
        <source>Territories...</source>
        <translation>Territórios...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2081"/>
        <source>Territories</source>
        <translation>Territórios</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1851"/>
        <source>Back</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1863"/>
        <source>Next</source>
        <translation>Próximo</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2069"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1953"/>
        <source>TheocBase website</source>
        <translation>Website do TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="418"/>
        <source>Convention week (no meeting) </source>
        <translation>Semana de Assembléia (sem reuniões) </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="534"/>
        <source>Public Talk:</source>
        <translation>Discurso Público:</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="74"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="98"/>
        <source>Theme</source>
        <translation>Equipe</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="125"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="151"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="163"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="176"/>
        <source>Meeting day and time</source>
        <translation>Dia e hora da reunião
</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="163"/>
        <source>From %1</source>
        <translation>De %1</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="109"/>
        <source>Outgoing speakers</source>
        <translation>Saída de Oradores</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="194"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 Orador saindo neste fim de semana</numerusform>
            <numerusform>%1 Orador saindo neste fim de semana</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="195"/>
        <source>No speakers away this weekend</source>
        <translation>Não há orador saindo neste fim de semana</translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="31"/>
        <source>Select at least one option</source>
        <translation>selecionar pelo menos uma opção</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="39"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Congregação %1</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="40"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="42"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="43"/>
        <source>Counselor</source>
        <translation>Conselheiro</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="44"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="45"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="46"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="47"/>
        <source>Public Meeting</source>
        <translation>Reunião Pública</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="48"/>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="49"/>
        <source>Watchtower Study</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="50"/>
        <source>Midweek Meeting</source>
        <translation>Reunião de Semana</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="51"/>
        <location filename="../print/printcombination.cpp" line="52"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Reunião Vida e Ministério Cristão</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="67"/>
        <location filename="../print/printcombination.cpp" line="68"/>
        <source>Combined Schedule</source>
        <translation>programa conjuntado</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="69"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="70"/>
        <source>Outgoing Speakers</source>
        <translation>Saída de Oradores</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="81"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>Semana Começando %1</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&lt;--</source>
        <translation>&lt;--</translation>
    </message>
</context>
<context>
    <name>PrintDocument</name>
    <message>
        <location filename="../print/printdocument.cpp" line="144"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>Formulário não encontrado</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="149"/>
        <source>Can&#x27;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>incapaz de ler arquivo</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="234"/>
        <source>Service Talk</source>
        <translation>Discurso de Serviço</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="243"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>Começa às</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSchedule</name>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="37"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Congregação %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="38"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="40"/>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="41"/>
        <source>Watchtower Study</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="42"/>
        <location filename="../print/printmidweekschedule.cpp" line="80"/>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Tesouros da Palavra de Deus</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="43"/>
        <location filename="../print/printmidweekschedule.cpp" line="81"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>FAÇA SEU MELHOR NO MINISTÉRIO</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="44"/>
        <location filename="../print/printmidweekschedule.cpp" line="82"/>
        <source>Living as Christians</source>
        <translation>Nossa vida cristã</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="69"/>
        <source>Midweek Meeting</source>
        <translation>Reunião de Semana</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="70"/>
        <source>Worksheet</source>
        <translation>Apostila</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="72"/>
        <source>Concluding Comments</source>
        <comment>See Workbook</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="73"/>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="74"/>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="75"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="76"/>
        <source>Counselor</source>
        <translation>Conselheiro</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="77"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="78"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="79"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Reunião Vida e Ministério Cristão</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="83"/>
        <source>Congregation Bible Study</source>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="84"/>
        <source>Circuit Overseer</source>
        <translation>Superintendente Circuito</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="85"/>
        <source>No regular meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="86"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="87"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>Lição</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="88"/>
        <source>Theme</source>
        <comment>Talk Theme description from workbook</comment>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="89"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="90"/>
        <source>Exercises</source>
        <translation>Exercícios</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="91"/>
        <source>Timing</source>
        <comment>Assignment completed in time?</comment>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="92"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="93"/>
        <source>Next study</source>
        <translation>Próxima característica</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="94"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>Cena</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="95"/>
        <location filename="../print/printmidweekschedule.cpp" line="96"/>
        <location filename="../print/printmidweekschedule.cpp" line="97"/>
        <location filename="../print/printmidweekschedule.cpp" line="389"/>
        <source>Class</source>
        <translation>Sala</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="98"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>Conselheiro da Sala B</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="99"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>Sala B</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="100"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>Salão Principal</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="101"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="102"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="103"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>Estudante</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="105"/>
        <source>Opening Comments</source>
        <comment>Customizable title for OC notes</comment>
        <translation>Comentários iniciais</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="106"/>
        <source>Review/Preview/Announcements</source>
        <comment>Customizable title for RPA notes</comment>
        <translation>Recapitulação/Visão geral da próxima semana/Anúncios</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="107"/>
        <source>Today</source>
        <translation>Hoje</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="108"/>
        <source>Next week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="386"/>
        <source>Other schools</source>
        <translation>Outras salas</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSlip</name>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="46"/>
        <source>Slip Template</source>
        <translation>Modelo de folha</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="368"/>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="381"/>
        <location filename="../print/printmidweekslip.cpp" line="383"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="422"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>segundo discurso</translation>
    </message>
</context>
<context>
    <name>PrintMidweekWorksheet</name>
    <message>
        <location filename="../print/printmidweekworksheet.cpp" line="61"/>
        <source>Class </source>
        <translation>Sala_ </translation>
    </message>
</context>
<context>
    <name>PrintOutgoingSchedule</name>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="59"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Congregação %1</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="60"/>
        <location filename="../print/printoutgoingschedule.cpp" line="174"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="62"/>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="63"/>
        <source>Watchtower Study</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="167"/>
        <source>Talk</source>
        <translation>Discurso</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="168"/>
        <source>Outgoing Speakers</source>
        <translation>Saída de Oradores</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="169"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="170"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="171"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="172"/>
        <source>Theme Number</source>
        <translation>Tema número</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="173"/>
        <source>Circuit</source>
        <translation>Circuito</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="175"/>
        <source>Start Time</source>
        <translation>Hora Início</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="177"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>NOTA: Prezado irmão , apesar do banco de dados ter uma manutenção cuidadosa, Às vezes tempo ou endereços podem estar desatualizados. Então, por favor Verifique estes através do JW.ORG . Obrigado !</translation>
    </message>
</context>
<context>
    <name>PrintTalksOfSpeakersList</name>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="37"/>
        <location filename="../print/printtalksofspeakerslist.cpp" line="38"/>
        <source>Public Talks</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="39"/>
        <source>Contact info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="40"/>
        <source>Talk numbers</source>
        <comment>Public talk numbers</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTerritoryAssignmentRecord</name>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="35"/>
        <source>Territory Assignment Record</source>
        <translation>Registro de Designação de Território</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="36"/>
        <source>Territory Coverage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="37"/>
        <source>Total number of territories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="38"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 meses</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="39"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 a 12 meses</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="40"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 meses atrás</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="41"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="175"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="176"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation>Endereços</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="177"/>
        <source>Address type</source>
        <translation>tipo de Endereço</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="178"/>
        <source>Assigned to</source>
        <translation>Entregue a</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="179"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="181"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Congregação %1</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="182"/>
        <source>Date checked out</source>
        <translation>Data de Saída</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="183"/>
        <source>Date checked back in</source>
        <translation>Data da devolução</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="184"/>
        <source>Date last worked</source>
        <translation>Data da última vez que foi trabalhado</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="185"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="186"/>
        <source>City</source>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="187"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="188"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="189"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Distrito</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="190"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="200"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nr.</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="201"/>
        <source>Locality</source>
        <translation>Localidade</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="202"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="203"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="204"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>CEP</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="205"/>
        <source>Name of publisher</source>
        <translation>Nome do publicador</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="206"/>
        <source>Remark</source>
        <translation>Observações</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="207"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="208"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Rua</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="209"/>
        <source>Territory</source>
        <translation>Território</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="210"/>
        <source>Terr. No.</source>
        <translation>Terr. No.</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="211"/>
        <source>Territory type</source>
        <translation>Tipo de território</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="212"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation>Até</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="213"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="214"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation>Soma</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryCard</name>
    <message>
        <location filename="../print/printterritorycard.cpp" line="35"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>Cartão do Mapa do Território</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="36"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation>Mapa do território</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="37"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="38"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="39"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="40"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="41"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTerritoryMapCard</name>
    <message>
        <location filename="../print/printterritorymapcard.cpp" line="49"/>
        <source>Template</source>
        <translation>Modelo</translation>
    </message>
</context>
<context>
    <name>PrintWeekendSchedule</name>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="36"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="37"/>
        <source>Public Meeting</source>
        <translation>Reunião Pública</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="40"/>
        <location filename="../print/printweekendschedule.cpp" line="157"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Congregação %1</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="41"/>
        <location filename="../print/printweekendschedule.cpp" line="158"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="43"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="44"/>
        <location filename="../print/printweekendschedule.cpp" line="61"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="46"/>
        <location filename="../print/printweekendschedule.cpp" line="63"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="47"/>
        <location filename="../print/printweekendschedule.cpp" line="100"/>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="48"/>
        <location filename="../print/printweekendschedule.cpp" line="102"/>
        <source>Watchtower Study</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="49"/>
        <source>Congregation Bible Study</source>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="50"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Reunião Vida e Ministério Cristão</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="51"/>
        <location filename="../print/printweekendschedule.cpp" line="66"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="52"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="53"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="60"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="64"/>
        <source>WT Reader</source>
        <translation>Leitor Sentinela</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="67"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="103"/>
        <source>Circuit Overseer</source>
        <translation>Superintendente Circuito</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="114"/>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="117"/>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="193"/>
        <source>Watchtower Conductor</source>
        <translation>Dirigente estudo Sentinela</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="196"/>
        <source>No regular meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintWeekendWorksheet</name>
    <message>
        <location filename="../print/printweekendworksheet.cpp" line="47"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="48"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="72"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="94"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="128"/>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="140"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="152"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="164"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="177"/>
        <source>Host</source>
        <translation>Hospitalidade</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sim</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>No</source>
        <translation>Nº</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;No</source>
        <translation>&amp;Não</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="159"/>
        <source>Wrong username and/or password</source>
        <translation>Usuário e/ou senha errada</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="267"/>
        <source>Database not found!</source>
        <translation>Dados não encontrados!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>Choose database</source>
        <translation>Escolha base de dados</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>Arquivo SQlite (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="294"/>
        <source>Database restoring failed</source>
        <translation>Falhou importação de dados</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="325"/>
        <location filename="../mainwindow.cpp" line="226"/>
        <source>Save changes?</source>
        <translation>Salvar alterações?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="263"/>
        <source>Database copied to </source>
        <translation>Dados copiados para </translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="37"/>
        <source>Database file not found! Searching path =</source>
        <translation>Dados não encontrados. Procurando =</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="56"/>
        <source>Database Error</source>
        <translation>Erro na base de dados</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="706"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>A versão deste aplicativo (%1) é mais antiga que a do banco de dados (%2). Há uma forte probabilidade de aparecer mensagens de erro e que as alterações não sejam salvas corretamente. Por favor baixe e instale a ultima versão para melhores resultados.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="351"/>
        <source>Circuit</source>
        <translation>Circuito</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="748"/>
        <source>Database updated</source>
        <translation>Dados atualizados</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="321"/>
        <location filename="../ccongregation.cpp" line="347"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>Visita do Sup. de Circuito</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="323"/>
        <location filename="../ccongregation.cpp" line="328"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (Não há Reunião)</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="323"/>
        <location filename="../ccongregation.cpp" line="349"/>
        <source>Convention week</source>
        <translation>Semana do Congresso</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="325"/>
        <location filename="../ccongregation.cpp" line="351"/>
        <source>Memorial</source>
        <translation>Celebração</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="356"/>
        <source>Review</source>
        <comment>Theocratic ministry school review</comment>
        <translation>Recapitulaçao oral</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="56"/>
        <location filename="../cpublictalks.cpp" line="347"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="57"/>
        <location filename="../cpublictalks.cpp" line="266"/>
        <location filename="../cpublictalks.cpp" line="348"/>
        <source>Last</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="257"/>
        <location filename="../school.cpp" line="424"/>
        <source>All</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="258"/>
        <location filename="../school.cpp" line="425"/>
        <source>Highlights</source>
        <translation>Destaques</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="262"/>
        <location filename="../school.cpp" line="429"/>
        <source>WT Reader</source>
        <translation>Leitor Sentinela</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="264"/>
        <location filename="../school.cpp" line="431"/>
        <source>Selected</source>
        <translation>Selecionado</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="323"/>
        <location filename="../school.cpp" line="486"/>
        <source>Assignment already has been made</source>
        <translation>Designação já preenchida</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="327"/>
        <location filename="../school.cpp" line="490"/>
        <source>Assigned student has other meeting parts</source>
        <translation>O estudante tem outras designaçoes nessa reuniao</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="331"/>
        <location filename="../school.cpp" line="494"/>
        <source>Student unavailable</source>
        <translation>Estudante não disponível</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="335"/>
        <source>Student part of family</source>
        <translation>Estudante parte de uma familia</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="434"/>
        <source>Recently together</source>
        <translation>Dupla recente</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="498"/>
        <source>Family member used in another TMS assignment</source>
        <translation>Familiar de estudante usado noutra designaçao da Escola</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="67"/>
        <source>I/O</source>
        <comment>Incoming/Outgoing</comment>
        <translation>Receber/Enviar</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="72"/>
        <source>Notes</source>
        <translation>Anotações</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="565"/>
        <source>Congregation Name</source>
        <translation>Nome da Congregação</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="273"/>
        <location filename="../historytable.cpp" line="322"/>
        <location filename="../school.cpp" line="854"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>CBS</source>
        <translation>EBC</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="468"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="469"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="208"/>
        <source>Assignment</source>
        <translation>Designação</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="470"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="209"/>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="471"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="210"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <location filename="../school.cpp" line="263"/>
        <location filename="../school.cpp" line="423"/>
        <location filename="../school.cpp" line="430"/>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <source>Student</source>
        <translation>Estudante</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="265"/>
        <source>Bible highlights:</source>
        <translation>Destaques:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="268"/>
        <source>No. 1:</source>
        <translation>No. 1:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="271"/>
        <source>No. 2:</source>
        <translation>No. 2:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="274"/>
        <source>No. 3:</source>
        <translation>No. 3:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="277"/>
        <source>Reader:</source>
        <translation>Leitor:</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="816"/>
        <location filename="../generatexml.cpp" line="107"/>
        <source>Default language not selected!</source>
        <translation>Selecione idioma</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="937"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>O cabeçalho do arquivo CSV não é válido.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="293"/>
        <source>Confirm password!</source>
        <translation>Confirmar senha</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2224"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>Um usuário com o mesmo endereço de e-mail já foi adicionado.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2440"/>
        <source>All talks have been added to this week</source>
        <translation>Todos os discursos foram adicionados para esta semana</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="265"/>
        <location filename="../mainwindow.cpp" line="906"/>
        <location filename="../publictalkedit.cpp" line="71"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importkhs.cpp" line="42"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importkhs.cpp" line="42"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import Complete</source>
        <translation>Importação completa</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="192"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="199"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="209"/>
        <source>Theme (could be a theme this speaker does not give)</source>
        <comment>The todo list Theme cell is in error</comment>
        <translation>Tema (este orador pode não ter proferido este esboço)</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="221"/>
        <location filename="../todo.cpp" line="232"/>
        <source>Date Already Scheduled</source>
        <translation>Esta data ja esta programada</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="90"/>
        <source>Unable to read new Workbook format</source>
        <translation>Incapaz de ler novo formato do manual de atividades</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="92"/>
        <source>Database not set to handle language &#x27;%1&#x27;</source>
        <translation>Banco de dados não definida para lidar com idiomas &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="94"/>
        <source>Unable to find year/month</source>
        <translation>Não foi possível encontrar ano / mês</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="95"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>Nada importado (datas não reconhecidas)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="99"/>
        <location filename="../wtimport.cpp" line="31"/>
        <location filename="../wtimport.cpp" line="32"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>Imported %1 semanas de %2 thru %3</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="416"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>Selecione os Nomes dos discursos para combinar os nomes que encontramos na apostila</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>Pres</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#L</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>#A</source>
        <comment>history table: abbreviation for &apos;Apply yourself to Reading and Teaching&apos;</comment>
        <translation>#Melhore</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="20"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="21"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="26"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="27"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="32"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>#3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="34"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>VM1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>VC2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>VC3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="41"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>BS-R</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>EB</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="32"/>
        <source>One-time Scanning of New Slip</source>
        <translation>Escanear apenas uma vez novo formulário</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="723"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>Reuniões de fim de semana para</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="735"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="843"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>Discursantes de nossa congregação para</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="859"/>
        <location filename="../mainwindow.cpp" line="870"/>
        <source>Outgoing Talks</source>
        <translation>Discursantes que saem</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="975"/>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="467"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="206"/>
        <location filename="../publictalkedit.cpp" line="68"/>
        <location filename="../schoolreminder.cpp" line="183"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="473"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="212"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Junto com</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Sister</source>
        <translation>Irmã</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="168"/>
        <source>Brother</source>
        <translation>Irmão</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Dear %1 %2</source>
        <translation>Prezado (a) %1 %2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="173"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>Abaixo encontrará detalhes sobre sua próxima designação</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="249"/>
        <source>Regards</source>
        <translation>Saudações</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="203"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>Publicador</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="96"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Visualizar programação do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="98"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Editar programação do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="100"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>Visualizar configuração do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="102"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>Editar configuração do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="104"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>Enviar lembretes da reunião do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="106"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Imprimir programa da reunião do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="108"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>Imprimir designações da reunião do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="110"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>Imprimir folhas do dirigente da reunião do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="112"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Visualizar programa da reunião do fim de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="114"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Editar programa da reunião do fim de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="116"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>Visualizar configurações da reunião do fim de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="118"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>Editar configurações da reunião do fim de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="120"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>Visualizar lista de discursos públicos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="122"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>Editar lista de discursos públicos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="124"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>Agendar hospitalidade</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="126"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Imprimir programa da reunião do fim de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="128"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>Imprimir folhas do dirigente da reunião do fim de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="130"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>Imprimir programa de oradores</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="132"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>Imprimir designações de oradores</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="134"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>Imprimir hospitalidade</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="136"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>Imprimir lista de discursos públicos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="146"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>Visualizar oradores públicos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="148"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>Editar oradores públicos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="138"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>Visualizar publicadores</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="140"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>Editar publicadores</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="142"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation>Visualizar dados do Estudante</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="144"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation>Editar dados do Estudante</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="150"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation>Visualizar privilégios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="152"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation>Editar privilégios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="154"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation>Visualizar histórico de discurso da reunião do meio de semana</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="156"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation>Visualizar disponibilidades</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="158"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation>Editar disponibilidades</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="160"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>Visualizar permissões</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="162"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>Editar permissões</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="164"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>Visualizar territórios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="166"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>Editar territórios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="168"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>Imprimir registro de território</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="170"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>Imprimir cartão de território</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="172"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>Imprimir mapa e folhas de endereço</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="174"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>Visualizar configurações de territórios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="176"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>Editar configurações de territórios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="178"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>Visualizar designações dos territórios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="180"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>Visualizar endereços dos territórios</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="182"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>Visualiza configurações da congregação</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="184"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>Editar configurações da congregação</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="186"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>Visualizar eventos especiais</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="188"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>Editar eventos especiais</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="190"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>Visualizar lista de cânticos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="192"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>Editar lista de cânticos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="194"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="205"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>Ancião</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="207"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>Presidente VMC</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>Superintendente VMC</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="211"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>Coord. de discursos públicos</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="213"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>Servo de Território</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>secretário</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>Superintendente do Serviço</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>Coordenador CA</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="221"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>Administrador</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="452"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>Largura não bate com a Altura</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="480"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>Desculpe, entrada inválida</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="174"/>
        <location filename="../settings.ui" line="177"/>
        <location filename="../settings.ui" line="1641"/>
        <source>Exceptions</source>
        <translation>Exceções</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="207"/>
        <source>Public Talks</source>
        <translation>Discursos Públicos</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="773"/>
        <source>Custom templates folder</source>
        <translation>Pasta de modelos personalizados</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="917"/>
        <source>Open database location</source>
        <translation>Abrir o local do banco de dados</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Backup database</source>
        <translation>Backup do Banco de Dados</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Restore database</source>
        <translation>Restaurar o Banco de Dados</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1462"/>
        <source>Names display order</source>
        <translation>Mostrar nomes em ordem</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1397"/>
        <source>Color palette</source>
        <translation>Paleta de cores</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1474"/>
        <source>By last name</source>
        <translation>Último nome</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1486"/>
        <source>By first name</source>
        <translation>Primeiro nome</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1385"/>
        <source>Light</source>
        <translation>Claro</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1351"/>
        <source>Dark</source>
        <translation>Escuro</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1576"/>
        <source>Show song titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2219"/>
        <location filename="../settings.ui" line="2369"/>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt; &lt;head /&gt; &lt;body&gt; &lt;p&gt; &lt;span style = &quot;color: # 540000;&quot;&gt; Erros &lt;/span&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2528"/>
        <source>Public talks maintenance</source>
        <translation>Manutenção de Discursos Públicos</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2620"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>Programa de hospitalidade para os oradores públicos</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3136"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3142"/>
        <source>Street types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3392"/>
        <source>Map marker scale:</source>
        <translation>Escala do marcador do mapa:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3418"/>
        <source>Geo Services</source>
        <translation>Serviços geográficos</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3426"/>
        <source>Google:</source>
        <translation>Google:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3436"/>
        <location filename="../settings.ui" line="3439"/>
        <source>API Key</source>
        <translation>Chave da API</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3446"/>
        <source>Here:</source>
        <translation>Here:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3453"/>
        <source>Default:</source>
        <translation>Padrão:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3463"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3468"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3473"/>
        <source>Here</source>
        <translation>Here</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3488"/>
        <location filename="../settings.ui" line="3491"/>
        <source>App Id</source>
        <translation>App Id</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3498"/>
        <location filename="../settings.ui" line="3501"/>
        <source>App Code</source>
        <translation>App Code</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3584"/>
        <source>Send E-Mail Reminders</source>
        <translation>Enviar designações por email</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3547"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>Enviar notificações quando fechar TheocBase</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3562"/>
        <source>E-Mail Options</source>
        <translation>Opções de envio por email</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3618"/>
        <source>Sender&#x27;s e-mail</source>
        <translation>Remetente email</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3628"/>
        <source>Sender&#x27;s name</source>
        <translation>Nome remetente</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3692"/>
        <source>Account</source>
        <translation>Conta</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3755"/>
        <source>Test Connection</source>
        <translation>Teste da Ligação</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <source>Printing</source>
        <translation>Imprimindo</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3788"/>
        <source>Users</source>
        <translation>Usuários</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3804"/>
        <source>E-mail:</source>
        <translation>E-mail:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3857"/>
        <source>Rules</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <location filename="../settings.ui" line="162"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="839"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="468"/>
        <source>Current congregation</source>
        <translation>Congregação atual</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface</source>
        <translation>Interface de Usuário</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1507"/>
        <source>User interface language</source>
        <translation>Idioma do Interface</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1063"/>
        <source>Security</source>
        <translation>Segurança</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1242"/>
        <source>Enable password</source>
        <translation>Habilitar senha</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>Enable database encryption</source>
        <translation>Encriptar base de dados</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1168"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="189"/>
        <location filename="../settings.ui" line="192"/>
        <location filename="../settings.ui" line="1851"/>
        <source>Life and Ministry Meeting</source>
        <translation>Reunião Vida e Ministério</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2044"/>
        <source>Remove Duplicates</source>
        <translation>Remover Duplicados</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2051"/>
        <source>Meeting Items</source>
        <translation>Itens da reunião</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2150"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2630"/>
        <source>Hide discontinued</source>
        <translation>Ocultar descontinuado</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2812"/>
        <source>Add songs</source>
        <translation>Adicionar Cânticos</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="2818"/>
        <source>Songs</source>
        <translation>Cânticos</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2890"/>
        <location filename="../settings.ui" line="2896"/>
        <source>Add song one at a time</source>
        <translation>Adicionar um cântico de cada vez</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2911"/>
        <source>Song number</source>
        <translation>Cântico número</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2918"/>
        <source>Song title</source>
        <translation>Título do Cântico</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2992"/>
        <source>Cities</source>
        <translation>Cidades</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3064"/>
        <source>Territory types</source>
        <translation>Tipos de territórios</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3241"/>
        <source>Addresses</source>
        <translation>Endereços</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3247"/>
        <source>Address types</source>
        <translation>Tipos de endereços</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3355"/>
        <source>Configuration</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <location filename="../settings.cpp" line="2238"/>
        <source>Access Control</source>
        <translation>Controle de Acesso</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3253"/>
        <source>Type number:</source>
        <translation>Número do tipo:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3148"/>
        <location filename="../settings.ui" line="3263"/>
        <location filename="../settings.ui" line="3794"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3217"/>
        <location filename="../settings.ui" line="3273"/>
        <source>Color:</source>
        <translation>Cor:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3158"/>
        <location filename="../settings.ui" line="3283"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3363"/>
        <source>Default address type:</source>
        <translation>Tipo de endereço padrão:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="228"/>
        <source>Territories</source>
        <translation>Territórios</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="1099"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="435"/>
        <source>Current Circuit Overseer</source>
        <translation>Superintendente de Circuito Atual</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="668"/>
        <source>Click to edit</source>
        <translation>Clique para editar</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1111"/>
        <location filename="../settings.ui" line="3748"/>
        <source>Username</source>
        <translation>Usuário</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <location filename="../settings.ui" line="3665"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="529"/>
        <location filename="../settings.ui" line="1869"/>
        <location filename="../settings.ui" line="1933"/>
        <source>Mo</source>
        <translation>Seg.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="534"/>
        <location filename="../settings.ui" line="1874"/>
        <location filename="../settings.ui" line="1938"/>
        <source>Tu</source>
        <translation>Ter.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="539"/>
        <location filename="../settings.ui" line="1879"/>
        <location filename="../settings.ui" line="1943"/>
        <source>We</source>
        <translation>Qua.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="544"/>
        <location filename="../settings.ui" line="1884"/>
        <location filename="../settings.ui" line="1948"/>
        <source>Th</source>
        <translation>Qui.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.ui" line="1889"/>
        <location filename="../settings.ui" line="1953"/>
        <source>Fr</source>
        <translation>Sex.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="554"/>
        <location filename="../settings.ui" line="1894"/>
        <location filename="../settings.ui" line="1958"/>
        <source>Sa</source>
        <translation>Sab.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="559"/>
        <location filename="../settings.ui" line="1899"/>
        <location filename="../settings.ui" line="1963"/>
        <source>Su</source>
        <translation>Dom.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1912"/>
        <source>Public Talk and Watchtower study</source>
        <translation>Discurso Público e Estudo da Sentinela</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1785"/>
        <location filename="../settings.ui" line="1814"/>
        <location filename="../settings.ui" line="2241"/>
        <location filename="../settings.ui" line="2302"/>
        <location filename="../settings.ui" line="2352"/>
        <location filename="../settings.ui" line="2391"/>
        <location filename="../settings.ui" line="2451"/>
        <location filename="../settings.ui" line="2474"/>
        <location filename="../settings.ui" line="2561"/>
        <location filename="../settings.ui" line="2761"/>
        <location filename="../settings.ui" line="2839"/>
        <location filename="../settings.ui" line="2862"/>
        <location filename="../settings.ui" line="2937"/>
        <location filename="../settings.ui" line="3013"/>
        <location filename="../settings.ui" line="3039"/>
        <location filename="../settings.ui" line="3085"/>
        <location filename="../settings.ui" line="3111"/>
        <location filename="../settings.ui" line="3180"/>
        <location filename="../settings.ui" line="3206"/>
        <location filename="../settings.ui" line="3302"/>
        <location filename="../settings.ui" line="3328"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1683"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>Visita do Sup. de Circuito</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1688"/>
        <source>Convention</source>
        <translation>Congresso</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1693"/>
        <source>Memorial</source>
        <translation>Celebração</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1698"/>
        <source>Zone overseer&#x27;s talk</source>
        <translation>Discurso Superintendente de Zona</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1703"/>
        <source>Other exception</source>
        <translation>Outras exceções</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1716"/>
        <location filename="../settings.cpp" line="606"/>
        <source>Start date</source>
        <translation>Data de início</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1747"/>
        <location filename="../settings.cpp" line="607"/>
        <source>End date</source>
        <translation>Data término</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1864"/>
        <location filename="../settings.ui" line="1928"/>
        <source>No meeting</source>
        <translation>Sem reuniões</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1830"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2084"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>Importar Apostila Nossa vida e Ministério Cristão</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2012"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>principal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2163"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="486"/>
        <location filename="../settings.ui" line="2201"/>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2328"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>Programa da Reunião do Meio de Semana para a reunião selecionada acima</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="237"/>
        <source>Reminders</source>
        <translation>Notificações</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3924"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>Executar um arquivo recebido do Help Desk</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2018"/>
        <source>Number of classes</source>
        <translation>Numero de salas</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2745"/>
        <source>Studies</source>
        <translation>Lições</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2644"/>
        <location filename="../settings.ui" line="2650"/>
        <source>Add subject one at a time</source>
        <translation>Adicione um assunto por vez</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2684"/>
        <source>Public talk number</source>
        <translation>Discurso Número</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2691"/>
        <source>Public talk subject</source>
        <translation>Tema do Discurso</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2701"/>
        <location filename="../settings.ui" line="2948"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2733"/>
        <source>Add congregations and speakers</source>
        <translation>Adicione congregações e oradores</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="826"/>
        <location filename="../settings.cpp" line="1101"/>
        <location filename="../settings.cpp" line="1169"/>
        <source>Number</source>
        <translation>Número</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2552"/>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="554"/>
        <source>Select a backup file</source>
        <translation>Escolha arquivo a restaurar</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="932"/>
        <location filename="../settings.cpp" line="1254"/>
        <location filename="../settings.cpp" line="1551"/>
        <location filename="../settings.cpp" line="1619"/>
        <location filename="../settings.cpp" line="1703"/>
        <location filename="../settings.cpp" line="1799"/>
        <source>Remove selected row?</source>
        <translation>Remover linhas selecionadas?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1030"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Um discurso com o mesmo numero já esta salvo!
Deseja interromper o discurso anterior?

Os discursos agendados serão movidos para a Lista de Tarefas</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1170"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1270"/>
        <source>Song number missing</source>
        <translation>Número do cântico faltando</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1273"/>
        <source>Song title missing</source>
        <translation>Falta título do cântico</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1283"/>
        <source>Song is already saved!</source>
        <translation>Cântico ja foi salvo!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1292"/>
        <source>Song added to database</source>
        <translation>Cântico adicionado ao banco de dados</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1329"/>
        <source>City</source>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1365"/>
        <location filename="../settings.cpp" line="1449"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1515"/>
        <source>City name missing</source>
        <translation>faltando o nome da cidade</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1524"/>
        <source>City is already saved!</source>
        <translation>Cidade ja foi salva!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1532"/>
        <source>City added to database</source>
        <translation>Cidade adicionada ao banco de dados</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1583"/>
        <source>Territory type name missing</source>
        <translation>Faltando tipo de território</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1592"/>
        <source>Territory type is already saved!</source>
        <translation>Tipo de território já está salvo!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1600"/>
        <source>Territory type added to database</source>
        <translation>Tipo de território adicionado ao banco de dados</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1669"/>
        <source>Name of the street type is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1676"/>
        <source>Street type is already saved!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1684"/>
        <source>Street type added to database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2001"/>
        <source>This is no longer an option. Please request help in the forum if this is needed.</source>
        <translation>Isso não é mais uma opção. Por favor, peça ajuda no fórum se for necessário.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2238"/>
        <source>Remove permissions for the selected user?</source>
        <translation>Remover permissões para o usuário selecionado?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2486"/>
        <source>Date</source>
        <translation>data</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="827"/>
        <location filename="../settings.cpp" line="2551"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="829"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>Lançado em</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="830"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>Descontinuado em</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="983"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>Descontinuando este discurso transferirá o programa junto com este esboço para a lista de tarefas. 
 </translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1102"/>
        <source>Revision</source>
        <translation>Revisão</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1406"/>
        <location filename="../settings.cpp" line="1450"/>
        <location filename="../settings.cpp" line="2115"/>
        <location filename="../settings.cpp" line="2171"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1407"/>
        <location filename="../settings.cpp" line="1451"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1756"/>
        <source>Number of address type is missing</source>
        <translation>número de endereço faltando</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1760"/>
        <source>Name of address type is missing</source>
        <translation>Nome do endereço faltando</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1769"/>
        <source>Address type is already saved!</source>
        <translation>O tipo de endereço já foi salvo!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1779"/>
        <source>Address type added to database</source>
        <translation>Tipo de endereço adicionado ao banco de dados</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1892"/>
        <source>Error sending e-mail</source>
        <translation>E-mail não enviado</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1977"/>
        <location filename="../settings.cpp" line="2687"/>
        <source>Select ePub file</source>
        <translation>Selecionar arquivo epub</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2006"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>O arquivo é de fonte confiável? Deseja avançar?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2010"/>
        <source>Command File</source>
        <translation>Comando de arquivo</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2403"/>
        <location filename="../settings.cpp" line="2452"/>
        <source>Meeting</source>
        <translation>Reunião</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2403"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>Remover toda a Reunião? ( Usar apenas para remover informação invalida do database)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2444"/>
        <source>Enter source material here</source>
        <translation>Adicione fonte de matéria aqui</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2452"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>Remover este discurso? ( usar apenas para remover informação invalida do database)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2487"/>
        <source>Bible Reading</source>
        <translation>Leitura da Bíblia</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2488"/>
        <source>Song 1</source>
        <translation>Cântico 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2489"/>
        <source>Song 2</source>
        <translation>Cântico 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2490"/>
        <source>Song 3</source>
        <translation>Cântico 3</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2550"/>
        <source>Meeting Item</source>
        <translation>item da reunião</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2553"/>
        <source>Timing</source>
        <translation>Côntrole de tempo</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2707"/>
        <source>Study Number</source>
        <translation>Número de Estudos</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2708"/>
        <source>Study Name</source>
        <translation>Nome do Estudante</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="831"/>
        <location filename="../settings.cpp" line="1103"/>
        <location filename="../settings.cpp" line="1171"/>
        <location filename="../settings.cpp" line="1330"/>
        <location filename="../settings.cpp" line="1366"/>
        <location filename="../settings.cpp" line="1452"/>
        <source>Language id</source>
        <translation>Nº Idioma</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1002"/>
        <source>Public talk number missing</source>
        <translation>Falta número do Discurso Publico</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1005"/>
        <source>Public talk subject missing</source>
        <translation>Falta o tema do Discurso Publico</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1026"/>
        <source>Public talk is already saved!</source>
        <translation>Discurso publico ja foi salvo!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1060"/>
        <source>Public talk added to database</source>
        <translation>Discurso foi adicionado à base de dados</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1067"/>
        <location filename="../settings.cpp" line="1299"/>
        <location filename="../settings.cpp" line="1538"/>
        <location filename="../settings.cpp" line="1606"/>
        <location filename="../settings.cpp" line="1690"/>
        <location filename="../settings.cpp" line="1786"/>
        <source>Adding failed</source>
        <translation>Não foi possível adicionar</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="605"/>
        <source>Exception</source>
        <translation>Exceção</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="608"/>
        <source>Meeting 1</source>
        <translation>Reunião 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="609"/>
        <source>Meeting 2</source>
        <translation>Reunião 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="572"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Dados restaurados. O programa será reiniciado.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2746"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>Remover TODAS as características de oratória selecionadas? (Só use para remover dados incorretos).</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1098"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1097"/>
        <source>Id</source>
        <translation>Idenificação</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1100"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="536"/>
        <source>Save database</source>
        <translation>Salvar base de dados</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="544"/>
        <source>Database backuped</source>
        <translation>Base de dados backuped</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="14"/>
        <source>Welcome to theocbase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="38"/>
        <source>Do not show again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="66"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="80"/>
        <source>Source</source>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="95"/>
        <source>Student</source>
        <translation>Estudante</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="103"/>
        <location filename="../qml/StudentAssignmentPanel.qml" line="150"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="139"/>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="149"/>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>Com estudante</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="164"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>O ajudante não deve ser alguém do sexo oposto.</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="186"/>
        <source>Study point</source>
        <translation>Melhore lição</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="203"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="209"/>
        <source>Completed</source>
        <translation>Concluido</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="226"/>
        <source>Volunteer</source>
        <translation>Substituto</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="261"/>
        <source>Timing</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="278"/>
        <source>Current Study</source>
        <translation>Estudo atual</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="294"/>
        <source>Exercise Completed</source>
        <translation>Exercicio Completo</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="304"/>
        <source>Next Study</source>
        <translation>Próxima Característica</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="325"/>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="53"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="115"/>
        <source>Street:</source>
        <translation>Rua:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="125"/>
        <source>Postalcode:</source>
        <translation>CEP:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="147"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>Pesquisa</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="162"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="179"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="197"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="75"/>
        <source>Country:</source>
        <translation>País:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="85"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>Estado:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="95"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>Cidade:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="105"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Distrito:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="135"/>
        <source>No.:</source>
        <translation>Nº:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="215"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="232"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Distrito</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="249"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Rua</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="266"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nr.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="283"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>CEP</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="300"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="317"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="349"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="354"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="53"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="79"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="156"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="197"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="256"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>Indefinido [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="299"/>
        <source>Edit address</source>
        <translation>Editar Endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="347"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>Adicionar endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="348"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>Por favor selecione um endereço na lista de resultados de pesquisa.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="407"/>
        <location filename="../qml/TerritoryAddressList.qml" line="417"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Procurar Endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="408"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>Endereço não foi encontrado</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="39"/>
        <source>ID</source>
        <translation>Nº</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>Território-Nº</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="83"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="106"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="129"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="173"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>Distrito</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="151"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="196"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>Rua</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="221"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nº.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="245"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>CEP</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="268"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>Geometria</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="290"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="300"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="325"/>
        <source>Add new address</source>
        <translation>Adicionar novo endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="336"/>
        <source>Edit selected address</source>
        <translation>Editar endereço selecionado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="347"/>
        <source>Remove selected address</source>
        <translation>Remover endereço selecionado</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="78"/>
        <source>Filename:</source>
        <translation>Nome do Arquivo:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="133"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>Match KML Fields</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="55"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>Limites do Território</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="63"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>Endereços</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="163"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>Descrição:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="180"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>Procure por &quot;Descrição&quot; se o território não for encontrado por &quot;Nome&quot;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="195"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>Território Nº</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="201"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>Localização</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="207"/>
        <source>Remark</source>
        <translation>Observações</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>Campos identicos</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="255"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>Endereço:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="266"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="304"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>Tipo de endereço:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="326"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>Nome do arquivo de saída para endereços com falha:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="396"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="405"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="133"/>
        <source>Group by:</source>
        <translation>Agrupar por:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="140"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="142"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="143"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>Ja foi trabalhado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="243"/>
        <source>Add new territory</source>
        <translation>Adicionar novo território</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="258"/>
        <source>Remove selected territory</source>
        <translation>Remover território selecionado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="354"/>
        <source>Remark</source>
        <translation>Observações</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="373"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>Tipo:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="400"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>Cidade:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="465"/>
        <source>Assignments</source>
        <translation>Designaçoes</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="832"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="871"/>
        <source>Addresses</source>
        <translation>Endereços</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="299"/>
        <source>No.:</source>
        <translation>No.:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="310"/>
        <source>Number</source>
        <translation>Numero</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="321"/>
        <source>Locality:</source>
        <translation>Localidade:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="331"/>
        <source>Locality</source>
        <translation>Localidade</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="435"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="500"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="514"/>
        <source>Publisher-ID</source>
        <translation>Publicador-ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="628"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>Retirado em</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="634"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>Entregue em</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="785"/>
        <source>Add new assignment</source>
        <translation>Adicionar nova designação</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="809"/>
        <source>Remove selected assignment</source>
        <translation>Remover designação selecionada</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="593"/>
        <source>Publisher</source>
        <comment>Dropdown column title</comment>
        <translation>Publicador</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="387"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Procurar Endereço</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="833"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="835"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#x27;No&#x27; if overlapping areas should remain in their territories and to add only the part, that doesn&#x27;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="869"/>
        <source>Join to the selected territory</source>
        <comment>Join two territories into one</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="897"/>
        <source>Add address to selected territory</source>
        <translation>Adicionar endereço ao território selecionado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="905"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>Designar ao território selecionado</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="913"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>Remover endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1004"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>% 1 de% 2 endereço (s) importado.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1019"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>Importar limites do território</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1029"/>
        <location filename="../qml/TerritoryMap.qml" line="1040"/>
        <location filename="../qml/TerritoryMap.qml" line="1055"/>
        <location filename="../qml/TerritoryMap.qml" line="1064"/>
        <location filename="../qml/TerritoryMap.qml" line="1097"/>
        <location filename="../qml/TerritoryMap.qml" line="1106"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>Importar dados do território</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1030"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>% n território (s) importado (s) ou atualizado.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1056"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Por favor selecione os campos de endereço e nome.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1065"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Os campos selecionados devem ser diferentes.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1073"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>Importar endereços de território</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1074"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>Os endereços serão adicionados ao território atual. Selecione primeiro um território.</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1098"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>% n endereço (s) importado.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1108"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>Nenhum território válido selecionado.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1110"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>O arquivo importado não pôde ser lido.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1142"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1144"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1173"/>
        <location filename="../qml/TerritoryMap.qml" line="1181"/>
        <source>Open file</source>
        <translation>Open file</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1174"/>
        <location filename="../qml/TerritoryMap.qml" line="1175"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>KML files (*.kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1174"/>
        <location filename="../qml/TerritoryMap.qml" line="1182"/>
        <location filename="../qml/TerritoryMap.qml" line="1190"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>All files (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1182"/>
        <location filename="../qml/TerritoryMap.qml" line="1183"/>
        <location filename="../qml/TerritoryMap.qml" line="1190"/>
        <location filename="../qml/TerritoryMap.qml" line="1191"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>CSV files (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1182"/>
        <location filename="../qml/TerritoryMap.qml" line="1190"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>Text files (*.txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1189"/>
        <source>Save file</source>
        <translation>Salvar arquivo</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="66"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>procura</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="152"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>Importar dados</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="196"/>
        <source>Show/hide streets</source>
        <comment>Show/hide streets of territories</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="208"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>Modo de edição</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="222"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="234"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>Remover limites</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="246"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="163"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>Zoom Full</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="173"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>Mostrar / ocultar territórios</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="184"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>Mostrar / ocultar marcadores</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="209"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="269"/>
        <location filename="../qml/TerritoryStreetList.qml" line="280"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="270"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="38"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="60"/>
        <source>Territory-ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="166"/>
        <source>Add new street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="177"/>
        <source>Remove selected street</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 meses</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 to 12 meses</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 meses atrás</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>Não trabalhado</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>Não designado</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>Território</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>territórios</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="114"/>
        <source>To Do List</source>
        <translation>Lista de tarefas</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="209"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>Não pode ser programado até que estes campos sejam corrigidos: %1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Incoming</source>
        <comment>incoming speaker</comment>
        <translation>Recebendo</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Outgoing</source>
        <comment>outgoing speaker</comment>
        <translation>Saindo</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="10"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="69"/>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="45"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>Discurso de Serviço</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="66"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="97"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="109"/>
        <location filename="../qml/WEMeetingModule.qml" line="314"/>
        <source>Song and Prayer</source>
        <translation>Cântico e Oração</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="110"/>
        <location filename="../qml/WEMeetingModule.qml" line="315"/>
        <source>Song %1 and Prayer</source>
        <translation>Cântico %1 e Oração</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="144"/>
        <source>PUBLIC TALK</source>
        <translation>DISCURSO PÚBLICO</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="161"/>
        <source>Send to To Do List</source>
        <translation>Enviar lista de tarefas</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="167"/>
        <source>Move to different week</source>
        <translation>tranferir para outra semana</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="176"/>
        <source>Clear Public Talk selections</source>
        <translation>Apagar discurso publicos selecionados</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="225"/>
        <source>WATCHTOWER STUDY</source>
        <translation>ESTUDO DE A SENTINELA</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="234"/>
        <source>Song %1</source>
        <translation>Cântico %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="255"/>
        <source>Import WT...</source>
        <translation>Importar WT...</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="283"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="284"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="34"/>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="58"/>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="46"/>
        <source>Watchtower Issue</source>
        <translation>Edição de A Sentinela</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="68"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artigo</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="86"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="106"/>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="128"/>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Leitor</translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <location filename="../cpersoncombobox.cpp" line="238"/>
        <source>Brother has other meeting parts</source>
        <translation>Irmão tem outras partes na reunião</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="233"/>
        <source>Person unavailable</source>
        <translation>Pessoa indisponivel</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="228"/>
        <source>Outgoing speaker</source>
        <translation>Orador que sai</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="118"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(Registo inexistente)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>Nova versão disponível</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>Baixar</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>Nova versão...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>Nenhuma atualização disponivel</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="112"/>
        <source>File reading failed</source>
        <translation>Leitura do arquivo falhou</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="450"/>
        <source>XML file generated in the wrong version.</source>
        <translation>Arquivo XML gerado na versão errada.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="517"/>
        <source>Persons - added </source>
        <translation>Membros - adicionados </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="555"/>
        <source>Persons - updated </source>
        <translation>Membros - atualizados </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="586"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>Escola do Ministério Teocrático - adicionado </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="588"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>Escola do Ministério Teocrático - programa atualizado </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="784"/>
        <source>Theocratic Ministry School - Study updated</source>
        <translation>Escola Ministério Teocratico - Característica atualizada</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="798"/>
        <source>Theocratic Ministry School - Study added</source>
        <translation>Escola Ministério Teocratico - Característica adicionada</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="834"/>
        <source>Public talks - theme added </source>
        <translation>Discursos Públicos - Tema adicionado </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="892"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>Discurso Público e A Sentinela - programa atualizado </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="898"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>Discurso Público e A Sentinela - programa adicionado </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="259"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>Nao Bater</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1621"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>Salvar endereços com falha</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1622"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>O arquivo está apenas em modo leitura</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>Não definido</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>Ok mas JSON indisponivel</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>Autorizando</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>A autorização falhou</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>Falta identificar cliente</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>Cliente secreto faltando</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>precisa codigo autenticador</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>Identificar por Token</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="196"/>
        <source>Number of weeks after selected date</source>
        <translation>Número de semanas depois de data selecionada</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="246"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Número de semanas em cinza depois de uma designação</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="220"/>
        <source>weeks</source>
        <translation>semanas</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="122"/>
        <source>Timeline</source>
        <translation>Cronograma</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="180"/>
        <source>Number of weeks before selected date</source>
        <translation>Número de semanas antes da data seleccionada</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="418"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>Publicadores</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="589"/>
        <source>CBS</source>
        <translation>DEB</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="684"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>Dir.</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="589"/>
        <location filename="../historytable.cpp" line="614"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>Leit.</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="611"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>Dest.</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="623"/>
        <source>TMS</source>
        <translation>E. M. T.</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="750"/>
        <source>SM</source>
        <translation>R. S.</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="768"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>DP</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="774"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>Pr</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="783"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>L-Sent</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="999"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1000"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1001"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1002"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1003"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1004"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Next &gt;</source>
        <translation>próximo</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>&lt; Back</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="41"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="67"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Adicione Programa da Escola.Copie (Ctrl + C / cmd + C)o programa da WTLibrary e cole abaixo (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="68"/>
        <source>Check schedule</source>
        <translation>Verifique a programação</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="71"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importar lições. Copie as lições da WTLibrary e cole abaixo (Ctrl + V / cmd +V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="72"/>
        <source>Check studies</source>
        <translation>Verifique as características</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="75"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Adicione cenas. Copie (Ctrl + C / cmd + C)as cenas da WTLibrary e cole abaixo (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="76"/>
        <source>Check settings</source>
        <translation>Verifique as cenas</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="80"/>
        <source>Check subjects</source>
        <translation>Verifique Temas</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="83"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Adicione oradores e congregações. Copie tudo para a área de tranferencia e cole abaixo (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="84"/>
        <source>Check data</source>
        <translation>Verifique data</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="88"/>
        <source>Check songs</source>
        <translation>Verificar Cânticos</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="100"/>
        <source>No schedule to import.</source>
        <translation>Não se encontra o programa para importação.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="142"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="143"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>Por favor adicione data do Início AAAA-MM-DD (ex 2011-01-03)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="150"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>A data não corresponde ao início da semana (segunda-feira)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="203"/>
        <source>Import songs</source>
        <translation>Importar Cânticos</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="245"/>
        <source>Only brothers</source>
        <translation>Só Irmãos</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="476"/>
        <source>Subject</source>
        <translation>Assunto</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="519"/>
        <source>id</source>
        <translation>Idenificação</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="522"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="525"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="528"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="531"/>
        <source>Public talks</source>
        <translation>Discursos Públicos</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="534"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="551"/>
        <location filename="../importwizard.cpp" line="552"/>
        <source>First name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="551"/>
        <location filename="../importwizard.cpp" line="552"/>
        <source>Last name</source>
        <translation>Sobrenome</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="179"/>
        <source>Import subjects</source>
        <translation>Importar matéria</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="180"/>
        <location filename="../importwizard.cpp" line="204"/>
        <source>Choose language</source>
        <translation>Escolha o Idioma</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>Save to database</source>
        <translation>Salvar na base de dados</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="79"/>
        <source>Add public talk&#x27;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>Adicione temas de discursos . Copie (Ctrl + C / cmd + C) os temas da WTLibrary e cole abaixo (Ctrl + V / cmd + V).
O número na 1ª coluna e o tema na 2ª.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="87"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="234"/>
        <source>date</source>
        <translation>data</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="237"/>
        <source>number</source>
        <translation>Número</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="240"/>
        <source>subject</source>
        <translation>Assunto</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="243"/>
        <source>material</source>
        <translation>material</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="354"/>
        <source>setting</source>
        <translation>Cena</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="416"/>
        <source>study</source>
        <translation>Característica</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="602"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="748"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Um discurso com o mesmo numero já esta salvo!
Deseja interromper o discurso anterior?

Os discursos agendados serão movidos para a Lista de Tarefas</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="751"/>
        <source>Previous talk: </source>
        <translation>Discurso anterior:</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="752"/>
        <source>New talk: </source>
        <translation>Novo discurso:</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="873"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>Temas de Discursos não encontrado. Adicione temas e tente de novo!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="900"/>
        <source> rows added</source>
        <translation> linhas adicionadas</translation>
    </message>
</context>
<context>
    <name>lmmWorksheetRegEx</name>
    <message>
        <location filename="../lmmworksheetregex.ui" line="203"/>
        <source>Language Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="14"/>
        <source>Talk Name in the Workbook</source>
        <translation>Nome do Discurso na Apostila</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="16"/>
        <source>Meeting Item</source>
        <translation>Item da Reunião</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="42"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>Editor de tipo de Discurso</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>Nome do usuário</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="311"/>
        <source>Select an assignment on the left to edit</source>
        <translation>Selecione uma designação à esquerda para editar</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="631"/>
        <source>Sister</source>
        <translation>Irmã</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="619"/>
        <source>Brother</source>
        <translation>Irmão</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="645"/>
        <source>Servant</source>
        <translation>Servo</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>Publicadores</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="811"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1019"/>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1218"/>
        <source>Public talks</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="958"/>
        <location filename="../personsui.ui" line="1211"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1191"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1275"/>
        <source>Watchtower reader</source>
        <translation>Leitor da Sentinela</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="657"/>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1149"/>
        <source>Cong. Bible Study reader</source>
        <translation>Leitor do Estudo Bíblico Cong.</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1304"/>
        <source>Meeting for field ministry</source>
        <translation>Reunião para o serviço de Campo</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="368"/>
        <location filename="../personsui.cpp" line="555"/>
        <source>First name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="404"/>
        <location filename="../personsui.cpp" line="554"/>
        <source>Last name</source>
        <translation>último nome</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="535"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="599"/>
        <source>E-mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1360"/>
        <source>Assignment</source>
        <translation>Designação</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1477"/>
        <source>Current Study</source>
        <translation>Estudo Atual</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1745"/>
        <location filename="../personsui.ui" line="1768"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1241"/>
        <source>Watchtower Study Conductor</source>
        <translation>Dirigente Estudo Sentinela</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="669"/>
        <source>Family Head</source>
        <translation>Chefe de família</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="681"/>
        <source>Family member linked to</source>
        <translation>Membro da família de</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="921"/>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1095"/>
        <source>Only Auxiliary Classes</source>
        <translation>Apenas 2ª Sala</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1068"/>
        <source>Only Main Class</source>
        <translation>Apenas Salão principal</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="711"/>
        <source>Active</source>
        <translation>Ativo</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="982"/>
        <source>Treasures From God&#x27;s Word</source>
        <translation>Tesouros da Palavra de Deus</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1033"/>
        <source>Initial Call</source>
        <translation>Primeira conversa</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="996"/>
        <source>Bible Reading</source>
        <translation>Leitura da Bíblia</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1102"/>
        <source>All Classes</source>
        <translation>Todas as salas</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1026"/>
        <source>Bible Study</source>
        <translation>Estudo Bíblico</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1142"/>
        <source>Congregation Bible Study</source>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1135"/>
        <source>Living as Christians Talks</source>
        <translation>Discursos de Nossa Vida Cristã</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="989"/>
        <source>Digging for Spiritual Gems</source>
        <translation>Encontre Jóias Espirituais</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1040"/>
        <source>Return Visit</source>
        <translation>Revisita</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="295"/>
        <source>Personal Info</source>
        <translation>Informações pessoais</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="723"/>
        <source>Host for Public Speakers</source>
        <translation>Anfitrião para Oradores Públicos</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="471"/>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="780"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1115"/>
        <source>Talk</source>
        <translation>Discurso</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1316"/>
        <source>History</source>
        <translation>Histórico</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1350"/>
        <source>date</source>
        <translation>data</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1355"/>
        <source>no</source>
        <translation>Nº</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1365"/>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1370"/>
        <source>Time</source>
        <translation>tempo</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1375"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Junto com</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1384"/>
        <source>Studies</source>
        <translation>Lições</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1449"/>
        <source>Study</source>
        <translation>Característica</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1454"/>
        <source>Date assigned</source>
        <translation>Data da designação</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1459"/>
        <source>Exercises</source>
        <translation>Exercícios</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1464"/>
        <source>Date completed</source>
        <translation>Data que completou</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1644"/>
        <source>Unavailable</source>
        <translation>Ausente</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1678"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1683"/>
        <source>End</source>
        <translation>Final</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="131"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="181"/>
        <location filename="../personsui.ui" line="1614"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="386"/>
        <source>A person with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>Já existe uma pessoa com esse nome:%1. Quer alterar o nome?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="605"/>
        <source>Remove student?</source>
        <translation>Remover estudante?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="582"/>
        <source>Remove student</source>
        <translation>Remover estudante</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="409"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>% 1 está programado para discursos públicos ! Estes discursos
serão movidos para a Lista de Tarefas se você removê-lo como orador.
Deseja remove-lo como Orador?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="596"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 esta programado para discursos públicos! Estes esboços serão
 movidos para a Lista de Tarefas se remover o estudante.</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="1081"/>
        <source>Remove study</source>
        <translation>Remover Estudo</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1109"/>
        <source>Copy to the clipboard</source>
        <translation>Copiar para área de tranferencia</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1112"/>
        <source>Copy</source>
        <translation>Cópia</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="235"/>
        <location filename="../printui.ui" line="326"/>
        <source>Schedule</source>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="390"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>Lista de Chamadas e Programas para Hospitalidade</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="783"/>
        <source>Midweek Meeting Title</source>
        <translation>Tema da Reunião do Meio de Semana</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="685"/>
        <source>Weekend Meeting Title</source>
        <translation>Tema da Reunião do Final de Semana</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="793"/>
        <source>Show Section Titles</source>
        <translation>Mostrar Títulos da Seção</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="698"/>
        <source>Review/Preview/Announcements Title</source>
        <comment>See S-140</comment>
        <translation>Recapitulação/Visão geral da próxima semana/Anúncios </translation>
    </message>
    <message>
        <location filename="../printui.ui" line="764"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>Comentários iniciais</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="258"/>
        <source>Assignment Slips for Assistants</source>
        <translation>Designações para Ajudante</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="361"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>Programa de Saída de Oradores</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="377"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>Designações de Saídas de Oradores</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="403"/>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="535"/>
        <source>Territory Map Card</source>
        <translation>Cartão do Mapa do Território</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="545"/>
        <source>Map and Addresses Sheets</source>
        <translation>Folhas de Mapa e Endereços</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="824"/>
        <source>Show public talk revision date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="725"/>
        <source>Show duration</source>
        <translation>Mostrar a duração</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="705"/>
        <source>Show time</source>
        <translation>Mostrar hora</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="810"/>
        <source>Show Workbook Issue no.</source>
        <translation>Mostrar Apostila edição no.</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="803"/>
        <source>Show Watchtower Issue no.</source>
        <translation>Mostrar Edição de A Sentinela no.</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="817"/>
        <source>Own congregation only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="968"/>
        <source>Territory number(s)</source>
        <translation>Número(s) de território</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="975"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>Separado por vírgulas; pressione Enter para atualizar</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="981"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="522"/>
        <source>Territory Record</source>
        <translation>Registro de território</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="870"/>
        <source>Template</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="896"/>
        <source>Paper Size</source>
        <translation>Tam. do Papel</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="958"/>
        <source>Print From Date</source>
        <translation>Imprimir desde a semana</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="916"/>
        <source>Print Thru Date</source>
        <translation>imprimir até a semana</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1039"/>
        <source>Printing</source>
        <translation>Imprimindo</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1042"/>
        <location filename="../printui.ui" line="1077"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="248"/>
        <source>Assignment Slips</source>
        <translation>Folha designação</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="718"/>
        <source>Share in Dropbox</source>
        <translation>Compartilhar no Dropbox</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="278"/>
        <location filename="../printui.ui" line="345"/>
        <source>Worksheets</source>
        <translation>Folhas do dirigente</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="565"/>
        <source>Meetings for field ministry</source>
        <translation>Reunião de serviço de campo</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="423"/>
        <source>Combination</source>
        <translation>Combinacão</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="632"/>
        <source>Additional Options</source>
        <translation>Opções Adicionais</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="660"/>
        <source>Show Song Titles</source>
        <translation>Mostrar títulos dos cânticos</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="735"/>
        <source>Show Counsel Text</source>
        <translation>Mostar Folha de Conselho</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="474"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>Programa discursos fora</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="268"/>
        <source>Print assigned only</source>
        <translation>Imprimir apenas territórios designados</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="372"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>Cole a informação da área de tranferência no processador de texto. (Ctrl+V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="419"/>
        <location filename="../printui.cpp" line="511"/>
        <location filename="../printui.cpp" line="1134"/>
        <source>file created</source>
        <translation>arquivo criado.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="969"/>
        <source>Invalid entry, sorry.</source>
        <translation>Desculpe, entrada inválida</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="383"/>
        <location filename="../printui.cpp" line="431"/>
        <location filename="../printui.cpp" line="491"/>
        <source>Save file</source>
        <translation>Salvar arquivo</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territories</source>
        <translation>Territórios</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="355"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>Personalizado...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="587"/>
        <source>Converting %1 to JPG file</source>
        <translation>Convertendo %1 to JPG file</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="207"/>
        <location filename="../printui.ui" line="461"/>
        <location filename="../printui.cpp" line="736"/>
        <source>Midweek Meeting</source>
        <translation>Reunião do Meio de Semana</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="745"/>
        <source>Closing Comments</source>
        <translation>Comentários Finais</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="962"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>nova folha com tamanho personalizado</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="962"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>Formatar: largura x altura. Largura e Altura pode ser de ou mm. Examplo 210mm x 297mm</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="298"/>
        <location filename="../printui.ui" line="448"/>
        <location filename="../printui.cpp" line="737"/>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="744"/>
        <source>Opening Comments</source>
        <translation>Comentários iniciais</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="193"/>
        <source>From %1</source>
        <translation>De %1</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="221"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>Essa data já tem um discurso programado.</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="226"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>Trocar Discursos</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="227"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="67"/>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="257"/>
        <location filename="../publictalkedit.ui" line="592"/>
        <location filename="../publictalkedit.ui" line="681"/>
        <location filename="../publictalkedit.ui" line="881"/>
        <location filename="../publictalkedit.cpp" line="47"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="169"/>
        <location filename="../publictalkedit.ui" line="866"/>
        <location filename="../publictalkedit.cpp" line="48"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="144"/>
        <location filename="../publictalkedit.ui" line="328"/>
        <location filename="../publictalkedit.cpp" line="50"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="232"/>
        <location filename="../publictalkedit.ui" line="617"/>
        <location filename="../publictalkedit.ui" line="642"/>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="411"/>
        <source>Watchtower Study</source>
        <translation>Estudo de A Sentinela</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="472"/>
        <location filename="../publictalkedit.cpp" line="52"/>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="542"/>
        <source>Source</source>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="285"/>
        <location filename="../publictalkedit.ui" line="932"/>
        <source>Move to different week</source>
        <translation>tranferir para outra semana</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="118"/>
        <location filename="../publictalkedit.ui" line="906"/>
        <source>Send to To Do List</source>
        <translation>Enviar lista de tarefas</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="311"/>
        <source>Clear Public Talk selections</source>
        <comment>Clear=Delete</comment>
        <translation>Apagar discurso publicos selecionados</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="567"/>
        <location filename="../publictalkedit.ui" line="658"/>
        <location filename="../publictalkedit.cpp" line="51"/>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="497"/>
        <location filename="../publictalkedit.ui" line="671"/>
        <source>Service Talk</source>
        <translation>Discurso de Serviço</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="759"/>
        <source>Outgoing Speakers This Weekend</source>
        <translation>Oradores designados para ir a outra congregação</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="833"/>
        <source>Add to Outgoing List</source>
        <translation>Adicione na lista de oradores que saem</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="876"/>
        <source>Start time</source>
        <translation>Hora Início</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="886"/>
        <source>Theme No.</source>
        <translation>Tema n.</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="979"/>
        <source>To Do List</source>
        <translation>Lista de tarefas</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1019"/>
        <source>Add item to schedule</source>
        <translation>Adicionar item ao programa</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1100"/>
        <source>Add Outgoing To Do item</source>
        <comment>Add OUT item</comment>
        <translation>Adicionar discurso a realizar fora</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="793"/>
        <location filename="../publictalkedit.ui" line="1045"/>
        <source>Remove item</source>
        <translation>Remover item</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1071"/>
        <source>Add Incoming To Do item</source>
        <translation>Adicionar discurso de orador de fora</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="207"/>
        <location filename="../publictalkedit.ui" line="871"/>
        <location filename="../publictalkedit.cpp" line="49"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.cpp" line="89"/>
        <source>Watchtower Study Edition</source>
        <translation>A sentinela Edição de estudo</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="256"/>
        <location filename="../publictalkedit.cpp" line="291"/>
        <source>From %1; speaker removed</source>
        <comment>From [scheduled date]; speaker removed</comment>
        <translation>From %1; Orador removido</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="294"/>
        <source>From %1; speaker moved to %2</source>
        <comment>From [scheduled date]; speaker moved to [new congregation]</comment>
        <translation>From %1; orador mudou-se para %2</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="340"/>
        <location filename="../publictalkedit.cpp" line="375"/>
        <source>From %1; talk discontinued</source>
        <comment>From [scheduled date]; talk discontinued</comment>
        <translation>A partir de% 1; discurso descontinuado</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="695"/>
        <source>In</source>
        <comment>Incoming</comment>
        <translation>Chegando</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="695"/>
        <source>Out</source>
        <comment>Outgoing</comment>
        <translation>Saindo</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="817"/>
        <source>The selected speaker has already public talk on this calendar month. Do you want to add?</source>
        <translation>Este irmão já tem um discurso marcado para o mesmo mês. Deseja desiginar mais um?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="946"/>
        <location filename="../publictalkedit.cpp" line="997"/>
        <location filename="../publictalkedit.cpp" line="1037"/>
        <source>The destination date already has a talk scheduled. What to do?</source>
        <translation>Esse destino ja tem um discurso marcado. O que fazer?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="947"/>
        <location filename="../publictalkedit.cpp" line="998"/>
        <source>&amp;Swap Talks</source>
        <translation>&amp;Trocar discurso</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="948"/>
        <location filename="../publictalkedit.cpp" line="999"/>
        <location filename="../publictalkedit.cpp" line="1038"/>
        <source>&amp;Move other talk to To Do List</source>
        <translation>&amp;Mover para lista de tarefas</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="949"/>
        <location filename="../publictalkedit.cpp" line="1000"/>
        <location filename="../publictalkedit.cpp" line="1039"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="966"/>
        <location filename="../publictalkedit.cpp" line="1012"/>
        <location filename="../publictalkedit.cpp" line="1048"/>
        <location filename="../publictalkedit.cpp" line="1057"/>
        <location filename="../publictalkedit.cpp" line="1079"/>
        <location filename="../publictalkedit.cpp" line="1112"/>
        <source>From %1</source>
        <translation>De %1</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1035"/>
        <source>Date Already Scheduled</source>
        <translation>Esta data já esta programada</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1066"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1066"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>Não pode ser programado até que estes campos sejam corrigidos: %1</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>Notificações</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="58"/>
        <source>Date range</source>
        <translation>Intervalo datas</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="159"/>
        <source>Send selected reminders</source>
        <translation>Mandar lembretes selecionados</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="177"/>
        <source>From</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="195"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="247"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="298"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="303"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="313"/>
        <source>Subject</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="318"/>
        <source>Message</source>
        <translation>Mensagem</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="308"/>
        <source>E-Mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="93"/>
        <source>Email sending...</source>
        <translation>Enviando email...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="94"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="120"/>
        <source>Error sending e-mail</source>
        <translation>E-mail não enviado</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>Conselheiro -Sala II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>Conselheiro -Sala III</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>Oração I</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>Oração II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Cancelamento - Designação em Nossa Vida e Ministério Cristão</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Designação para Nossa Vida e Ministério Cristão</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="184"/>
        <source>Assignment</source>
        <translation>Designação</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="187"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="189"/>
        <source>Source Material</source>
        <translation>Fonte de matéria</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="203"/>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="213"/>
        <location filename="../schoolreminder.cpp" line="215"/>
        <source>Study</source>
        <translation>Característica</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="222"/>
        <source>Main hall</source>
        <translation>Salão Principal</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="225"/>
        <source>Auxiliary classroom 1</source>
        <translation>Sala Auxiliar 1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="228"/>
        <source>Auxiliary classroom 2</source>
        <translation>Sala Auxiliar 2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>Local da parte:</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="237"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Leitor para o Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="239"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Ajudante para %1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="244"/>
        <source>Cancellation</source>
        <translation>Cancelamento</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <location filename="../schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>Substituto</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>Característica</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>Exercicio Completo</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>Nova Característica:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <comment>for sisters assignment</comment>
        <translation>Cena:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>Concluído</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>Contrôle de tempo:</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="59"/>
        <source>Bible highlights</source>
        <translation>Destaques da Bíblia</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="61"/>
        <source>reading</source>
        <translation>Lendo</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="65"/>
        <source>Assignment result</source>
        <translation>Conselho a ser dado</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="130"/>
        <source>Do not assign the next study</source>
        <translation>Não designar nova característica</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="159"/>
        <location filename="../schoolresult.cpp" line="369"/>
        <source>Leave on current study</source>
        <translation>Permanecer na característica atual</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="239"/>
        <source>Select setting</source>
        <translation>Selecione cena</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="246"/>
        <source>Timing</source>
        <translation>Côntrole de tempo</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="247"/>
        <source>The timing is empty. Save?</source>
        <translation>Salvar sem controle do tempo?</translation>
    </message>
</context>
<context>
    <name>schoolui</name>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <location filename="../schoolui.cpp" line="36"/>
        <source>Source</source>
        <translation>Fonte da matéria</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="41"/>
        <source>Class</source>
        <translation>Sala</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="111"/>
        <source>Bible highlights:</source>
        <translation>Destaques:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="115"/>
        <source>No. 1:</source>
        <translation>No. 1:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="119"/>
        <source>No. 2:</source>
        <translation>No. 2:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="123"/>
        <source>No. 3:</source>
        <translation>No. 3:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="127"/>
        <source>Reader:</source>
        <translation>Leitor:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="176"/>
        <source>No school</source>
        <translation>Semana sem Escola</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="329"/>
        <source>Nothing to display</source>
        <translation>Nada a apresentar</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="495"/>
        <source>No assignment has been made!</source>
        <translation>Nenhuma designação foi feita!</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="500"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>Por favor, importe novo Programa da Escola usando a Watchtower Library (Configuração-&gt;Escola Ministério Teocratico...)</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="542"/>
        <source>Show Details...</source>
        <translation>Ver detalhes</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>Congregações e oradores</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="400"/>
        <source>Congregation...</source>
        <translation>Congregação...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="371"/>
        <source>Speaker...</source>
        <translation>Orador...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1332"/>
        <source>Toggle Talks Editable</source>
        <translation>Ativar/desativar edição dos Discursos</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1300"/>
        <source>Add Multiple Talks</source>
        <translation>Adicionar multiplos discursos</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="95"/>
        <location filename="../speakersui.ui" line="136"/>
        <location filename="../speakersui.ui" line="180"/>
        <location filename="../speakersui.ui" line="246"/>
        <location filename="../speakersui.ui" line="1303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="494"/>
        <source>Select a Congregation or Speaker</source>
        <translation>Escolha a Congregação ou Orador</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="725"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="792"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.cpp" line="173"/>
        <source>Circuit</source>
        <translation>Circuito</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="667"/>
        <location filename="../speakersui.ui" line="1685"/>
        <location filename="../speakersui.cpp" line="181"/>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="92"/>
        <source>Speakers</source>
        <translation>Oradores</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="133"/>
        <source>Group by congregation</source>
        <translation>Grupo por Congregação</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="177"/>
        <source>Group by circuit</source>
        <translation>Grupo por Circuito</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="221"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="243"/>
        <source>Configure Filter</source>
        <translation>Configurar filtro</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="882"/>
        <source>Congregation Details</source>
        <translation>Detalhes da Congregação</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="894"/>
        <source>Meeting Times</source>
        <translation>Horário das Reuniões</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="999"/>
        <location filename="../speakersui.ui" line="1169"/>
        <source>Mo</source>
        <translation>Seg</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1004"/>
        <location filename="../speakersui.ui" line="1174"/>
        <source>Tu</source>
        <translation>Ter</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1009"/>
        <location filename="../speakersui.ui" line="1179"/>
        <source>We</source>
        <translation>Qua</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1014"/>
        <location filename="../speakersui.ui" line="1184"/>
        <source>Th</source>
        <translation>Qui</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1019"/>
        <location filename="../speakersui.ui" line="1189"/>
        <source>Fr</source>
        <translation>Sex</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1024"/>
        <location filename="../speakersui.ui" line="1194"/>
        <source>Sa</source>
        <translation>Sab</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1029"/>
        <location filename="../speakersui.ui" line="1199"/>
        <source>Su</source>
        <translation>Dom</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1363"/>
        <source>Personal Info</source>
        <translation>Informações pessoais</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1786"/>
        <location filename="../speakersui.cpp" line="835"/>
        <source>First Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1838"/>
        <source>Mobile</source>
        <translation>Celular</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1576"/>
        <location filename="../speakersui.cpp" line="836"/>
        <source>Last Name</source>
        <translation>Último Nome</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup_2</source>
        <translation>buttonGroup_2</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1540"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup</source>
        <translation>buttonGroup</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1377"/>
        <source>Public Talks</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1646"/>
        <source>E-mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1467"/>
        <source>Notes</source>
        <translation>Anotações</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="189"/>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="363"/>
        <location filename="../speakersui.cpp" line="543"/>
        <source>Undefined</source>
        <translation>Não definido</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="446"/>
        <location filename="../speakersui.cpp" line="451"/>
        <source>%1 Meeting Day/Time</source>
        <translation>%1 Reunião dia/hora</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="576"/>
        <source>A speaker with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>Já existe um Orador com esse nome: %1. Quer alterar o nome?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="708"/>
        <source>The congregation has speakers!</source>
        <translation>A congregação tem oradores!</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="713"/>
        <source>Remove the congregation?</source>
        <translation>Remover a congregação?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="759"/>
        <source>Remove the speaker?</source>
        <translation>Remover o orador?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="764"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>Este orador está programado para fazer discursos! Estes discursos serão movidos para a Lista de Tarefas se voce remover o orador.</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="828"/>
        <source>Missing Information</source>
        <translation>Faltando informações</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="828"/>
        <source>Select congregation first</source>
        <translation>primeiro selecione a congregação</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="861"/>
        <source>New Congregation</source>
        <translation>Nova Congregação</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="929"/>
        <source>Add Talks</source>
        <translation>Adicionar discursos</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="929"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>Digite o n° dos discursos separados por vírgulas ou pontos finais</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="965"/>
        <source>Change congregation to &#x27;%1&#x27;?</source>
        <translation>Alterar Congregação para &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="967"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>Este orador esta programado para ir a outras congregações. Estes discursos serão movidos para a Lista de Tarefas se mudar a congregação.</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>Página inicial</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="241"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation>Conflito de versão: as alterações na nuvem foram feitas com uma versão mais recente!</translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="247"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation>Conflito de versão: os dados da nuvem precisam ser atualizados com a mesma versão por um usuário autorizado.</translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>Territórios</translation>
    </message>
</context></TS>