﻿/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cterritories.h"

GeocodeResult::GeocodeResult(QObject *parent)
    : QObject(parent)
{
}

GeocodeResult::~GeocodeResult()
{
}

void GeocodeResult::setCountry(QString value)
{
    m_country = value;
}

void GeocodeResult::setState(QString value)
{
    m_state = value;
}

void GeocodeResult::setCounty(QString value)
{
    m_county = value;
}

void GeocodeResult::setCity(QString value)
{
    m_city = value;
}

void GeocodeResult::setDistrict(QString value)
{
    m_district = value;
}

void GeocodeResult::setStreet(QString value)
{
    m_street = value;
}

void GeocodeResult::setPostalCode(QString value)
{
    m_postalCode = value;
}

void GeocodeResult::setHouseNumber(QString value)
{
    m_houseNumber = value;
}

QString GeocodeResult::houseNumber()
{
    return m_houseNumber;
}

void GeocodeResult::setLatitude(double value)
{
    m_latitude = value;
}

void GeocodeResult::setLongitude(double value)
{
    m_longitude = value;
}

QString GeocodeResult::wktGeometry()
{
    return "POINT(" + QVariant(m_longitude).toString() + " " + QVariant(m_latitude).toString() + ")";
}

void GeocodeResult::setText(QString value)
{
    m_text = value;
}

StreetResult::StreetResult(QObject *parent)
    : QObject(parent), m_isChecked(false)
{
}

StreetResult::~StreetResult()
{
}

void StreetResult::setIsChecked(bool value)
{
    m_isChecked = value;
}

void StreetResult::setStreetName(QString value)
{
    m_streetName = value;
}

void StreetResult::setWktGeometry(QString value)
{
    m_wktGeometry = value;
}

void StreetResult::setIsAlreadyAdded(bool isAlreadyAdded)
{
    m_isAlreadyAdded = isAlreadyAdded;
}

StreetResultModel::StreetResultModel()
{
}

StreetResultModel::StreetResultModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QHash<int, QByteArray> StreetResultModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[IsCheckedRole] = "isChecked";
    items[StreetNameRole] = "streetName";
    items[IsAlreadyAddedRole] = "isAlreadyAdded";
    items[WktGeometryRole] = "wktGeometry";
    return items;
}

int StreetResultModel::rowCount(const QModelIndex & /*parent*/) const
{
    return streetResults.count();
}

int StreetResultModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 8;
}

QVariantMap StreetResultModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QVariant StreetResultModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > streetResults.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return streetResults[index.row()]->isChecked();
        case 1:
            return streetResults[index.row()]->streetName();
        case 2:
            return streetResults[index.row()]->isAlreadyAdded();
        case 3:
            return streetResults[index.row()]->wktGeometry();
        }
    }

    switch (role) {
    case IsCheckedRole:
        return streetResults[index.row()]->isChecked();
    case StreetNameRole:
        return streetResults[index.row()]->streetName();
    case IsAlreadyAddedRole:
        return streetResults[index.row()]->isAlreadyAdded();
    case WktGeometryRole:
        return streetResults[index.row()]->wktGeometry();
    }

    return QVariant();
}

bool StreetResultModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    StreetResult *currStreetResult = streetResults[index.row()];

    if (role == Qt::EditRole) {
        int column = index.column();

        switch (column) {
        case 0:
            currStreetResult->setIsChecked(value.toBool());
            emit dataChanged(this->index(index.row(), 0), this->index(index.row(), 0));
            break;
        case 1:
            currStreetResult->setStreetName(value.toString());
            emit dataChanged(this->index(index.row(), 0), this->index(index.row(), 1));
            break;
        case 2:
            currStreetResult->setIsAlreadyAdded(value.toBool());
            emit dataChanged(this->index(index.row(), 0), this->index(index.row(), 2));
            break;
        case 3:
            currStreetResult->setWktGeometry(value.toString());
            emit dataChanged(this->index(index.row(), 0), this->index(index.row(), 3));
            break;
        default:
            break;
        }
    } else {
        switch (role) {
        case Roles::IsCheckedRole:
            currStreetResult->setIsChecked(value.toBool());
            emit dataChanged(this->index(index.row(), 0), this->index(index.row(), 0));
            break;
        case Roles::StreetNameRole:
            currStreetResult->setStreetName(value.toString());
            emit dataChanged(this->index(index.row(), 1), this->index(index.row(), 1));
            break;
        case Roles::IsAlreadyAddedRole:
            currStreetResult->setIsAlreadyAdded(value.toBool());
            emit dataChanged(this->index(index.row(), 2), this->index(index.row(), 2));
            break;
        case Roles::WktGeometryRole:
            currStreetResult->setWktGeometry(value.toString());
            emit dataChanged(this->index(index.row(), 3), this->index(index.row(), 3));
            break;
        default:
            break;
        }
    }

    return true;
}

Qt::ItemFlags StreetResultModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

QModelIndex StreetResultModel::index(int row, int column, const QModelIndex &parent)
        const
{
    if (hasIndex(row, column, parent)) {
        return createIndex(row, column);
    }
    return QModelIndex();
}

void StreetResultModel::addStreetResult(StreetResult *streetResult)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    streetResults << streetResult;
    endInsertRows();
}

bool StreetResultModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    if (row < 0 || count < 1 || (row + count) > streetResults.size())
        return false;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    for (int i = 0; i < count; i++) {
        streetResults.removeAt(row);
    }
    endRemoveRows();
    return true;
}

StreetResult *StreetResultModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        StreetResult *item = streetResults[index.row()];
        if (item)
            return item;
    }
    return nullptr;
}

StreetResultSortFilterProxyModel::StreetResultSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *StreetResultSortFilterProxyModel::source() const
{
    return sourceModel();
}

void StreetResultSortFilterProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
}

void StreetResultSortFilterProxyModel::setFilterCongregationId(int value)
{
    m_congregationId = value;
    invalidateFilter();
}

QString StreetResultSortFilterProxyModel::filterSearchText() const
{
    return m_filterSearchText;
}

void StreetResultSortFilterProxyModel::setFilterSearchText(QString value)
{
    m_filterSearchText = value;
    invalidateFilter();
}

bool StreetResultSortFilterProxyModel::filterHideAddedStreets() const
{
    return m_filterHideAddedStreets;
}

void StreetResultSortFilterProxyModel::setFilterHideAddedStreets(bool filterHideAddedStreets)
{
    m_filterHideAddedStreets = filterHideAddedStreets;
    invalidateFilter();
}

bool StreetResultSortFilterProxyModel::filterAcceptsRow(int sourceRow,
                                                        const QModelIndex &sourceParent) const
{
    QModelIndex indexStreetName = sourceModel()->index(sourceRow, 1, sourceParent);
    QModelIndex indexIsAlreadyAdded = sourceModel()->index(sourceRow, 2, sourceParent);
    return (sourceModel()->data(indexStreetName).toString().contains(m_filterSearchText, Qt::CaseInsensitive))
            && (m_filterHideAddedStreets ? !sourceModel()->data(indexIsAlreadyAdded).toBool() : true);
}

bool StreetResultSortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    return leftData.toString() < rightData.toString();
}

int Boundary::territoryId() const
{
    return m_territoryId;
}

void Boundary::setTerritoryId(int value)
{
    m_territoryId = value;
}

QGeoPath Boundary::path() const
{
    return m_path;
}

void Boundary::setPath(const QGeoPath &value)
{
    m_path = value;
}

QVariantList Boundary::coordinates() const
{
    QVariantList coordinates;
    if (m_path.isValid())
        foreach (const QGeoCoordinate &coordinate, m_path.path())
            coordinates << QVariant::fromValue(coordinate);
    return coordinates;
}

bool Boundary::isHole() const
{
    return m_isHole;
}

void Boundary::setIsHole(bool value)
{
    m_isHole = value;
}

int Street::streetId() const
{
    return m_streetId;
}

void Street::setStreetId(int value)
{
    m_streetId = value;
}

int Street::territoryId() const
{
    return m_territoryId;
}

void Street::setTerritoryId(int value)
{
    m_territoryId = value;
}

QGeoPath Street::path() const
{
    return m_path;
}

void Street::setPath(const QGeoPath &value)
{
    m_path = value;
}

QVariantList Street::coordinates() const
{
    QVariantList coordinates;
    if (m_path.isValid())
        foreach (const QGeoCoordinate &coordinate, m_path.path())
            coordinates << QVariant::fromValue(coordinate);
    return coordinates;
}

int Street::streetTypeId() const
{
    return m_streetTypeId;
}

void Street::setStreetTypeId(int value)
{
    m_streetTypeId = value;
}

CSVSchema::CSVSchema(QObject *parent)
    : QObject(parent), m_delimiter(""), m_fields(QList<QString>())
{
}

CSVSchema::~CSVSchema()
{
}

QString CSVSchema::delimiter() const
{
    return m_delimiter;
}

void CSVSchema::setDelimiter(const QString &delimiter)
{
    m_delimiter = delimiter;
}

QList<QString> CSVSchema::fields() const
{
    return m_fields;
}

void CSVSchema::setFields(const QList<QString> &fields)
{
    m_fields = fields;
}

cterritories::cterritories(QObject *parent)
    : QObject(parent)
{
    addressRequest = new QNetworkAccessManager();
    connect(addressRequest, SIGNAL(finished(QNetworkReply *)),
            SLOT(addressRequestFinished(QNetworkReply *)));
}

cterritories::~cterritories()
{
}

DataObjectListModel *cterritories::getAddressTypes()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    int defaultlang = sql->getLanguageDefaultId();

    // list of address-types
    sql_items addressTypeItems = sql->selectSql("SELECT * FROM territory_addresstype WHERE active = 1 AND lang_id = '" + QVariant(defaultlang).toString() + "' ORDER BY addresstype_number;");
    if (addressTypeItems.size() == 0) {
        sql_item s;
        s.insert("addresstype_number", 1);
        s.insert("addresstype_name", tr("Do not call", "Territory address type"));
        s.insert("color", "#ff0000");
        s.insert("lang_id", defaultlang);
        sql->insertSql("territory_addresstype", &s, "id");
        addressTypeItems = sql->selectSql("SELECT * FROM territory_addresstype WHERE active = 1 AND lang_id = '" + QVariant(defaultlang).toString() + "' ORDER BY addresstype_number;");
    }
    DataObjectListModel *addressTypeListModel = new DataObjectListModel();
    for (unsigned int i = 0; i < addressTypeItems.size(); i++) {
        addressTypeListModel->addDataObject(DataObject(addressTypeItems[i].value("addresstype_number").toInt(),
                                                       addressTypeItems[i].value("addresstype_name").toString(),
                                                       addressTypeItems[i].value("color").toString()));
    }
    return addressTypeListModel;
}

DataObjectListModel *cterritories::getStreetTypes()
{
    sql_class *sql = &Singleton<sql_class>::Instance();

    // list of street-types
    sql_items streetTypeItems = sql->selectSql("SELECT * FROM territory_streettype WHERE active = 1 ORDER BY streettype_name;");

    DataObjectListModel *streetTypeListModel = new DataObjectListModel();
    for (unsigned int i = 0; i < streetTypeItems.size(); i++) {
        streetTypeListModel->addDataObject(DataObject(streetTypeItems[i].value("id").toInt(),
                                                      streetTypeItems[i].value("streettype_name").toString(),
                                                      streetTypeItems[i].value("color").toString()));
    }
    return streetTypeListModel;
}

/**
 * @brief getAllTerritories - Get all territories from database
 * @return - List of territories
 */
QVariantList cterritories::getAllTerritories(QVariantList territoryIds)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QVariantList list;
    QString territoryIdFilter("");
    if (territoryIds.length() > 0) {
        territoryIdFilter += " AND id IN (";
        QString sep;
        for (int i = 0; i < territoryIds.length(); i++) {
            int id = territoryIds[i].value<int>();
            territoryIdFilter += sep + QVariant(id).toString();
            sep = ",";
        }
        territoryIdFilter += ")";
    }
    sql_items allTerritories = sql->selectSql("SELECT * FROM territory WHERE active" + territoryIdFilter + " ORDER BY territory_number");
    if (!allTerritories.empty()) {
        for (unsigned int i = 0; i < allTerritories.size(); i++) {
            sql_item item = allTerritories[i];
            territory *t = new territory(item.value("id").toInt(), item.value("uuid").toString());
            t->setTerritoryNumber(item.value("territory_number").toInt());
            t->setLocality(item.value("locality").toString());
            t->setCityId(item.value("city_id").toInt());
            t->setTypeId(item.value("type_id").toInt());
            t->setPriority(item.value("priority").toInt());
            t->setWktGeometry(item.value("wkt_geometry").toString());
            t->setIsDirty(false);
            list.append(QVariant::fromValue(t)); // qVariantFromValue(reinterpret_cast<territory *>(t)));
        }
    }
    return list;
}

/**
 * @brief getTerritory - Get a territory object by name
 * @param locality - territory locality
 * @return - territory object or 0 if not exist
 */
territory *cterritories::getTerritory(QString locality)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlcommand;

    sqlcommand = QString("SELECT * FROM territory WHERE locality = '%1' AND active").arg(locality);

    sql_items ts = sql->selectSql(sqlcommand);
    if (!ts.empty()) {
        territory *t = new territory(ts[0].value("id").toInt(), ts[0].value("uuid").toString());
        t->setTerritoryNumber(ts[0].value("territory_number").toInt());
        t->setLocality(ts[0].value("locality").toString());
        t->setCityId(ts[0].value("city_id").toInt());
        t->setTypeId(ts[0].value("type_id").toInt());
        t->setPriority(ts[0].value("priority").toInt());
        t->setWktGeometry(ts[0].value("wkt_geometry").toString());
        t->setIsDirty(false);
        return t;
    } else {
        return nullptr;
    }
}

/**
 * @brief getTerritoryByNumber - Get a territory object by number
 * @param number - number of the territory
 * @return - territory object or 0 if not exist
 */
territory *cterritories::getTerritoryByNumber(int number)
{
    sql_class *sql = &Singleton<sql_class>::Instance();

    sql_items ts = sql->selectSql("SELECT * FROM territory WHERE active AND territory_number = " + QString::number(number));
    if (!ts.empty()) {
        territory *t = new territory(ts[0].value("id").toInt(), ts[0].value("uuid").toString());
        t->setTerritoryNumber(ts[0].value("territory_number").toInt());
        t->setLocality(ts[0].value("locality").toString());
        t->setCityId(ts[0].value("city_id").toInt());
        t->setTypeId(ts[0].value("type_id").toInt());
        t->setPriority(ts[0].value("priority").toInt());
        t->setWktGeometry(ts[0].value("wkt_geometry").toString());
        t->setIsDirty(false);
        return t;
    } else {
        return nullptr;
    }
}

/**
 * @brief getTerritoryById - Get a territory object by id
 * @param id - Id in database
 * @return - territory object or 0 if not exist
 */
territory *cterritories::getTerritoryById(int id)
{
    sql_class *sql = &Singleton<sql_class>::Instance();

    sql_items ts = sql->selectSql("territory", "id", QString::number(id), "");
    if (!ts.empty()) {
        territory *t = new territory(ts[0].value("id").toInt(), ts[0].value("uuid").toString());
        t->setTerritoryNumber(ts[0].value("territory_number").toInt());
        t->setLocality(ts[0].value("locality").toString());
        t->setCityId(ts[0].value("city_id").toInt());
        t->setTypeId(ts[0].value("type_id").toInt());
        t->setPriority(ts[0].value("priority").toInt());
        t->setWktGeometry(ts[0].value("wkt_geometry").toString());
        t->setIsDirty(false);
        return t;
    } else {
        return nullptr;
    }
}

/**
 * @brief removeTerritory - Remove territory from the database
 * @param id - Territory's id in the database
 * @return - success or failure
 */
bool cterritories::removeTerritory(int id)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    // deactive territory in table
    sql_item s;
    s.insert("active", 0);
    bool isTerritoryRemoved = sql->updateSql("territory", "id", QString::number(id), &s);
    if (isTerritoryRemoved) {
        sql->updateSql("territory_assignment", "territory_id", QString::number(id), &s);
        sql->updateSql("territory_address", "territory_id", QString::number(id), &s);
        sql->updateSql("territory_street", "territory_id", QString::number(id), &s);
    }
    return isTerritoryRemoved;
}

/**
 * @brief importKmlGeometry - Import territory boundaries from kml-file
 * @param fileUrl - file name
 * @param nameMatchField, descriptionMatchField - match kml and database fields
 *               0 = none
 *               1 = territory_number
 *               2 = locality
 * @return - number of imported addresses; -1 = failure
 */
int cterritories::importKmlGeometry(QUrl fileUrl, int nameMatchField, int descriptionMatchField, bool searchByDescription)
{
    if (!fileUrl.isValid())
        return -1;

    QString fileName = fileUrl.toLocalFile();

    QFile file(fileName);
    file.open(QIODevice::ReadOnly);

    QXmlQuery query;
    query.bindVariable("kmlFile", &file);
    query.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; declare variable $kmlFile external; doc($kmlFile)/kml/Document");

    QString result;
    query.evaluateTo(&result);
    file.close();

    QBuffer device;
    device.setData(result.toUtf8());
    device.open(QIODevice::ReadOnly);

    QXmlQuery boundariesQuery;
    boundariesQuery.bindVariable("inputDocument", &device);
    // Placemark-Tags
    boundariesQuery.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; doc($inputDocument)//Placemark/<Placemark>{./node()}</Placemark>");

    QXmlResultItems boundariesResult;
    boundariesQuery.evaluateTo(&boundariesResult);
    QXmlItem boundaryResult(boundariesResult.next());
    QString boundary("");
    int iPlacemark(0);

    sql_class *sql;
    sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();

    int importCount = 0;

    while (!boundaryResult.isNull()) {
        iPlacemark += 1;
        boundariesQuery.setFocus(boundariesResult.current());

        // name-Tag
        boundariesQuery.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; name/string()");
        QString nameResult;

        boundariesQuery.evaluateTo(&nameResult);
        nameResult = nameResult.trimmed();

        // description-Tag
        boundariesQuery.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; description/string()");
        QString descriptionResult;
        boundariesQuery.evaluateTo(&descriptionResult);
        descriptionResult = descriptionResult.trimmed();

        // Polygon-Tag
        boundariesQuery.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; (MultiGeometry/Polygon|Polygon)");
        QXmlResultItems polygonsResult;
        boundariesQuery.evaluateTo(&polygonsResult);
        QXmlItem polygonResult(polygonsResult.next());

        QString wktGeometry("");
        QString sep("");
        int iPolygon(0);
        while (!polygonResult.isNull()) {
            iPolygon += 1;
            wktGeometry += sep + "(";
            boundariesQuery.setFocus(polygonsResult.current());

            boundariesQuery.setQuery("node()");
            boundariesQuery.evaluateTo(&boundary);

            // outerBoundary coordinates
            boundariesQuery.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; outerBoundaryIs/LinearRing/coordinates");
            QXmlResultItems outerBoundariesResult;
            boundariesQuery.evaluateTo(&outerBoundariesResult);

            QXmlItem outerBoundaryResult(outerBoundariesResult.next());

            if (!outerBoundaryResult.isNull()) {
                boundariesQuery.setFocus(outerBoundariesResult.current());
                boundariesQuery.setQuery("node()");
                boundariesQuery.evaluateTo(&boundary);

                QStringList newGeometry;
                QStringList coordinates = boundary.split(" ");
                for (int i = 0; i < coordinates.size(); i++) {
                    QStringList values = coordinates[i].split(",");
                    if (values.size() >= 2) {
                        newGeometry << values[0].trimmed() + " " + values[1].trimmed();
                    }
                }
                wktGeometry += "(" + newGeometry.join(", ") + ")";
            }

            sep = ",";

            // innerBoundary coordinates
            boundariesQuery.setFocus(polygonsResult.current());
            boundariesQuery.setQuery("node()");
            boundariesQuery.setQuery("declare default element namespace \"http://www.opengis.net/kml/2.2\"; innerBoundaryIs/LinearRing/coordinates");
            QXmlResultItems innerBoundariesResult;
            boundariesQuery.evaluateTo(&innerBoundariesResult);

            QXmlItem innerBoundaryResult(innerBoundariesResult.next());
            int iInnerBoundary(0);
            while (!innerBoundaryResult.isNull()) {
                iInnerBoundary += 1;
                boundariesQuery.setFocus(innerBoundariesResult.current());
                boundariesQuery.setQuery("node()");
                boundariesQuery.evaluateTo(&boundary);

                QStringList newGeometry;
                QStringList coordinates = boundary.split(" ");
                for (int i = 0; i < coordinates.size(); i++) {
                    QStringList values = coordinates[i].split(",");
                    if (values.size() >= 2) {
                        newGeometry << values[0].trimmed() + " " + values[1].trimmed();
                    }
                }
                wktGeometry += sep + "(" + newGeometry.join(", ") + ")";
                sep = ",";
                innerBoundaryResult = innerBoundariesResult.next();
            }

            wktGeometry += ")";

            polygonResult = polygonsResult.next();
        }

        boundaryResult = boundariesResult.next();

        if (iPolygon > 1)
            wktGeometry = "MULTIPOLYGON (" + wktGeometry + ")";
        else
            wktGeometry = "POLYGON " + wktGeometry;

        territory *t = nullptr;
        switch (nameMatchField) {
        case 1:
            t = getTerritoryByNumber(QVariant(nameResult).toInt());
            break;
        case 2:
            t = getTerritory(nameResult);
            break;
        }

        if (searchByDescription && !t) {
            switch (descriptionMatchField) {
            case 1:
                t = getTerritoryByNumber(QVariant(descriptionResult).toInt());
                break;
            case 2:
                t = getTerritory(descriptionResult);
                break;
            }
        }
        if (!t) {
            t = new territory();
            t->setTerritoryNumber(-1);
        }
        if (t) {
            t->setWktGeometry(wktGeometry);
            switch (nameMatchField) {
            case 1:
                t->setTerritoryNumber(QVariant(nameResult).toInt());
                break;
            case 2:
                t->setLocality(nameResult);
                break;
            case 3:
                t->setRemark(nameResult);
                break;
            }
            switch (descriptionMatchField) {
            case 1:
                t->setTerritoryNumber(QVariant(descriptionResult).toInt());
                break;
            case 2:
                t->setLocality(descriptionResult);
                break;
            case 3:
                t->setRemark(descriptionResult);
                break;
            }
            if (t->save())
                importCount += 1;
        }
    }

    sql->commitTransaction();

    return importCount;
}

/**
 * @brief getTerritoryBoundaries - Loads the territory boundaries from the database and emits a boundariesLoaded signal.
 * @param territoryId
 *               0 = default; load all boundaries
 *               n = load boundaries of the given territory
 */
void cterritories::getTerritoryBoundaries(int territoryId)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlQuery = "SELECT id, wkt_geometry FROM territories WHERE wkt_geometry IS NOT NULL AND wkt_geometry <> ''";
    if (territoryId > 0) {
        sqlQuery += " AND id = " + QVariant(territoryId).toString();
        // remove the territory's old points
        QMap<int, QGeoCoordinate>::iterator i = m_allPoints.find(territoryId);
        while (i != m_allPoints.end() && i.key() == territoryId) {
            i = m_allPoints.erase(i);
        }
    } else {
        // remove all territories' old points
        m_allPoints.clear();
    }
    sql_items territories = sql->selectSql(sqlQuery);

    QVariantMap boundaryMap;
    QVariantMap allPointsMap;
    if (!territories.empty()) {
        for (unsigned int i = 0; i < territories.size(); i++) {
            const sql_item territory = territories[i];
            int territory_id = territory.value("id").toInt();
            QString input = territory.value("wkt_geometry").toString();
            QRegularExpression regExp("((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))\\s*((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))");
            QRegularExpression polygonRegex("\\(.*?\\)"); //g, polygonMatch;
            QRegularExpression multiPolygonRegex("\\(\\(.*?\\)\\)"); //g, multiPolygonMatch;
            int outerPolygonIndex = 0;
            QRegularExpressionMatchIterator iMultiPolygon = multiPolygonRegex.globalMatch(input);
            while (iMultiPolygon.hasNext()) {
                QRegularExpressionMatch multiPolygonMatch = iMultiPolygon.next();
                QString multiPolygon = multiPolygonMatch.captured(0);
                QRegularExpressionMatchIterator iPolygon = polygonRegex.globalMatch(multiPolygon);

                int innerPolygonIndex = 0;
                while (iPolygon.hasNext()) {
                    QRegularExpressionMatch polygonMatch = iPolygon.next();
                    QString polygon = polygonMatch.captured(0);

                    Boundary boundary;
                    QGeoPath geoPath;

                    int i = 0;
                    QRegularExpressionMatchIterator iMatch = regExp.globalMatch(polygon);
                    while (iMatch.hasNext()) {
                        QRegularExpressionMatch match = iMatch.next();
                        double longitude = match.captured(1).toDouble();
                        double latitude = match.captured(2).toDouble();
                        QGeoCoordinate coord(latitude, longitude);
                        geoPath.addCoordinate(coord);
                        m_allPoints.insert(territory_id, coord); // insertMulti(territory_id, coord);
                        i += 1;
                    }
                    boundary.setTerritoryId(territory_id);
                    boundary.setPath(geoPath);
                    boundary.setIsHole(innerPolygonIndex > 0);

                    // workaround for territory within boundary of another territory:
                    // boundaries are sorted in reverse order by combination of area size and polygon-index in geometry definition
                    // in order to ensure that smaller territories are on top of larger territories
                    double width = geoPath.boundingGeoRectangle().width();
                    double height = geoPath.boundingGeoRectangle().height();
                    QString sortOrder = QString::number(9999999 - width * height, 'f', 9) + "_" + QString::number(999 - innerPolygonIndex);

                    boundaryMap[sortOrder + "_" + QVariant(territory_id).toString()] = QVariant::fromValue(boundary); // qVariantFromValue(boundary);
                    innerPolygonIndex += 1;
                }
                outerPolygonIndex += 1;
            }
        }
    }

    emit boundariesLoaded(boundaryMap, territoryId);
}

/**
 * @brief setTerritoryBoundary - Set and save the boundary of a territory.
 * @param territoryId - territory id
 * @param path - geometry path
 * @param dissolve - if true old and new boundaries are dissolved, otherwise old boundary is replaced
 * @param overlappedTerritoryIds - ids of the territories which intersect the given path
 */
bool cterritories::setTerritoryBoundary(int territoryId, QVariantList path, bool dissolve, QVariantList overlappedTerritoryIds, bool annexOverlappingAreas)
{
    // convert path
    OGRSpatialReference osr;
    OGRPolygon polygon;
    OGRLinearRing poRing;
    for (int i = 0; i < path.length(); i++) {
        QGeoCoordinate c = path[i].value<QGeoCoordinate>();
        poRing.addPoint(c.longitude(), c.latitude());
    }
    poRing.closeRings();
    polygon.addRing(&poRing);
    OGRGeometry *newGeometry = polygon.getLinearGeometry();

    return setTerritoryBoundary(territoryId, newGeometry, dissolve, overlappedTerritoryIds, annexOverlappingAreas);
}

bool cterritories::setTerritoryBoundary(int territoryId, QString wktGeometry, bool dissolve, QVariantList overlappedTerritoryIds, bool annexOverlappingAreas)
{
    // convert path
    OGRSpatialReference osr;
    OGRGeometry *newGeometry = nullptr;
    QByteArray bytes = wktGeometry.toUtf8();
    const char *data = bytes.constData();
    OGRErr err = OGRGeometryFactory::createFromWkt(data, &osr, &newGeometry);
    if (err != OGRERR_NONE) {
        // process error, like emit signal
        return false;
    } else
        return setTerritoryBoundary(territoryId, newGeometry, dissolve, overlappedTerritoryIds, annexOverlappingAreas);
}

bool cterritories::setTerritoryBoundary(int territoryId, OGRGeometry *newGeometry, bool dissolve, QVariantList overlappedTerritoryIds, bool annexOverlappingAreas)
{
    territory *t = nullptr;
    t = getTerritoryById(territoryId);
    if (t) {
        OGRSpatialReference osr;
        OGRPolygon polygon;

        if (dissolve) {
            OGRGeometry *oldTerritoryGeometry = t->getOGRGeometry();
            OGRGeometry *dissolvedTerritoryGeometry = nullptr;

            if (oldTerritoryGeometry == nullptr || oldTerritoryGeometry->IsEmpty())
                dissolvedTerritoryGeometry = newGeometry->clone();
            else {
                if (!oldTerritoryGeometry->IsValid()) {
                    // TODO: try to repair the geometry (self-intersecting polygon, ...)
                    // wait until GDAL 3.0 and GEOS 3.8 is available on all systems
                    //                    oldTerritoryGeometry = oldTerritoryGeometry->MakeValid();
                    //                    if (oldTerritoryGeometry != nullptr)
                    //                        oldTerritoryGeometry = oldTerritoryGeometry->Buffer(0.0);
                    return false;
                }
                if (oldTerritoryGeometry != nullptr && oldTerritoryGeometry->IsValid())
                    dissolvedTerritoryGeometry = oldTerritoryGeometry->Union(newGeometry);
                else
                    return false;
            }
            if (dissolvedTerritoryGeometry != nullptr)
                newGeometry = dissolvedTerritoryGeometry;
            else
                return false;
        }

        if (overlappedTerritoryIds.length() > 0) {
            // remove overlapping areas
            OGRGeometry *tmpTerritoryGeometry = newGeometry->clone();
            QVariantList overlappedTerritories = getAllTerritories(overlappedTerritoryIds);
            for (int i = 0; i < overlappedTerritories.length(); i++) {
                territory *overlappedTerritory = overlappedTerritories[i].value<territory *>();
                if (overlappedTerritory->territoryId() != territoryId) {
                    OGRGeometry *overlappedTerritoryGeometry = overlappedTerritory->getOGRGeometry();
                    if (overlappedTerritoryGeometry != nullptr && !overlappedTerritoryGeometry->IsEmpty())
                        tmpTerritoryGeometry = tmpTerritoryGeometry->Difference(overlappedTerritoryGeometry);
                }
            }

            if (annexOverlappingAreas) {
                // save new boundary before annexing overlapping areas
                qDebug() << "Save new boundary of territory" << t->territoryNumber() << "before annexing overlapping areas.";
                if (!t->setOGRGeometry(tmpTerritoryGeometry))
                    return false;
                if (t->isDirty())
                    t->save();

                // annex overlaping areas
                sql_class *sql = &Singleton<sql_class>::Instance();
                sql->startTransaction();
                bool successfullyAnnexed = true;
                for (int i = 0; i < overlappedTerritories.length(); i++) {
                    territory *overlappedTerritory = overlappedTerritories[i].value<territory *>();
                    if (overlappedTerritory->territoryId() != territoryId) {
                        successfullyAnnexed = annexArea(territoryId, overlappedTerritory->territoryId(), newGeometry);
                    }
                    if (!successfullyAnnexed)
                        break;
                }
                if (!successfullyAnnexed)
                    sql->rollbackTransaction();
                else
                    sql->commitTransaction();

                newGeometry = t->getOGRGeometry();
            } else {
                newGeometry = tmpTerritoryGeometry;
            }
        }

        // save new boundary
        if (t->setOGRGeometry(newGeometry)) {
            if (t->isDirty())
                t->save();
        }

        // add streets and addresses of territories without boundary if they are within the current territory's area
        sql_class *sql = &Singleton<sql_class>::Instance();
        QString sqlQuery = "SELECT * FROM territory WHERE wkt_geometry IS NULL AND active";
        sql_items territoryWithoutBoundaryRows = sql->selectSql(sqlQuery);
        if (!territoryWithoutBoundaryRows.empty()) {
            for (unsigned int i = 0; i < territoryWithoutBoundaryRows.size(); i++) {
                sql_item territoryWithoutBoundaryRow = territoryWithoutBoundaryRows[i];
                moveStreetsToTerritory(territoryWithoutBoundaryRow.value("id").toInt(), territoryId);
                moveAddressesToTerritory(territoryWithoutBoundaryRow.value("id").toInt(), territoryId);
            }
        }
        return true;
    }
    return false;
}

/**
 * @brief splitTerritory - Split territory using a polygon which defines the area that is cut off and created as a new territory
 * @param territoryId - territory id
 * @param cutAreaPath - polygon path of the cut area
 * @return - id of the newly created territory
 */
int cterritories::splitTerritory(int territoryId, QVariantList cutAreaPath)
{
    int newTerritoryId = 0;
    territory *t = nullptr;
    t = getTerritoryById(territoryId);
    if (!t)
        return 0;

    OGRSpatialReference osr;

    // convert polygon path
    OGRLinearRing poRing;
    for (int i = 0; i < cutAreaPath.length(); i++) {
        QGeoCoordinate c = cutAreaPath[i].value<QGeoCoordinate>();
        poRing.addPoint(c.longitude(), c.latitude());
    }
    poRing.closeRings();
    OGRPolygon cutAreaPolygon;
    OGRGeometry *cutAreaGeometry = nullptr;
    cutAreaPolygon.addRing(&poRing);
    cutAreaGeometry = cutAreaPolygon.getLinearGeometry();

    OGRGeometry *territoryGeometry = t->getOGRGeometry();
    if (territoryGeometry == nullptr)
        return 0;

    // create new territory and annex intersection with cut area
    OGRGeometry *insideCutAreaGeometry = nullptr;
    insideCutAreaGeometry = territoryGeometry->Intersection(cutAreaGeometry);
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();
    if (annexArea(newTerritoryId, territoryId, insideCutAreaGeometry)) {
        sql->commitTransaction();
        return newTerritoryId;
    } else {
        sql->rollbackTransaction();
        return 0;
    }
}

QString cterritories::getCongregationAddress() const
{
    ccongregation c;
    ccongregation::congregation myCongregation;
    myCongregation = c.getMyCongregation();
    return myCongregation.address;
}

void cterritories::geocodeAddress(QString address)
{
    QNetworkRequest request;
    request.setUrl(getGeocodeUrl(address));
    addressRequest->get(request);
}

void cterritories::addressRequestFinished(QNetworkReply *reply)
{
    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    if (reply->error() != QNetworkReply::NoError) {
        reply->close();
        emit geocodeError(reply->errorString());
        return;
    }

    QList<GeocodeResult *> geocodeResultList = getGeocodeResults(doc);
    QVariantList geocodeVariantList;
    for (int i = 0; i < geocodeResultList.length(); i++) {
        //geocodeVariantList.append(qVariantFromValue(reinterpret_cast<GeocodeResult *>(geocodeResultList[i])));
        geocodeVariantList.append(QVariant::fromValue(geocodeResultList[i]));
    }

    emit geocodeFinished(geocodeVariantList);
}

void cterritories::streetRequestFinished(QNetworkReply *reply)
{
    int territoryId = reply->property("territoryId").toInt();
    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    if (reply->error() != QNetworkReply::NoError) {
        reply->close();
        emit streetRequestFailed(reply->errorString());
        return;
    }

    QList<StreetResult *> streetResultList = getStreetResults(doc, territoryId);
    QVariantList streetVariantList;
    for (int i = 0; i < streetResultList.length(); i++) {
        //streetVariantList.append(qVariantFromValue(reinterpret_cast<StreetResult *>(streetResultList[i])));
        streetVariantList.append(QVariant::fromValue(streetResultList[i]));
    }

    emit streetListReceived(streetVariantList);
}

QUrl cterritories::getGeocodeUrl(QString address)
{
    QSettings settings;
    int service = settings.value("geo_service_provider/default", 0).toInt();
    switch (service) {
    case 1: {
        // Google geocoding service
        QString googleAPIkey = settings.value("geo_service_provider/google_api_key", "").toString();
        googleAPIkey = googleAPIkey.isEmpty() ? "" : "&key=" + googleAPIkey;
        return QUrl("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false" + googleAPIkey);
    }
    case 2: {
        // Here geocoding service
        QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
        QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
        hereAppId = hereAppId.isEmpty() ? "" : "&app_id=" + hereAppId;
        hereAppCode = hereAppCode.isEmpty() ? "" : "&app_code=" + hereAppCode;
        return QUrl("https://geocoder.api.here.com/6.2/geocode.json?&searchtext=" + address + hereAppId + hereAppCode);
    }
    default:
        // OSM geocoding service
        return QUrl("https://nominatim.openstreetmap.org/?format=json&addressdetails=1&q=" + address);
    }
}

QList<GeocodeResult *> cterritories::getGeocodeResults(QJsonDocument &doc)
{
    QList<GeocodeResult *> geocodeResultList;

    QSettings settings;
    int service = settings.value("geo_service_provider/default", 0).toInt();
    switch (service) {
    case 1: {
        // Google geocoding service
        if (doc.isObject()) {
            QJsonObject obj = doc.object();

            QString status = obj.value(QStringLiteral("status")).toString();
            if (status == "OK") {
                QJsonArray results = obj.value(QStringLiteral("results")).toArray();

                for (int iResult = 0; iResult < results.size(); iResult++) {
                    GeocodeResult *geocodeResult = new GeocodeResult(this);
                    QJsonObject result = results.at(iResult).toObject();
                    QJsonArray addressComponents = result.value(QStringLiteral("address_components")).toArray();

                    for (int iAddressComp = 0; iAddressComp < addressComponents.size(); iAddressComp++) {
                        QJsonObject addressComponent = addressComponents.at(iAddressComp).toObject();
                        QJsonArray addressTypes = addressComponent.value(QStringLiteral("types")).toArray();
                        QJsonValue addressType("country");
                        if (addressTypes.contains(addressType))
                            geocodeResult->setCountry(addressComponent.value(QStringLiteral("short_name")).toString());

                        addressType = "administrative_area_level_1";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setState(addressComponent.value(QStringLiteral("short_name")).toString());

                        addressType = "administrative_area_level_2";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setCounty(addressComponent.value(QStringLiteral("long_name")).toString());

                        addressType = "locality";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setCity(addressComponent.value(QStringLiteral("long_name")).toString());

                        addressType = "sublocality";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setDistrict(addressComponent.value(QStringLiteral("long_name")).toString());

                        addressType = "route";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setStreet(addressComponent.value(QStringLiteral("long_name")).toString());

                        addressType = "street_number";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setHouseNumber(addressComponent.value(QStringLiteral("long_name")).toString());

                        addressType = "postal_code";
                        if (addressTypes.contains(addressType))
                            geocodeResult->setPostalCode(addressComponent.value(QStringLiteral("long_name")).toString());
                    }

                    QString formattedAddress = result.value(QStringLiteral("formatted_address")).toString();
                    geocodeResult->setText(formattedAddress);

                    QJsonObject geometry = result.value(QStringLiteral("geometry")).toObject();
                    QJsonObject location = geometry.value(QStringLiteral("location")).toObject();
                    double lat = location.value(QStringLiteral("lat")).toDouble();
                    geocodeResult->setLatitude(lat);
                    double lon = location.value(QStringLiteral("lng")).toDouble();
                    geocodeResult->setLongitude(lon);

                    geocodeResultList.append(geocodeResult);
                }
            } else {
                QString errorMessage = obj.value(QStringLiteral("error_message")).toString();
                emit geocodeError(errorMessage);
            }
        }
        break;
    }
    case 2: {
        // Here geocoding service
        if (doc.isObject()) {
            QJsonObject obj = doc.object();
            QJsonObject response = obj.value(QStringLiteral("Response")).toObject();
            QJsonArray views = response.value(QStringLiteral("View")).toArray();
            for (int iView = 0; iView < views.size(); iView++) {
                QJsonObject view = views.at(iView).toObject();
                QJsonArray results = view.value(QStringLiteral("Result")).toArray();
                for (int iResult = 0; iResult < results.size(); iResult++) {
                    GeocodeResult *geocodeResult = new GeocodeResult(this);

                    QJsonObject result = results.at(iResult).toObject();
                    QJsonObject location = result.value(QStringLiteral("Location")).toObject();

                    QJsonObject address = location.value(QStringLiteral("Address")).toObject();
                    geocodeResult->setCountry(address.value(QStringLiteral("Country")).toString());
                    geocodeResult->setState(address.value(QStringLiteral("State")).toString());
                    geocodeResult->setCounty(address.value(QStringLiteral("County")).toString());
                    geocodeResult->setCity(address.value(QStringLiteral("City")).toString());
                    geocodeResult->setDistrict(address.value(QStringLiteral("District")).toString());
                    geocodeResult->setStreet(address.value(QStringLiteral("Street")).toString());
                    geocodeResult->setHouseNumber(address.value(QStringLiteral("HouseNumber")).toString());
                    geocodeResult->setPostalCode(address.value(QStringLiteral("PostalCode")).toString());

                    QString formattedAddress = address.value(QStringLiteral("Label")).toString();
                    geocodeResult->setText(formattedAddress);

                    QJsonObject displayPosition = location.value(QStringLiteral("DisplayPosition")).toObject();
                    double lat = displayPosition.value(QStringLiteral("Latitude")).toDouble();
                    geocodeResult->setLatitude(lat);
                    double lon = displayPosition.value(QStringLiteral("Longitude")).toDouble();
                    geocodeResult->setLongitude(lon);
                    geocodeResultList.append(geocodeResult);
                }
            }
        }
        break;
    }
    default:
        // OSM geocoding service
        if (doc.isArray()) {
            QJsonArray results = doc.array();
            for (int iResult = 0; iResult < results.size(); iResult++) {
                GeocodeResult *geocodeResult = new GeocodeResult(this);
                QJsonObject result = results.at(iResult).toObject();

                QJsonObject address = result.value(QStringLiteral("address")).toObject();
                geocodeResult->setCountry(address.value(QStringLiteral("country_code")).toString());
                geocodeResult->setState(address.value(QStringLiteral("state")).toString());
                geocodeResult->setCounty(address.value(QStringLiteral("county")).toString());
                geocodeResult->setCity(address.value(QStringLiteral("city")).toString());
                geocodeResult->setDistrict(address.value(QStringLiteral("suburb")).toString());
                geocodeResult->setStreet(address.value(QStringLiteral("road")).toString());
                geocodeResult->setHouseNumber(address.value(QStringLiteral("house_number")).toString());
                geocodeResult->setPostalCode(address.value(QStringLiteral("postcode")).toString());

                QString formattedAddress = result.value(QStringLiteral("display_name")).toString();
                geocodeResult->setText(formattedAddress);

                double lat = result.value(QStringLiteral("lat")).toString().toDouble();
                geocodeResult->setLatitude(lat);
                double lon = result.value(QStringLiteral("lon")).toString().toDouble();
                geocodeResult->setLongitude(lon);

                geocodeResultList.append(geocodeResult);
            }
        }
        break;
    }
    return geocodeResultList;
}

QList<StreetResult *> cterritories::getStreetResults(QJsonDocument &doc, int territoryId)
{
    QList<StreetResult *> streetResultList;
    territory *t = getTerritoryById(territoryId);
    if (!t)
        return streetResultList;
    // Parse WKT boundary of the territory for intersection
    QString wktGeometry = t->wktGeometry();
    OGRSpatialReference osr;
    OGRGeometry *territoryGeometry = nullptr;
    QByteArray bytes = wktGeometry.toUtf8();
    const char *data = bytes.constData();
    OGRErr err = OGRGeometryFactory::createFromWkt(data, &osr, &territoryGeometry);
    if (err != OGRERR_NONE) {
        // process error, like emit signal
        return streetResultList;
    }

    // OSM overpass service
    if (doc.isObject()) {
        QJsonObject obj = doc.object();
        QJsonArray elements = obj.value(QStringLiteral("elements")).toArray();

        // create a list of already added streets for checking if new streets are already added
        QList<QString> territoryStreetList;
        sql_class *sql = &Singleton<sql_class>::Instance();
        QString sqlQuery = "SELECT street_name FROM territorystreets WHERE territory_id = "
                + QVariant(t->territoryId()).toString()
                + " GROUP BY street_name";
        sql_items streets = sql->selectSql(sqlQuery);
        foreach (sql_item street, streets) {
            territoryStreetList.append(street.value("street_name").toString().toLower());
        }

        for (int iElement = 0; iElement < elements.size(); iElement++) {
            QJsonObject element = elements.at(iElement).toObject();
            QJsonObject tags = element.value(QStringLiteral("tags")).toObject();
            QString streetName = tags.value(QStringLiteral("name")).toString();

            // create linestring from current geometry
            QJsonArray points = element.value(QStringLiteral("geometry")).toArray();
            OGRLineString *lineString = new OGRLineString();
            for (int iPoint = 0; iPoint < points.size(); iPoint++) {
                QJsonObject point = points.at(iPoint).toObject();
                lineString->addPoint(point.value(QStringLiteral("lon")).toDouble(),
                                     point.value(QStringLiteral("lat")).toDouble());
            }
            StreetResult *streetResult = nullptr;
            // search for previously added street geometry part
            for (int iStreet = 0; iStreet < streetResultList.size(); iStreet++) {
                if (streetResultList[iStreet]->streetName() == streetName) {
                    streetResult = streetResultList[iStreet];
                    break;
                }
            }
            char *wkt_new = nullptr;
            OGRGeometry *streetGeometry = nullptr;
            streetGeometry = territoryGeometry->Intersection(lineString);
            if (streetGeometry && !streetGeometry->IsEmpty()) {
                if (!streetResult) {
                    // set street geometry
                    streetResult = new StreetResult(this);
                    streetResult->setStreetName(streetName);
                    streetGeometry->exportToWkt(&wkt_new);
                    streetResult->setWktGeometry(QString(wkt_new));
                    streetResult->setIsAlreadyAdded(territoryStreetList.contains(streetName.toLower()));
                    streetResultList.append(streetResult);
                } else {
                    // append street geometry to previously added
                    OGRMultiLineString *multiLineString = new OGRMultiLineString();
                    QByteArray bytes = streetResult->wktGeometry().toUtf8();
                    const char *data = bytes.constData();
                    multiLineString->importFromWkt(&data);
                    multiLineString->addGeometry(streetGeometry);
                    multiLineString->exportToWkt(&wkt_new);
                    streetResult->setWktGeometry(QString(wkt_new));
                }
            }
        }
    }
    std::sort(streetResultList.begin(), streetResultList.end(),
              [](const StreetResult *a, const StreetResult *b) {
                  return QString::localeAwareCompare(a->streetName(), b->streetName()) < 0;
              });
    return streetResultList;
}

/**
 * @brief annexArea - Annex area of another territory
 * @param territoryId - territory into which area is annexed; if id is 0, a new territory is created
 * @param fromTerritoryId - disconnect area from given territory
 * @param areaGeometry - polygon path of the cut area
 * @return - success
 */
bool cterritories::annexArea(int &intoTerritoryId, int fromTerritoryId, OGRGeometry *areaGeometry)
{
    territory *fromTerritory = nullptr;
    fromTerritory = getTerritoryById(fromTerritoryId);
    if (!fromTerritory || areaGeometry == nullptr || !areaGeometry->IsValid())
        return false;

    OGRSpatialReference osr;
    OGRGeometry *fromTerritoryGeometry = fromTerritory->getOGRGeometry();
    OGRGeometry *intoTerritoryGeometry = nullptr;

    sql_class *sql = &Singleton<sql_class>::Instance();

    // cut and update territory's boundary
    OGRGeometry *outsideAreaGeometry = nullptr;
    outsideAreaGeometry = fromTerritoryGeometry->Difference(areaGeometry);
    qDebug() << "Cut and update boundary of territory" << fromTerritory->territoryNumber();
    if (fromTerritory->setOGRGeometry(outsideAreaGeometry))
        fromTerritory->save();

    // get intersecting area which will be annexed
    OGRGeometry *insideAreaGeometry = nullptr;
    insideAreaGeometry = fromTerritoryGeometry->Intersection(areaGeometry);
    if (insideAreaGeometry->IsEmpty()) {
        qDebug() << "Intersecting area with territory" << fromTerritory->territoryNumber() << "is empty!";
        return false;
    }
    if (insideAreaGeometry->IsValid()) {
        switch (insideAreaGeometry->getGeometryType()) {
        case wkbPolygon:
            break;
        case wkbMultiPolygon:
            insideAreaGeometry = insideAreaGeometry->UnionCascaded();
            break;
        case wkbGeometryCollection: {
            qDebug() << "Clean up geometry collection of intersection with territory" << fromTerritory->territoryNumber();
            OGRGeometryCollection *poColl = insideAreaGeometry->toGeometryCollection();
            OGRGeometry *validGeometry = nullptr;
            for (auto &&poSubGeom : *poColl) {
                switch (poSubGeom->getGeometryType()) {
                case wkbPolygon:
                    if (validGeometry == nullptr)
                        validGeometry = poSubGeom;
                    else
                        validGeometry = validGeometry->Union(poSubGeom);
                    break;
                case wkbMultiPolygon:
                    if (validGeometry == nullptr)
                        validGeometry = poSubGeom;
                    else
                        validGeometry = validGeometry->Union(poSubGeom);
                    break;
                case wkbLineString:
                    break;
                default:
                    qDebug() << "Subgeometry type is" << poSubGeom->getGeometryName();
                    break;
                }
            }

            if (validGeometry->getGeometryType() == wkbMultiPolygon)
                insideAreaGeometry = validGeometry->UnionCascaded();
            else {
                insideAreaGeometry = validGeometry;
            }
            insideAreaGeometry = insideAreaGeometry->Buffer(0.0);
            if (!insideAreaGeometry->IsValid()) {
                qDebug() << "Cleaned up intersection is invalid.";
                return false;
            }
            break;
        }
        default:
            return false;
        }
    } else {
        qDebug() << "Intersecting area with territory" << fromTerritory->territoryNumber() << "is invalid!";
        return false;
    }

    if (insideAreaGeometry != nullptr) {
        territory *intoTerritory = nullptr;
        QString sqlQuery;
        if (intoTerritoryId != 0) {
            // annex into existing territory
            intoTerritory = getTerritoryById(intoTerritoryId);
            if (!intoTerritory)
                return false;
            else {
                intoTerritoryGeometry = intoTerritory->getOGRGeometry();
                if (intoTerritoryGeometry != nullptr) {
                    if (intoTerritoryGeometry->IsEmpty())
                        intoTerritoryGeometry = insideAreaGeometry->clone();
                    else
                        intoTerritoryGeometry = intoTerritoryGeometry->Union(insideAreaGeometry);
                    if (intoTerritoryGeometry == nullptr) {
                        qDebug() << "Union of area with territory" << fromTerritory->territoryNumber() << "is null!";
                        return false;
                    }
                    if (intoTerritoryGeometry->IsEmpty()) {
                        qDebug() << "Union of area with territory" << fromTerritory->territoryNumber() << "is empty!";
                        return false;
                    }
                    if (!intoTerritoryGeometry->IsValid()) {
                        qDebug() << "Union of area with territory" << fromTerritory->territoryNumber() << "is invalid!";
                        return false;
                    }
                    if (intoTerritoryGeometry->getGeometryType() == wkbMultiPolygon)
                        intoTerritoryGeometry = intoTerritoryGeometry->UnionCascaded();
                } else
                    intoTerritoryGeometry = areaGeometry;
                qDebug() << "Annex into existing territory" << intoTerritory->territoryNumber();
                if (intoTerritory->setOGRGeometry(intoTerritoryGeometry))
                    intoTerritory->save();
                else
                    return false;
            }
        } else {
            // annex into new territory
            intoTerritory = new territory();
            intoTerritory->setTerritoryNumber(-1);
            intoTerritory->setLocality(fromTerritory->locality());
            intoTerritory->setCityId(fromTerritory->cityId());
            intoTerritory->setTypeId(fromTerritory->typeId());
            intoTerritoryGeometry = insideAreaGeometry;
            qDebug() << "Annex into new territory.";
            if (intoTerritory->setOGRGeometry(intoTerritoryGeometry)) {
                intoTerritory->save();
                intoTerritoryId = intoTerritory->territoryId();

                // copy assignments
                sqlQuery = "SELECT * FROM territory_assignment WHERE territory_id = " + QVariant(fromTerritoryId).toString() + " AND active ORDER BY checkedout_date";
                sql_items territoryAssignmentRows = sql->selectSql(sqlQuery);
                if (!territoryAssignmentRows.empty()) {
                    for (unsigned int i = 0; i < territoryAssignmentRows.size(); i++) {
                        sql_item assignmentRow = territoryAssignmentRows[i];
                        TerritoryAssignment *newTerritoryAssignment = new TerritoryAssignment(intoTerritory->territoryId());
                        newTerritoryAssignment->setPersonId(assignmentRow.value("person_id").toInt());
                        newTerritoryAssignment->setCheckedOutDate(assignmentRow.value("checkedout_date").toDate());
                        newTerritoryAssignment->setCheckedBackInDate(assignmentRow.value("checkedbackin_date").toDate());
                        newTerritoryAssignment->save();
                    }
                }
            } else
                return false;
        }

        // move street (or parts) within area to the territory
        moveStreetsToTerritory(fromTerritoryId, intoTerritoryId);

        // move addresses within the area to the territory
        moveAddressesToTerritory(fromTerritoryId, intoTerritoryId);
    }
    return true;
}

void cterritories::moveStreetsToTerritory(int fromTerritoryId, int intoTerritoryId)
{
    OGRSpatialReference osr;
    sql_class *sql = &Singleton<sql_class>::Instance();
    territory *intoTerritory = getTerritoryById(intoTerritoryId);
    OGRGeometry *intoTerritoryGeometry = nullptr;
    intoTerritoryGeometry = intoTerritory->getOGRGeometry();

    // move street (or parts) within area to the territory
    QString sqlQuery = "SELECT * FROM territory_street WHERE territory_id = " + QVariant(fromTerritoryId).toString() + " AND active ORDER BY street_name";
    sql_items territoryStreetRows = sql->selectSql(sqlQuery);
    if (!territoryStreetRows.empty()) {
        for (unsigned int i = 0; i < territoryStreetRows.size(); i++) {
            sql_item streetRow = territoryStreetRows[i];
            // parse WKT boundary of the street and check for intersection with territory's boundary
            QString wktGeometry = streetRow.value("wkt_geometry").toString();
            OGRGeometry *streetGeometry = nullptr;
            QByteArray bytes = wktGeometry.toUtf8();
            const char *data = bytes.constData();
            OGRErr err = OGRGeometryFactory::createFromWkt(data, &osr, &streetGeometry);
            if (err == OGRERR_NONE) {
                if (intoTerritoryGeometry->Intersects(streetGeometry)) {
                    OGRGeometry *insideAreaStreetGeometry = nullptr;
                    insideAreaStreetGeometry = intoTerritoryGeometry->Intersection(streetGeometry);
                    char *wkt_new = nullptr;
                    // check for already existing street
                    sqlQuery = QString("SELECT * FROM territory_street WHERE territory_id = %1 AND street_name = '%2' AND from_number = '%3' AND to_number = '%4' AND quantity = %5 AND streettype_id = %6 AND active ORDER BY id")
                                       .arg(QVariant(intoTerritory->territoryId()).toString())
                                       .arg(streetRow.value("street_name").toString().replace("\'", "\'\'"))
                                       .arg(streetRow.value("from_number").toString())
                                       .arg(streetRow.value("to_number").toString())
                                       .arg(streetRow.value("quantity").toString())
                                       .arg(streetRow.value("streettype_id").toString());
                    sql_items existingTerritoryStreetRows = sql->selectSql(sqlQuery);
                    if (existingTerritoryStreetRows.size() > 0) {
                        // add geometry to existing street
                        wktGeometry = existingTerritoryStreetRows[0].value("wkt_geometry").toString();
                        OGRGeometry *existingStreetGeometry = nullptr;
                        bytes = wktGeometry.toUtf8();
                        data = bytes.constData();
                        OGRErr err = OGRGeometryFactory::createFromWkt(data, &osr, &existingStreetGeometry);
                        if (err == OGRERR_NONE) {
                            insideAreaStreetGeometry = existingStreetGeometry->Union(insideAreaStreetGeometry);
                            insideAreaStreetGeometry = OGRGeometryFactory::forceToLineString(insideAreaStreetGeometry);
                            insideAreaStreetGeometry->exportToWkt(&wkt_new);

                            sql_item existingStreetItem;
                            int existingStreetId = existingTerritoryStreetRows[0].value("id").toInt();
                            existingStreetItem.insert(":id", existingStreetId);
                            existingStreetItem.insert(":wkt_geometry", wkt_new);
                            if (sql->execSql("UPDATE territory_street SET wkt_geometry = :wkt_geometry WHERE id = :id", &existingStreetItem, true)) {
                            }
                        }
                    } else {
                        // add new street
                        insideAreaStreetGeometry->exportToWkt(&wkt_new);
                        TerritoryStreet *newTerritoryStreet = new TerritoryStreet(
                                intoTerritory->territoryId(),
                                streetRow.value("street_name").toString(),
                                streetRow.value("from_number").toString(),
                                streetRow.value("to_number").toString(),
                                streetRow.value("quantity").toInt(),
                                streetRow.value("streettype_id").toInt(),
                                QString(wkt_new));
                        newTerritoryStreet->save();
                    }

                    OGRGeometry *outsideAreaStreetGeometry = nullptr;
                    outsideAreaStreetGeometry = streetGeometry->Difference(insideAreaStreetGeometry);
                    outsideAreaStreetGeometry = outsideAreaStreetGeometry->Buffer(-0.001);
                    outsideAreaStreetGeometry->exportToWkt(&wkt_new);

                    sql_item s;
                    int streetId = streetRow.value("id").toInt();
                    s.insert(":id", streetId);
                    s.insert(":wkt_geometry", wkt_new);
                    s.insert(":active", !outsideAreaStreetGeometry->IsEmpty());
                    if (sql->execSql("UPDATE territory_street SET wkt_geometry = :wkt_geometry, active = :active WHERE id = :id", &s, true)) {
                    }
                }
            }
        }
    }
}

void cterritories::moveAddressesToTerritory(int fromTerritoryId, int intoTerritoryId)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    territory *intoTerritory = getTerritoryById(intoTerritoryId);
    OGRGeometry *intoTerritoryGeometry = nullptr;
    intoTerritoryGeometry = intoTerritory->getOGRGeometry();

    // move addresses within the area to the territory
    QString sqlQuery = "SELECT * FROM territory_address WHERE territory_id = " + QVariant(fromTerritoryId).toString() + " AND active ORDER BY street, housenumber";
    sql_items territoryAddressRows = sql->selectSql(sqlQuery);
    if (!territoryAddressRows.empty()) {
        for (unsigned int i = 0; i < territoryAddressRows.size(); i++) {
            sql_item addressRow = territoryAddressRows[i];

            QRegularExpression regExp("((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))\\s*((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))");
            QRegularExpressionMatch match = regExp.match(addressRow.value("wkt_geometry").toString());
            if (match.hasMatch()) {
                double lon = match.captured(1).toDouble();
                double lat = match.captured(2).toDouble();
                OGRPoint *point = new OGRPoint(lon, lat);

                if (point->Within(intoTerritoryGeometry)) {
                    sql_item s;
                    int addressId = addressRow.value("id").toInt();
                    s.insert(":id", addressId);
                    s.insert(":territory_id", intoTerritory->territoryId());
                    if (sql->execSql("UPDATE territory_address SET territory_id = :territory_id WHERE id = :id", &s, true)) {
                    }
                }
            }
        }
    }
}

CSVSchema *cterritories::getCSVschema(QUrl fileUrl, QString delimiter)
{
    CSVSchema *schema = new CSVSchema();

    QFile csvFile(fileUrl.toLocalFile());
    if (!csvFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << fileUrl.toLocalFile();
        return schema;
    }
    QString headerLine;
    QTextStream in(&csvFile);
    if (!in.atEnd()) {
        headerLine = in.readLine();
    }
    csvFile.close();

    if (!headerLine.isEmpty()) {
        QList<QString> delimiters = { ",", "\t", ";", "|", ":" };
        if (delimiter != "")
            delimiters = { delimiter };
        for (int i = 0; i < delimiters.length(); i++) {
            QList<QString> fields = headerLine.split(delimiters[i]);
            if (fields.count() > 1) {
                schema->setDelimiter(delimiters[i]);
                schema->setFields(fields);
                return schema;
            }
        }
    }

    return schema;
}

/**
 * @brief importAddresses - Import territory addresses from csv-file
 * @param fileUrl - file name
 * @param addressField, nameField - field index in csv file
 * @param delimiter - csv delimiter
 * @param territoryId - territory to import into
 * @param addressTypeId - type of addresses
 * @param failedFileUrl - output filename for failed addresses
 * @return - number of imported addresses; negative number = failure
 */
int cterritories::importAddresses(QUrl fileUrl, int addressField, int nameField,
                                  QString delimiter, int territoryId, int addressTypeNumber, QUrl failedFileUrl)
{
    if (territoryId < 1)
        return -2;

    QFile csvFile(fileUrl.toLocalFile());
    if (!csvFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return -1;

    // skip header
    QString header;
    if (!csvFile.atEnd())
        header = csvFile.readLine();

    sql_class *sql;
    sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();

    int rowCount = 0;
    int importCount = 0;

    QString failedAddresses;

    while (!csvFile.atEnd()) {
        rowCount += 1;
        QString line = csvFile.readLine();
        QStringList values = line.split(delimiter);
        if (addressField >= values.size())
            break;
        QString address = values[addressField];
        QString addressName = values[nameField].trimmed();

        bool importFailed = false;
        for (int i = 0; i < 5; i++) {
            QNetworkAccessManager manager;
            QEventLoop q;
            QTimer tT;

            tT.setSingleShot(true);
            connect(&tT, SIGNAL(timeout()), &q, SLOT(quit()));
            connect(&manager, SIGNAL(finished(QNetworkReply *)),
                    &q, SLOT(quit()));

            QNetworkRequest request;
            request.setUrl(getGeocodeUrl(address));
            QNetworkReply *reply = manager.get(request);

            tT.start(5000); // 5s timeout
            q.exec();

            if (tT.isActive()) {
                // download complete
                tT.stop();

                QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
                QList<GeocodeResult *> geocodeResultList = getGeocodeResults(doc);

                if (geocodeResultList.length() == 1) {
                    TerritoryAddress *ta =
                            new TerritoryAddress(territoryId,
                                                 geocodeResultList[0]->country(),
                                                 geocodeResultList[0]->state(),
                                                 geocodeResultList[0]->county(),
                                                 geocodeResultList[0]->city(),
                                                 geocodeResultList[0]->district(),
                                                 geocodeResultList[0]->street(),
                                                 geocodeResultList[0]->houseNumber(),
                                                 geocodeResultList[0]->postalCode(),
                                                 geocodeResultList[0]->wktGeometry());

                    if (addressName.startsWith("\""))
                        addressName.remove(0, 1);
                    if (addressName.endsWith("\""))
                        addressName.remove(addressName.size() - 1, 1);
                    ta->setName(addressName);
                    ta->setAddressTypeNumber(addressTypeNumber);

                    if (ta->save()) {
                        importFailed = false;
                        importCount += 1;
                        break;
                    }
                } else
                    importFailed = true;

            } else {
                // timeout
                importFailed = true;
            }
        }

        if (importFailed)
            failedAddresses += line;

        emit addressImportProgressChanged(rowCount, importCount);
    }

    sql->commitTransaction();

    // output failed addresses
    if (!failedAddresses.isEmpty() && failedFileUrl.isValid()) {
        QFile outputfile(failedFileUrl.toLocalFile());
        if (QFile::exists(failedFileUrl.toLocalFile()))
            QFile::remove(failedFileUrl.toLocalFile());
        if (!outputfile.open(QIODevice::WriteOnly | QIODevice::Text))
            QMessageBox::warning(nullptr,
                                 tr("Save failed addresses", "Territory address import"),
                                 tr("The file is in read only mode", "Save failed addresses in territory data import"));
        else {
            QTextStream out(&outputfile);
            out.setCodec("UTF-8");
            out << header << failedAddresses;
            outputfile.close();
        }
    }

    return importCount;
}

void cterritories::requestStreetList(int territoryId)
{
    territory *t = getTerritoryById(territoryId);
    QString wktGeometry = t->wktGeometry();
    QString latLonList;

    // use bounding box, since polygon may result in too long http request
    OGRSpatialReference osr;
    OGRGeometry *territoryGeometry = nullptr;
    QByteArray bytes = wktGeometry.toUtf8();
    const char *data = bytes.constData();
    OGRErr err = OGRGeometryFactory::createFromWkt(data, &osr, &territoryGeometry);
    if (err != OGRERR_NONE) {
        // process error, like emit signal
    } else {
        OGREnvelope territoryEnvelope;
        territoryGeometry->getEnvelope(&territoryEnvelope);
        latLonList = QString("%1 %2 %1 %4 %3 %4 %3 %2 %1 %2")
                             .arg(territoryEnvelope.MinY)
                             .arg(territoryEnvelope.MinX)
                             .arg(territoryEnvelope.MaxY)
                             .arg(territoryEnvelope.MaxX);
    }
    if (latLonList != "") {
        QString overpassAPIUrl = "http://overpass-api.de/api/interpreter?data=";
        QString osmQLString = "[out:json][timeout:25];"
                              "way(poly:\""
                + latLonList + "\")[highway][name];"
                               "out geom;";
        QNetworkRequest request;
        request.setUrl(overpassAPIUrl + osmQLString);
        QNetworkAccessManager *streetRequest;
        streetRequest = new QNetworkAccessManager();
        connect(streetRequest, SIGNAL(finished(QNetworkReply *)),
                SLOT(streetRequestFinished(QNetworkReply *)));
        QNetworkReply *reply = streetRequest->get(request);
        reply->setProperty("territoryId", territoryId);
    }
}

void cterritories::getTerritoryStreets(int territoryId)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlQuery = "SELECT id, territory_id, wkt_geometry, streettype_id FROM territorystreets WHERE wkt_geometry IS NOT NULL AND wkt_geometry <> ''";
    sql_items streets = sql->selectSql(sqlQuery);

    QVariantMap streetMap;
    if (!streets.empty()) {
        for (unsigned int i = 0; i < streets.size(); i++) {
            const sql_item street = streets[i];
            int street_id = street.value("id").toInt();
            int territory_id = street.value("territory_id").toInt();
            int streettype_id = street.value("streettype_id").toInt();
            QString input = street.value("wkt_geometry").toString();
            QRegularExpression regExp("((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))\\s*((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))");
            // catch MULTILINESTRING and LINESTRING
            QRegularExpression multiLineStringRegex("\\([\\d,.\\- ]*?\\)"); //g, multiLineStringMatch;
            QRegularExpressionMatchIterator iMultiLineString = multiLineStringRegex.globalMatch(input);

            int iLine = 0;
            while (iMultiLineString.hasNext()) {
                QRegularExpressionMatch lineStringMatch = iMultiLineString.next();
                QString lineString = lineStringMatch.captured(0);
                Street street;
                QGeoPath geoPath;

                QRegularExpressionMatchIterator iMatch = regExp.globalMatch(lineString);
                while (iMatch.hasNext()) {
                    QRegularExpressionMatch match = iMatch.next();
                    double longitude = match.captured(1).toDouble();
                    double latitude = match.captured(2).toDouble();
                    QGeoCoordinate coord(latitude, longitude);
                    geoPath.addCoordinate(coord);
                }
                street.setStreetId(street_id);
                street.setTerritoryId(territory_id);
                street.setPath(geoPath);
                street.setStreetTypeId(streettype_id);

                streetMap[QVariant(street_id).toString() + "_" + QVariant(iLine).toString()] = QVariant::fromValue(street); // qVariantFromValue(street);
                iLine += 1;
            }
        }
    }
    emit streetsLoaded(streetMap, territoryId);
}

/**
 * @brief getClosestPoint - Returns the closest from one of the territory boundaries' points to the given point.
 * @param point - center point
 * @param tolerance - search radius
 */
QGeoCoordinate cterritories::getClosestPoint(QGeoCoordinate point, double tolerance)
{
    QGeoCoordinate closestPoint;
    if (m_allPoints.count() > 0) {
        double distance(tolerance);
        QMap<int, QGeoCoordinate>::const_iterator i = m_allPoints.begin();
        while (i != m_allPoints.end()) {
            double newDistance(point.distanceTo(i.value()));
            if (newDistance < distance) {
                distance = newDistance;
                closestPoint = i.value();
            }
            ++i;
        }
    }
    return closestPoint;
}
