/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import net.theocbase.mobile 1.0

Page {
    id: publisherspage
    property string searchtext: ""

    function loadList(positionname){
        searchtext = ""
        publishersListView.model = _publishers.getAllPublishersList()
        if (typeof positionname !== "undefined") {
            positionTimer.currentName = positionname
            positionTimer.start()
        }
    }

    SystemPalette { id: palette }

    Publishers { id: _publishers }

    Timer {
        id: positionTimer
        interval: 500
        property string currentName
        onTriggered: {
            for(var i=0;i<publishersListView.model.length;i++){
                if (publishersListView.model[i].name === currentName){
                    publishersListView.positionViewAtIndex(i,ListView.Center)
                    break;
                }
            }
        }
    }

    Component.onCompleted: {
        publishersListView.model = _publishers.getAllPublishersList()        
    }

    header:  ToolBar {
        height: 45 * dpi + toolbarTopMargin       
        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: publisherspage.leftPadding
            anchors.rightMargin: publisherspage.rightPadding
            anchors.topMargin: toolbarTopMargin
            ToolButton {
                icon.source: "qrc:///search.svg"
                icon.color: "white"
                Layout.fillHeight: true
                Layout.preferredWidth: height
                onClicked: textSearch.forceActiveFocus()
            }
            TextField {
                id: textSearch
                Layout.fillWidth: true
                onTextChanged: searchtext = text
                visible: activeFocus || text.length > 0
                opacity: visible ? 1 : 0                                
                topPadding: 2*dpi
                bottomPadding: 2*dpi
                background: Rectangle { color: palette.base }
                Behavior on opacity { NumberAnimation{ duration: 500 } }
            }
            Label {
                text: qsTr("Publishers")
                visible: !textSearch.visible
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                font.pointSize: app_info.fontsize
                color: "white"
            }

            ToolButton {
                icon.source: "qrc:///publisher_add.svg"
                icon.color: enabled ? "white" : "grey"
                Layout.fillHeight: true
                Layout.preferredWidth: height
                enabled: canEditPublishers
                onClicked: {
                    stackView.clearAndPush(detailPage, {p: _publishers.addNew()})
                }
            }
        }
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentWidth: availableWidth
        ListView {
            id: publishersListView
            width: publisherspage.availableWidth
            orientation: ListView.Vertical
            spacing: 0
            delegate: listComponent

            onDragEnded: if (header.refresh) { console.log("REFRESH"); syncpage.runSync() }

            ListHeader {
                id: header
                flickable: publishersListView
                y: -publishersListView.contentY - height
            }
        }
    }

    // list view component
    Component {
        id: listComponent
        Rectangle {
            height: visible ? 60 * dpi : 0
            width: publisherspage.availableWidth
            clip: true

            border.width: 0
            border.color: palette.mid
            visible: searchtext === "" || model.modelData.name.toLowerCase().indexOf(searchtext.toLowerCase()) > -1

            color: listComponentMouseArea.pressed && row.x == 0 ? "#D9D9D9" : palette.base

            property int id: model.modelData.id
            property bool activeRow: publishersListView.currentIndex === model.index
            onActiveRowChanged: if(!activeRow) row.x = x


            //property int idNo: idNumber
            RowLayout{
                id: row
                x: parent.x
                anchors.top: parent.top
                height: parent.height
                width: parent.width
                spacing: 5*dpi
                Rectangle {
                    Layout.leftMargin: 5*dpi
                    height: 50 * dpi
                    width: height
                    color: "transparent"
                    Image {
                        anchors.fill: parent
                        source: !model.modelData.active ? "qrc:///publisher_inactive.svg" :
                                                          (model.modelData.gender === Publisher.Male ? "qrc:///publisher_male.svg" : "qrc:///publisher_female.svg")
                        sourceSize: Qt.size(60,60)
                    }
                }

                Text {
                    id: textItem
                    Layout.fillWidth: true
                    text: model.modelData.name
                    font.pointSize: app_info.fontsize
                    renderType: Text.QtRendering
                    color: palette.text
                }
                ToolButton {
                    width: 20 * dpi
                    height: 20 * dpi
                    icon.source: "qrc:/chevron_right.svg"
                    background: null
                }
            }

            Rectangle {
                id: deleteBox
                anchors.left: row.right
                anchors.top: parent.top
                height: parent.height
                width: 80*dpi
                color: "red"
                Image {
                    id: deleteIcon
                    anchors.fill: parent
                    anchors.margins: 5*dpi
                    source: "qrc:///delete.svg"
                    fillMode: Image.PreserveAspectFit
                    sourceSize: Qt.size(60,60)
                }
                ColorOverlay {
                    anchors.fill: deleteIcon
                    source: deleteIcon
                    color: "white"
                }
            }

            Rectangle {
                width: parent.width
                anchors.bottom: parent.top
                anchors.topMargin: 0
                height: app_info.linewidth
                color: palette.mid
                visible: index == 0
            }
            Rectangle {
                width: parent.width
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                height: app_info.linewidth
                color: palette.mid
            }
            SwipeArea {
                id: listComponentMouseArea
                anchors.fill: parent
                property int swipex: 0
                property bool rejectNextClick: false
                onClicked: {
                    if (rejectNextClick){
                        rejectNextClick = false
                        return
                    }

                    if (row.x <= -80){
                        if (mouse.x > (width-80*dpi)){
                            console.log("delete publisher")
                            _publishers.remove(model.modelData.id)
                            loadList()
                        }else{
                            row.x = 0
                        }
                        return
                    }
                    publishersListView.currentIndex = model.index                    
                    stackView.clearAndPush(detailPage, {p: _publishers.getPublisher(id)})
                }
                onMove: {
                    if (!canEditPublishers)
                        return
                    if (x > 0) return
                    row.x = x < -80*dpi ? -80*dpi : x
                    swipex = x
                    publishersListView.interactive = false
                    publishersListView.currentIndex = model.index
                    rejectNextClick = true
                }
                onSwipe: {
                    if (!canEditPublishers)
                        return
                    row.x = swipex > -80*dpi ? 0 : -80*dpi
                    publishersListView.interactive = true
                    if (row.x == -80*dpi)
                        rejectNextClick = true
                    //console.log("onswipe")
                }
                onCanceled: {
                    //row.x = 0
                    publishersListView.interactive = true
                    //console.log("oncancelled")
                }
            }
        }
    }

    Component {
        id: detailPage
        PublisherDetail{
            onPageClosed: loadList(p.lastname + ", " + p.firstname)
        }
    }

}
