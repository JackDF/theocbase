import QtQuick 2.2

Rectangle {
    width: 300
    height: 150

    property alias model: list.model

    ListView {
        id: list
        anchors.fill: parent


        clip: true

        highlight: hightlightItem

        highlightFollowsCurrentItem: true
        highlightRangeMode: ListView.StrictlyEnforceRange
        preferredHighlightBegin: 50
        preferredHighlightEnd: 50

        currentIndex: 4

        focus: true

        onCurrentItemChanged: {
            console.log("Current item:" + currentIndex)
        }

        delegate: delegateItem

        model: ListModel {
            ListElement {
                name: "Grey"
                colorCode: "grey"
            }

            ListElement {
                name: "Red"
                colorCode: "red"
            }

            ListElement {
                name: "Blue"
                colorCode: "blue"
            }

            ListElement {
                name: "Green"
                colorCode: "green"
            }
            ListElement {
                name: "Grey"
                colorCode: "grey"
            }
            ListElement {
                name: "Yellow"
                colorCode: "yellow"
            }
            ListElement {
                name: "Orange"
                colorCode: "orange"
            }
            ListElement {
                name: "Pink"
                colorCode: "pink"
            }
            ListElement {
                name: "Cyan"
                colorCode: "cyan"
            }
        }
    }

    Component {
        id: delegateItem
        Item {
            width: parent.width
            height: 50
            Rectangle {
                id: row1

                height: 50
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    text: model.modelData
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter

                    color: list.currentIndex == index ? "black" : "grey"
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("clicked" + index)
                    list.currentIndex = index
                }
            }
        }
    }

    Component {
        id: hightlightItem
        Rectangle {
            Rectangle {
                anchors.top: parent.top
                width: parent.width
                height: 1
                color: "black"
            }
            Rectangle {
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: "black"
            }
        }
    }


}


