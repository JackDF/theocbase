/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import net.theocbase.mobile 1.0
import net.theocbase 1.0
import "js/moment.js" as Moment

ApplicationWindow {   
    id: appWindow
    visible: true
    width: 480
    height: 800
    title: "TheocBase"
    flags: Qt.Window | Qt.MaximizeUsingFullscreenGeometryHint

    property var appState: Qt.application.state
    property real dpi: Math.max((Screen.pixelDensity / 6),1)
    property int activepage: 1
    property int toolbarTopMargin: Qt.platform.os == "ios" ? IOSUtil.unsafeTopMargin : appWindow.height - Screen.desktopAvailableHeight
    property int leftMargin: Qt.platform.os === "ios" ? IOSUtil.unsafeLeftMargin : 0
    property int bottomMargin: Qt.platform.os === "ios" ? IOSUtil.unsafeBottomMargin : 0
    property int rightMargin: Qt.platform.os === "ios" ? IOSUtil.unsafeRightMargin : 0

    property bool canViewMidweekMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewMidweekMeetingSchedule)
    property bool canEditMidweekMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditMidweekMeetingSchedule)
    property bool canViewWeekendMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanViewWeekendMeetingSchedule)
    property bool canEditWeekendMeetingSchedule: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanEditWeekendMeetingSchedule)
    property bool canViewPublishers: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanViewPublishers))
    property bool canEditPublishers: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanEditPublishers))
    property bool canViewMeetingNotes: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanViewMeetingNotes))
    property bool canEditMeetingNotes: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanEditMeetingNotes))

    function reloadMainpage(){
        stackView.navigation.get(0).refreshList(0)
    }

    // activate home, publishers or settings page
    function openPage(page){
        switch (page) {
        case 1:
            if (stackView.navigation.depth === 0)
                stackView.navigation.push(Qt.resolvedUrl("MainPage.qml"), { objectName: "MainPage" }, StackView.Immediate)
            else if (stackView.navigation.currentItem.objectName === "MainPage")
               stackView.navigation.currentItem.refreshList()
            else
                stackView.navigation.pop(null, StackView.Immediate)
            stackView.navigation.get(0).activateTabByDay()
            break
        case 2:
            stackView.navigation.pop(null)
            var item = stackView.navigation.push(Qt.resolvedUrl("PublishersPage.qml"), { objectName : "PublishersPage" }, StackView.Immediate)
            break;
        case 3:
            stackView.navigation.pop(null)
            stackView.navigation.push(Qt.resolvedUrl("SettingsPage.qml"), StackView.Immediate)
            break
        default: return
        }
    }

    function readImportFile() {
        if (shareUtils.receivedUrl !== "") {
            var f = shareUtils.receivedUrl
            shareUtils.receivedUrl = ""

            if (!ccloud.logged) {
                msg.show("Import File", "You are not logged in. Please log in and try again.")
                return
            }

            console.log(f)
            var filename = f.substring(f.lastIndexOf('/')+1)
            console.log(filename)
            var result = ""
            if (/^.*(.thb)$/.test(filename)) {
                // thb file
                var clickedFunc = function(ok) {
                    if (ok) {
                        var p = stackView.find(function(item,index){return item.objectName === "DataExchange"})
                        if (p === null) {
                            stackView.push(Qt.resolvedUrl("DataExchange.qml"),
                                           {objectName: "DataExchange", filePath: f}, StackView.Immediate)
                        }else if (p === stackView.currentItem) {
                            p.readCurrentFile(f)
                        }else{
                            stackView.pop(p,
                                          StackView.Immediate,
                                          {filePath: f})
                        }
                    }
                    msg.onButtonClicked.disconnect(clickedFunc)
                }
                msg.onButtonClicked.connect(clickedFunc)
                msg.showYesNo("Import File","Do you want to import .thb-file?",-1)
                return
            } else if (/^w_.*(.epub)$/.test(filename)) {
                result = wtImport.importFile(f)
            } else if (/^mwb_.*(.epub)$/.test(filename)) {
                result = mwbImport.importFile(f)
            } else {
                result = "Unknown file type"
            }
            msg.show("", result)
            reloadMainpage()
        }
    }

    function setTheme() {
        var background = palette.window
        var isDark = (background.r * 0.299 + background.g * 0.587 + background.b * 0.114) < 0.5

        Material.theme = isDark ? Material.Dark : Material.Light
        Material.background = palette.window
        console.log(Material.theme === Material.Dark ? "Dark" : "Light")
    }

    SystemPalette {
        id: palette
        onWindowChanged: setTheme()
    }
    MWBImport { id: mwbImport }
    WTImport { id: wtImport }
    Connections {
        id: shareUtilsConnection
        target: shareUtils
        function onReceivedUrlChanged(filename) {
            if (appState === Qt.ApplicationActive)
                readImportFile()
            else
                console.log("onReceivedUrlChanged !!! app is not in active state")
        }
        function onDbcallback(token) {
            console.log("onDbcallback" + token)
            ccloud.authentication.setToken(token)
            settings_ui.loginRequested = false
        }
    }

    Component.onCompleted: {        
        app_info.dpi = dpi        
    }

    onAppStateChanged: {
        if (appState === Qt.ApplicationActive){
            console.log("application state active")
            setTheme()

            cloudtimer.start()
            console.log("login requested ??????? " + settings_ui.loginRequested.toString())
            if (shareUtilsConnection.enabled)
                readImportFile()
        }else if (appState === Qt.ApplicationInactive){
            console.log("application state inactive")
        }else if (appState === Qt.ApplicationSuspended){
            console.log("application state suspended")
            //console.log("logged = " + ccloud.logged())
            //console.log("updates available = " + ccloud.checkCloudUpdates())
            //backgroundDebug
            //var d = new Date()
            //ccloud.backgroundDebug = "Background test " + Qt.formatDateTime(d, Qt.locale(), Locale.ShortFormat)
        }else if (appState === Qt.ApplicationHidden){
            console.log("application state hidden")
        }
    }

    SynchronizePage {
        id: syncpage
        z: 2
    }

    Timer {
        id: cloudtimer
        interval: 5000
        onTriggered: {
            var differenceMinutes = Math.floor((new Date().getTime() - settings_ui.lastCloudUpdates.getTime())/1000/60)
            if (differenceMinutes > 5){
                // more than 5 minutes from last check
                if (ccloud.logged){
                    // logged - check updates
                    ccloud.showError = false
                    ccloud.checkCloudUpdates()
                    ccloud.showError = true
                    // save timestamp
                    settings_ui.lastCloudUpdates = new Date()
                }
            }
        }
    }

    Cloud {
        id: ccloud
        property bool autoSync: false
        property bool showError: true
        property bool running: false
        onStateChanged: {
            if (state == Cloud.Both){
                console.log("Sync state changed: both changes")
            }else if (state == Cloud.Upload){
                console.log("Sync state changed: upload")
            }else if (state == Cloud.Download){
                console.log("Sync state changed: download")
            }
        }
        onSyncConflict: {
            console.log("sync conflict");
            syncpage.visible = false
            msg.showYesNo("TheocBase Cloud",
                          qsTr("The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?")
                          .arg(values.toString()),2);
        }
        onSyncStarted: running = true
        onSyncFinished: {
            running = false
            console.log("sync finished")
            console.log(stackView.navigation.depth)
            var mainPage = stackView.navigation.find(function(item, index) { return item.objectName === "MainPage" })
            mainPage.refreshList(0)
            //stackView.navigation.get(0).refreshList(0);
            switch(stackView.navigation.depth) {
            case 1:
                mainPage.activateTabByDay()
                //stackView.navigation.get(0).activateTabByDay()
                break
            case 2:
                //var p = stackView.find(function(item,index){return item.objectName === "DataExchange"})
                var item = stackView.navigation.find(function(item, index) { return item.objectName === "PublishersPage" }) //.get(1).loadList()
                if (item !== null)
                    item.loadList()
                break
            }
            settings_ui.lastCloudUpdates = new Date()
        }
        onError: {
            syncpage.visible = false;
            if (showError)
                msg.show("TheocBase Cloud",message);
        }
        onDifferentLastDbUser: {
            syncpage.visible = false;
            msg.show("TheocBase Cloud",
                     "The database has been synchronized last time by other cloud user.");
        }

        onCloudResetFound: {
            syncpage.visible = false;
            msg.showYesNo("TheocBase Cloud",
                          qsTr("The cloud data has been reset. Your local data will be replaced. Continue?"),3);
        }
        onLoginRequired: {
            console.log("login required " + url)
            shareUtils.loginDropbox(url)
            settings_ui.loginRequested = true
        }
        onLoggedChanged: {            
            if (ok && !moment(ccloud.lastSyncTime).isValid())
                syncpage.runSync()            
        }
        Component.onCompleted: {
            ccloud.initAccessControl()
        }
    }

    MsgBox {
        id: msg        
        onButtonClicked: {
            switch (id){
            case 1:
                // sync updates
                if (ok) syncpage.runSync();
                break
            case 2:
                // sync conflict
                syncpage.continueSync(ok);                
                break
            case 3:
                // cloud data reset
                if (ok) {
                    ccloud.clearDatabase();
                    syncpage.runSync();
                }
                break;
            default: break
            }
        }
    }
    Settings {
        id: settings_ui
        category: "ui"
        property bool showTime: true
        property bool showSongTitles: false
        property date lastCloudUpdates: new Date('2016-01-01')
        property bool loginRequested: false
    }

    NavigationStack {
        id: stackView
        anchors.fill: parent
        Component.onCompleted: {
            openPage(1)
            if (!ccloud.logged){
                if (!settings_ui.loginRequested)
                    ccloud.login()
            }
        }
    }

    ToolBar {
        id: navbar
        z: 2
        height: stackView.useSplitView || stackView.currentDetailitem == undefined ?
                    (Qt.platform.os === "ios" ? 50 : 50*dpi) + appWindow.bottomMargin : 0        
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        Material.background: Material.theme == Material.Dark ? palette.window : "#e1e2e1"
        Material.foreground: Material.theme == Material.Dark ? palette.mid : "black"
        Material.accent: Material.theme == Material.Dark ? "white" : "#254f8e"
        RowLayout {
            anchors.fill: parent
            anchors.bottomMargin: appWindow.bottomMargin
            spacing: 0
            Item { Layout.fillWidth: true; Layout.fillHeight: true }
            ToolButton {
                id: todayButton
                icon.source: "qrc:/home.svg"
                Layout.maximumWidth: height*3
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                checked: true                
                onClicked: {
                    activepage = 1
                    checked = true
                    publishersButton.checked = false
                    settingsButton.checked = false
                    console.log("today clicked");
                    openPage(1)                    
                }
            }
            ToolButton {
                id: publishersButton
                Layout.maximumWidth: height*3
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                icon.source: "qrc:/publishers.svg"
                visible: canViewPublishers
                onClicked: {
                    activepage = 2
                    checked = true
                    todayButton.checked = false
                    settingsButton.checked = false
                    console.log("publishers clicked");
                    openPage(2)
                }
            }
            ToolButton {
                id: settingsButton
                icon.source: "qrc:/settings.svg"
                Layout.maximumWidth: height*3
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                onClicked: {
                    activepage = 3
                    checked = true
                    todayButton.checked = false
                    publishersButton.checked = false
                    console.log("settings clicked");
                    openPage(3)
                }
            }
            Item { Layout.fillWidth: true; Layout.fillHeight: true }
        }
    }
}

