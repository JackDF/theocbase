/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Page {
    id: wtSongEdit
    title: qsTr("Watchtower Song", "Page title")

    property CPTMeeting meeting
    property bool startSong: true

    header: BaseToolbar {
        title: wtSongEdit.title
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: {
                stackView.pop()
            }
        }
    }


    ScrollView {
        width: parent.width
        height: parent.height
        contentWidth: layout.width

        ColumnLayout {
            id: layout
            width: wtSongEdit.availableWidth
            // Prayer
            RowTitle {
                text: qsTr("Prayer")
                visible: !startSong
            }
            ComboBoxTable {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                currentText: meeting.finalPrayer ? meeting.finalPrayer.fullname : ""
                currentId: meeting.finalPrayer ? meeting.finalPrayer.id : -1
                col1Name: chairmanTitle.text
                visible: !startSong
                onBeforeMenuShown: {
                    model = controller.brotherList(Publisher.Prayer)
                }
                onRowSelected: {
                    var prayer = CPersons.getPerson(id)
                    meeting.finalPrayer = prayer
                    meeting.save()
                }
                PublicMeetingController { id: controller }
            }

            // WT Song
            RowTitle {
                text: qsTr("Song")
            }
            NumberSelector {
                Layout.fillWidth: true
                Layout.preferredHeight: height
                Layout.bottomMargin: 10
                maxValue: 151
                selectedValue: startSong ? meeting.songWtStart : meeting.songWtEnd
                onSelectedValueChanged: {
                    if (startSong) {
                        if (selectedValue === meeting.songWtStart)
                            return
                        meeting.songWtStart = selectedValue
                    } else {
                        if (selectedValue === meeting.songWtEnd)
                            return
                        meeting.songWtEnd = selectedValue
                    }
                    meeting.save()
                }
            }
        }
    }
}
