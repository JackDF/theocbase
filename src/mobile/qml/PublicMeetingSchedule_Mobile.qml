/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Rectangle {
    id: rectangle2
    height: 350
    Layout.preferredHeight: 310 * dpi + finalTalkRow.height
    //property double dpi: 1.0
    property bool showTime: false
    property bool editpossible: true
    property CPTMeeting cptMeeting
    width: 400

    function loadSchedule(date){
        showTime = settings_ui.showTime
        pmController.date = date

        cptMeeting = pmController.meeting
        console.log("public meeting starttime " + Qt.formatTime(cptMeeting.startTime, Qt.DefaultLocaleShortDate))

        var t = cptMeeting.startTime
        songStartRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : ""
        t.setMinutes(t.getMinutes() + 5)
        ptThemeRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : "30"
        t.setMinutes(t.getMinutes() + 30)
        wtSongRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : ""
        t.setMinutes(t.getMinutes() + 5)
        wtStudyRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : cptMeeting.wtTime
        t.setMinutes(t.getMinutes() + cptMeeting.wtTime)
        if (cptMeeting.finalTalk !== ""){
            finalTalkRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : "30"
            t.setMinutes(t.getMinutes() + 30)
        }
        songEndRow.timeText.text = showTime ? Qt.formatTime(t, Qt.DefaultLocaleShortDate) : ""
    }

    PublicMeetingController { id: pmController }

    // public talk

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent
        spacing: 0

        RowLayout {
            Layout.preferredHeight: 40 * dpi
            Label {
                text: "○○● " + Qt.locale().dayName(wkDate.getDay(), Locale.LongFormat) +
                      " | " + qsTr("Weekend Meeting").toUpperCase()
                font.pointSize: app_info.fontsizeSmall
                color: Qt.rgba(47/255, 72/255, 112/255, 1)
                Layout.leftMargin: 5*dpi
                Layout.preferredHeight: 40*dpi
                Layout.alignment: Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
                Layout.fillWidth: true
                elide: Text.ElideRight
            }
            Item {
                Layout.preferredHeight: 40 * dpi
                width: height
                ToolButton {
                    icon.source: cptMeeting && cptMeeting.notes ? "qrc:/notes-text.svg" : "qrc:/notes.svg"
                    icon.color: Qt.rgba(47/255, 72/255, 112/255, 1)
                    visible: canViewMeetingNotes && editpossible
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    onClicked: {
                        stackView.clearAndPush(Qt.resolvedUrl("MeetingNotes.qml"),
                                               { "pageTitle": qsTr("Midweek Meeting"), "meeting": cptMeeting, "editable": canEditMeetingNotes })
                    }
                }
            }
        }

        ScheduleRowItem {
            id: songStartRow
            Layout.fillWidth: true
            themeText.color: "grey"
            themeText.text: !cptMeeting || cptMeeting.songTalk < 1 ? qsTr("Song and Prayer") : qsTr("Song %1 and Prayer").arg(cptMeeting.songTalk) +
                                                                     (settings_ui.showSongTitles ? "\n" + cptMeeting.songTalkTitle : "")
            nameText1.text: cptMeeting ? cptMeeting.chairman ? cptMeeting.chairman.fullname : "" : ""
            nameText1.color: "grey"
            timeboxColor: showTime ? "grey" : "transparent"
            clickable: canEditWeekendMeetingSchedule && editpossible
            imageSource: clickable ? "qrc:/chevron_right.svg" : ""
            onClicked: {                
                var detailsPage = stackView.clearAndPush(Qt.resolvedUrl("WEMeetingChairmanPanel.qml"),
                                                         {meeting: cptMeeting})
            }
        }
        Rectangle {
            color: Qt.rgba(47/255, 72/255, 112/255, 0.1)
            Layout.fillWidth: true
            Layout.preferredHeight: 70*dpi
            ColumnLayout {
                anchors.fill: parent
                spacing: 0                
                Text {
                    id: text1
                    color: Qt.rgba(47/255, 72/255, 112/255, 1)
                    text: qsTr("PUBLIC TALK")
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: app_info.fontsize
                }
                ScheduleRowItem {
                    id: ptThemeRow
                    Layout.fillWidth: true
                    themeText.text: cptMeeting ? cptMeeting.themeNumber > 0 ? cptMeeting.theme + " (" + cptMeeting.themeNumber + ")" : "" : ""
                    nameText1.text: cptMeeting ? cptMeeting.speaker ? cptMeeting.speaker.fullname : "" : ""
                    nameText2.text: cptMeeting ? cptMeeting.speaker ?  "(" + cptMeeting.speaker.congregationName + ")" : "" : ""
                    timeboxColor: Qt.rgba(47/255, 72/255, 112/255, 1)
                    showTopLine: true
                    clickable: canEditWeekendMeetingSchedule && editpossible
                    imageSource: clickable ? "qrc:///chevron_right.svg" : ""
                    onClicked: {                        
                        var detailsPage = stackView.clearAndPush(Qt.resolvedUrl("PublicTalkPanel.qml"),
                                                                 {meeting: cptMeeting})
                    }
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 10*dpi
            color: "white"
        }


        Rectangle {
            id: rectWtStudy
            color: Qt.rgba(77/255, 101/255, 77/255, 0.1)
            Layout.fillWidth: true
            Layout.preferredHeight: 110*dpi

            ColumnLayout {
                id: columnLayout3
                spacing: 0
                anchors.fill: parent

                Text {
                    id: text5
                    color: Qt.rgba(77/255, 101/255, 77/255, 1)
                    text: qsTr("WATCHTOWER STUDY")
                    font.bold: true
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: app_info.fontsize
                }                

                ScheduleRowItem {
                    id: wtSongRow
                    Layout.fillWidth: true
                    themeText.text: qsTr("Song %1").arg(!cptMeeting || cptMeeting.songWtStart < 1 ? "" :
                                                                                                    cptMeeting.songWtStart +
                                                                                                    (settings_ui.showSongTitles ? "\n" + cptMeeting.songWtStartTitle : ""))
                    themeText.color: "grey"
                    timeboxColor: showTime ? "grey" : "transparent"
                    showTopLine: true
                    clickable: canEditWeekendMeetingSchedule && editpossible
                    imageSource: clickable ? "qrc:/chevron_right.svg" : ""
                    onClicked: {                        
                        var detailsPage = stackView.clearAndPush(Qt.resolvedUrl("WatchtowerSongPanel.qml"),
                                                                 {meeting: cptMeeting, startSong: true})
                    }
                }

                ScheduleRowItem {
                    id: wtStudyRow
                    Layout.fillWidth: true
                    themeText.text: cptMeeting ? cptMeeting.wtTheme : ""
                    nameText1.text: cptMeeting ? cptMeeting.wtConductor ? cptMeeting.wtConductor.fullname +  " (" + qsTr("Conductor") + ")" : "" : ""
                    nameText2.text: cptMeeting ? cptMeeting.wtReader ? cptMeeting.wtReader.fullname + " (" + qsTr("Reader") + ")" : "" : ""
                    timeboxColor: Qt.rgba(77/255, 101/255, 77/255, 1)
                    clickable: canEditWeekendMeetingSchedule && editpossible
                    imageSource: clickable ? "qrc:/chevron_right.svg" : ""
                    onClicked: {
                        stackView.pop(null)
                        var detailsPage = stackView.clearAndPush(Qt.resolvedUrl("WatchtowerStudyPanel.qml"),
                                                                 {meeting: cptMeeting})
                    }
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "<html><a href='#'>" + qsTr("Import WT...") + "</a></html>"
                        visible: canEditWeekendMeetingSchedule && editpossible && parent.themeText.text == "" && cptMeeting &&
                                 cptMeeting.date > new Date("2019-03-03")
                        onLinkActivated: {
                            console.log("WT IMPORT")
                            shareUtils.openFile("epub")
                        }
                    }
                }
            }
            Rectangle { color: "lightgrey"; anchors.verticalCenter: parent.top; width: parent.width; height: app_info.linewidth }
        }

        ScheduleRowItem {
            id: finalTalkRow
            timeText.text: "30"
            timeboxColor: "black"
            visible: cptMeeting && cptMeeting.finalTalk !== ""
            themeText.text: cptMeeting ? cptMeeting.finalTalk : ""
            nameText1.text: cptMeeting ? cptMeeting.finalTalkSpeakerName : ""
        }

        ScheduleRowItem {
            id: songEndRow
            timeText.text: ""
            timeboxColor: showTime ? "grey" : "transparent"
            themeText.text: !cptMeeting || cptMeeting.songWtEnd < 1 ? qsTr("Song and Prayer") : qsTr("Song %1 and Prayer").arg(cptMeeting.songWtEnd) +
                                                                      (settings_ui.showSongTitles ? "\n" + cptMeeting.songWtEndTitle : "")
            themeText.color: "grey"
            nameText1.text: cptMeeting ? cptMeeting.finalPrayer ? cptMeeting.finalPrayer.fullname : "" : ""
            clickable: canEditWeekendMeetingSchedule && editpossible
            imageSource: clickable ? "qrc:/chevron_right.svg" : ""
            onClicked: {               
                var detailsPage = stackView.clearAndPush(Qt.resolvedUrl("WatchtowerSongPanel.qml"),
                                                         {meeting: cptMeeting, startSong: false})
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
