/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import Qt.labs.calendar 1.0
import net.theocbase 1.0

Popup {
    id: calendar
    parent: Overlay.overlay
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    implicitWidth: 400 * _dpi
    implicitHeight: 300 * _dpi
    property date selectedDate: new Date()
    property real _dpi: typeof dpi == "undefined" ? 1 : dpi

    GridLayout {
        id: columnLayout
        anchors.fill: parent
        columns: 3

        ToolButton {            
            icon.source: "qrc:/chevron_left.svg"
            onClicked: {
                var newDate = new Date(selectedDate)
                newDate.setMonth(newDate.getMonth() -1)
                selectedDate = newDate
            }
        }

        Label {
            id: label
            text: selectedDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            MouseArea {
                anchors.fill: parent
                onClicked: selectedDate = new Date()
            }
        }

        ToolButton {
            icon.source: "qrc:/chevron_right.svg"
            onClicked: {
                var newDate = new Date(selectedDate)
                newDate.setMonth(newDate.getMonth() +1)
                selectedDate = newDate
            }
        }

        DayOfWeekRow {
            Layout.columnSpan: 3
            Layout.fillWidth: true
        }
        MonthGrid {
            id: calendarGrid
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 3
            month: calendar.selectedDate.getMonth()
            year: calendar.selectedDate.getFullYear()
            spacing: 2
            delegate: Label {
                readonly property bool selected: model.day === calendar.selectedDate.getDate() &&
                                                 model.month === calendar.selectedDate.getMonth() &&
                                                 model.year === calendar.selectedDate.getFullYear()
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                opacity: model.month === calendarGrid.month ? 1 : 0
                text: model.day
                font.bold: selected
                color: selected ? Material.background : Material.foreground
                background: Rectangle {
                    anchors.centerIn: parent
                    width: Math.min(parent.width, parent.height) + 4
                    height: width
                    radius: width / 2
                    color: parent.selected ? Material.foreground : "transparent"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        calendar.selectedDate = model.date
                        calendar.close()
                    }
                }
            }
        }
    }
}
