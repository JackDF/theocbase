/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.1
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Page {
    id: assignmentDialog
    property bool canSendMidweekMeetingReminders: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanSendMidweekMeetingReminders)
    property string returnValue: ""
    property LMM_Assignment currentAssignment
    property int oldSpeakerId: -1
    property int oldAssistantId: -1
    property int currentindex: -1
    property int modelLength: 0
    property bool editableTheme: currentAssignment &&
                                 (currentAssignment.talkId == LMM_Schedule.TalkType_COTalk ||
                                  currentAssignment.talkId == LMM_Schedule.TalkType_LivingTalk1 ||
                                  currentAssignment.talkId == LMM_Schedule.TalkType_LivingTalk2 ||
                                  currentAssignment.talkId == LMM_Schedule.TalkType_LivingTalk3)

    signal gotoNext()
    signal gotoPrevious()    

    AssignmentController { id: myController }
    onCurrentAssignmentChanged: {
        if (!currentAssignment) return
        oldSpeakerId = currentAssignment.speaker ? currentAssignment.speaker.id : -1
        oldAssistantId = currentAssignment.assistant ? currentAssignment.assistant.id : -1
    }
    function saveChanges(){
        if (Qt.inputMethod.visible)
            Qt.inputMethod.hide()

        if (!currentAssignment)
            return

        var s_id = currentAssignment.speaker ? currentAssignment.speaker.id : -1
        var a_id = currentAssignment.assistant ? currentAssignment.assistant.id : -1

        if (currentAssignment.note !== textAreaNotes.text ||
                s_id !== oldSpeakerId || a_id !== oldAssistantId){
            currentAssignment.note = textAreaNotes.text
            currentAssignment.save()
            console.log("assignment saved")
            oldSpeakerId = s_id
            oldAssistantId = a_id
        }
        return true
    }

    header: ToolBar {
        height: 45 * dpi + toolbarTopMargin        
        RowLayout {
            anchors.fill: parent
            anchors.topMargin: toolbarTopMargin
            ToolButton {
                icon.source: "qrc:/back.svg"
                icon.color: "white"
                onClicked: {
                    if (!saveChanges())
                        return
                    stackView.pop()
                }
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("Details", "Page title")
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pointSize: app_info.fontsize
                color: "white"
            }
            ToolButton {
                icon.source: "qrc:/share.svg"
                icon.color: "white"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    shareUtils.setPoint(mapToGlobal(width/2, height))
                    shareUtils.shareText(currentAssignment.getReminderText())
                }
            }
            ToolButton {
                icon.source: "qrc:/assignment_previous.svg"
                icon.color: enabled ? "white" : "grey"
                enabled: currentindex > 0
                onClicked: {
                    if (!saveChanges()) return;
                    gotoPrevious()
                }
            }
            ToolButton {
                icon.source: "qrc:/assignment_next.svg"
                icon.color: enabled ? "white" : "grey"
                enabled: currentindex < (modelLength -1)
                onClicked: {
                    if (!saveChanges()) return;
                    gotoNext()
                }
            }
        }
    }

    ScrollView {
        id: flickable
        width: parent.width
        height: parent.height
        contentWidth: availableWidth
        ColumnLayout {
            id: layout
            width: assignmentDialog.availableWidth
            spacing: 0
            //anchors.margins: 10
            RowTitle {
                text: qsTr("Theme")
            }
            TextField {
                Layout.fillWidth: true
                text: currentAssignment ? currentAssignment.theme : ""
                leftPadding: 10*dpi
                rightPadding: 10*dpi
                wrapMode: Text.WordWrap
                readOnly: !editableTheme
                onEditingFinished: {
                    if (editableTheme && currentAssignment.theme != text) {
                        currentAssignment.theme = text
                        currentAssignment.save()
                    }
                }
            }

            RowTitle {
                text: qsTr("Source")
            }
            TextArea {
                Layout.fillWidth: true
                text: currentAssignment ? currentAssignment.source : ""               
                wrapMode: Text.WordWrap
                leftPadding: 10*dpi
                rightPadding: 10*dpi
                font.pointSize: app_info.fontsize
                readOnly: !editableTheme
                onEditingFinished: {
                    if (editableTheme && currentAssignment.source != text) {
                        currentAssignment.source = text
                        currentAssignment.save()
                    }
                }
            }

            RowTitle {
                id: speakerTitle
                text: currentAssignment &&
                      currentAssignment.talkId === LMM_Schedule.TalkType_CBS ? qsTr("Conductor")
                                                                             : qsTr("Speaker")
            }

            ComboBoxTable {
                Layout.fillWidth: true
                currentText: currentAssignment ? currentAssignment.speakerFullName : ""
                currentId: currentAssignment.speaker ? currentAssignment.speaker.id : -1
                col1Name: speakerTitle.text
                enabled: currentAssignment &&
                         currentAssignment.talkId !== LMM_Schedule.TalkType_COTalk &&
                         currentAssignment.talkId !== LMM_Schedule.TalkType_SampleConversationVideo &&
                         currentAssignment.talkId !== LMM_Schedule.TalkType_ApplyYourself
                onBeforeMenuShown: {
                    model = currentAssignment.getSpeakerList()
                }
                onRowSelected: {
                    var speaker = CPersons.getPerson(id)
                    currentAssignment.speaker = speaker
                    saveChanges()
                }
                onTooltipRequest: {
                    if (model.get(row).id !== "undefined") {
                        var userid = model.get(row).id
                        rowTooltip = myController.getHistoryTooltip(userid, app_info.fontsizeSmall)
                    }
                }
            }

            RowTitle {
                id: readerTitle
                text: qsTr("Reader")
                visible: currentAssignment && currentAssignment.talkId === LMM_Schedule.TalkType_CBS
            }

            ComboBoxTable {
                Layout.fillWidth: true
                currentText: currentAssignment && currentAssignment.assistant ? currentAssignment.assistant.fullname : ""
                currentId: currentAssignment.assistant ? currentAssignment.assistant.id : -1
                visible: currentAssignment && currentAssignment.talkId === LMM_Schedule.TalkType_CBS
                col1Name: readerTitle.text
                onBeforeMenuShown: {
                    model = currentAssignment.getAssistantList()
                }
                onRowSelected: {
                    var reader = CPersons.getPerson(id)
                    currentAssignment.assistant = reader
                    saveChanges()
                }
            }

            RowTitle { text: qsTr("Notes") }

            TextArea {
                id: textAreaNotes
                Layout.fillWidth: true
                Layout.minimumHeight: 100*dpi
                text: currentAssignment ? currentAssignment.note : ""
                font.pointSize: app_info.fontsize
                wrapMode: Text.WordWrap
                leftPadding: 10*dpi
                rightPadding: 10*dpi
                background: Rectangle {
                    color: "transparent"
                }
                onEditingFinished: saveChanges()
            }
        }
    }
}
