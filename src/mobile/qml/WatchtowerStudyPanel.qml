/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Page {
    id: wtStudyEdit
    title: "Watchtower Study"
    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            wt_controller.date = meeting.date
    }
    property PublicMeetingController wt_controller: PublicMeetingController {}
    header: BaseToolbar {
        title: wtStudyEdit.title
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: {
                stackView.pop()
            }
        }
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentWidth: width

        ColumnLayout {
            id: layout
            width: wtStudyEdit.availableWidth

            // Issue
            RowTitle {
                text: qsTr("Watchtower Issue")
            }
            NumberSelector {
                maxValue: 12
                Layout.fillWidth: true
                Layout.preferredHeight: height
                selectedValue: meeting.wtIssue == ""  ? 0 : parseInt(meeting.wtIssue)
                onSelectedValueChanged: {
                    var checkValue = 0
                    if (meeting.wtIssue != "")
                        checkValue = parseInt(meeting.wtIssue)
                    if (selectedValue !== checkValue) {
                        meeting.wtIssue = selectedValue
                        meeting.save()
                    }
                }
            }

            // Article
            RowTitle {
                text: qsTr("Article", "The number of Watchtower article")
                visible: meeting.date > new Date("2019-03-03")
            }
            TextField {
                Layout.fillWidth: true
                leftPadding: 10*dpi
                rightPadding: 10*dpi
                text: meeting.wtSource
                //enabled: false
                visible: meeting.date > new Date("2019-03-03")
                onEditingFinished: {
                    if (meeting.wtSource != text) {
                        meeting.wtSource = text
                        meeting.save()
                    }
                }
            }

            // Theme
            RowTitle {
                text: qsTr("Theme")
            }
            TextField {
                text: meeting.wtTheme
                Layout.fillWidth: true
                leftPadding: 10*dpi
                rightPadding: 10*dpi
                wrapMode: Text.WordWrap
                onEditingFinished: {
                    if (meeting.wtTheme != text) {
                        meeting.wtTheme = text
                        meeting.save()
                    }
                }
            }

            // WT Conductor
            RowTitle {
                id: conductorTitle
                text: qsTr("Conductor", "Watchtower study conductor")
            }
            ComboBoxTable {
                Layout.fillWidth: true
                currentText: meeting.wtConductor ? meeting.wtConductor.fullname : ""
                currentId: meeting.wtConductor ? meeting.wtConductor.id : -1
                col1Name: conductorTitle.text
                onBeforeMenuShown: {
                    model = wt_controller.brotherList(Publisher.WtCondoctor)
                }
                onRowSelected: {
                    var wtconductor = CPersons.getPerson(id)
                    meeting.wtConductor = wtconductor
                    meeting.save()
                }
            }

            // WT Reader
            RowTitle {
                id: readerTitle
                text: qsTr("Reader", "Watchtower study reader")
            }
            ComboBoxTable {
                Layout.fillWidth: true
                currentText: meeting.wtReader ? meeting.wtReader.fullname : ""
                currentId: meeting.wtReader ? meeting.wtReader.id : -1
                col1Name: readerTitle.text                
                onBeforeMenuShown: {
                    model = wt_controller.brotherList(Publisher.WtReader)
                }
                onRowSelected: {
                    var wtreader = CPersons.getPerson(id)
                    meeting.wtReader = wtreader
                    meeting.save()
                }
            }
        }
    }
}
