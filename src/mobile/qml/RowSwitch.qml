import QtQuick 2.3
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1
//import QtQuick.Controls.Styles 1.2

Item {
    id: rowswitch
    width: parent.width
    implicitWidth: 200
    height: 40*dpi
    Layout.fillWidth: true

    property string title: ""
    property bool checked: false
    property bool checkbox: false
    property ButtonGroup exclusiveGroup
    property bool firstrow: false
    property alias textColor: textTitle.color
    property color backgroundColor: palette.base


    signal clicked(bool checked)

    SystemPalette {
        id: palette
    }

    SystemPalette {
        id: paletteDisabled
        colorGroup: SystemPalette.Disabled
    }

    Rectangle {
        anchors.fill: parent
        // grey background color when pressed
        color: switchmousearea.pressed ? palette.highlight : backgroundColor

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width - 20*dpi
            Text {
                id: textTitle
                anchors.verticalCenter: parent.verticalCenter
                text: title
                color: enabled ? palette.text : paletteDisabled.text
                font.pointSize: app_info.fontsize
                width: parent.width - (sw.visible ? sw.width : chkBox.width)
                renderType: Text.QtRendering
                wrapMode: Text.WordWrap
            }
            Switch {
                id: sw
                checked: rowswitch.checked
                anchors.verticalCenter: parent.verticalCenter
                onCheckedChanged: {
                    rowswitch.checked = checked
                    rowswitch.clicked(checked)
                }
                visible: !checkbox
            }

            CheckBox {
                id: chkBox
                checked: rowswitch.checked
                anchors.verticalCenter: parent.verticalCenter
                onCheckedChanged: { rowswitch.checked = checked }                
                visible: checkbox
                Component.onCompleted: {
                    if (exclusiveGroup)
                        exclusiveGroup.addButton(chkBox)
                }
            }
        }

        // extend selectable area to whole width when chekcbox
        MouseArea {
            id: switchmousearea
            anchors.fill: parent
            visible: chkBox.visible
            onClicked: {
                if (!chkBox.checked) chkBox.checked = true
            }
        }

        Rectangle {
            width: parent.width
            anchors.top: parent.top
            height: app_info.linewidth
            color: palette.mid
            visible: firstrow
        }
        Rectangle {
            width: parent.width
            anchors.bottom: parent.bottom
            height: app_info.linewidth
            color: palette.mid
        }
    }
}
