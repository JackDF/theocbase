/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "iosutil.h"
#include <UIKit/UIKit.h>
#include "../cloud/cloud_controller.h"

@interface QIOSApplicationDelegate
@end

@interface QIOSApplicationDelegate (MyQtAppDelegate)
@end

@implementation QIOSApplicationDelegate (MyQtAppDelegate)

- (void)applicationDidEnterBackground:(UIApplication *)application
{
#pragma unused(application)
    NSLog(@"In the background");
    cloud_controller c;
    if (c.logged()){
        NSLog(@"Logged in");
    }else{
        NSLog(@"Logged out");
    }
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
#pragma unused(application)
    NSLog(@"background handler called");
    bool upd = false;
    cloud_controller c;
    if (c.logged()){
        NSLog(@"Cloud login ok");
        upd = c.checkCloudUpdates();
        NSLog(@"Updates available: %d",upd);
    }else{
        NSLog(@"Not logged into cloud");
    }
    completionHandler(upd ? UIBackgroundFetchResultNewData : UIBackgroundFetchResultNoData);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(nullable NSDictionary *)launchOptions
{
#pragma unused(launchOptions)
    application.statusBarStyle = UIStatusBarStyleLightContent;
    
    return YES;
}

@end

iosutil::iosutil(QObject *parent) :
    QObject(parent), mUnsafeTopMargin(0), mUnsafeBottomMargin(0), mUnsafeLeftMargin(0), mUnsafeRightMargin(0)
{

}

void iosutil::initiOS()
{
    setStatusBarColorLight();
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    NSLog(@"iOS init");
}

void iosutil::setStatusBarColorLight()
{
    //[[UIApplication sharedApplication].keyWindow.rootViewController preferredStatusBarStyle: UIStatusBarStyleLightContent];
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

QString iosutil::getDeviceName()
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        return "ipad";
    }else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return "iphone";
    }else{
        return "unsupported ios device";
    }
}

int iosutil::unsafeTopMargin() const
{
    return mUnsafeTopMargin;
}

int iosutil::unsafeBottomMargin() const
{
    return mUnsafeBottomMargin;
}

int iosutil::unsafeLeftMargin() const
{
    return mUnsafeLeftMargin;
}

int iosutil::unsafeRightMargin() const
{
    return mUnsafeRightMargin;
}

void iosutil::orientationChanged(int orientation)
{    
    Q_UNUSED(orientation)
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        mUnsafeTopMargin = static_cast<int>(window.safeAreaInsets.top);
        mUnsafeBottomMargin = static_cast<int>(window.safeAreaInsets.bottom);
        mUnsafeLeftMargin = static_cast<int>(window.safeAreaInsets.left);
        mUnsafeRightMargin = static_cast<int>(window.safeAreaInsets.right);
    }
    emit unsafeMarginsChanged();
}
