/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PUBLISHERS_MODELVIEW_H
#define PUBLISHERS_MODELVIEW_H

#include <QObject>
#include <QString>
#include <QList>
#include <QDebug>
#include "../../cpersons.h"
#include "../../family.h"

class cpersons;

class publishers_modelview : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(person::Gender gender READ gender WRITE setGender NOTIFY genderChanged)
    Q_PROPERTY(bool active READ active CONSTANT)

public:
    publishers_modelview(QObject *parent = 0) : QObject(parent)
    { mId = -1;  }
    publishers_modelview(int id, QString name, person::Gender gender, bool active, QObject *parent = 0) :
        QObject(parent),mName(name),mId(id),mGender(gender),mActive(active)
    {}

    QString name(){ return mName; }
    void setName(QString value){ mName = value; emit nameChanged(); }

    int id(){ return mId; }
    void setId(int id){ mId = id; emit idChanged(id); }

    person::Gender gender(){ return mGender; }
    void setGender(person::Gender arg){ mGender = arg; emit genderChanged(arg);}

    bool active(){ return mActive; }

    Q_INVOKABLE QVariant getAllPublishersList()
    {
        cpersons cp;
        QList<person *> all = cp.getAllPersons(0);

        QList<QObject *> list;
        qDebug() << "pubishers" << all.count();
        for(person *p : all){
            publishers_modelview *item = new publishers_modelview(p->id(),
                                                                  p->fullname("LastName, FirstName"),
                                                                  p->gender(),
                                                                  !(p->usefor() & person::IsBreak),
                                                                  this);
            list.append(item);
        }        
        qDeleteAll(all);
        all.clear();
        return QVariant::fromValue(list);
    }

    Q_INVOKABLE person *getPublisher(int id)
    {
        cpersons cp;
        return cp.getPerson(id);
    }

    Q_INVOKABLE person *addNew()
    {
        return new person();
    }

    Q_INVOKABLE bool remove(int id)
    {
        cpersons cp;
        return cp.removePerson(id);
    }

    Q_INVOKABLE bool save(person *p){
        qDebug() << "save publisher";
        if (p->id() < 0){
            p->setCongregationid(1);
        }
        return p->update();
    }

public slots:

signals:
    void nameChanged();
    void idChanged(int id);
    void genderChanged(person::Gender arg);

private:
    QString mName;
    int mId;
    person::Gender mGender;
    bool mActive;
};

#endif // PUBLISHERS_MODELVIEW_H
