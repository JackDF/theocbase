/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PRINTCONTROLLER_H
#define PRINTCONTROLLER_H

#include <QObject>
#include <QStandardPaths>

#include "../../print/printdocument.h"
#include "../../print/printmidweekschedule.h"
#include "../../print/printmidweekworksheet.h"
#include "../../print/printmidweekslip.h"
#include "../../print/printweekendschedule.h"
#include "../../print/printhospitality.h"
#include "../../print/printoutgoingschedule.h"
#include "../../print/printoutgoingassignment.h"
#include "../../print/printweekendworksheet.h"
#include "../../print/printtalksofspeakerslist.h"
#include "../../print/printcombination.h"
#include "../../print/printterritoryassignmentrecord.h"
#include "../../print/printterritorycard.h"
#include "../../print/printterritorymapcard.h"

#include "printutil.h"

class PrintController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList templateList READ templateList NOTIFY typeChanged)

    Q_PROPERTY(TemplateData::TemplateTypes templateType READ templateType WRITE setTemplateType NOTIFY typeChanged)
    Q_PROPERTY(QString templateName READ templateName WRITE setTemplateName NOTIFY templateNameChanged);
    Q_PROPERTY(QDate fromDate READ fromDate WRITE setFromDate NOTIFY fromDateChanged);
    Q_PROPERTY(QDate toDate READ toDate WRITE setToDate NOTIFY toDateChanged);
    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged);
    Q_PROPERTY(bool assistantSlip READ assistantSlip WRITE setAssistantSlip NOTIFY assistantSlipChanged);
    Q_PROPERTY(bool assignedOnly READ assignedOnly WRITE setAssignedOnly NOTIFY assignedOnlyChanged);
public:
    PrintController(QObject *parent = nullptr);

    QStringList templateList();
    TemplateData::TemplateTypes templateType();
    void setTemplateType(TemplateData::TemplateTypes type);

    QString templateName();
    void setTemplateName(QString name);

    QDate fromDate();
    void setFromDate(QDate d);

    QDate toDate();
    void setToDate(QDate d);

    int fontSize();
    void setFontSize(int size);

    bool assistantSlip();
    void setAssistantSlip(bool slip);

    bool assignedOnly();
    void setAssignedOnly(bool only);

    Q_INVOKABLE void initDate(QDate d);
    Q_INVOKABLE void print();

signals:
    void typeChanged();
    void templateNameChanged();
    void fromDateChanged();
    void toDateChanged();
    void fontSizeChanged();
    void assistantSlipChanged();
    void assignedOnlyChanged();
    void previewReady();

private:
    QSharedPointer<PrintDocument> currentTemplate;
    QStringList mTemplateList;
    QSharedPointer<PrintUtil> mPrintUtil;
    int mFontSize = 0;
    bool mAssistantSlip = false;
    bool mAssignedOnly = true;
};

#endif // PRINTCONTROLLER_H
