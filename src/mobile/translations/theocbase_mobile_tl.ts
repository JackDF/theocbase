<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tl">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Huwag iatas ang susunod na aralin</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Iwanan ang kasalukuyang aralin</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Invalid ang data</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>I-add ang timing?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Reperensya</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudyante</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resulta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Natapos</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Boluntaryo</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Pumili ng boluntaryo</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Kasalukuyang aralin</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Natapos ang pagsasanay</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Kasunod na aralin</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pumili ng susunod na aralin</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nota</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Detalye ng Paaralan</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Simulan ang stopwatch</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Ihinto ang stopwatch</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Tagpo</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Pumili ng tagpo</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Reperensya</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kunduktor</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Tagapagsalita</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Tagabasa</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>KAYAMANAN MULA SA SALITA NG DIYOS</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>MAGING MAHUSAY SA MINISTERYO</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>PAMUMUHAY BILANG KRISTIYANO</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Tagapayo</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Awit %1 at Panalangin</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Awit</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kunduktor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Tagabasa</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Pambungad na Komento</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Panalangin</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Iwanan ang kasalukuyang aralin</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Simulan ang stopwatch</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Ihinto ang stopwatch</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>I-add ang timing</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Reperensya</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudyante</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resulta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Natapos</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Kasalukuyang Aralin</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Natapos na Pagsasanay</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Susunod na Aralin</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pumili ng susunod na aralin</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Boluntaryo</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stopwatch</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Ang inatasan at assistant ay dapat na pareho ang kasarian</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalye</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Pull to refresh...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Release to refresh...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Username o Email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Gumawa ng Account</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reset Password</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Nalimot ang passwaord</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Login Page</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Linggo simula %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schedule</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Worksheets</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Awit at Panalangin</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Awit %1 at Panalangin</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>PAHAYAG PANGMADLA</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>PAG-AARAL SA ANG BANTAYAN</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Awit %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kunduktor</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Tagabasa</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Pangalan</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Apelyido</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brother</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sister</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Tagapaglingkod</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Pamilya</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Miyembro ng pamilya ni</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Impormasyon para ma-contact</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telepono</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Naaatasan sa lahat ng Klase</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Sa Main Hall lamang maaatasan</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Sa Karagdagang Klase lamang maaatasan</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Ulo ng Pamilya</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Impormasyon</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktibo</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Kayamanan Mula sa Salita ng Diyos</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Pagbabasa ng Biblia</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Unang Pag-uusap</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Pagbabalik Muli</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Pag-aaral sa Biblia</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Pahaya sa Pamumuhay Bilang Kristiyano</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Pag-aaral ng Kongregasyon sa Biblia</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Tagabasa sa CBS</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Bagong Mamamahayag</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Panalangin</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Mamamahayag</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Tagpo</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Impormasyon</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Homepage</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Feedback</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Huling nag-synchronize: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schedule</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Ipakita ang Oras</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Ipakita ang Haba ng Oras</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>User Interface</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Wika</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Pangalan</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Nagsi-Sychronize...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Bilang ng linggo bago ang napiling petsa</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Bilang ng linggo matapos ng napiling petsa</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>linggo</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Bilang ng linggo na tatakpan maatpos ang atas</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Parehong may pagbabago sa lokal at cloud (%1 helera). Nais mo bang panatilihin ang nilalaman sa local?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Na-reset ang data sa cloud. Ang iyong lokal na data ay mapapalitan. Nais mo bang ipagpatuloy?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Huwag iatas ang susunod na aralin</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Di naka set</translation>
    </message>
</context></TS>