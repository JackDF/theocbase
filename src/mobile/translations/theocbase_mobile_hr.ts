<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hr">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nemoj dodijeliti sljedeću lekciju</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Ostaje na istoj lekciji</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Podaci su pogrešni</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Dodati vrijeme?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Izvor</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Učenik</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Sugovornik</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ishod</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Dovršeno</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Dobrovoljac</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Odaberi dobrovoljca</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Trenutna lekcija</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Vježba učinjena</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Sljedeća lekcija</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Odaberi sljedeću lekciju</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Vrijeme</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Bilješke</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Pojedinosti škole</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pokreni mjerenje vremena</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zaustavi štopericu</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Okvir</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Odaberi okvir</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Izvor</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Voditelj</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Govornik</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čitać</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Bilješke</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Bilješke</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>BLAGO IZ BOŽJE RIJEČI</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>NAUČIMO KVALITETNIJE PROPOVIJEDATI</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ŽIVIMO KAO KRŠĆANI</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsjedavajući</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Voditelj dodatnog razreda</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pjesma %1 i molitva</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pjesma</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Voditelj</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čitać</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Uvodna riječ</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Molitva</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Ostaje na istoj lekciji</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pokreni mjerenje vremena</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zaustavi mjerenje vremena</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Dodati vrijeme?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Izvor</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Učenik</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ishod</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Dovršeno</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Vrijeme</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Trenutna lekcija</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Vježba učinjena</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Sljedeća lekcija</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Odaberi sljedeću lekciju</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Bilješke</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Suradnik</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volonter</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Štoperica</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Sugovornik ne smije biti osoba suprotnog spola</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalji</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Povuci kako bi osvježio</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Otpusti kako bi osvježio</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Korisničko ime</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Lozinka</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prijava</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Korisničko ime ili e-pošta</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Adresa e-pošte</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Kreiraj korisnički račun</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Resetiraj lozinku</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Adresa e-pošte nije pronađena!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Zaboravljena lozinka</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Prijava</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Tjedan od %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Raspored</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Radni listovi</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Raspored za putujuće govornike</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Ispis</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Pjesma i molitva</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pjesma %1 i molitva</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>Javno predavanje</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>Razmatranje Stražarske kule</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Pjesma %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Voditelj</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čitač</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Prezime</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brat</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sestra</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Imenovani brat</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Obitelj</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Član obitelji</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontakt podaci</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Broj telefona</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Adresa e-pošte</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Svi razredi</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Samo u glavnoj dvorani</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Samo dodatni razredi</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Poglavar obitelji</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Sugovornik</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktivan</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsjedavajući</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Blago iz Božje Riječi</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Čitanje Biblije</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Prvi razgovor</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Ponovni posjet</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblijski tečaj</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Živimo kao kršćani</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Skupštinsko razmatranje Biblije</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Čitač skup. razmatranja Biblije</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Novi objavitelj</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Molitva</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Objavitelji</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Postavke</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Odjava</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Verzija</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase početna stranica</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Povratne informacije</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Sinkronizirano %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Raspored</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Prikaži vrijeme</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Prikaži trajanje</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Korisničko sučelje</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Jezik</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prijava</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-pošta</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Naziv</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sinkroniziranje...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Vremenska crta</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Broj tjedana prije odabranog datuma</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Broj tjedana nakon odabranog datuma</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>tjedni</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Zasivljeni broj tjedana nakon zadatka</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Neke promjene na uređaju identične su onima pohranjenim u oblaku (%1 rows). Želiš li zadržati promjene spremljene na uređaju?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Podaci iz oblaka su poništeni. Zamijenit će ih podaci s uređaja. Nastaviti?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nemoj dodijeliti sljedeću lekciju</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nije zadano</translation>
    </message>
</context></TS>