<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Neprideliť ďalší rečnícky znak</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Nechať aktuálny rečnícky znak</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Neplatné údaje</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Pridať čas?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Študent</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Výsledok</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ukončené</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Dobrovoľník</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Vybrať dobrovoľníka</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aktuálny rečnícky znak</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Cvičenia urobené</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Ďalší rečnícky znak</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Vybrať ďalší rečnícky znak</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Čas</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Detaily k úlohe</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Spustiť stopky</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zastaviť stopky</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Rámec</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Zvoliť rámec</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vedie</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Rečník</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Číta</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaily</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Poznámky</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>POKLADY Z BOŽIEHO SLOVA</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>ZLEPŠUJME SA V SLUŽBE</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ŽIVOT KRESŤANA</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsedajúci</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Radca</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pieseň č. %1 a modlitba</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pieseň</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vedie</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Číta</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Úvodné slová</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitba</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importovať program...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Nechať aktuálny znak</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Spustiť stopky</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zastaviť stopky</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Uložiť čas?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Študent</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Výsledok</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ukončené</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Čas</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aktuálny rečnícky znak</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Cvičenia urobené</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Ďalší rečnícky znak</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Vybrať ďalší rečnícky znak</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Dobrovoľník</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stopky</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Partnerom by nemala byť osoba opačného pohlavia.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Štúdia</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaily</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Potiahni pre načítanie...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Uvoľni pre načítanie...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Používateľské meno</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prihlásiť sa</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Používateľské meno alebo email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Vytvoriť nový účet</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Resetovať heslo</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Email nenájdený</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Zabudnuté heslo</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Prihlasovacia stránka</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Týždeň začínajúci %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>VYSIELANÍ REČNÍCI</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>Tento víkend je preč %1 rečník</numerusform>
            <numerusform>Tento víkend sú preč %1 rečníci</numerusform>
            <numerusform>Tento víkend je preč %1 rečníkov</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Tento víkend nie je preč žiaden rečník</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Program</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Pracovné zošity</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Plán vysielaných rečníkov</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Úlohy vysielaných rečníkov</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Tlač</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Pieseň a modlitba</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pieseň č. %1 a modlitba</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>VEREJNÁ PREDNÁŠKA</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>Štúdium Strážnej veže</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Pieseň č. %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vedie</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Číta</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importovať SV...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Zbor</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Rečník</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefón</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hostiteľ</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Verejná prednáška</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Meno</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Priezvisko</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brat</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sestra</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Menovaný</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Rodina</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Členovia rodiny prepojení s</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktné informácie</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefón</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Všetky triedy</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Iba hlavná trieda</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Iba ďalšie triedy</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Hlava rodiny</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktívny</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsedajúci</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Poklady z Božieho slova</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Čítanie Biblie</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Prvý rozhovor</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Opätovná návšteva</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblické štúdium</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Body v časti Život kresťana</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Zborové študium Biblie</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Číta na Zborovom štúdiu Biblie</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nový zvestovateľ</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitba</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Hostiteľ pre hosťujúcich rečníkov</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Zhromaždenie v týždni</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Prejav</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Zhromaždenie cez víkend</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Dozorca štúdia Strážnej veže</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Čitateľ na štúdiu Strážnej veže</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Zvestovatelia</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Zoznam položiek na výber</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Odhlásiť</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Verzia</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase domovská stránka</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Spätná väzba</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Naposledy synchronizované: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Program</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Zobraziť čas</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Zobraziť trvanie</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Používateľské rozhranie</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prihlásiť sa</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Meno</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchronizujem...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Predsedajúci</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pieseň</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Predsedajúci zhromaždenia cez víkend</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Pieseň</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Pieseň k Strážne veži</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Vydanie Strážnej veže</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Študijný článok č.</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Vedie</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Číta</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Časová os</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Počet týždňov pred zvoleným dátumom</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Počet týždňov po zvolenom dátume</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>týždne</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Počet týždňov sivou farbou po úlohe</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Rovnaké zmeny boli nájdené lokálne a v cloud-e (%1 záznamov). Chcete zachovať lokálne zmeny?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Údaje v cloude boli odstránené. Tvoje lokálne údaje budú nahradené. Pokračovať?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Neprideliť ďalší rečnícky znak</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nenastavené</translation>
    </message>
</context></TS>