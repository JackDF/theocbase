<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Název</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Datum</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nepřidělovat další znak</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Ponechat aktuální znak</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Neplatná data</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Přidat časování?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Student</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Pomocník</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Splněno</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Dobrovolník</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Vybrat dobrovolníka</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aktuální znak</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Cvičení splněno</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Další znak</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Vybrat další znak</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Časování</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Podrobnosti školy</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Spustit stopky</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zastavit stopky</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Rámec</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Výběr rámce</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>No exception</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Circuit overseer&apos;s visit</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Circuit assembly</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Regional convention</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Memorial</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Other exception</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Week starting %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Dny shromáždění</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Shromáždění v týdnu</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Shromáždění o víkendu</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vede</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Řečník</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čte</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detail</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Poznámky</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>Poklady z božího Slova</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>KAZATELSKÁ SLUŽBA</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ŽIVOT KŘESŤANA</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Předsedající</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Rádce</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Píseň č. %1 a modlitba</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Píseň</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vede</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čte</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Úvodní slova</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitba</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Harmonogram importu...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>MH</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Závěrečné poznámky</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Shromáždění v týdnu</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Ponechat aktuální znak</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Spustit stopky</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zastavit stopky</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Přidat časování?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Student</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Splněno</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Časování</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aktuální znak</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Cvičení splněno</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Další znak</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Vybrat další znak</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Pomocník</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Dobrovolník</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stopky</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Partnerem by neměla být osoba opačného pohlaví.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Studie</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detaily</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Táhnout pro obnovení...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Uvolnit pro obnovení...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Přihlásit</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Uživatelské jméno nebo email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Nový účet</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reset Hesla</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>E-mailová adresa nenalezena!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Zapomenuté heslo</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Přihlašovací stránka</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Týden od %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Poznámky</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Vyslaní řečníci</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>Tento týden chybí jeden řečník</numerusform>
            <numerusform>Tento týden chybí %1 řečníci</numerusform>
            <numerusform>Tento týden chybí %1 řečníků</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Tento týden nechybí žádný řečník</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Print Options</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Program</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Pracovní sešity</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Rozpis cestujících řečníků</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Přiřazení chybějících řečníků</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Call List and Hospitality Schedule</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Přednášky řečníků</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Shromáždění v týdnu</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Shromáždění o víkendu</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Combination</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Assignment Slips</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Template</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Assignment Slips for Assistant</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Print Assigned Only</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Text size</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Custom Templates</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Píseň a modlitba</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Píseň %1 a modlitba</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>Veřejná přednáška</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>Studium Strážné věže</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Píseň č. %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vede</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čte</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Import SV...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Víkendové shromáždění</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Shromáždění v týdnu</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Sbor</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Řečník</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informace</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>VEŘEJNÁ PŘEDNÁŠKA</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Bratr</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sestra</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Jmenovaný</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Rodina</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Rodinný příslušník spojený s</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktní informace</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Všechny třídy</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Pouze hlavní třída</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Pouze další třídy</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Hlava rodiny</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Pomocník</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktivní</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Předsedající</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Poklady z Božího Slova</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Čtení Bible</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>První rozhovor</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Opětovná návštěva</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblické studium</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Proslovy v části Život křesťana</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Sborové studium Bible</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Čtení na sborovém studiu Bible</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nový zvěstovatel</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitba</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Hostitel pro řečníka</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Shromáždění v týdnu</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Proslov</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Víkendové shromáždění</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Vední Strážné věže</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Čtenář pro Strážnou věž </translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Duchovní drahokamy</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Společný rozhovor o videu</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Zvěstovatelé</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Seznam pro výběr</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Nastavení</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Odhlásit</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Domácí stránka TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Zpětná vazba</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Naposledy synchronizováno: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Program</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Zobrazit čas</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Zobrazit trvání</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Uživatelské rozhraní</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Přihlášení</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Zobrazit názvy písní</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Printing</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Custom Templates</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchronizuji...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Předsedající</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Píseň</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Předsedající o víkendu</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Píseň</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Píseň k SV</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitba</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Vydání Strážné věže</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Studijní článek č.</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Vede</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Čte</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Časová osa</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Počet týdnů před zvoleným datumem</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Počet týdnů po zvoleném datumu</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>týdny</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Počet týdnů šedivou barvou po úkolu</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Stejné změny lze nalézt jak lokálně, tak i v cloudu (%1 řádků). Chcete zachovat lokální změny?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>V cloudu je nová databáze. Tvoje lokální údaje budou nahrazeny. Pokračovat?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nepřidělovat další znak</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nenastaveno</translation>
    </message>
</context></TS>