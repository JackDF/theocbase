<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nie przydzielaj kolejnej lekcji</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Pozostaw aktualną lekcję</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Nieprawidłowe dane</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Czy dodać informacje o czasie?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Materiał</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Uczestnik Szkoły</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Pomocnik/-ca</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ocena</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Zaliczone</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Ochotnik</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Wybierz ochotnika</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aktualna lekcja</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ćwiczenia zostały wykonane</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Następna lekcja</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Wybierz następną lekcję</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Uwagi</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Uruchom stoper</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zatrzymaj stoper</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Tło</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Wybierz tło</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Tydzień od %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Zebranie w tygodniu</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Zebranie w weekend</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Źródło</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Prowadzący</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Mówca</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektor</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Uwagi</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>SKARBY ZE SŁOWA BOŻEGO</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>ULEPSZAJMY SWĄ SŁUŻBĘ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>CHRZEŚCIJAŃSKI TRYB ŻYCIA</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Przewodniczący</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Doradca</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pieśń %1 i Modlitwa</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pieśń</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Prowadzący</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektor</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Uwagi Wstępne</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitwa</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Zaimportuj Program...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Uwagi końcowe</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Pozostaw aktualną lekcję</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Uruchom stoper</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zatrzymaj stoper</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Czy dodać informacje o czasie?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Źródło</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Uczestnik Szkoły</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ocena</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Zaliczone</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Aktualna lekcja</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ćwiczenia zostały wykonane</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Następna lekcja</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Wybierz następną lekcję</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Uwagi</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Pomocnik/-ca</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Ochotnik</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stoper</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Pomocnik nie powinien być tej samej płci</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lekcja</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Szczegóły</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Przeciągnij by zaktualizować...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Upuść by zaktualizować...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Nazwa Użytkownika</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Zaloguj</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Nazwa Użytkownika lub Email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Załóż Konto</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Resetuj Hasło</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Brak adresu email!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Zapomniałem Hasła</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Strona Logowania</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Tydzień od %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>MÓWCY WYJEZDNI</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Grafik</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Arkusze</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Grafik Mówców Wyjeżdżających</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Drukuj</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Zebranie w tygodniu</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Zebranie w weekend</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Łączony</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Kartki z przydzielonymi zadaniami</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Szablon</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Kartki z przydziałem zadania dla pomocnika/-cy</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Drukuj tylko przydzielone</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Wielkość tekstu</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Szablony użytkownika</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Pieśń i Modlitwa</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pieśń %1 i Modlitwa</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>WYKŁAD PUBLICZNY</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>STUDIUM STRAŻNICY</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Pieśń %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Prowadzący</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektor</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importuj Str..</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Zbór</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Mówca</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Tel komórkowy</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Gospodarz</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Wykład publiczny</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brat</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Siostra</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Usługujący</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Rodzina</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Członek rodziny powiązany z</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Informacje Kontaktowe</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Wszystkie Klasy</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Tylko Klasa Główna</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Tylko Klasy Dodatkowe</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Głowa Rodzina</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Pomocnik/-ca</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktywuj</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Przewodniczący</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Skarby ze Słowa Bożego</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Czytanie Biblii</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Pierwsza Rozmowa</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Odwiedziny Ponowne</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Studium Biblijne</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Chrześcijański Tryb Życia - Przemówienia</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Zborowe Studium Biblii</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lektor na Zb. Studium Biblii</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nowy głosiciel</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitwa</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Gospodarz dla mówcy</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Tel komórkowy</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Zebranie w tygodniu</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Przemówienie</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Zebranie w weekend</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Prowadzący Studium Strażnicy</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lektor na studium Strażnicy</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Głosiciele</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Tła</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Wyloguj</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Strona WWW TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Opinie</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Ostatnia synchronizacja: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Grafik</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Pokazuj Czas</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Pokazuj Długość</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Interfejs użytkownika</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>język</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Imię i nazwisko</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Pokazuj Tytuły Pieśni</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Drukowanie</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Szablony użytkownika</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchronizacja...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Przewodniczący</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pieśń</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Przewodniczący zebrania w tygodniu</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Pieśń</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Modlitwa</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Wydanie Strażnicy</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artykuł nr</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Temat</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Prowadzący</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lektor</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Terminarz</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Znaleziono te same zmiany zapisane lokalnie i w chmurze ( w %1 wierszach). Czy chcesz zachować zmiany zapisane lokalnie?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nie przydzielaj kolejnej lekcji</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nieustawiony</translation>
    </message>
</context></TS>