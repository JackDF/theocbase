<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Date</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ne pas spécifier la prochaine leçon</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Continuer avec la même leçon</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Donnée invalide</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ajouter la durée ?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Élève</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Terminée</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontaire</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Sélectionner un volontaire</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Leçon en cours</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercices faits</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Prochaine leçon</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Sélectionnez la prochaine leçon</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Détails de l&apos;école</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Démarrer le chronomètre</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Arrêter le chronomètre</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Cadre</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Sélectionnez le cadre</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>No exception</source>
        <translation>Sans exception</translation>
    </message>
    <message>
        <source>Circuit overseer&#x27;s visit</source>
        <translation>Visite du responsable de circonscription</translation>
    </message>
    <message>
        <source>Circuit assembly</source>
        <translation>Assemblée de circonscription</translation>
    </message>
    <message>
        <source>Regional convention</source>
        <translation>Assemblée régionale</translation>
    </message>
    <message>
        <source>Memorial</source>
        <translation>Mémorial</translation>
    </message>
    <message>
        <source>Other exception</source>
        <translation>Autre exception</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Semaine du %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Jours de réunion</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Détails</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <comment>Page title</comment>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>JOYAUX DE LA PAROLE DE DIEU</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>APPLIQUE-TOI AU MINISTÈRE</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VIE CHRÉTIENNE</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Conseiller</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cantique %1 et Prière</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importer le programme..
</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>S1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>S2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Paroles de conclusion</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Travailler la même leçon</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Démarrer le chronomètre</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Arrêter le chronomètre</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ajouter la durée?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Élève</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Terminée</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Leçon à travailler actuellement</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercices faits</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Prochaine leçon</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Choisir la prochaine leçon</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontaire</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Arrêter le chronomètre</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>L&apos;interlocuteur ne devrait pas être quelqu&apos;un de l&apos;autre sexe.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Détails</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Tirer pour actualiser...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Relâcher pour actualiser...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Nom d’utilisateur ou email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>e-mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Créer un compte</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>réinitialiser le mot de passe</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>L&apos;adresse électronique est inconnue!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Mot de passe oublié</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Page de connexion</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Semaine du %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ORATEURS SORTANTS</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 orateur sortant ce week-end</numerusform>
            <numerusform>%1 orateurs sortant ce week-end</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Pas d&apos;orateurs sortant ce week-end</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Options d&apos;impression</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programme</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Feuilles de travail</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Programme des orateurs sortants</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Attributions des orateurs sortants</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Liste d&apos;appels et gestion de l’hospitalité</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Discours des orateurs</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Combinaison</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Fiches d&apos;attribution d&apos;exposé</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Modèle</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Fiches d’attribution d&apos;exposé pour les interlocuteurs</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Imprimer uniquement les attributions assignées</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Taille du texte</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Modèles personnalisés</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Cantique et Prière</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cantique %1 et Prière</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISCOURS PUBLIC</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ÉTUDE DE LA TOUR DE GARDE</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Cantique %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importer la TG (ePub)</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Discours public</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Frère</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sœur</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Frère nommé</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Famille</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Membre de la famille de</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Toutes les classes</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Salle principale uniquement</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Salles secondaires uniquement</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Chef de Famille</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Joyaux de la Parole de Dieu</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Lecture de la Bible</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Premier contact</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Nouvelle visite</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Cours biblique</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Discours Vie chrétienne</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lecteur de l&apos;Étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nouveau proclamateur</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Reçoit les orateurs publics</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Discours</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Conducteur de la Tour de Garde</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lecteur de l&apos;étude de la Tour de Garde</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Perles spirituelles</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Discussion avec vidéo</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Proclamateurs</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Liste de sélection</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Page d&apos;accueil TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Avis</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Synchronisé le: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programme</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Afficher l&apos;heure</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Afficher la durée</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Interface Utilisateur</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Voir le titre des cantiques</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Modèles personnalisés</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchronisation...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Président de la réunion de week-end</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Cantique</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Édition de La Tour de Garde</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Article</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Conducteur</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lecteur</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Chronologie</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Nombre de semaines avant la date sélectionnée</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Nombre de semaines après la date sélectionnée</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semaines</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Nombre de semaines grisées après une attribution</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Les mêmes modifications sont présentes localement et dans le &quot;cloud&quot; (%1 lignes). Voulez-vous conserver les modifications locales ?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Les données du nuage ont été réinitialisées. Vos données locales vont être remplacées. Voulez-vous continuer ?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ne pas attribuer la prochaine leçon</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Non défini</translation>
    </message>
</context></TS>