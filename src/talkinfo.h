#ifndef TALKINFO_H
#define TALKINFO_H

enum MeetingSection {
    TreasuresFromGodsWord,
    ApplyYourselfToTheFieldMinistry,
    LivingAsChristians,
    PublicTalk,
    Watchtower
};

class talkinfo
{
public:
    talkinfo(int meeting, MeetingSection meetingSection, bool canCounsel);

    int meeting() const; // 0=midweek, 1=weekend
    MeetingSection meetingSection() const;
    bool canCounsel() const;

private:
    int _meeting;
    MeetingSection _meetingSection;
    bool _canCounsel;
};

#endif // TALKINFO_H
