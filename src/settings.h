/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtWidgets>
#include <QDebug>
#include <QComboBox>
#include <QString>
#include <QQmlContext>
#include <QQuickItem>
#include <QCryptographicHash>
#include <QIcon>
#include "sql_class.h"
#include "accesscontrol.h"
#include "importwizard.h"
#include "smtp/SmtpMime"
#include "school.h"
#include "speakersui.h"
#include "ccongregation.h"
#include "googlemediator.h"
#include "lmm_meeting.h"
#include "lmm_schedule.h"
#include "talkTypeComboBox.h"
#include "dateeditbox.h"
#include "general.h"
#include "lmmtalktypeedit.h"
#include "cloud/cloud_controller.h"
#include "cterritories.h"
#include "importlmmworkbook.h"
#include "lmmworksheetregex.h"
#include "applyyourselfimport.h"
#include "simplecrypt.h"
#include "general.h"

extern QTranslator translator;
extern QTranslator qtTranslator;
extern QString theocbaseDirPath;

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

    class PublicTalksSFProxyModel : public QSortFilterProxyModel
    {
    public:
        PublicTalksSFProxyModel(QObject *parent = 0);
        void setDiscontinuedHidden(bool value);
        bool getIsDiscontinuedHidden() { return isDiscontinuedHidden; }

    protected:
        bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const Q_DECL_OVERRIDE;

    private:
        bool isDiscontinuedHidden;
    };

public:
    Settings(QWidget *parent = 0);
    ~Settings();
    void setCloud(cloud_controller *c);

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::Settings *ui;
    sql_class *sql;
    AccessControl *ac;

    void applyAuthorizationRules();
    void showGeneralPage();
    void showDatesPage();
    void showExceptionsPage();
    void showLMMPage();
    void showSchoolPage();
    void showPublicTalkPage();
    void showSongPage();
    void showTerritoriesPage();
    void showPrintPage();
    void showReminderPage();
    void showAccessControlPage();

    //void loadSchoolProgram();
    void loadSchoolStudies();
    //void loadSchoolSettings();
    void loadPublicTalks();
    void loadSpeakers();
    void loadSongs();
    void loadCities();
    void loadTerritoryTypes();
    void loadStreetTypes();
    void loadAddressTypes();
    void loadUsers();
    void loadRules();
    void handleComboPrintTemplate(QComboBox *, int, QLabel *, QString type = "");
    void fillComboBox(QComboBox *, QLabel *, QDir *, QString, QString);
    void loadLMMMeeting();
    void loadLMMSchedule();
    QTableWidgetItem *newAlignedTableWidgetItem(QString data, int alignment);

    bool esitelmatCellUpdate;
    bool songCellUpdate;
    bool citiesCellUpdate;
    bool territoryTypesCellUpdate;
    bool streetTypesCellUpdate;
    bool addressTypesCellUpdate;
    bool schoolCellUpdate;
    bool usersCellUpdate;
    bool rulesCellUpdate;
    bool psw_Changed;
    void loadAssignmentSlipImage();
    QList<QPair<int, QString>> l;

    bool saveGeneralPage;
    bool saveExceptionPage;
    bool saveLMMPage;
    bool savePublicTalkPage;
    bool saveSongPage;
    bool savePrintPage;
    bool saveTerritoryPage;
    bool saveRemindersPage;
    bool saveAccessControlPage;
    googleMediator *mediator;
    int lmmYear;
    QHash<QDate, int> lmmdateToIdx;
    QDate nextLMMDate;
    bool isFirstWeek;
    LMM_Meeting currentLMMMeeting;
    QFrame *calPopup;
    QDateEdit *oDate;
    QStandardItemModel *publicTalksTable;
    PublicTalksSFProxyModel *publicTalksProxyModel;
    cloud_controller *cloud;
    void reloadSettings();

    struct EmailAccount {
        EmailAccount(QString _name, QString _host, int _port, SmtpClient::ConnectionType _connType) :
            name(_name), host(_host), port(_port), connType(_connType) {}
        QString name;
        QString host;
        int port;
        SmtpClient::ConnectionType connType;
    };
    QList<EmailAccount> emailAccounts;

private slots:
    void on_listWidget_clicked(QModelIndex index);
    void on_buttonCloseDialog_clicked();
    void on_comboBoxUILanguage_currentIndexChanged(int index);
    void on_lineEdit_psw2_editingFinished();
    void on_checkBox_md5_clicked();
    void on_buttonRemoveStudies_clicked();
    void on_tableWidget_SchoolStudyPoints_itemChanged(QTableWidgetItem *item);
    void on_buttonAddPublicTalks_clicked();
    void on_toolButtonRemovePublicTalk_clicked();
    void on_publicTalksTable_itemChanged(QStandardItem *item);
    void on_buttonAddOnePublicTalk_clicked();
    void on_buttonAddCongSpeakers_clicked();
    void on_buttonRestoreBackup_clicked();
    void on_buttonSaveBackup_clicked();
    void on_buttonAddException_clicked();
    void on_buttonRemoveException_clicked();
    void on_comboBoxException_currentIndexChanged(int index);
    void on_dateEditException1_dateChanged(QDate date);
    void on_buttonAddStudies_clicked();
    void on_pushButtonEmailTest_clicked();
    void on_btnJumpToCongregationDayTime_clicked();
    void on_btnImportLMM_clicked();
    void on_btnCopyTMStoLMM_clicked();
    void on_btnImportSQL_clicked();
    void on_btnAddLMMMeeting_clicked();
    void on_btnRemoveLMMMeeting_clicked();
    void on_btnAddLMMSchedule_clicked();
    void on_btnRemoveLMMSchedule_clicked();
    void on_cboLMMYear_activated(const QString &arg1);
    void on_gridLMMMeeting_itemChanged(QTableWidgetItem *item);
    void on_gridLMMMeeting_currentItemChanged(QTableWidgetItem *, QTableWidgetItem *);
    void on_gridLMMMeeting_itemSelectionChanged();
    void on_gridLMMSchedule_itemChanged(QTableWidgetItem *item);
    void on_gridLMMSchedule_itemSelectionChanged();
    void on_btnBrowseCustomTemplates_clicked();
    void on_toolButtonAddSongs_clicked();
    void on_toolButtonRemoveSong_clicked();
    void on_toolButtonAddOneSong_clicked();
    void on_tableWidgetSongs_cellChanged(int row, int column);    
    void addMidweekMeetingSchedule(QAction *action);

    void on_tableWidgetCities_cellChanged(int row, int column);
    void on_toolButtonAddCity_clicked();
    void on_toolButtonRemoveCity_clicked();
    void on_tableWidgetTerritoryTypes_cellChanged(int row, int column);
    void on_toolButtonAddTerritoryType_clicked();
    void on_toolButtonRemoveTerritoryType_clicked();
    void on_checkBoxHideDiscontinued_stateChanged(int arg1);
    void on_tableWidgetSectionClicked(int index);
    void on_tableWidgetStreetTypes_cellChanged(int row, int column);
    void on_pushButtonNewStreetTypeColor_clicked();
    void on_toolButtonAddStreetType_clicked();
    void on_toolButtonRemoveStreetType_clicked();
    void on_tableWidgetAddressTypes_cellChanged(int row, int column);
    void on_toolButtonAddAddressType_clicked();
    void on_toolButtonRemoveAddressType_clicked();
    void on_pushButtonNewAddressTypeColor_clicked();
    void on_btnOpenDBLocation_clicked();
    void on_btnRemoveDuplicatesLMM_clicked();
    void on_toolButtonAddUser_clicked();
    void on_toolButtonRemoveUser_clicked();
    void on_userRoleAssignmentWidget_stateChanged(int index);
    void on_comboEmailAccounts_activated(int index);

signals:
    void discontinueTalk(int themeId);
};

#endif // SETTINGS_H
