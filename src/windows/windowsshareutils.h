/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef WINDOWSSHAREUTILS_H
#define WINDOWSSHAREUTILS_H

#include "../shareutils.h"
#include <QDebug>
#include <QWindow>

class WindowsShareUtils : public PlatformShareUtils
{
public:
    WindowsShareUtils(QObject *parent = nullptr);
    void share(const QString shareText, const QString email, QPoint pos) override;

private:
    bool initShareSheet(quintptr id, const QString shareText);
};

#endif // WINDOWSSHAREUTILS_H
