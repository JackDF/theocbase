#include "machelper.h"
#include <Appkit/AppKit.h>
#include <AppKit/NSToolbar.h>
#include <AppKit/NSView.h>
#include <AppKit/NSWindow.h>
#include <QDebug>

void MacHelper::initToolbar(QMacToolBar *toolbar)
{
    qDebug() << "init ok";
    [toolbar->nativeToolbar() setDisplayMode:NSToolbarDisplayModeIconOnly];
}

void MacHelper::colorizeTitleBar(QWindow *qtwindow)
{
    NSView *view = (NSView *)qtwindow->winId();
    NSWindow *window = [view window];
    //window.backgroundColor = [NSColor colorWithCalibratedRed:(35/255.0f) green:(54/255.0f) blue:(94/255.0f) alpha:0.1];
    window.titlebarAppearsTransparent = true;
    window.styleMask |= NSWindowStyleMaskFullSizeContentView;
    window.titleVisibility = NSWindowTitleHidden;
}
