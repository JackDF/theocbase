/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOL_H
#define SCHOOL_H

#include <QObject>
#include <QString>
#include <QStandardItemModel>
#include "school_item.h"
#include <vector>
#include <memory>
#include "sql_class.h"
#include "ccongregation.h"
#include "cpersons.h"
#include "school_setting.h"
#include "schoolstudy.h"

/**
 * @brief The school class - The school class is base class for school functions
 *        TODO: Create a new class for theme
 */
class school
{
public:
    school();
    ~school();

    /**
     * @brief getProgram - Get school assignments on a one week
     * @param dest - List of assignments
     * @param date - First date of week
     * @return - success or not
     */
    bool getProgram(std::vector<std::unique_ptr<school_item> > &dest,const QDate &date);

    /**
     * @brief getStudents - Get grid list of students (columns are full name, last date of assignment, last date of selected assignment and notice)
     *                      This is used when assigning a new student for talk.
     * @param assignmentnumber - Assignment number (0 [highlights], 1,2,3 or 10 [review reader] )
     * @param date - First day of week
     * @param classnumber - Class number (1, 2 or 3)
     * @param selected - Selected student (will be shown in bold font)
     * @param onlyBrothers - Show only brothers
     * @return - Model list of students
     */
    QStandardItemModel *getStudents(int assignmentnumber, QDate date, int classnumber, QString selected = "", bool onlyBrothers = false);

    /**
     * @brief getAssistant - Get grid list of assistants (columns are full name, last date of all assignment, last date of assistant assignment,
     *                       last date when worked together with student and notice).
     *                       This is used when choosing an assistant
     * @param date - First day of week
     * @param studentid - Student id
     * @param classnumber - Class number (1, 2 or 3)
     * @param selected - Selected assistant (will be shown in bold font)
     * @return - Model list of assistants
     */
    QStandardItemModel *getAssistant(int assignmentnumber, QDate date, int studentid, int classnumber, QString selected = "", bool brother = false);

    /**
     * @brief getStudies - Get all studies
     * @return - List of studies
     */
    QList<schoolStudy *> getStudies();

    /**
     * @brief getStudentStudies - Get student's studies
     * @param personid - person id number in database
     * @param studynumber
     * @return - List of studies
     */
    QList<schoolStudentStudy *> getStudentStudies(int personid, int studynumber = 0);

    /**
     * @brief getActiveStudyPoint - Get student's current study
     * @param personid - Student's id in database
     * @param number - Assignment number
     * @param createnew - To create a new study if opened studies not found
     * @return - schoolStudentStudy object
     */
    schoolStudentStudy *getActiveStudyPoint(int personid, school_item::AssignmentType type, bool createnew = true,
                                            QDate meetingdDate = QDate());

    /**
     * @brief getStudyUsed - Get study that used in specific assignment
     * @param personid - Student's id in database
     * @param meetingday - First day of week
     * @param type - Assignment type
     * @return - schoolStudentStudy object
     */
    schoolStudentStudy *getStudyUsed(int personid, QDate firstdayofweek, school_item::AssignmentType type);

    /**
    * @brief addStudy Insert a new study for student
    * @param personid - Student's id in the database
    * @param st - The schoolStudy object is to be saved (but empty id)
    * @return - The schoolStudy object with store id
    */
   schoolStudentStudy *addStudentStudy(int personid, schoolStudentStudy *st);

    int getTheme(QDate date, int number);

    int addTheme(QDate date, int number, QString theme, QString source, bool onlybrothers);

    QList<school_setting *> getAllSettings();
    /**
     * @brief getAllSettings This function gets all settings
     * @return QList of school_setting objects
     */
    QList<school_setting *> getAllSettings(person::Gender g);

    /**
     * @brief deleteAllSettings Delete all settings
     *         Note! This function puprose is delete invalid data from database
     * @return true is success
     */
    bool deleteAllSettings();

    /**
     * @brief getClassesQty - Get quantity of classes
     *        1 = Only main class
     *        2 = Main class and a one aux. class
     *        3 = Main class and two aux. classes
     * @return - quantity
     */
    int getClassesQty();

    /**
     * @brief setClassesQty - Set quantity of classes
     * @param value - Quantity
     */
    void setClassesQty(int value);

    /**
     * @brief getSchoolDay - Get day of school
     *                       Monday = 1, Tuesday = 2 etc.
     * @return - Day number
     */
    int getSchoolDay();

    /**
     * @brief setSchoolDay - Set day of school
     *                       Monday = 1, Tuesday = 2 etc.
     * @param value - Day number
     */
    void setSchoolDay(int value);

    /**
     * @brief getNoSchool - Is aux. school on the selected date?
     * @param date
     * @param classno
     * @return
     */
    bool getNoSchool(QDate date, int classno);

    /**
     * @brief setNoSchool - Set aux. school to be held or not on selected date
     * @param date
     * @param classno
     * @param noschool
     */
    void setNoSchool(QDate date, int classno, bool noschool);

private:
    QString getShortAssignmentName(int number, bool assistant = false);
    QList<school_setting *> settingsQuery(QString sqlQuery);
    // member variables
    sql_class *sql;
    ccongregation c;
    QDate m_firstdayofweek;
    int m_ClassesQty;
    int m_schoolday;

public slots:

signals:

};

#endif // SCHOOL_H
