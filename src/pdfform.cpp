#include "pdfform.h"

pdfform::pdfform() {}

void pdfform::append(field* f) {fields.append(f);}

QString pdfform::fdfvalue() {
    QString value("%FDF-1.2\r\n1 0 obj\r\n<</FDF<</Fields[");
    field* f;
    foreach (f, fields) {
        value+=f->fdfvalue();
    }
    value+="]>>>>\r\nendobj\r\ntrailer\r\n<</Root 1 0 R>>\r\n%%EOF\r\n";

    return value;
}


pdfform::field::field(QString name, bool useParens):name(name), useParens(useParens) {}

QString pdfform::field::Name() const { return name; }

QString pdfform::field::value() {return "";}

QString pdfform::field::fdfvalue() {
    return
            "<</T(" +
            name +
            ")/V" +
            (useParens ?
                // non-unicode: "(" +  value().replace("\\","\\\\").replace("(","\\(").replace(")","\\)") + ")" :
                "<" + convertUnicode16BE(value().replace("\\","\\\\").replace("(","\\(").replace(")","\\)")) + ">" :
                value()
            ) +
            ">>";
}

QString pdfform::field::convertUnicode16BE(QString txt) {
    QString ret("FEFF");
    for (QChar c : txt) {
        ushort iC = c.unicode();
        ret.append(QString::number((iC & 0xF000)>>12, 16));
        ret.append(QString::number((iC & 0x0F00)>>8 , 16));
        ret.append(QString::number((iC & 0x00F0)>>4 , 16));
        ret.append(QString::number((iC & 0x000F)    , 16));
    }
    return ret;
}



pdfform::checkbox::checkbox(QString name):field(name, false), ckvalue(false) {}

bool pdfform::checkbox::getValue() {return ckvalue;}

void pdfform::checkbox::setValue(bool v) {ckvalue = v;}

QString pdfform::checkbox::value() {return ckvalue ? "/Yes" : "/Off";}


pdfform::textbox::textbox(QString name):field(name, true) {}

QString pdfform::textbox::getValue() {return txtvalue;}

void pdfform::textbox::setValue(QString v) {txtvalue = v;}

QString pdfform::textbox::value() {return txtvalue;}
