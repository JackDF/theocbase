#include "ceditablecombobox.h"

cEditableComboBox::cEditableComboBox(QWidget *parent) :
    QComboBox(parent),
    state(idle),
    _autoAddNewDuringSet(false)
{
    ignoreTimer.start();
    connect(this, SIGNAL(activated(int)), this, SLOT(_activated(int)));
    connect(this,SIGNAL(editTextChanged(QString)), this, SLOT(_editTextChanged(QString)));
    setMaxVisibleItems(20);
}

void cEditableComboBox::hidePopup()
{
    state = idle;
    ignoreTimer.restart();
    emit popupClosed(this);
    QComboBox::hidePopup();
}


void cEditableComboBox::showPopup()
{
    QString name = this->objectName();
    bool filterRequested(state == filtered);
    state = popup;
    emit beforeShowPopup(name, this, filterRequested);
    QComboBox::showPopup();
}

void cEditableComboBox::setCurrentText(QString value)
{
    int idx(this->findText(value,Qt::MatchExactly));
    if (idx < 0){
        if(value.isEmpty()) {
            this->setCurrentIndex(-1);
        } else if (_autoAddNewDuringSet){
            this->addItem(value);
        } else {
            QComboBox::setCurrentText(value);
        }
    } else {
        this->setCurrentIndex(idx);
    }
}

bool cEditableComboBox::getAutoAddNewDuringSet() const
{
    return _autoAddNewDuringSet;
}

void cEditableComboBox::setAutoAddNewDuringSet(const bool value)
{
    _autoAddNewDuringSet = value;
}

cEditableComboBox::states cEditableComboBox::getState() const
{
    return state;
}

void cEditableComboBox::setState(const states &value)
{
    state = value;
    if (state == idle)
        ignoreTimer.restart();
}

bool cEditableComboBox::okIgnore()
{
    return ignoreTimer.elapsed() < 500;
}

void cEditableComboBox::resetIgnoreTimer()
{
    ignoreTimer.restart();
}

void cEditableComboBox::focusInEvent(QFocusEvent *e)
{
    emit focusIn(this);
    QComboBox::focusInEvent(e);
}

void cEditableComboBox::focusOutEvent(QFocusEvent *e)
{
    states oldState = state;
    // ignore if popping up. Since the popup is getting the focus, "this" is still mostly in focus.
    if (oldState != popup && oldState != leaving) {
        state = leaving;
        emit focusOut(this, oldState);
    }
    QComboBox::focusOutEvent(e);
    state = idle;
}

void cEditableComboBox::_activated(int index)
{
    Q_UNUSED(index);
    state = idle;
}

void cEditableComboBox::_editTextChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    if (state != popup)
        state = filtered;
}
