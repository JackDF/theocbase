/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    id: rowitem
    width: 400
    height: visible ? 40 : 0
    Layout.fillWidth: true
    Layout.preferredHeight: visible ? 40 : 0

    property alias timeText: textboxtext
    property alias timebox: textbox
    property alias themeText: textTheme
    property alias nameText1: textSpeaker
    property alias nameText2: textAssistant    
    property bool clickable: false
    property bool editable: false
    property int fontsize: defaultfontsize
    property alias button1: editButton
    property alias button2: editButton2
    property alias button3: editButton3
    property alias button4: editButton4
    property alias button1icon: editButton.icon.source
    property alias button2icon: editButton2.icon.source
    property alias button3icon: editButton3.icon.source
    property alias button4icon: editButton4.icon.source
    property string button1tooltip: ""
    property string button2tooltip: ""
    property string button3tooltip: ""
    property string button4tooltip: ""
    property bool rowHovered: listRowMouseArea.containsMouse || button1.hovered

    signal clicked()
    signal clicked2()
    signal clicked3()

    Rectangle {
        anchors.fill: parent
        color: "grey"
        opacity: listRowMouseArea.containsMouse || editButton.hovered ||
                 editButton2.hovered || editButton3.hovered || editButton4.hovered ? 0.1 : 0
        MouseArea {
            anchors.fill: parent
            id: listRowMouseArea
            hoverEnabled: true
            visible: editable
            onClicked: {
                if (mouseX >= (width - 40))
                    rowitem.clicked()
                mouse.accepted = false
            }
        }
    }

    GridLayout {
        id: gridLayout1
        rowSpacing: 0
        rows: 2
        columns: 3
        anchors.fill: parent
        anchors.leftMargin: 20

        Rectangle {
            id: textbox
            color: "transparent"
            Layout.rowSpan: 2
            Layout.preferredHeight: 20
            Layout.preferredWidth: 20
            radius: 2
            Text {
                id: textboxtext
                color: "white"
                text: "10"
                anchors.fill: parent
                anchors.margins: 10
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                minimumPointSize: 8
                fontSizeMode: Text.HorizontalFit
                wrapMode: Text.WordWrap
            }
        }

        Text {
            id: textTheme
            text: ""
            Layout.fillWidth: true
            Layout.rowSpan: 2
            font.pointSize: fontsize
            elide: Text.ElideRight
            Layout.preferredHeight: gridLayout1.height
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
        }

        ColumnLayout {
            Layout.rowSpan: 2
            Layout.topMargin: 4
            Layout.bottomMargin: 4
            Layout.fillHeight: true
            spacing: 1
            Layout.rightMargin: 45
            Text {
                id: textSpeaker
                text:  ""
                verticalAlignment: Text.AlignVCenter
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: fontsize
                visible: text !== ""
            }
            Text {
                id: textAssistant
                text: ""
                verticalAlignment: Text.AlignVCenter
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                font.pointSize: fontsize
                visible: text !== ""
            }
        }
    }

    RowLayout {
        id: buttonsRow
        //anchors.verticalCenterOffset: -parent.height/2
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 0
        spacing: 0
        visible: editable
        Item { Layout.fillWidth: true }
        ToolButton {
            id: editButton4
            property string tooltipText: "Delete"
            icon.source: ""
            Layout.alignment: Qt.AlignVCenter
            width: height
            visible: icon.source != "" && (listRowMouseArea.containsMouse ||
                                           hovered || editButton.hovered || editButton2.hovered || editButton3.hovered)
            onClicked: rowitem.clicked3()
            ToolTip.delay: 1000
            ToolTip.timeout: 5000
            ToolTip.visible: hovered && button4tooltip != ""
            ToolTip.text: button4tooltip
        }
        ToolButton {
            id: editButton3
            property string tooltipText: ""
            icon.source: ""
            Layout.alignment: Qt.AlignVCenter
            width: height
            visible: icon.source != "" && (listRowMouseArea.containsMouse ||
                                           hovered || editButton.hovered || editButton2.hovered || editButton4.hovered)
            onClicked: rowitem.clicked3()
            ToolTip.delay: 1000
            ToolTip.timeout: 5000
            ToolTip.visible: hovered && button3tooltip != ""
            ToolTip.text: button3tooltip
        }
        ToolButton {
            id: editButton2
            property string tooltipText: ""
            icon.source: ""
            Layout.alignment: Qt.AlignVCenter
            width: height
            visible: icon.source != "" && (listRowMouseArea.containsMouse ||
                                           hovered || editButton.hovered || editButton3.hovered || editButton4.hovered)
            onClicked: rowitem.clicked2()
            ToolTip.delay: 1000
            ToolTip.timeout: 5000
            ToolTip.visible: hovered && button2tooltip != ""
            ToolTip.text: button2tooltip
        }
        ToolButton {
            id: editButton
            property string tooltipText: ""
            icon.source: "qrc:/icons/edit.svg"
            Layout.alignment: Qt.AlignVCenter
            width: height
            visible: (listRowMouseArea.containsMouse ||
                                           hovered || editButton2.hovered || editButton3.hovered || editButton4.hovered)
            onClicked: rowitem.clicked()
        }
    }


}
