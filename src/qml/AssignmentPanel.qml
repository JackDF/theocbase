/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import net.theocbase 1.0

Item {
    id: assignmentDialog
    property string title: "Assignment Dialog"
    width: 300
    height: 400
    property string returnValue: ""
    property LMM_Assignment currentAssignment
    property bool editableTheme: currentAssignment &&
                                 (currentAssignment.talkId == LMM_Schedule.TalkType_COTalk ||
                                  currentAssignment.talkId == LMM_Schedule.TalkType_LivingTalk1 ||
                                  currentAssignment.talkId == LMM_Schedule.TalkType_LivingTalk2 ||
                                  currentAssignment.talkId == LMM_Schedule.TalkType_LivingTalk3)

    AssignmentController { id: myController }
    ShareUtils { id: shareUtils }

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/title.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Theme")
                ToolTip.visible: hovered
            }
            TextArea {
                id: textTheme
                background: null
                text: currentAssignment ? currentAssignment.theme : ""
                font.pointSize: 11
                wrapMode: Text.WordWrap
                topPadding: 0
                bottomPadding: 0
                Layout.fillWidth: true
                font.bold: true
                selectByMouse: true
                readOnly: !editableTheme
                onEditingFinished: {
                    // save theme if CO service talk or living talk
                    if (editableTheme && currentAssignment.theme != text) {
                        currentAssignment.theme = text
                        currentAssignment.save()
                    }
                }
            }
        }

        RowLayout {
            visible: currentAssignment ? currentAssignment.source : ""
            ToolButton {
                icon.source: "qrc:/icons/wt_source.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Source")
                ToolTip.visible: hovered
            }
            TextArea {
                id: textareaSource
                Layout.fillWidth: true
                background: null
                text: currentAssignment ? currentAssignment.source : ""
                font.pointSize: 11
                readOnly: !editableTheme
                wrapMode: Text.WordWrap
                topPadding: 0
                bottomPadding: 0
                selectByMouse: true
                onEditingFinished: {
                    if (editableTheme && currentAssignment.source != text) {
                        currentAssignment.source = text
                        currentAssignment.save()
                    }
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                ToolTip.text: currentAssignment && currentAssignment.talkId === 1 ? qsTr("Chairman") : qsTr("Speaker")
                ToolTip.visible: hovered
            }
            ComboBoxTable {
                id: comboSpeaker
                currentText: currentAssignment ? currentAssignment.speakerFullName : ""
                Layout.fillWidth: true
                enabled: currentAssignment &&
                         currentAssignment.talkId != LMM_Schedule.TalkType_COTalk &&
                         currentAssignment.talkId != LMM_Schedule.TalkType_SampleConversationVideo &&
                         currentAssignment.talkId != LMM_Schedule.TalkType_ApplyYourself
                column3.title: qsTr("Selected", "Dropdown column title")
                column4.title: qsTr("All", "Dropdown column title")
                onBeforeMenuShown: {
                    //                      if (typeof model === "undefined")
                    model = currentAssignment.getSpeakerList()
                    rowTooltip: "?"
                    column2.resizeToContents()
                }
                onRowSelected: {
                    currentAssignment.speaker = id < 1 ? null : myController.getPublisherById(id)
                    currentAssignment.save()
                }
                onTooltipRequest: {
                    if (model.get(row).id === "undefined")
                        return;
                    var userid = model.get(row).id
                    rowTooltip = myController.getHistoryTooltip(userid)
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/notes.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ToolTip.text: qsTr("Note")
                ToolTip.visible: hovered
            }
            ScrollView {
                id: view
                Layout.fillWidth: true
                clip: true

                TextArea {
                    id: texteditNote
                    text: currentAssignment ? currentAssignment.note : ""
                    font.pointSize: 11
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    onEditingFinished: {
                        if (currentAssignment.note != text) {
                            currentAssignment.note = text
                            currentAssignment.save()
                        }
                    }
                }
            }
        }

        Item { Layout.fillHeight: true }

        RowLayout {
            Layout.alignment: Qt.AlignRight
            ToolButton {
                id: copyButton
                icon.source: "qrc:/icons/copy.svg"
                Layout.alignment: Qt.AlignRight
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    shareUtils.copyToClipboard(currentAssignment.getReminderText())
                }
            }

            ToolButton {
                id: shareButton
                icon.source: "qrc:/icons/share.svg"
                Layout.alignment: Qt.AlignRight
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    var pos = mapToGlobal(0, height)
                    shareUtils.share(currentAssignment.getReminderText(), "", pos)
                }
            }
        }
    }
}
