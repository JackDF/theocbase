/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2021, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import net.theocbase 1.0

Item {
    property string title: ""
    property var meeting
    property bool editable: false

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        Label {
            text: title            
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/notes.svg"
                background: null
                ToolTip.text: qsTr("Notes", "Meeting Notes")
                ToolTip.visible: hovered
            }
            TextArea{
                id: texteditNote
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: meeting ? meeting.notes : ""
                readOnly: !editable
                font.pointSize: 11

                onEditingFinished: {
                    if (meeting.notes !== text) {
                        meeting.notes = text
                        meeting.saveNotes();
                    }
                }
            }
        }
        Item { Layout.fillHeight: true }
    }
}
