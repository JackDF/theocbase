import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls 1.4 as QC1
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.12
import net.theocbase 1.0

Item {
    id: comboBoxTable
    property string currentText
    property alias model: tableView.model
    property alias combo: comboButton
    property bool showComboAlways: true
    property alias column1: col1
    property alias column2: col2
    property alias column3: col3
    property alias column4: col4
    property alias column5: col5
    property alias label: textlabel
    property string rowTooltip: ""
    signal beforeMenuShown
    signal rowSelected(var id)
    signal focusLost
    signal tooltipRequest(var row)

    Layout.preferredHeight: comboButton.height
    Layout.preferredWidth: comboButton.width
    width: 100

    onCurrentTextChanged: {
        setCurrentText(currentText)
    }
    function setCurrentText(newtext){
        if(comboButton.find(newtext) === -1)
            combomodel.append({text: newtext})
        comboButton.currentIndex = comboButton.find(newtext)
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        z: 3
        propagateComposedEvents: true
        onEntered: {
            if (!showComboAlways){
                textlabel.visible = false
                comboButton.visible = true
            }
        }
        onExited: {
            if (!showComboAlways){
                textlabel.visible = true
                comboButton.visible = false
            }
        }
    }

    Label {
        id: textlabel
        anchors.fill: parent
        text: currentText
        visible: !showComboAlways
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        color: myPalette.text
    }

    ComboBox {
        AssignmentController { id: myController }
        id: comboButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        visible: showComboAlways
        model: ListModel {
            id: combomodel
            ListElement { text: "" }
        }
        Component.onCompleted: {
            if (typeof topInset !== "undefined")
                topInset = 0
            if (typeof bottomInset !== "undefined")
                bottomInset = 0
            setCurrentText(comboBoxTable.currentText)
        }

        MouseArea {
            anchors.fill: parent
            z: 2
            onClicked: {
                if (!dropdown.visible){
                    comboBoxTable.beforeMenuShown()

                    var screen = myController.getScreenAvailableGeometry()

                    if (dropdown.height > screen.height) {
                        dropdown.height = screen.height - 10;
                    }

                    var pos = myController.getDialogPosition(dropdown.height, dropdown.width,
                                                             mouse.x, mouse.y)
                    dropdown.x = pos.x
                    dropdown.y = pos.y + height

                    if (currentText != ""){
                        tableView.selection.clear()
                        var rowindex = model.find(currentText,2)
                        tableView.selection.select(rowindex, rowindex)
                    }
                    dropdown.show()
                    dropdown.requestActivate()
                }else{
                    dropdown.close()
                }
            }
        }
    }

    Window {
        id: dropdown
        height: 200
        width:  370
        flags: Qt.Popup
        color: Material.background
        //visible: false
        SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

        onActiveChanged: {
            console.log("active changed (visible = " + visible + ")")
            if (!active) {
                console.log("lost focus")
                dropdown.close()
                focusLost()
            }
        }

        onFocusObjectChanged: {
            console.log("focus object changed")
            if(!object){
                dropdown.close()
            }
        }
        onClosing: {
            rowTooltip = ""
        }

        QC1.TableView {
            id: tableView
            width: dropdown.width
            height: dropdown.height
            focus: true
            sortIndicatorVisible: true

            onSortIndicatorColumnChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)
            onSortIndicatorOrderChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)

            selectionMode: QC1.SelectionMode.SingleSelection
            property int hoveredRow: -1

            rowDelegate: Rectangle {
                height: 30
                color: styleData.selected ? myPalette.highlight : (styleData.alternate? myPalette.alternateBase : myPalette.base)
                TooltipArea {
                    hoverEnabled: true
                    onEntered: { color = myPalette.highlight; tableView.hoveredRow = styleData.row }
                    onExited: { color = (styleData.alternate ? myPalette.alternateBase : myPalette.base); ; tableView.hoveredRow = -1 }
                    text: rowTooltip
                    onToolTipRequest: {
                        comboBoxTable.tooltipRequest(styleData.row)
                    }
                }
            }
            itemDelegate: Label {
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                color: tableView.hoveredRow === styleData.row ? myPalette.highlightedText : myPalette.text
                text: styleData.value ? styleData.value : ""
                elide: Text.ElideRight
                font.bold: styleData.selected
            }

            QC1.TableViewColumn {
                id: col1
                role: "id"
                title: "ID"
                width: 10
                visible: false
            }

            QC1.TableViewColumn {
                id: col2
                role: "name"
                title: qsTr("Name")
                width: 100
            }
            QC1.TableViewColumn {
                id: col3
                role: "date"
                title: qsTr("Date")
                width: 100
            }
            QC1.TableViewColumn {
                id: col4
                role: "date2"
                title: "Date2"
                width: 120
            }
            QC1.TableViewColumn {
                id: col5
                role: "icon"
                title: ""
                width: 30
                delegate: Item {
                    width: 30
                    height: 30

                    Image {
                        source: typeof styleData.value === "undefined" ? "" : styleData.value
                        width: 25
                        height: 25
                        sourceSize.width: 64
                        sourceSize.height: 64
                        anchors.centerIn: parent
                        ColorOverlay {
                            anchors.fill: parent
                            source: parent
                            color: materialForeground
                        }
                    }
                }
            }

            onModelChanged: {
                dropdown.height = tableView.model.rowCount() === 0 ? 100 : (tableView.model.rowCount()+1) *30
                var newWidth = 20
                var i
                for (i = 0; i < tableView.columnCount; i++) {
                    if (tableView.getColumn(i).visible)
                        newWidth += tableView.getColumn(i).width
                }
                dropdown.width = Math.max(comboBoxTable.width, newWidth)
            }

            onClicked: {
                console.log("Row " + row + " clicked")
                if (typeof tableView.model.get(row).id === "undefined"){
                    console.log("empty row selected")
                    comboButton.currentIndex = -1
                    rowSelected(-1)
                }else{
                    console.log("Selected id: " + tableView.model.get(row).id)
                    var newtext = tableView.model.get(row).name
                    if(comboButton.find(newtext) === -1)
                        combomodel.append({text: newtext})
                    comboButton.currentIndex = comboButton.find(newtext)
                    rowSelected(tableView.model.get(row).id)
                }
                dropdown.close()
                //delete model
            }

        }
    }
}
