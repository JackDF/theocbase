import QtQuick 2.5
import QtLocation 5.6
import QtGraphicalEffects 1.0
import QtPositioning 5.8
import QtQuick.Controls 1.4
import net.theocbase 1.0

MapPolygon {
    property alias mainPolygon: mainPolygon

    id: mainPolygon
    border.width: 2
    border.color: 'red'
    color: 'red'
    opacity: 0.5
}
