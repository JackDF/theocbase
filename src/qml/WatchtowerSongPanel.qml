import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

ScrollView {
    id: wtSongEdit
    property string title: "Watchtower Song"
    width: 500
    clip: true

    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }

    property bool startSong: true

    anchors.fill: parent

    PublicMeetingController {
        id: controller
    }

    ColumnLayout {
        id: layout
        x: 10
        y: 10
        width: wtSongEdit.width - 20

        // Prayer
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                background: null
                ToolTip.text: qsTr("Prayer")
                ToolTip.visible: hovered
                visible: !startSong
            }
            ComboBoxTable {
                Layout.fillWidth: true
                currentText: meeting.finalPrayer ? meeting.finalPrayer.fullname : ""
                column4.visible: false
                visible: !startSong
                onBeforeMenuShown: {
                    model = controller.brotherList(Publisher.Prayer)
                    column2.resizeToContents()
                }
                onRowSelected: {
                    var prayer = CPersons.getPerson(id)
                    meeting.finalPrayer = prayer
                    meeting.save()
                }
            }
        }

        // WT Song
        RowLayout {
            ToolButton {
                icon.source: "qrc:/icons/song.svg"
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                background: null
                ToolTip.text: qsTr("Song")
                ToolTip.visible: hovered
                Layout.fillWidth: true
            }
            NumberSelector {
                Layout.fillWidth: true
                Layout.preferredHeight: height
                maxValue: 151
                selectedValue: startSong ? meeting.songWtStart : meeting.songWtEnd
                onSelectedValueChanged: {
                    if (startSong) {
                        if (selectedValue === meeting.songWtStart)
                            return
                        meeting.songWtStart = selectedValue
                    } else {
                        if (selectedValue === meeting.songWtEnd)
                            return
                        meeting.songWtEnd = selectedValue
                    }
                    meeting.save()
                }
            }
        }
    }
}

