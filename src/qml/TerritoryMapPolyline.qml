import QtQuick 2.5
import QtLocation 5.6
import QtGraphicalEffects 1.0
import QtPositioning 5.8
import QtQuick.Controls 1.4
import net.theocbase 1.0

MapPolyline {
    id: mainPolyline

    //visible: false

    property int territoryId
    property int streetId
    property alias mainPolyline: mainPolyline
    property bool isTerritorySelected: false
    property int showStreet
    property int lineWidth: 7
    property var color

    states: [
        State {
            name: "Unassigned"; when: territoryId == 0
            PropertyChanges { target: mainPolyline; opacity: 0.4; visible: true; line.color: 'orange' }
        },
        State {
            name: "IsStreetSelected"; when: isTerritorySelected && showStreet >= 1
            PropertyChanges { target: mainPolyline; opacity: 0.4; visible: true }
        },
        State {
            name: "IsNotSelected"; when: !isTerritorySelected && showStreet === 2
            PropertyChanges { target: mainPolyline; opacity: 0.2; visible: true }
        },
        State {
            name: "Hidden"; when: !isTerritorySelected && showStreet < 2
            PropertyChanges { target: mainPolyline; opacity: 0; visible: false }
        }
        ]

    line.width: lineWidth
    line.color: color
    opacity: 0.3

    Connections {
        target: parent
        function onRefreshTerritoryMapStreets(showStreets) {
            showStreet = showStreets
        }
    }

    Connections {
        target: territoryTreeModel
        function onSelectedChanged() {
            if (territoryTreeModel.selectedTerritory)
            {
                isTerritorySelected = territoryTreeModel.selectedTerritory.territoryId === territoryId ? true : false
            }
            else
                isTerritorySelected = false
        }
    }
}
