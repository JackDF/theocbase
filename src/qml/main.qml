/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 1.4 as QC1
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2
import net.theocbase 1.0
import QtGraphicalEffects 1.0
import QtWebView 1.1
import Qt.labs.settings 1.0

Rectangle {
    id: root

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    Material.primary: Material.theme === Material.Dark ? "#969696" : "#c7c7c7"

    color: Material.primary

    property bool canViewMidweekMeetingSchedule: false
    property bool canEditMidweekMeetingSchedule: false
    property bool canViewWeekendMeetingSchedule: false
    property bool canEditWeekendMeetingSchedule: false
    property bool canSendMidweekMeetingReminders: false
    property bool canViewMeetingNotes: false
    property bool canEditMeetingNotes: false
    property date currentdate
    property date mwDate
    property date wkDate
    property alias bibleReading: midweekMeeting.bibleReading
    property int defaultfontsize: 13
    property color materialBackground: Material.background
    property color materialForeground: Material.foreground
    property color materialPrimary: Material.primary
    property color materialAccent: Material.accent
    property bool showSongTitles: false

    signal importClicked()
    signal fileDropped(url fileurl)
    signal publicMeetingEdit()
    signal gotoNextWeek()
    signal gotoPreviousWeek()
    signal showCalendar()
    signal showTimeline()
    signal sidebarWidthChanged(int width)

    function setDate(newdate){
        sidebar.keepClosed = true
        loadUISettings()
        root.forceActiveFocus()
        console.log("setDate function " + newdate.toString())
        currentdate = newdate
        // load Life and Ministry Meeting data
        midweekMeeting.loadSchedule(currentdate)
        weekendMeeting.loadSchedule(currentdate)
        outgoingSpeakers.reload()

        // show exception text
        congCtrl.clearExceptionCache()
        exceptionText.text = congCtrl.getExceptionText(currentdate)

        // get meeting days
        var mDay = congCtrl.getMeetingDay(currentdate,1)
        var mDDay = congCtrl.getMeetingDay(new Date(0), 1)
        var wDay = congCtrl.getMeetingDay(currentdate, 3)
        var wDDay = congCtrl.getMeetingDay(new Date(0), 3)

        midweekMeeting.editpossible = mDay > 0
        weekendMeeting.editpossible = wDay > 0

        var mDate = new Date(currentdate)
        mDate.setDate(mDate.getDate() + (mDay === 0 ? mDDay : mDay) -1)
        mwDate = mDate
        var wDate = new Date(currentdate)
        wDate.setDate(wDate.getDate() + (wDay === 0 ? wDDay : wDay) -1)
        wkDate = wDate

        flowView.calculateHeight()
        sidebar.keepClosed = false

        todo.reload()
        return true
    }
    function openStartPage(ignoreSettings) {
        startpageLoader.active = true
        if (ignoreSettings || settings_ui.show_start_page)
            startPagePopup.open()
    }    
    function loadUISettings() {
        var settingsObj = Qt.createQmlObject('import Qt.labs.settings 1.0; Settings {category: "ui"; property bool show_song_titles: false}',
                                           root,
                                           "Settings");
        showSongTitles = settingsObj.show_song_titles
    }

    Keys.forwardTo: weekendMeeting

    Settings {
        id: settings_ui
        category: "ui"
        property bool show_start_page: true
        property string latestArticle: ""
    }

    CongregationCtrl { id: congCtrl }

    Component {
        id: monochromeEffect
        Colorize {
            objectName: "Colorize"
            hue: 0.5
            saturation: 0
            lightness: 0.5
        }
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (contextMenu.opened)
                contextMenu.close()
            if (mouse.button === Qt.RightButton)
                contextMenu.popup()
        }
        Menu {
            id: contextMenu
            MenuItem {
                text: "Midweek Meeting"
                checked: midweekMeeting.visible
                onTriggered: {
                    lmMeeting.visible = !lmMeeting.visible
                    flowView.calculateHeight()
                }
            }
            MenuItem {
                text: "Weekend Meeting"
                checked: weekendMeeting.visible                
                onTriggered: {
                    publicMeeting.visible = !publicMeeting.visible
                    flowView.calculateHeight()
                }
            }
            MenuItem {
                text: "Outgoing Speakers"
                checked: outgoingSpeakers.visible
                onTriggered: {
                    outgoingSpeakers.visible = !outgoingSpeakers.visible
                    flowView.calculateHeight()
                }
            }
        }
    }

    Rectangle {
        id: exceptionBar
        color: "red"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: visible ? 30 : 0
        visible: exceptionText.text != ""
        Layout.rightMargin: -20
        Text {
            id: exceptionText
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: ""
            color: "white"
            font.bold: true
            font.pointSize: 16
            visible: text != ""
        }
    }


    RowLayout {
        id: splitView
        anchors.fill: parent
        anchors.topMargin: exceptionBar.height
        spacing: 0

        ScrollView {
            id: scrollView
            Layout.fillHeight: true
            Layout.fillWidth: true                        
            contentHeight: flowView.height + 40

            // Use light theme for schedule view
            Material.theme: Material.Light

            Flow {
                id: flowView
                flow: Flow.TopToBottom
                height: 500
                x: 20
                y: 20
                width: scrollView.width - 40
                spacing: 10
                property int columns: scrollView.width < 1000 ? 1 : 2
                property int columnWidth: columns == 1 ? width : (width-spacing)/2

                onWidthChanged: calculateHeight()
                onPositioningComplete: {
                    if (childrenRect.width > width)
                        calculateHeight()
                }

                function calculateHeight()
                {
                    var fullHeight = 0
                    var i
                    var firstHeight = 0
                    var arr = []
                    for (i = 0; i < children.length; i++) {
                        if (children[i].visible && children[i].objectName !== "Colorize") {
                            fullHeight += children[i].height + spacing
                            if (firstHeight == 0)
                                firstHeight = children[i].height
                            arr.push(children[i].height + spacing)
                        }
                    }
                    if (columns == 1 || fullHeight == 0) {
                        height = fullHeight
                    } else {
                        var oneCol = fullHeight / 2
                        var newHeight = 0
                        for (i = 0; i < arr.length; i++) {
                            if (i < arr.length -1) {
                                var nextHeight = newHeight + arr[i]
                                if (nextHeight > oneCol) {
                                    newHeight = Math.min(nextHeight, (fullHeight - newHeight)) - spacing
                                    break
                                }
                            }
                            newHeight = arr[i]
                        }
                        height = newHeight
                    }
                }

                // Midweek Meeting
                MWMeetingModule {
                    id: midweekMeeting
                    width: flowView.columnWidth
                    layer.effect: monochromeEffect
                    layer.enabled: !editpossible
                    visible: canViewMidweekMeetingSchedule && currentdate.getFullYear() > 2015
                    onShowEditPanel: {
                        sidebarLoader.loadPage(name, args)
                        weekendMeeting.lastEditPage = ""
                    }
                }
                // Weekend Meeting
                WEMeetingModule {
                    id: weekendMeeting
                    width: flowView.columnWidth
                    layer.effect: monochromeEffect
                    layer.enabled: !editpossible
                    visible: canViewWeekendMeetingSchedule
                    onShowEditPanel: {
                        sidebarLoader.loadPage(name, args)
                        midweekMeeting.lastEditAssignment = 0
                    }
                    onMovedTodoList: todo.reload()
                }

                OutgoingSpeakersModule {
                    id: outgoingSpeakers
                    width: flowView.columnWidth
                    currentDate: root.currentdate
                    onShowEditPanel: {
                        sidebarLoader.loadPage(name, args)
                        midweekMeeting.lastEditAssignment = 0
                        weekendMeeting.lastEditPage = ""
                    }
                    onMovedTodoList: todo.reload()
                }
            }
        }

        Rectangle {
            id: sidebar
            color: Material.background
            Layout.preferredWidth: width
            width: 0
            property bool keepClosed: false
            Layout.fillHeight: true
            onWidthChanged: sidebarWidthChanged(width)
            Label {
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.WordWrap
                text: qsTr("Select an assignment on the left to edit")
                visible: parent.width > 0 && sidebarLoader.source == ""
            }

            Loader {
                id: sidebarLoader
                anchors.fill: parent
                onSourceChanged: {
                    //if (source == "" && sidebar.width > 0)
                    //    sidebar.width = 0
                }
                onLoaded: {
                    console.log("sidebar content loaded " + sidebar.keepClosed.toString())
                    if (sidebar.width == 0 && !sidebar.keepClosed)
                        sidebar.width = 300
                }
                function loadPage(filename, args) {
                    root.forceActiveFocus()
                    if (filename === "")
                        source = ""
                    else
                        setSource(filename, args)
                }
            }
            Behavior on width {
                enabled: !mouseAreaSplit.drag.active
                PropertyAnimation{}
            }

            MouseArea {
                id: mouseAreaSplit
                anchors.fill: parent
                anchors.rightMargin: parent.width - 2
                hoverEnabled: true
                cursorShape: Qt.SplitHCursor
                drag.axis: Drag.XAxis
                drag.target: parent
                onMouseXChanged: {
                    if (drag.active) {
                        sidebar.width = sidebar.width - mouseX
                    }
                }
            }
        }
    }

    // Sidebar open/close button
    RoundButton {
        anchors.right: parent.right
        anchors.rightMargin: sidebar.width == 0 ? 0 : sidebar.width - (width/2)
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 84
        icon.source: sidebar.width == 0 ? "qrc:/icons/sidebar_open.svg" : "qrc:/icons/sidebar_close.svg"
        icon.color: "white"
        Material.background: "#254f8e"
        visible: canEditMidweekMeetingSchedule || canEditWeekendMeetingSchedule
        onClicked: {
            root.forceActiveFocus()
            if (sidebar.width == 0)
                sidebar.width = 300
            else
                sidebar.width = 0
        }
    }

    TodoPanel {
        id: todo
        parent: scrollView
        onMovedToSchedule: {
            if (incoming)                
                weekendMeeting.reload()
            else
                outgoingSpeakers.reload()
        }
    }

    Popup {
        id: startPagePopup
        width: 480
        height: root.height > 640 ? 640 : root.height
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        clip: true
        padding: 0       
        Loader {
            id: startpageLoader
            anchors.fill: parent
            active: false
            source: "qrc:/startup/StartSlider.qml"
        }
        Connections {
            target: startpageLoader.item
            function onClose() { startPagePopup.close() }
            function onNewsFound() { if (!startPagePopup.opened) startPagePopup.open() }
        }
    }

    // ToDo Button
    RoundButton {
        anchors.right: parent.right
        anchors.rightMargin: sidebar.width == 0 ? 0 : sidebar.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 42
        icon.source: "qrc:/icons/todo_list.svg"
        icon.color: "white"
        Material.background: "#254f8e"
        visible: canEditWeekendMeetingSchedule
        Rectangle {
            width: parent.width / 3
            height: width
            radius: width / 2
            anchors.right: parent.right
            anchors.top: parent.top
            color: "red"
            visible: todo.length > 0
            Text {
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: todo.length
                color: "white"
            }
        }
        onClicked: {
            todo.open()
        }

    }

    // Timeline Button
    RoundButton {
        anchors.right: parent.right
        anchors.rightMargin: sidebar.width == 0 ? 0 : sidebar.width
        anchors.bottom: parent.bottom
        //anchors.bottomMargin: 30
        icon.source: "qrc:/icons/timeline.svg"
        icon.color: "white"
        Material.background: "#254f8e"
        visible: canEditMidweekMeetingSchedule
        onClicked: showTimeline()
    }

}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
