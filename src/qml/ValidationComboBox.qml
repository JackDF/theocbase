import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

Item {
    //property string currentText
    property alias currentIndex: validationComboBox.currentIndex
    property alias textRole: validationComboBox.textRole
    property alias model: validationComboBox.model
    property bool isUntouched: true
    property bool isDataValid: false
    property bool showComboAlways: false
    property bool isEditing: showComboAlways

    signal selectedRowChanged(var row)
    signal activated()

    function find(text)
    {
        return validationComboBox.find(text)
    }

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        z: 3
        propagateComposedEvents: true

        onClicked: mouse.accepted = false;
        onPressed: mouse.accepted = false;
        onReleased: mouse.accepted = false;
        onDoubleClicked: mouse.accepted = false;
        onPositionChanged: mouse.accepted = false;
        onPressAndHold: mouse.accepted = false;

        onExited: {
            isEditing = showComboAlways
        }
    }

    Rectangle {
        id: invalidDataRect

        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        border.width: 1
        border.color: "red"
        opacity: 1
        color: "transparent"

        states: [
            State {
                name: "Untouched"; when: isUntouched
                PropertyChanges { target: invalidDataRect; opacity: 0 }
            },
            State {
                name: "ValidData"; when: isDataValid
                PropertyChanges { target: invalidDataRect; opacity: 0 }
            },
            State {
                name: "InvalidData"; when: !isDataValid
                PropertyChanges { target: invalidDataRect; opacity: 1 }
            }]

        transitions:[
            Transition {
                from: "ValidDate"
                to: "InvalidData"
                animations: invalidDataAnimation
            },
            Transition {
                from: "Untouched"
                to: "InvalidData"
                animations: invalidDataAnimation
            }]
    }

    Label {
        id: textlabel
        anchors.fill: parent
        text: validationComboBox.currentText
        visible: !isEditing
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        anchors.leftMargin: 5
        color: myPalette.text
        //renderType: Text.NativeRendering
        elide: "ElideRight"

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                isEditing = true
                if (!styleData.selected)
                    selectedRowChanged(styleData.row)
            }
        }
    }

    ComboBox {
        id: validationComboBox
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent
        visible: isEditing
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        onActivated: {
            parent.activated(index)
            if (!styleData.selected)
                selectedRowChanged(styleData.row)
        }

        onAccepted: {
            visible = showComboAlways;
        }

        Component.onCompleted: {
            if (styleData.row < 0)
                currentIndex = -1
        }
    }
}
