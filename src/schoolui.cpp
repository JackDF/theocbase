/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "schoolui.h"

schoolui::schoolui(QDate date, QObject *parent) : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    m_firstdayofweek = date;
    table = 0;
    edittable = new QTableWidget();
    edittable->setSelectionMode(QAbstractItemView::NoSelection);
    edittable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    edittable->verticalHeader()->setVisible(false);
    edittable->setWordWrap(true);
    edittable->setColumnCount(3 + 2 * s_class.getClassesQty());

    edittable->setHorizontalHeaderItem(0,new QTableWidgetItem(""));
    edittable->setHorizontalHeaderItem(1,newTableWidget(tr("Theme") + " / " + tr("Source")));
    edittable->setHorizontalHeaderItem(2,newTableWidget(tr("Source")));
    edittable->setColumnHidden(2,true);
    int schools = 1;
    for(int i = 0;i<s_class.getClassesQty()*2;i+=2)
    {
        QTableWidgetItem *item = new QTableWidgetItem(tr("Class") + QString::number(schools));
        edittable->setHorizontalHeaderItem(3+i,item);
        QTableWidgetItem *buttonitem = new QTableWidgetItem("-");
        edittable->setHorizontalHeaderItem(4+i,buttonitem);
        edittable->setColumnWidth(4+i,30);
        edittable->setColumnHidden(4+i,true);
        edittable->horizontalHeader()->setSectionResizeMode(3+i,QHeaderView::ResizeToContents);
        schools += 1;
    }

    edittable->setColumnWidth(0,100);
    //edittable->horizontalHeader()->setSectionResizeMode(0,QHeaderView::ResizeToContents);
    edittable->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    edittable->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);

    //QString style;
    //style.append("QTableView::item {padding: 2px}");
    edittable->setGridStyle(Qt::NoPen);
    edittable->setFrameShape(QFrame::NoFrame);
}

schoolui::~schoolui()
{
    if (edittable && !edittable->parent())
        delete edittable;
    if (table && !table->parent())
        delete table;
}

void schoolui::setDate(QDate date)
{
    if (date != m_firstdayofweek) schoolprogram.clear();
    m_firstdayofweek = date;
}

QDate schoolui::getDate()
{
    return m_firstdayofweek;
}

// get edit table
QTableWidget *schoolui::getSchoolEditTable()
{
    edittable->setRowCount(0);

    if (!s_class.getProgram(schoolprogram,m_firstdayofweek))
        return edittable;

    QFont f = edittable->font();
    f.setBold(true);

    int currentrow = -1;
    for(unsigned int i = 0;i<schoolprogram.size();i++)
    {
        const school_item &ss = *schoolprogram[i];
        QString studentname = "";
        if(ss.getStudent()){
            studentname = ss.getStudent()->fullname();
        }
        QString repname = "";
        if (ss.getVolunteer()){
            repname = ss.getVolunteer()->fullname();
        }

        QTableWidgetItem *numberitem = new QTableWidgetItem;
        int number = 0;

        switch(ss.getAssignmentNumber())
        {
        case 0:
            numberitem->setText(tr("Bible highlights:"));
            number = person::Highlights;
            break;
        case 1:
            numberitem->setText(tr("No. 1:"));
            number = person::No1;
            break;
        case 2:
            numberitem->setText(tr("No. 2:"));
            number = person::No2;
            break;
        case 3:
            numberitem->setText(tr("No. 3:"));
            number = person::No3;
            break;
        case 10:
            numberitem->setText(tr("Reader:"));
            number = person::WtReader;
            break;
        }
        if(ss.getClassNumber() == 1){
            currentrow = edittable->rowCount();
            edittable->insertRow(currentrow);
            edittable->setItem(currentrow,0,new QTableWidgetItem(ss.getNumberName()));

            QTableWidgetItem *subjectitem = new QTableWidgetItem;
            subjectitem->setToolTip(ss.getTheme());
            subjectitem->setText(ss.getTheme());
            subjectitem->setFont(f);

            edittable->setItem(currentrow,1,subjectitem);
            edittable->setItem(currentrow,2,new QTableWidgetItem(""));

            if(ss.getAssignmentNumber() == 2 || ss.getAssignmentNumber() == 3)
            {
                edittable->insertRow(currentrow+1);
                edittable->setItem(currentrow+1,0,new QTableWidgetItem(""));
                edittable->setItem(currentrow+1,1,new QTableWidgetItem(ss.getSource()));
                edittable->setSpan(currentrow,ss.getClassNumber()*2+1,2,1);
            }
        }


        schoolComboBox *combo = new schoolComboBox();
        combo->setProperty("number",number);
        combo->setProperty("onlybrothers",ss.getOnlyBrothers());
        combo->setProperty("class",ss.getClassNumber());
        combo->setProperty("currentrow",currentrow);
        combo->setProperty("demonstration", ss.getType() == school_item::Demonstration);
        if (repname != ""){
            combo->setCurrentText("> " + repname);
        }else{
            combo->setCurrentText(studentname);
        }

        QObject::connect(combo,SIGNAL(beforeShowPopup(QString,schoolComboBox*)),
                         this,SLOT(beforeStudentListShown(QString,schoolComboBox*)));

        connect(combo,SIGNAL(activated(int)),this,SLOT(schoolComboBoxSelected(int)));
        schoolComboBox *combo2 = 0;
        if(ss.getClassNumber() > 1 && ss.getAssignmentNumber() == 1){
            // no school checkbox
            QCheckBox *chb = new QCheckBox(edittable);
            chb->setObjectName("combo_" + QVariant(ss.getClassNumber()).toString());
            chb->setProperty("class",ss.getClassNumber());
            chb->setText(tr("No school"));
            edittable->setCellWidget(currentrow-1,ss.getClassNumber()*2+1,chb);
            connect(chb,SIGNAL(clicked()),this,SLOT(noSchoolClicked()));

            bool noschool = s_class.getNoSchool(m_firstdayofweek, ss.getClassNumber());
            chb->setChecked(noschool);
            combo->setEnabled(!noschool);

        }else if(ss.getAssignmentNumber() == 2 || ss.getAssignmentNumber() == 3){
            // disable combobox if no school
            if (ss.getClassNumber() > 1){
                QCheckBox *chb = qobject_cast<QCheckBox *>(edittable->cellWidget(0,ss.getClassNumber()*2+1));
                if (chb) combo->setEnabled(!chb->isChecked());
                edittable->setSpan(currentrow,ss.getClassNumber()*2+1,2,1);
            }

            // assistant row
            combo2 = new schoolComboBox();
            combo2->setProperty("number",combo->property("number").toInt());
            combo2->setProperty("class",ss.getClassNumber());
            combo2->setProperty("assistant",true);
            combo2->setProperty("onlybrothers",ss.getOnlyBrothers());
            combo2->setProperty("demonstration", ss.getType() == school_item::Demonstration);
            combo2->setProperty("student",ss.getStudent() ? ss.getStudent()->id() : -1);
            combo2->setProperty("studentgender", ss.getStudent() ? ss.getStudent()->gender() == person::Male ? "B" : "S" : "");
            QString assistantname = "";
            if (ss.getAssistant()) assistantname = ss.getAssistant()->fullname();
            combo2->setCurrentText(assistantname);
            QObject::connect(combo2,SIGNAL(beforeShowPopup(QString,schoolComboBox*)),
                             this,SLOT(beforeStudentListShown(QString,schoolComboBox*)));
            connect(combo2,SIGNAL(activated(int)),this,SLOT(schoolComboBoxSelected(int)));

            combo2->setEnabled(ss.getStudent() && ss.getType() == school_item::Demonstration);
            if(ss.getDone()) combo2->setEnabled(false);
        }

        // done button
        QToolButton *button = new QToolButton();
        button->setText("...");
        QIcon doneicon = QIcon();
        if(ss.getDone())
        {
            combo->setEnabled(false);
            if (repname != ""){
                doneicon.addPixmap(QPixmap(":/images/dialog-warning.png"));
            }else{
                doneicon.addPixmap(QPixmap(":/images/dialog-ok-apply.png"));
            }
        }
        button->setIcon(doneicon);

        connect(button,SIGNAL(clicked()),this,SLOT(schoolResultButtonClicked()));
        button->setProperty("class",ss.getClassNumber());
        button->setProperty("number",ss.getAssignmentNumber());
        button->setProperty("currentrow",currentrow);
        edittable->resizeRowToContents(currentrow);
        if (edittable->rowHeight(currentrow) < combo->sizeHint().height()+4){
            edittable->setRowHeight(currentrow,combo->sizeHint().height()+4);
        }
        edittable->setCellWidget(currentrow,ss.getClassNumber()*2+1, this->comboboxLayout(combo,button,combo2));

    }

    edittable->setFixedHeight(edittable->verticalHeader()->length()+2+ edittable->horizontalHeader()->height());
    ccongregation::exceptions e = c.isException(m_firstdayofweek);
    if(e == ccongregation::CircuitAssembly || e == ccongregation::RegionalConvention){
        edittable->setEnabled(false);
    }else if(e == ccongregation::Other){
        if(c.getMeetingDay(m_firstdayofweek,ccongregation::tms) == 0) edittable->setEnabled(false);
    }

    return edittable;
}


QTableWidget *schoolui::getSchoolTable()
{
    table = new QTableWidget(0,2+ s_class.getClassesQty());
    table->setFrameStyle(0);
    table->setStyleSheet("QTableView {background-color: rgba(0, 0, 0,0)}");
    table->setGridStyle(Qt::NoPen);

    table->setWordWrap(true);

    table->horizontalHeader()->setVisible(false);
    table->verticalHeader()->setVisible(false);
    table->setSelectionMode(QAbstractItemView::NoSelection);

    table->setColumnWidth(0,100);

    table->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    for (int i = 2;i<2+s_class.getClassesQty();i++){
        table->horizontalHeader()->setSectionResizeMode(i,QHeaderView::ResizeToContents);
    }

    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table->setMouseTracking(true);

    QFont f = table->font();
    f.setBold(true);

    if(this->schoolprogram.size() == 0)
        if (!s_class.getProgram(schoolprogram, m_firstdayofweek)) return table;

    int rownumber = -1;
    if (schoolprogram.size() > 0){
        for(unsigned int i = 0;i<schoolprogram.size();i++)
        {
            school_item &s = *schoolprogram[i];

            QString studentname = "";
            if (s.getStudent()) studentname = s.getStudent()->fullname();
            QString assistantname = "";
            if (s.getAssistant()) assistantname = s.getAssistant()->fullname();

            if(s.getClassNumber() == 1){
                rownumber = table->rowCount();
                table->insertRow(rownumber);
                table->setItem(rownumber,0,newTableWidget(s.getNumberName()));
                table->setItem(rownumber,1,newTableWidget(s.getTheme()));
                table->item(rownumber,1)->setFont(f);

                if (s.getVolunteer()){
                    table->setItem(rownumber,2,newTableWidget("> " + s.getVolunteer()->fullname()));
                }else{
                    table->setItem(rownumber,2,newTableWidget(studentname));
                }

                // TMS 2015 - show assistant also for brother in some cases
                if(s.getStudent() && !assistantname.isEmpty()){
                    if(table->rowCount() == rownumber+1){
                        table->insertRow(rownumber+1);
                        table->setSpan(rownumber,1,2,1);
                    }
                    table->setItem(rownumber+1,2,newTableWidget(assistantname));
                }

            }else{
                if (s.getVolunteer()){
                    table->setItem(rownumber,1+s.getClassNumber(),newTableWidget("> " + s.getVolunteer()->fullname()));
                }else{
                    table->setItem(rownumber,1+s.getClassNumber(),newTableWidget(studentname));
                }

                // TMS 2015 - show assistant also for brother in some cases
                if(s.getStudent() && !assistantname.isEmpty()){
                    if(table->rowCount() == rownumber+1) table->insertRow(rownumber+1);
                    table->setItem(rownumber+1,1+s.getClassNumber(),newTableWidget(assistantname));
                }
            }
        }
    }else{
        table->insertRow(0);
        table->setItem(0,0,new QTableWidgetItem(tr("Nothing to display")));
    }

    table->setFixedHeight(table->verticalHeader()->length()+2);

    table->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(table,SIGNAL(customContextMenuRequested(QPoint)),
            this,SLOT(showContextMenu(QPoint)));

    return table;

}

// show list of students in combobox
void schoolui::beforeStudentListShown(QString name, schoolComboBox *object)
{
    qDebug() << "before " + object->property("number").toString() + " name=" + name;
    QString ct = object->currentText();

    int taskno = object->property("number").toInt();
    int classno = object->property("class").toInt();    
    QStandardItemModel *itemmodel;
    if(object->property("assistant").toBool()) {
        taskno = person::Assistant;
        itemmodel = s_class.getAssistant(taskno,m_firstdayofweek,object->property("student").toInt(),classno,
                                         ct, object->property("studentgender").toString() == "B");
    }else{
        itemmodel = s_class.getStudents(taskno,m_firstdayofweek,classno,
                                        ct, object->property("onlybrothers").toBool());
    }

    object->clear();
    QTableView *view = new QTableView(object);
    view->verticalHeader()->hide();
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setSelectionMode(QAbstractItemView::SingleSelection);

    view->setSortingEnabled(true);
    object->setModel(itemmodel);
    object->setView(view);

    object->setModelColumn(1);
    // hide first column that contains student ids
    view->hideColumn(0);
    // hide column 4 when for brothers
    if (!object->property("assistant").toBool() && (object->property("onlybrothers").toBool() || !(taskno == person::No2 || taskno == person::No3))) view->hideColumn(4);
    // calculate width of dropdown list
    // scrollbar width
    int w = view->style()->pixelMetric(QStyle::PM_ScrollBarExtent);
    for(int i = 1;i<itemmodel->columnCount();i++){
        if (i == itemmodel->columnCount()-1){
            // width of image column
            view->setColumnWidth(i,25);
            w += 25;
        }else{
            // adjust column width to contents
            view->horizontalHeader()->setSectionResizeMode(i,QHeaderView::ResizeToContents);
            w += view->columnWidth(i);
        }
    }
    view->setMinimumWidth(w);
}

// student selected from combobox
void schoolui::schoolComboBoxSelected(int index)
{
    schoolComboBox *sc = qobject_cast<schoolComboBox *>(sender());

    sc->setModelColumn(1);

    QString personid = sc->model()->index(index,0).data(0).toString();
    qDebug() << personid + " valittu";
    int number = 0;
    switch(sc->property("number").toInt())
    {
    case person::Highlights:
        number = 0;
        break;
    case person::No1:
        number = 1;
        break;
    case person::No2:
        number = 2;
        break;
    case person::No3:
        number = 3;
        break;
    case person::WtReader:
        number = 10;
        break;
    }
    int schoolnumber = sc->property("class").toInt();// (col-1)/2;
    bool assistant = sc->property("assistant").isValid();
    person *p = 0;
    cpersons pc;
    p = pc.getPerson(QVariant(personid).toInt());

    if(p){
        if((number == 2 || number == 3) && !assistant){
            // assistant
            QWidget *w = qobject_cast<QWidget *>(sc->parent());
            if (w){
                schoolComboBox *ac = qobject_cast<schoolComboBox *>(w->layout()->itemAt(2)->widget());
                if (ac){
                    ac->setCurrentText("");
                    // activate assistant option when sister or assignment only for brother (based on nwt or marked with asterisk)
                    // TMS 2015 change
                    if (p->gender() == person::Female ||
                            (getDate().year() >= 2015 && sc->property("demonstration").toBool())){
                        ac->setEnabled(true);
                        ac->setProperty("studentgender", p->gender() == person::Male ? "B" : "S");
                    }else{                        
                        ac->setCurrentText("");
                        ac->setEnabled(false);
                    }
                    //ac->setEnabled(p->gender() == person::Female);
                    //if(p->gender() == person::Male) ac->setCurrentText("");
                    ac->setProperty("student",personid);
                }
            }
        }
    }else{
        //p->id = -1;
        qDebug() << "not selected!";
    }

    for(unsigned int i = 0;i<schoolprogram.size();i++)
    {
        if(schoolprogram[i]->getAssignmentNumber() == number && schoolprogram[i]->getClassNumber() == schoolnumber){

            if(!assistant){
                schoolprogram[i]->setStudent( p );
                if(!p || p->gender() == person::Male) schoolprogram[i]->setAssistant(0);
            }else{
                schoolprogram[i]->setAssistant(p);
            }

            qDebug() << " store id = " << schoolprogram[i]->getScheduleId();
            // save changes to the database
            schoolprogram[i]->save();
        }
    }
    QFontMetrics fm(sc->font());
    int width=fm.width(sc->currentText()) + 50;
    if (edittable->columnWidth(schoolnumber*2+1) < width){
        edittable->horizontalHeader()->setSectionResizeMode(schoolnumber*2+1,QHeaderView::Fixed);
        edittable->setColumnWidth(schoolnumber*2+1,width);
    }
    edittable->updateGeometry();
}

// school result button
void schoolui::schoolResultButtonClicked()
{
    int no,classno;
    QToolButton *button = qobject_cast<QToolButton *>(sender());
    no = button->property("number").toInt();
    classno = button->property("class").toInt();
    qDebug() << "Assignments no = " + QString::number(no) + " classnumber =" + QString::number(classno);


    int currentindex = -1;
    for(unsigned int i = 0;i<schoolprogram.size();i++)
    {
        if(schoolprogram[i]->getAssignmentNumber() == no && schoolprogram[i]->getClassNumber() == classno){
            if (!schoolprogram[i]->getStudent()){
                QMessageBox::information(0,"",tr("No assignment has been made!"));
                return;
            }
            qDebug() << schoolprogram[i]->getScheduleId();
            if(schoolprogram[i]->getScheduleId() <= 0){
                QMessageBox::information(0,"",tr("Please import new school schedule from Watchtower Library (Settings->Theocratic Ministry School...)"));
                return;
            }
            currentindex = i;
            break;
        }
    }

    if (currentindex > -1){
        school_item &currentitem = *schoolprogram[currentindex];
        schoolresult sr(currentitem);

        sr.setParent(edittable,Qt::Sheet);
        sr.setWindowModality(Qt::WindowModal);

        sr.exec();

        if(sr.result() == QDialog::Accepted){
            // update button icon when dialog accepted
            getSchoolEditTable();
        }
    }


}

//
QTableWidgetItem *schoolui::newTableWidget(const QString &text)
{
    QTableWidgetItem *qtwi;
    qtwi = new QTableWidgetItem(text);
    qtwi->setToolTip(text);
    qtwi->setTextAlignment(Qt::AlignTop | Qt::AlignLeft);
    //qtwi->setStatusTip(text);
    return qtwi;
}

// context menu for school table
void schoolui::showContextMenu(const QPoint &pos)
{
    qDebug() << "context menu" << pos.x() << pos.y();
    QMenu myMenu;
    myMenu.addAction(tr("Show Details..."));

    QTableWidgetItem *item = table->itemAt(pos);
    if (!item){
        qDebug() << "empty area";
        return;
    }
    if (item->column() < 2) return;
    if (item->text() == "") return;

    QAction* selectedItem = myMenu.exec(QCursor::pos());
    if (selectedItem){
        if (item){
            emit contextMenuShowStudent(item->text());
        }
    }
}

// no school checkbox clicked
void schoolui::noSchoolClicked()
{
    QCheckBox *c = qobject_cast<QCheckBox *>(sender());
    if(!c){
        return;
    }
    int classno = c->property("class").toInt();
    // save or remove 'no school' info
    s_class.setNoSchool(m_firstdayofweek, classno, (c->checkState() == Qt::Checked));

    // enable or disable student and assistant comboboxes
    for(int i = 1;i<edittable->rowCount();i++){
        qDebug() << QVariant(classno*2+2).toString() + "-rivi" + QVariant(i).toString();

        QWidget *w = qobject_cast<QWidget *>(edittable->cellWidget(i,classno*2+1));
        if (w){
            // student combobox
            schoolComboBox *sc = qobject_cast<schoolComboBox *>(w->layout()->itemAt(0)->widget());
            if (sc){
                sc->setEnabled(!c->isChecked());
                sc->setCurrentText("");
            }
            if (w->layout()->count() > 2){
                // assistant combobox
                schoolComboBox *ac = qobject_cast<schoolComboBox *>(w->layout()->itemAt(2)->widget());
                if (ac){
                    ac->setEnabled(false);
                    ac->setCurrentText("");
                }
            }
        }
    }
}

QWidget *schoolui::comboboxLayout(schoolComboBox *combo, QToolButton *button, schoolComboBox *combo2 = 0)
{
    QWidget *wdg = new QWidget();
    QGridLayout *layout = new QGridLayout(wdg);
    combo->setFixedHeight(combo->sizeHint().height());
    button->setFixedHeight(combo->sizeHint().height());

    combo->setProperty("name","COMBO1");
    layout->addWidget(combo,1,0);
    button->setProperty("name","BUTTON1");
    layout->addWidget(button,1,1);
    if (combo2){
        combo2->setProperty("name","COMBO2");
        combo2->setFixedHeight(combo2->sizeHint().height());
        layout->addWidget(combo2,2,0);
    }
    layout->setRowStretch(0,1);
    layout->setMargin(0);
    layout->setSpacing(2);
    wdg->setMinimumHeight(combo->sizeHint().height() + 4);

    return wdg;
}
