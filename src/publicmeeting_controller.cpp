#include "publicmeeting_controller.h"

using namespace tbAvailability;

publicmeeting_controller::publicmeeting_controller(QObject *parent)
    : QObject(parent), meetingLoaded(false)
{
}

QDateTime publicmeeting_controller::date() const
{
    return m_date.startOfDay();
}

bool publicmeeting_controller::setDate(QDateTime date)
{
    QDate _date = date.offsetFromUtc() < 0 ? date.toUTC().date() : date.date();
    m_date = _date;
    meetingLoaded = false;
    emit dateChanged(date);
    emit meetingChanged();
    return date.isValid();
}

cptmeeting *publicmeeting_controller::getMeeting()
{
    if (!meetingLoaded)
        loadMeeting();
    return m_meeting.data();
}

QAbstractItemModel *publicmeeting_controller::brotherList(person::UseFor usefor)
{
    auto itemmodel = new QStandardItemModel(0, 3, this);
    itemmodel->setItemRoleNames(defaultRoles());

    ccongregation c;
    QDate meetingDate = m_date.addDays(c.getMeetingDay(m_date, ccongregation::pm) - 1);
    WeekendMeetingAvailabilityChecker checker(meetingDate, m_date);
    Availability a = checker.Get(usefor);

    itemmodel->appendRow(new QStandardItem());
    for (int n = 0; n < a.Count(); ++n) {
        AvailabilityItem *ai = a.GetItem(n);
        auto item = new QStandardItem();
        item->setData(ai->Id, Qt::UserRole + 1);
        item->setData(ai->DisplayName, Qt::UserRole + 2);
        item->setData(ai->GetDateLastAssigned(usefor).toString("yyyy-MM-dd"), Qt::UserRole + 3);

        if (ai->OutsideSpeaker) {
            item->setData("qrc:/icons/warning_busy.svg", Qt::UserRole + 5);
        } else if (ai->OnHoliday) {
            continue;
        } else if (ai->HasAssignmentsOtherThan(usefor, m_date)) {
            item->setData("qrc:/icons/warning.svg", Qt::UserRole + 5);
        }
        itemmodel->appendRow(item);
    }
    auto sortmodel = new SortFilterProxyModel(this, itemmodel, Qt::UserRole + 2, "brother");
    return sortmodel;
}

QAbstractItemModel *publicmeeting_controller::speakerListLocal()
{
    ccongregation c;
    return speakerList(0, c.getMyCongregation().id);
}

QAbstractItemModel *publicmeeting_controller::speakerList(const int talkId, const int congregation_id)
{
    auto itemmodel = new QStandardItemModel(0, 3, this);
    itemmodel->setItemRoleNames(defaultRoles());

    ccongregation c;
    if (!m_date.isValid())
        m_date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1);
    QDate meetingDate = m_date.addDays(c.getMeetingDay(m_date, ccongregation::pm) - 1);
    WeekendMeetingAvailabilityChecker checker(meetingDate, m_date);

    bool onlyLocal = c.getMyCongregation().id == congregation_id;
    Availability a = onlyLocal ? checker.GetLocalPublicSpeaker(talkId) : checker.GetPublicSpeaker(congregation_id, talkId);
    qDebug() << a.Count();
    itemmodel->appendRow(new QStandardItem());
    for (int n = 0; n < a.Count(); ++n) {
        AvailabilityItem *ai = a.GetItem(n);
        auto item = new QStandardItem();
        item->setData(ai->Id, Qt::UserRole + 1);
        item->setData(ai->DisplayName, Qt::UserRole + 2);
        item->setData(ai->GetDateLastAssigned(person::PublicTalk).toString("yyyy-MM-dd"), Qt::UserRole + 3);
        if (ai->OutsideSpeaker) {
            item->setData("qrc:/icons/warning_busy.svg", Qt::UserRole + 5);
        } else if (ai->OnHoliday) {
            continue;
        } else if (ai->HasAssignmentsOtherThan(person::UseFor::PublicTalk, m_date)) {
            item->setData("qrc:/icons/warning.svg", Qt::UserRole + 5);
        }
        itemmodel->appendRow(item);
    }
    auto sortmodel = new SortFilterProxyModel(this, itemmodel, Qt::UserRole + 2, "brother");
    return sortmodel;
}

QAbstractItemModel *publicmeeting_controller::themeList(int speakerId)
{
    auto itemmodel = new QStandardItemModel(0, 3, this);
    QHash<int, QByteArray> roles;
    roles[ThemeFilterProxyModel::ThemeListRoles::id] = "id";
    roles[ThemeFilterProxyModel::ThemeListRoles::name] = "name";
    roles[ThemeFilterProxyModel::ThemeListRoles::date] = "date";
    roles[ThemeFilterProxyModel::ThemeListRoles::date_val] = "date_val";
    roles[ThemeFilterProxyModel::ThemeListRoles::icon] = "icon";
    roles[ThemeFilterProxyModel::ThemeListRoles::number] = "number";
    itemmodel->setItemRoleNames(roles);

    cpublictalks cp;
    std::vector<ThemeListItem> list = cp.getThemeList("", speakerId == 0 ? nullptr : QSharedPointer<person>(cpersons::getPerson(speakerId)).data());

    itemmodel->appendRow(new QStandardItem());
    for (auto row : list) {
        auto item = new QStandardItem();
        item->setData(row.id, ThemeFilterProxyModel::ThemeListRoles::id);
        item->setData(QString::number(row.number) + " " + row.theme, ThemeFilterProxyModel::ThemeListRoles::name);
        item->setData(row.last.isValid() ? row.last.toString("yyyy-MM-dd") : "", ThemeFilterProxyModel::ThemeListRoles::date);
        item->setData(row.last, ThemeFilterProxyModel::ThemeListRoles::date_val);
        item->setData(row.number, ThemeFilterProxyModel::ThemeListRoles::number);
        itemmodel->appendRow(item);
    }
    auto sortmodel = new ThemeFilterProxyModel(this, itemmodel, ThemeFilterProxyModel::ThemeListRoles::number);
    return sortmodel;
}

QAbstractItemModel *publicmeeting_controller::congregationList()
{
    auto itemmodel = new QStandardItemModel(0, 3, this);
    itemmodel->setItemRoleNames(defaultRoles());
    ccongregation cc;
    QList<ccongregation::congregation> allCongregations = cc.getAllCongregations();

    itemmodel->appendRow(new QStandardItem());
    for (ccongregation::congregation const &cong : allCongregations) {
        auto item = new QStandardItem();
        item->setData(cong.id, Qt::UserRole + 1);
        item->setData(cong.name, Qt::UserRole + 2);
        itemmodel->appendRow(item);
    }

    auto sortmodel = new SortFilterProxyModel(this, itemmodel, Qt::UserRole + 2, "brother");
    return sortmodel;
}

QAbstractItemModel *publicmeeting_controller::hospitalityList()
{
    auto itemmodel = new QStandardItemModel(0, 3, this);
    itemmodel->setItemRoleNames(defaultRoles());
    ccongregation cc;
    QDate meetingDate = m_date.addDays(cc.getMeetingDay(m_date, ccongregation::pm) - 1);

    HostpitalityChecker checker(meetingDate, m_date);
    Availability a = checker.GetHospitality();

    itemmodel->appendRow(new QStandardItem());
    for (int n = 0; n < a.Count(); ++n) {
        AvailabilityItem *ai = a.GetItem(n);
        auto item = new QStandardItem();
        item->setData(ai->Id, Qt::UserRole + 1);
        item->setData(ai->DisplayName, Qt::UserRole + 2);
        item->setData(ai->GetDateLastAssigned(person::Hospitality).toString("yyyy-MM-dd"), Qt::UserRole + 3);
        if (ai->OutsideSpeaker) {
            item->setData("qrc:/icons/warning_busy.svg", Qt::UserRole + 5);
        } else if (ai->OnHoliday) {
            continue;
        } else if (ai->HasAssignmentsOtherThan(person::UseFor::Hospitality, m_date)) {
            item->setData("qrc:/icons/warning.svg", Qt::UserRole + 5);
        }
        itemmodel->appendRow(item);
    }
    auto sortmodel = new SortFilterProxyModel(this, itemmodel, Qt::UserRole + 2, "host");
    return sortmodel;
}

bool publicmeeting_controller::moveToTodo()
{
    cptmeeting *meeting = getMeeting();
    if (!meeting->speaker())
        return false;
    todo t(true);
    t.setCongregation(meeting->speaker()->congregationName());
    t.setSpeaker(meeting->speaker()->fullname());
    t.setTheme(QString("%1 (%2)").arg(meeting->themeName(), QVariant(meeting->themeNumber()).toString()));
    t.setNotes(tr("From %1").arg(date().toString(Qt::ISODate)));
    t.save();
    meeting->theme = cpttheme();
    meeting->setSpeaker(nullptr);
    meeting->save();
    emit meetingChanged();
    return true;
}

bool publicmeeting_controller::moveTo(const QDateTime selectedDate)
{
    if (!selectedDate.isValid())
        return false;
    // use first day of week
    QDate _selectedDate = selectedDate.offsetFromUtc() < 0 ? selectedDate.toUTC().date() : selectedDate.date();
    QDate newDate = _selectedDate.addDays(1 - _selectedDate.dayOfWeek());
    if (newDate == date().date()) {
        return false;
    }

    cpublictalks c;
    QSharedPointer<cptmeeting> newMeeting(c.getMeeting(newDate));
    cptmeeting *currentMeeting = getMeeting();
    if (newMeeting->id > 0 && (newMeeting->speaker() || newMeeting->themeNumber() > 0)) {
        // conflict
        ccongregation cong;
        QDate d2 = newDate.addDays(cong.getMeetingDay(newDate, ccongregation::pm) - 1);
        QMessageBox msgbox;
        msgbox.setText(tr("The destination date already has a talk scheduled.") +
                       QString("\n%1\n%2\n%3").arg(
                           d2.toString(Qt::DefaultLocaleShortDate),
                           newMeeting->speaker() ? newMeeting->speaker()->fullname() : "",
                           newMeeting->themeName()));
        QPushButton *swapButton = msgbox.addButton(tr("Swap Talks", "Button text"), QMessageBox::YesRole);
        msgbox.addButton(tr("Cancel", "Button text"), QMessageBox::RejectRole);
        msgbox.exec();
        if (msgbox.clickedButton() == swapButton) {
            cptmeeting temp;
            temp.theme = currentMeeting->theme;
            temp.setSpeaker(currentMeeting->speaker());
            currentMeeting->theme = newMeeting->theme;
            currentMeeting->setSpeaker(newMeeting->speaker());
            newMeeting->theme = temp.theme;
            newMeeting->setSpeaker(temp.speaker());
            currentMeeting->save();
            newMeeting->save();
        } else
            return false;
    } else {
        newMeeting->theme = currentMeeting->theme;
        newMeeting->setSpeaker(currentMeeting->speaker());
        newMeeting->save();
        currentMeeting->theme = cpttheme();
        currentMeeting->setSpeaker(nullptr);
        currentMeeting->save();
    }
    emit meetingChanged();
    return true;
}

void publicmeeting_controller::reload()
{
    meetingLoaded = false;
    loadMeeting();
    emit meetingChanged();
}

void publicmeeting_controller::loadMeeting()
{
    cpublictalks cpt;
    m_meeting.reset(cpt.getMeeting(m_date));
    meetingLoaded = true;
}

QHash<int, QByteArray> publicmeeting_controller::defaultRoles()
{
    QHash<int, QByteArray> roles;
    roles[Qt::UserRole + 1] = "id";
    roles[Qt::UserRole + 2] = "name";
    roles[Qt::UserRole + 3] = "date";
    roles[Qt::UserRole + 4] = "date_val";
    roles[Qt::UserRole + 5] = "icon";
    return roles;
}

bool publicmeeting_controller::ThemeFilterProxyModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    if (source_left.row() == 0 || source_right.row() == 0)
        return source_left.row() < source_right.row();
    bool ret = false;

    switch (this->sortRole()) {
    case ThemeFilterProxyModel::ThemeListRoles::name:
    case ThemeFilterProxyModel::ThemeListRoles::number: {
        auto val1 = sourceModel()->data(source_left, ThemeFilterProxyModel::ThemeListRoles::number).toInt();
        auto val2 = sourceModel()->data(source_right, ThemeFilterProxyModel::ThemeListRoles::number).toInt();
        ret = val1 < val2;
        break;
    }
    case ThemeFilterProxyModel::ThemeListRoles::date: {
        auto val1 = sourceModel()->data(source_left, ThemeFilterProxyModel::ThemeListRoles::date_val).toDate();
        auto val2 = sourceModel()->data(source_right, ThemeFilterProxyModel::ThemeListRoles::date_val).toDate();
        ret = val1 < val2;
        break;
    }
    default:
        ret = SortFilterProxyModel::lessThan(source_left, source_right);
    }
    return ret;
}
