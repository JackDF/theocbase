/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "schoolreminder.h"

schoolreminder::schoolreminder(QObject *parent)
    : QObject(parent)
    , id(0)
    , userId(0)
    , objectId(0)
    , objectType(0)
    , counter(0)
    , cancelled(false)
{
}

schoolreminder::~schoolreminder()
{
}

QList<schoolreminder *> schoolreminder::getRemindersList(QDate from, QDate to)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_items m_parts = sql->selectSql(QString("SELECT l.date,l.chairman,l.counselor2,l.counselor3,l.prayer_beginning,l.prayer_end,l.id lmm_id,t.type ttype,e.* "
                                               "FROM (SELECT 1 type UNION ALL SELECT 2 type union all select 3 type union all select 4 union all select 5) t "
                                               "INNER JOIN lmm_meeting  l "
                                               "LEFT JOIN e_reminder e on l.id = e.object_id and t.type = e.type "
                                               "WHERE date(l.date,'+6 day') > '%1' AND l.date < '%2' ORDER BY l.date")
                                               .arg(
                                                       from.toString(Qt::ISODate),
                                                       to.isNull() ? from.addYears(1).toString(Qt::ISODate) : to.toString(Qt::ISODate)));

    QList<schoolreminder *> reminders;
    for (sql_item sitem : m_parts) {
        QDate d = sitem.value("date").toDate();
        d = d.addDays(c.getMeetingDay(d, ccongregation::tms) - 1);
        //if (d < today) continue;

        switch (sitem.value("ttype").toInt()) {
        case 1:
            fillReminder(reminders, sitem, d, "chairman", tr("Chairman"));
            break;
        case 2:
            fillReminder(reminders, sitem, d, "counselor2", tr("Counselor-Class II"));
            break;
        case 3:
            fillReminder(reminders, sitem, d, "counselor3", tr("Counselor-Class III"));
            break;
        case 4:
            fillReminder(reminders, sitem, d, "prayer_beginning", tr("Prayer I"));
            break;
        case 5:
            fillReminder(reminders, sitem, d, "prayer_end", tr("Prayer II"));
        }
    }

    sql_items a_parts = sql->selectSql(QString("SELECT l.date, l.assignee_id, l.assistant_id, l.id lmm_id, l.classnumber, "
                                               "e.*, t.type ttype, s.theme, s.source, s.talk_id, s.study_number FROM "
                                               "(SELECT 7 type UNION ALL SELECT 8) t "
                                               "INNER JOIN lmm_assignment l "
                                               "LEFT JOIN e_reminder e ON l.id = e.object_id AND t.type = e.type "
                                               "LEFT JOIN lmm_schedule s ON l.lmm_schedule_id = s.id "
                                               "WHERE date(l.date,'+6 day') > '%1' AND l.date < '%2' ORDER BY l.date, s.talk_id")
                                               .arg(
                                                       from.toString(Qt::ISODate),
                                                       to.isNull() ? from.addYears(1).toString(Qt::ISODate) : to.toString(Qt::ISODate)));
    for (sql_item sitem : a_parts) {
        QDate d = sitem.value("date").toDate();
        d = d.addDays(c.getMeetingDay(d, ccongregation::tms) - 1);
        //if (d < today) continue;
        int dbTalkID(sitem.value("talk_id").toInt());
        int talkID(0);
        int sequence(0);
        LMM_Schedule::splitDbTalkId(dbTalkID, talkID, sequence);
        if (sitem.value("ttype").toInt() == 7) {
            fillReminder(reminders, sitem, d, "assignee_id", LMM_Schedule::getFullStringTalkType(talkID));
        } else {
            fillReminder(reminders, sitem, d, "assistant_id", LMM_Schedule::getFullStringTalkType(talkID));
        }
    }
    return reminders;
}

void schoolreminder::save()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    // save reminder
    sql_item si;
    si.insert("object_id", objectId);
    si.insert("user_id", userId);
    si.insert("type", objectType);
    if (id > 0) {
        // update
        si.insert("counter", counter + 1);

        sql->updateSql("e_reminder", "id", QVariant(id).toString(), &si);
    } else {
        // insert a new
        si.insert("counter", 1);
        sql->insertSql("e_reminder", &si, "id");
    }
}

QString schoolreminder::get_iCal()
{
    iCal::VCALENDAR c("theocbase");
    iCal::VEVENT ev;
    c.AddEvent(&ev);

    ccongregation cong;
    ccongregation::congregation my_congregation(cong.getMyCongregation());

    QDateTime startTime = this->date.startOfDay();
    startTime.setTime(QTime::fromString(my_congregation.time_meeting1, "hh:mm"));
    QDateTime endTime = startTime.addSecs(60 * 105);

    ev.setStart(startTime);
    ev.setEnd(endTime);

    ev.setUid(QVariant(this->objectId).toString() + "@theocbase.net");
    ev.setLocation(my_congregation.address.length() == 0 ? "" : my_congregation.address);
    ev.setSummary(this->theme);
    ev.setDescription(this->reminderText);

    if (this->cancelled)
        ev.setStatus("CANCELLED");

    return c.toString();
}

void schoolreminder::fillReminder(QList<schoolreminder *> &list, const sql_item sitem, const QDate d, const QString field,
                                  QString assignmentName, bool cancellation)
{
    cpersons cp;
    sql_class *sql = &Singleton<sql_class>::Instance();

    QSharedPointer<person> assignee(cp.getPerson(sitem.value(field).toInt()));
    if (assignee) {
        if (assignee->email().contains("@")) {
            int counter = sitem.value("counter").toInt();
            if (!cancellation) {
                if (sitem.value(field) != sitem.value("user_id"))
                    fillReminder(list, sitem, d, "user_id", assignmentName, true); // cancellation
            }
            schoolreminder *reminder = new schoolreminder(this);
            reminder->reminderSubject = cancellation ? tr("Cancellation - Our Christian Life and Ministry Meeting Assignment") : tr("Our Christian Life and Ministry Meeting Assignment");

            reminder->theme = assignmentName;

            QString brotherSister = assignee->gender() == person::Female
                    ? QObject::tr("Sister")
                    : QObject::tr("Brother");

            // keep the placeholders in the localizable text ("Dear %1 %2") so that translators
            // can swap the order if required...
            QString appellation = QObject::tr("Dear %1 %2").arg(brotherSister, assignee->lastname());
            QString introPara = QObject::tr("Please find below details of your upcoming assignment:");

            QString content;

            content.append(appellation);
            content.append("\n\n");
            content.append(introPara);
            content.append("\n\n");

            content.append(QString("%1: %2\n").arg(tr("Name"), assignee->fullname()));
            content.append(QString("%1: %2\n").arg(QObject::tr("Date"), d.toString(Qt::DefaultLocaleShortDate)));
            content.append(QString("%1: <assignment>\n").arg(tr("Assignment")));

            if (sitem.contains("theme"))
                content.append(QString("%1: %2\n").arg(tr("Theme"), sitem.value("theme").toString()));
            if (sitem.contains("source"))
                content.append(QString("%1: %2\n").arg(tr("Source Material"), sitem.value("source").toString()));

            if (sitem.contains("talk_id")) {
                person *student = cp.getPerson(sitem.value("assignee_id").toInt());
                if (student)
                    student->setParent(reminder);
                person *assistant = cp.getPerson(sitem.value("assistant_id").toInt());
                if (assistant)
                    assistant->setParent(reminder);
                int talkID(0);
                int sequence(0);
                LMM_Schedule::splitDbTalkId(sitem.value("talk_id").toInt(), talkID, sequence);
                LMM_Assignment a(talkID, sequence);
                if (a.canHaveAssistant()) {
                    content.append(QString("%1: %2\n").arg(tr("Assistant"), assistant ? assistant->fullname() : "-"));
                }
                if (a.canCounsel() && field == "assignee_id" && student) {
                    if (d < QDate(2019, 1, 7)) {
                        LMM_Assignment_ex ax(talkID, sequence, -1, -1);
                        ax.setDate(d);
                        ax.setSpeaker(student);
                        QString unknowncounselpoint;
                        schoolStudentStudy *study = ax.getCounselPoint(unknowncounselpoint);
                        QString studyTxt = study ? QVariant(study->getNumber()).toString() : "";
                        content.append(QString("%1: %2\n").arg(tr("Study"), unknowncounselpoint.isEmpty() ? studyTxt : QString("%1 (%2)").arg(unknowncounselpoint, studyTxt)));
                    } else {
                        content.append(QString("%1: %2\n").arg(tr("Study"), sitem.value("study_number").toString()));
                    }
                    LMM_Meeting lmm;
                    if (lmm.classes() > 1) {
                        QString classText = "";
                        switch (sitem.value("classnumber").toInt()) {
                        case 1:
                            classText = tr("Main hall");
                            break;
                        case 2:
                            classText = tr("Auxiliary classroom 1");
                            break;
                        case 3:
                            classText = tr("Auxiliary classroom 2");
                            break;
                        }
                        content.append(QString("%1: %2\n").arg(tr("To be given in", "Refer to main hall or aux. classroom. See S-89"), classText));
                    }
                }
                if (field == "assistant_id") {
                    if (a.talkId() == LMM_Schedule::TalkType_CBS) {
                        assignmentName = tr("Reader for Congregation Bible Study");
                    } else {
                        assignmentName = tr("Assistant to %1", "%1 is student's name").arg(student ? student->fullname() : "");
                    }
                }
            }
            if (cancellation)
                assignmentName = tr("Cancellation") + ", " + assignmentName;
            content.replace("<assignment>", assignmentName);

            // sign-off...
            content.append("\n\n");
            content.append(QObject::tr("Regards"));
            content.append("\n--\n"); // standard signature separator
            content.append(sql->getSetting("email_from_name"));

            reminder->reminderText = content;
            reminder->toEmail = assignee->email();
            reminder->toName = assignee->fullname();
            reminder->date = d;
            reminder->objectType = sitem.value("ttype").toInt();
            reminder->objectId = sitem.value("lmm_id").toInt();
            reminder->userId = assignee->id();
            reminder->counter = counter;
            reminder->id = sitem.value("id").toInt();
            reminder->cancelled = cancellation;
            list.append(reminder);
        }
    }
}
