#include "dropbox.h"

dropbox::dropbox(QString clientIdentifier, QString sharedKey, QObject *parent) :
    QObject(parent)
{
    dropboxAuth = new QOAuth2AuthorizationCodeFlow(this);
    dropboxAuth->setAuthorizationUrl(QUrl("https://www.dropbox.com/oauth2/authorize"));
    dropboxAuth->setAccessTokenUrl(QUrl("https://api.dropboxapi.com/oauth2/token"));
    dropboxAuth->setClientIdentifier(clientIdentifier);
    dropboxAuth->setClientIdentifierSharedKey(sharedKey);

    QString token = settings.value("dropbox/token","").toString();
    dropboxAuth->setToken(token);

    connect(dropboxAuth,&QOAuth2AuthorizationCodeFlow::granted,[=](){
        qDebug() << "DROPBOX GRANTED" << dropboxAuth->token();
        settings.setValue("dropbox/token",dropboxAuth->token());
    });
    connect(dropboxAuth,&QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,this,&dropbox::authorizeWithBrowser);
    connect(dropboxAuth,&QOAuth2AuthorizationCodeFlow::statusChanged,this,&dropbox::statusChanged);
    auto replyHandler = new QOAuthHttpServerReplyHandler(55738,this);
    replyHandler->setCallbackPath("dropbox");
    dropboxAuth->setReplyHandler(replyHandler);
}

void dropbox::authenticate()
{
    if (!dropboxAuth->token().isEmpty()) {
        qDebug() << "Already authenticated!";
        return;
    }

    // Authenticate
    dropboxAuth->grant();
}

void dropbox::revoke()
{
    initCall();
    qDebug() << "https://api.dropboxapi.com/2/auth/token/revoke";
    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QByteArray by = "null";
    auto reply = dropboxAuth->post(QUrl("https://api.dropboxapi.com/2/auth/token/revoke"),by);
    connect(reply,&QNetworkReply::finished,[=](){
        qDebug() << "reply finished" << reply->readAll();
    });
    // clear token
    this->setToken("");
    settings.setValue("dropbox/name","");
    settings.setValue("dropbox/email","");
    settings.setValue("dropbox/syncfile","");
}

void dropbox::setToken(const QString token)
{
    dropboxAuth->setToken(token);
    settings.setValue("dropbox/token",token);
    QAbstractOAuth::Status newstatus = token.isEmpty() ?
                QAbstractOAuth::Status::NotAuthenticated :
                QAbstractOAuth::Status::Granted;
    if (dropboxAuth->status() != newstatus)
        emit dropboxAuth->statusChanged(newstatus);
}

bool dropbox::logged()
{
    return !dropboxAuth->token().isEmpty();
}

QString dropbox::errorString()
{
    return m_error;
}

QDateTime dropbox::upload(QString localFileName, QString relativeCloudFileName)
{
    QFile file(localFileName);
    if (!file.open(QIODevice::ReadOnly))
        return QDateTime();
    QByteArray postData = file.readAll();
    file.close();

    return upload(postData,relativeCloudFileName);
}

QDateTime dropbox::upload(QByteArray data, QString relativeCloudFileName)
{
    initCall();

    QNetworkRequest req;
    req.setUrl(QUrl("https://content.dropboxapi.com/2/files/upload"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
// relativeCloudFileName will need URL encoded here, I think
    req.setRawHeader("Dropbox-API-Arg", QString("{\"path\":\"%1\",\"mode\":\"overwrite\",\"autorename\": true, \"mute\": true }").arg(relativeCloudFileName).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/octet-stream");

    auto reply = dropboxAuth->networkAccessManager()->post(req, data);
    getReply(reply)

    QDateTime dt;
    QJsonDocument doc = readReply(reply);
    if (m_error.isEmpty()) {
        qDebug() << doc;
        qDebug() << doc.object().value("server_modified").toString();
        dt = QDateTime::fromString(doc.object().value("server_modified").toString(),Qt::ISODate);
    }
    return dt;
}


void dropbox::download(QString relativeCloudFileName, QString localFileName, QDateTime &modifiedDate)
{
    QByteArray data;
    download(relativeCloudFileName,data,modifiedDate);

    QFile file(localFileName);
    file.open(QIODevice::WriteOnly);
    file.write(data);
    file.close();
}

void dropbox::download(QString relativeCloudFileName, QByteArray &content, QDateTime &modifiedDate)
{
    initCall();

    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QNetworkRequest req;
    req.setUrl(QUrl("https://content.dropboxapi.com/2/files/download"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
// relativeCloudFileName will need URL encoded here, I think
    QString temp = QString("{\"path\":\"%1\"}").arg(relativeCloudFileName);
    req.setRawHeader("Dropbox-API-Arg", temp.toUtf8());

    auto reply = dropboxAuth->networkAccessManager()->post(req,QByteArray());
    getReply(reply)

    content = reply->readAll();

    // get modified time
    QJsonDocument doc = readReply(reply, true);
    if (m_error.isEmpty())
        modifiedDate = QDateTime::fromString(doc.object().value("server_modified").toString(),Qt::ISODate);    
}

QDateTime dropbox::getModifiedDate(QString cloudFileName)
{
    initCall();

    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/files/get_metadata"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"path\":\"%1\"}").arg(cloudFileName);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply)

    QJsonDocument doc = readReply(reply);
    if (m_error.isEmpty()) {
        QDateTime dt = QDateTime::fromString(doc.object().value("server_modified").toString(), Qt::ISODate);
        return dt;
    }else {
        return QDateTime();
    }
}

bool dropbox::deleteFile(QString cloudFileName)
{
    initCall();

    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/files/delete_v2"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"path\":\"%1\"}").arg(cloudFileName);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply)

    QJsonDocument doc = readReply(reply);

    if (m_error.isEmpty()) {
        return doc.object().contains("metadata");
    } else {
        return  false;
    }
}

bool dropbox::createFolder(QString path)
{
    initCall();

    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/files/create_folder_v2"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"path\":\"%1\"}").arg(path);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply)

    QJsonDocument doc = readReply(reply);
    qDebug() << doc;    
    if (m_error.isEmpty()) {
        return doc.object().contains("metadata");
    } else {
        return false;
    }
}

QString dropbox::getSharedLink(QString path)
{
    initCall();

    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/sharing/list_shared_links"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"path\":\"%1\"}").arg(path);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply)

    QJsonDocument doc = readReply(reply);

    QString sharedLink = "";
    if (doc.object().contains("links")) {
        QJsonArray array = doc.object().value("links").toArray();
        if (array.count() > 0) {
            QString pathLower = array[0].toObject().value("path_lower").toString();
            if (path.compare(pathLower, Qt::CaseInsensitive) == 0)
                sharedLink = array[0].toObject().value("url").toString();
        }
    }

    if (sharedLink.isEmpty()) {
        req.setUrl(QUrl("https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings"));
        reply = dropboxAuth->networkAccessManager()->post(req,postData);
        getReply(reply);
        doc = readReply(reply);
        if (doc.object().contains("url"))
            sharedLink = doc.object().value("url").toString();
    }
    return sharedLink;
}

bool dropbox::fileExists(QString path)
{
    initCall();

    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/files/get_metadata"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"path\":\"%1\"}").arg(path);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply)

    QJsonDocument doc = readReply(reply);
    qDebug() << doc;
    if (m_error.isEmpty()) {
        return doc.object().contains("name");
    } else {
        return false;
    }
}

DBAccount *dropbox::getAccountInfo(bool forceLoad)
{
    if (!m_account)
        m_account = new DBAccount(this);

    initCall();
    QSettings settings;

    if (logged() && (forceLoad || settings.value("dropbox/email","").toString().isEmpty() ||
            settings.value("dropbox/name","").toString().isEmpty())) {
        qDebug() << "load account from dropbox" << forceLoad <<
                    settings.value("dropbox/email","");
        dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);
        QNetworkRequest req;
        req.setUrl(QUrl("https://api.dropboxapi.com/2/users/get_current_account"));
        req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");

        QByteArray postData = "null";
        auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
        getReply(reply)

        QJsonDocument doc = readReply(reply);

        m_account->setName(doc.object().value("name").toObject().value("display_name").toString());
        settings.setValue("dropbox/name",m_account->getName());
        m_account->setEmail(doc.object().value("email").toString());
        settings.setValue("dropbox/email",m_account->getEmail());
    } else {
        m_account->setName(settings.value("dropbox/name","").toString());
        m_account->setEmail(settings.value("dropbox/email","").toString());
    }

    m_account->setSyncFile(settings.value("dropbox/syncfile","").toString());
    return m_account;
}

QAbstractItemModel *dropbox::searchFile(QString query)
{
    initCall();
    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);

    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/files/search_v2"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"query\":\"%1\", \"options\":{ \"filename_only\":true }}").arg(query);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply);

    QHash<int,QByteArray> roles;
    roles[Qt::UserRole+1] = "id";
    roles[Qt::UserRole+2] = "name";
    roles[Qt::UserRole+3] = "path";
    roles[Qt::UserRole+4] = "sharedby";

    QStandardItemModel *model = new QStandardItemModel(0,4,this);
    model->setItemRoleNames(roles);

    QJsonDocument doc = readReply(reply);    
    QJsonArray array = doc.object()["matches"].toArray();
    for(QJsonValue v : array) {
        QJsonObject metaDataRoot = v.toObject().value("metadata").toObject();
        QJsonObject metadata = metaDataRoot.value("metadata").toObject();
        if (metadata.contains("path_display")) {
            QString id = metadata.value("id").toString();
            QString name = metadata.value("name").toString();
            QString path = metadata.value("path_display").toString();
            QString sharedBy = metadata.contains("sharing_info") ? getFileOwner(id) : "";

            QStandardItem *item = new QStandardItem();
            item->setData(id,roles.key("id"));
            item->setData(name,roles.key("name"));
            item->setData(path,roles.key("path"));
            item->setData(sharedBy,roles.key("sharedby"));

            model->setRowCount(model->rowCount()+1);
            model->setItem(model->rowCount()-1,item);
        }
    }
    return model;
}

QList<DBUser> dropbox::listFolderMembers(QString path)
{
    initCall();
    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);

    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/files/get_metadata"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString args = QString("{\"path\":\"%1\"}").arg(path);
    QByteArray postData = args.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req, postData);
    getReply(reply);

    QList<DBUser> list;
    QJsonDocument doc = readReply(reply);
    qDebug() << doc;

    if (doc.object().contains("sharing_info")) {
        QJsonObject sharingObj = doc.object()["sharing_info"].toObject();

        QString sharedFolderId;

        if (sharingObj.contains("shared_folder_id"))
            sharedFolderId = sharingObj["shared_folder_id"].toString();
        else if (sharingObj.contains("parent_shared_folder_id"))
            sharedFolderId = sharingObj["parent_shared_folder_id"].toString();

        if (!sharedFolderId.isEmpty()) {
            args = QString("{\"shared_folder_id\":\"%1\"}").arg(sharedFolderId);
            postData = args.toUtf8();

            req.setUrl(QUrl("https://api.dropboxapi.com/2/sharing/list_folder_members"));
            reply = dropboxAuth->networkAccessManager()->post(req,postData);
            getReply(reply);
            doc = readReply(reply);
            if (doc.object().contains("users")) {
                QJsonArray usersArray = doc.object()["users"].toArray();
                for (QJsonValue v: usersArray) {
                    QString accessType = v.toObject()["access_type"].toObject()[".tag"].toString();
                    QJsonObject userObj = v.toObject().value("user").toObject();
                    qDebug() << "Dropbox user:" << userObj["display_name"].toString()
                             << userObj["email"].toString() << accessType;
                    DBUser usr;
                    usr.displayName = userObj["display_name"].toString();
                    usr.email = userObj["email"].toString();
                    usr.owner = (accessType == "owner");
                    list.append(usr);
                }
            }
        }
    }   
    return list;
}

QString dropbox::getFileOwner(QString id)
{
    initCall();
    dropboxAuth->setContentType(QAbstractOAuth::ContentType::Json);

    QNetworkRequest req;
    req.setUrl(QUrl("https://api.dropboxapi.com/2/sharing/list_file_members"));
    req.setRawHeader("Authorization", QString("Bearer %1").arg(dropboxAuth->token()).toUtf8());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString temp = QString("{\"file\":\"%1\"}").arg(id);

    QByteArray postData = temp.toUtf8();

    auto reply = dropboxAuth->networkAccessManager()->post(req,postData);
    getReply(reply);        

    QString owner = "";

    QJsonDocument doc = readReply(reply);
    QJsonArray array = doc.object()["users"].toArray();
    for(QJsonValue v : array) {
        QJsonObject accesstype = v.toObject().value("access_type").toObject();
        QJsonObject user = v.toObject().value("user").toObject();

        if (accesstype.value(".tag").toString() == "owner")
            owner = user.value("display_name").toString();
    }    
    return owner;
}

void dropbox::initCall()
{
    if (!dropboxAuth->networkAccessManager()) {
        dropboxAuth->setNetworkAccessManager(new QNetworkAccessManager(dropboxAuth));
    }
    m_error = "";
}

QJsonDocument dropbox::readReply(QNetworkReply *reply, bool readFromHeader)
{
    QJsonDocument doc = readFromHeader ?
                QJsonDocument::fromJson(reply->rawHeader("Dropbox-API-Result")) :
                QJsonDocument::fromJson(reply->readAll());
    int status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    // 200 = ok
    // 400 = bad parameters
    // 401 = bad or expired access token
    if (status != 200) {
        if (status == 401)
            dropboxAuth->grant();
        if (doc.object().contains("error_summary"))
            m_error = doc.object().value("error_summary").toString() +
                    QString(" (%1)").arg(status);
        else
            m_error = QString::number(status);
        if (reply->error() != QNetworkReply::NoError)
            m_error += QString(", Network error: %1 %2").arg(reply->error()).arg(reply->errorString());
        qDebug() << "HTTP Status" << status << "error" << m_error;
    }
    reply->deleteLater();
    return doc;
}
