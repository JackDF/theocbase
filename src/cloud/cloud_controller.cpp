#include "cloud_controller.h"

cloud_controller::cloud_controller(QObject *parent)
    : QObject(parent)
{
    qmlRegisterType<cloud_controller>("net.theocbase", 1, 0, "Cloud");
    qmlRegisterType<dropbox>("net.theocbase", 1, 0, "Dropbox");
    qmlRegisterType<DBAccount>("net.theocbase", 1, 0, "DBAccount");
    mDropbox = new dropbox(constants::dropboxclientid(), constants::dropboxsharedkey());
    mDBAccount = mDropbox->getAccountInfo();
    s.setAuthentication(mDropbox);

    sql_class *sql = &Singleton<sql_class>::Instance();

    connect(mDropbox, &dropbox::authorizeWithBrowser, this, &cloud_controller::loginRequired);
    connect(mDropbox, &dropbox::statusChanged, [=](QAbstractOAuth::Status st) {
        switch (st) {
        case QAbstractOAuth::Status::Granted:
            initDropbox();
            emit loggedChanged(true);
            break;
        case QAbstractOAuth::Status::NotAuthenticated:
            emit loggedChanged(false);
            break;
        default:
            return;
        }
    });
    connect(mDBAccount, &DBAccount::syncFileChanged, [=] {
        QFile localAccessFile(cachedAccessControlFile());
        if (localAccessFile.exists())
            localAccessFile.remove();
        initAccessControl();
    });
    connect(&s, &sync_cloud::syncConflict, this, &cloud_controller::syncConflict);
    connect(&s, &sync_cloud::progressed, [=](int value) { emit syncProgressed(value, 100); });
    connect(&s, &sync_cloud::ready, [=] {
        sql->saveSetting("local_changes", "false");
        mSyncState = SyncState::Synchronized;
        emit syncFinished();
        syncTimeCheck();
        emit stateChanged(mSyncState);
    });
    connect(&s, &sync_cloud::error, this, &cloud_controller::error);
    connect(&s, &sync_cloud::differentLastDbUser, this, &cloud_controller::differentLastDbUser);
    connect(&s, &sync_cloud::cloudResetFound, this, &cloud_controller::cloudResetFound);
    connect(&s, &sync_cloud::cloudResetReady, this, &cloud_controller::cloudResetFinished);

    mSyncState = QVariant(sql->getSetting("local_changes", "false")).toBool() ? SyncState::Upload : SyncState::Synchronized;
    connect(sql, &sql_class::dbChanged, this, &cloud_controller::databaseChanged, Qt::QueuedConnection);

    syncTimeCheck();
}

void cloud_controller::login()
{
    if (!QSslSocket::supportsSsl()) {
        emit error("This device doesn't support SSL.\n"
                   "Make sure that OpenSSL is installed.");
        return;
    }
    mDropbox->authenticate();
}

bool cloud_controller::logged()
{
    return mDropbox->logged();
}

void cloud_controller::logout(int clearDB)
{
    QSettings settings;
    settings.setValue("dropbox/syncfile", "");

    mDropbox->revoke();

    // clear database
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->blockSignals(true);

    switch (clearDB) {
    case 0:
        // don't clear database
        sql->saveSetting("last_dbsync_id", "0");
        sql->saveSetting("last_dbsync_time", "-1");
        break;
    case 1:
        clearDatabase();
        sql->saveSetting("last_db_user", "");
        break;
    case 2:
        AccessControl *ac = &Singleton<AccessControl>::Instance();
        if (ac->user() && !ac->user()->hasPermission(Permission::Rule::CanEditPermissions)) {
            // clear database, if not owner of the cloud storage
            clearDatabase();
            sql->saveSetting("last_db_user", "");
        } else {
            // don't clear database, if owner of the cloud storage
            sql->saveSetting("last_dbsync_id", "0");
            sql->saveSetting("last_dbsync_time", "-1");
        }
        break;
    }
    // reset access control
    QFile localAccessControlFile(cachedAccessControlFile());
    if (localAccessControlFile.exists())
        localAccessControlFile.remove();
    settings.setValue("local_access_control", -1);
    initAccessControl();

    sql->blockSignals(false);
    setLastSyncTime(QDateTime());
    emit loggedChanged(false);
}

bool cloud_controller::checkCloudUpdates()
{
    bool updates = s.cloudUpdateAvailable();
    if (updates) {
        mSyncState = (mSyncState == SyncState::Upload) ? SyncState::Both : SyncState::Download;
        emit stateChanged(mSyncState);
    }
    qDebug() << "updates checked" << updates;
    return updates;
}

cloud_controller::SyncState cloud_controller::syncState()
{
    return mSyncState;
}

void cloud_controller::synchronize(bool ignoreUser)
{
    emit syncStarted();
    // load access control file
    QSettings settings;
    int localModified = settings.value("local_access_file", -1).toInt();
    QString acFile = cloudAccessControlFile();
    QDateTime dt = mDropbox->getModifiedDate(acFile);
    if (dt.toMSecsSinceEpoch() > localModified)
        loadAccessControl();
    // sync database
    s.sync(ignoreUser);
}

void cloud_controller::continueSynchronize(bool keepLocalChanges)
{
    s.continueSync(keepLocalChanges);
}

void cloud_controller::uploadAccessControl()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    QSettings settings;

    QString dbFile = cloudAccessControlFile();
    QJsonObject jsObj;
    ac->write(jsObj);
    QJsonDocument jsDoc(jsObj);
    QDateTime dt = mDropbox->upload(jsDoc.toJson(QJsonDocument::Compact), dbFile);
    qDebug() << "access control file uploaed" << dt;
    if (dt.isValid())
        settings.setValue("local_access_control", dt.toSecsSinceEpoch());

    // save cached file
    QFile cacheFile(cachedAccessControlFile());
    cacheFile.open(QFile::WriteOnly);
    cacheFile.write(jsDoc.toJson(QJsonDocument::Compact));
}

void cloud_controller::initAccessControl()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    // get publisher and admin roles
    const Role *publisherRole = nullptr;
    const Role *adminRole = nullptr;
    for (const Role &role : ac->roles()) {
        if (role.id() == Permission::RoleId::Publisher)
            publisherRole = &role;
        if (role.id() == Permission::RoleId::Administrator)
            adminRole = &role;
    }
    if (logged()) {
        // Logged to Dropbox
        // get Dropbox account info
        DBAccount *dbUser = mDropbox->getAccountInfo();

        QFile localFile(cachedAccessControlFile());
        qDebug() << "cache file" << localFile;
        bool valid = false;
        if (localFile.exists()) {
            qDebug() << "found cached file";
            // use saved local file
            localFile.open(QFile::ReadOnly);
            QJsonDocument doc = QJsonDocument::fromJson(localFile.readAll());
            ac->load(doc.object(), valid);
            localFile.close();
            if (valid) {
                ac->setUser(ac->findUser(dbUser->getEmail()));
                if (ac->user())
                    return;
            }
        }
        // download access control file from Dropbox
        loadAccessControl();
        QString dbFile = cloudAccessControlFile();
        User *usr = ac->findUser(dbUser->getEmail());
        if (usr) {
            ac->setUser(usr);
        } else {
            // create new access control file
            if (!dbFile.compare(defaultDropboxDir + "access_control.json", Qt::CaseInsensitive)) {
                // the current user is owner of Dropbox folder
                // replace previously created admin user
                usr = ac->findUser("admin");
                if (usr) {
                    usr->setEmail(dbUser->getEmail());
                    usr->setName(dbUser->getName());
                    // make sure user has admin rights
                    if (adminRole)
                        usr->addRole(adminRole);
                }
            }
            if (!usr) {
                // create new user and give the viewer rights
                ac->addUser(dbUser->getName(), dbUser->getEmail());
                usr = ac->findUser(dbUser->getEmail());
                if (publisherRole)
                    usr->addRole(publisherRole);
            }
            ac->setUser(usr);
        }
        // save cache file
        QJsonObject jsObj;
        ac->write(jsObj);
        QJsonDocument jsDoc(jsObj);
        localFile.open(QFile::WriteOnly);
        localFile.write(jsDoc.toJson());
    } else {
        // Not logged to Dropbox
        for (int i = ac->users().count(); i-- > 0;)
            ac->removeUser(ac->users()[i]->id());
        ac->addUser("ADMIN", "admin");
        User *usr = ac->findUser("admin");
        for (const Role &role : ac->roles()) {
            if (role.id() != Permission::RoleId::Administrator)
                usr->addRole(&role);
        }
        ac->setUser(usr);
    }
}

void cloud_controller::loadAccessControl()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    QSettings settings;

    QString acFile = cloudAccessControlFile();
    QByteArray b;
    QDateTime dt;
    mDropbox->download(acFile, b, dt);
    if (dt.isValid()) {
        // load file
        bool valid = false;
        QJsonDocument tempJSDoc = QJsonDocument::fromJson(b);
        ac->load(tempJSDoc.object(), valid);
        settings.setValue("local_access_control", dt.toSecsSinceEpoch());
        // update local file
        QFile localFile(cachedAccessControlFile());
        localFile.open(QFile::WriteOnly);
        localFile.write(tempJSDoc.toJson());
    }
}

void cloud_controller::runTest()
{
    s.runTest();

    //    qDebug() << "runTest" << QThread::currentThread();
    //    QThread *thread = new QThread(this);
    //    sync_cloud *sc = new sync_cloud();

    //    sc->moveToThread(thread);
    //    connect(thread,&QThread::started,sc,&sync_cloud::runTest);
    //    thread->start();
}

bool cloud_controller::canResetCloudData()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    return logged() && ac->user() && ac->user()->hasPermission(PermissionRule::CanDeleteCloudData);
}

void cloud_controller::resetCloudData()
{
    if (!canResetCloudData())
        return;
    emit cloudResetStarted();
    s.resetCloudData();
}

void cloud_controller::clearDatabase()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->clearDatabase();
}

dropbox *cloud_controller::authentication()
{
    return mDropbox;
}

QDateTime cloud_controller::lastSyncTime() const
{
    return mLastSyncTime;
}

void cloud_controller::setLastSyncTime(QDateTime value)
{
    if (mLastSyncTime != value) {
        mLastSyncTime = value;
        emit lastSyncTimeChanged(value);
    }
}

QString cloud_controller::debugBackground()
{
    QSettings settings;
    QString value = settings.value("mobile/backgroundtest", "").toString();
    return value;
}

void cloud_controller::setDebugBackground(QString value)
{
    QSettings settings;
    settings.setValue("mobile/backgroundtest", value);
}

void cloud_controller::syncTimeCheck()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QDateTime dt;
    uint timestamp = QVariant(sql->getSetting("last_dbsync_time")).toUInt();
    if (timestamp > 0)
        dt.setTime_t(timestamp);
    setLastSyncTime(dt);
}

void cloud_controller::initDropbox()
{
    qDebug() << "Init dropbox";
    if (mDropbox->getAccountInfo(true)->getSyncFile().isEmpty()) {
        // set default syncfile
        QString defaultSyncFile = defaultDropboxDir + "syncfile.json";
        QDateTime dt = mDropbox->getModifiedDate(defaultSyncFile);
        if (!dt.isValid())
            mDropbox->upload(QByteArray("{}"), defaultSyncFile); // create an empty file
        mDropbox->getAccountInfo()->setSyncFile(defaultSyncFile);
        // reset sync id
        sql_class *sql = &Singleton<sql_class>::Instance();
        sql->saveSetting("last_dbsync_id", "0");
        sql->saveSetting("last_dbsync_time", "-1");
    }
}

const QString cloud_controller::cachedAccessControlFile()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + QDir::separator() + "access_control.json";
}

const QString cloud_controller::cloudAccessControlFile()
{
    if (!logged())
        return "";
    const QFileInfo syncFileInfo(mDropbox->getAccountInfo()->getSyncFile());
    QString dropboxFolder = syncFileInfo.path();
    return dropboxFolder + "/access_control.json";
}

void cloud_controller::databaseChanged(const QString tablename)
{
    if (tablename != "e_reminder" && tablename != "lmm_workbookregex") {
        SyncState newstate = (mSyncState == SyncState::Download) ? SyncState::Both : SyncState::Upload;
        if (newstate != mSyncState) {
            mSyncState = newstate;
            sql_class *sql = &Singleton<sql_class>::Instance();
            sql->saveSetting("local_changes", "true");
            emit stateChanged(mSyncState);
        }
    }
}
