#ifndef DROPBOX_H
#define DROPBOX_H

#include <QUrl>
#include <QFile>
#include <QString>
#include <QDebug>
#include <QOAuth2AuthorizationCodeFlow>
#include <QOAuthHttpServerReplyHandler>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QMessageBox>
#include <QSettings>
#include <QEventLoop>
#include <QStandardItemModel>

class DBAccount : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY(QString email READ getEmail NOTIFY emailChanged)
    Q_PROPERTY(QString syncFile READ getSyncFile WRITE setSyncFile NOTIFY syncFileChanged)
public:
    DBAccount(QObject *parent = nullptr) : QObject(parent){}
    DBAccount(QString name, QString email,QObject *parent = nullptr) : QObject(parent){
        m_name = name;
        m_email = email;
    }
    QString getName() const { return m_name; }
    void setName(const QString name) { m_name = name; emit nameChanged(name); }
    QString getEmail() const { return m_email; }
    void setEmail(const QString email) { m_email = email; emit emailChanged(email); }
    QString getSyncFile() const {
        QSettings s;
        return s.value("dropbox/syncfile","").toString();
    }
    void setSyncFile(const QString path) {        
        QSettings s;
        if (path == s.value("dropbox/syncfile","").toString())
            return;
        s.setValue("dropbox/syncfile", path);
        emit syncFileChanged(path);
    }
signals:
    void nameChanged(QString name);
    void emailChanged(QString email);
    void syncFileChanged(QString path);
private:
    QString m_name = "";
    QString m_email = "";
};

struct DBUser {
public:
    QString displayName;
    QString email;
    bool owner;
};

// I would put this in a method, but I get an illegal indirection, so this is the best I can do
#define getReply(reply) \
    QEventLoop loop; \
    connect(reply,&QNetworkReply::finished,&loop,&QEventLoop::quit); \
    loop.exec(); \

class dropbox : public QObject
{
    Q_OBJECT
public:    
    dropbox(QObject *parent = nullptr) : QObject(parent) {}
    dropbox(QString clientIdentifier, QString sharedKey, QObject *parent = nullptr);

    void authenticate();
    void revoke();
    Q_INVOKABLE void setToken(const QString token);
    bool logged();
    QString errorString();

    QDateTime upload(QString localFileName, QString relativeCloudFileName);
    QDateTime upload(QByteArray data, QString relativeCloudFileName);

    void download(QString relativeCloudFileName, QString localFileName, QDateTime &modifiedDate);
    void download(QString relativeCloudFileName, QByteArray &content, QDateTime &modifiedDate);

    QDateTime getModifiedDate(QString cloudFileName);
    bool deleteFile(QString cloudFileName);

    bool createFolder(QString path);
    QString getSharedLink(QString path);

    bool fileExists(QString path);

    Q_INVOKABLE DBAccount *getAccountInfo(bool forceLoad = false);

    Q_INVOKABLE QAbstractItemModel *searchFile(QString query);
    QList<DBUser> listFolderMembers(QString path);

signals:
    void authorizeWithBrowser(const QUrl &url);
    void statusChanged(QAbstractOAuth::Status status);

private:
    QString getFileOwner(QString id);

    QString m_error = "";
    QOAuth2AuthorizationCodeFlow *dropboxAuth;
    QSettings settings;
    DBAccount *m_account = nullptr;

    void initCall();
    QJsonDocument readReply(QNetworkReply *reply, bool readFromHeader = false);
};

#endif // DROPBOX_H
