/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYNC_H
#define SYNC_H
#include <QXmlStreamWriter>
#include "sql_class.h"
#include "generatexml.h"
#include "cpersons.h"
#include "school.h"
#include "ccongregation.h"
#include "cpublictalks.h"

/*
 * version 5:
 *   added onlybrothers to school data
 *   added day/time to congregation data
 * version 6:
 *   added families
 * version 7:
 *   Life and ministry meeting
 * version 8:
 *   Uuid added to publishers sync (workaround to prevent duplicate publishers when mixing data exchange and cloud sync)
 * version 9:
 *   Volunteer in LMM Student assignment
*/
const int versionnumber = 9;

class csync : public QObject
{
    Q_OBJECT

public:
    csync(QObject *parent = nullptr);

    enum SyncType
    {
        Publisher,
        WeekendMeeting,
        PublicTalk,
        MidweekMeeting,
        Obsolete
    };
    Q_ENUM(SyncType)

    void CreateXMLFile(QString filename, QDate tempdate,
                       int spinbox,
                       bool publishers, bool speakers,
                       bool mwmeeting, bool studyhistory, bool publicmeeting, bool outgoing);

    Q_INVOKABLE void readXmlFile(QString filename);

    /**
     * @brief readFromTmsWare - Import data from TMSWare
     * @param filename - CSV file name
     */
    void readFromTmsWare(QString filename);

private:
    void syncInfo();
    void syncPersons(QList< QHash<QString,QString> >plist);
    void syncSchool(QList< QHash<QString,QString> > slist, int classes);
    void syncMidweekMeeting(QVector<QPair<QHash<QString, QString>, QVector<QHash<QString, QString> > > > mwlist);
    void syncFamilies(QList < QHash<QString,QString> > flist);
    void syncSchoolStudies(QList< QHash<QString, QString> > stlist);
    void syncPublicTalks(QVector< QList<QString> > themelist,
                         QVector< QList<QString> > speakerthemes,
                         QVector<QHash<QString, QString> > meetinglist);

    QHash<QString,QString> parseTmsWareStudents(QStringList rowstring, QStringList headerrow);
    QHash<QString,QString> parseTmsWareTalkHistory(QStringList rowstring, QStringList headerrow);
    QHash<QString,QString> parseTmsWareStudyHistory(QStringList rowstring, QStringList headerrow);

    /**
     * @brief validateTmsWareStudentsHeader - Validate header row of TMSWare students file
     * @param header - List of header texts
     * @return - true or false
     */
    bool validateTmsWareStudentsHeader(QStringList header);

    /**
     * @brief validateTmsWareTalkHistoryHeader - Validate header row of TMSWare talk history file
     * @param header - List of header texts
     * @return - true or false
     */
    bool validateTmsWareTalkHistoryHeader(QStringList header);

    /**
     * @brief validateTmsWareStudyHistoryHeader - Validate header row of TMSWare study history file
     * @param header - List of header texts
     * @return - true or false
     */
    bool validateTmsWareStudyHistoryHeader(QStringList header);

    QDate parseDate(QString datestring);
    sql_class *sql;

    ccongregation::congregation defaultcongregation;
    person *findPerson(QHash<QString, QString> container, QString namePrefix);
signals:
    void newReportRow(QString text, SyncType typ);
    void progressBarChanged(int value);

};
#endif // SYNC_H
