/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TODO_H
#define TODO_H

#include <QList>
#include <QStandardItemModel>
#include "sql_class.h"
#include "ccongregation.h"
#include "cpersons.h"
#include "cpublictalks.h"
#include "general.h"

class todo
{
public:
    static QList<todo *> getList();

    todo(bool isIncoming);
    todo(int id, bool isIncoming);

    bool getIsIncoming() const;

    int getId() const;
    void setId(int value);

    QDate getDate() const;
    void setDate(const QDate &value);

    QString getSpeaker() const;
    void setSpeaker(const QString &value);

    QString getCongregation() const;
    void setCongregation(const QString &value);

    QString getTheme() const;
    void setTheme(const QString &value);

    QString getNotes() const;
    void setNotes(const QString &value);

    // these are valid only after a call to moveToSchedule, and are only useful if the move failed.
    QDate get_date() const;
    int get_congregationId() const;
    person *get_speaker();
    int get_themeId();

    void deleteme();
    void save();
    QString moveToSchedule();

private:
    sql_class *sql;
    int id;
    bool isIncoming;
    QString speaker;
    QString congregation;
    QString theme;
    QString notes;
    QDate _date;
    int _congregationId;
    QSharedPointer<person> _speaker;
    int _themeId;
};

#endif // TODO_H
