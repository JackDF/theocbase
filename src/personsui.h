/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERSONS_H
#define PERSONS_H

#include <QtWidgets>
#include <QListWidgetItem>
#include <QStringListModel>
#include <QAbstractItemModel>
#include <QListView>
#include <vector>
#include <sql_class.h>
#include "accesscontrol.h"
#include <school.h>
#include "person.h"
#include "cpersons.h"
#include "ccongregation.h"
#include <QPair>
#include "family.h"

namespace Ui {
class personsui;
}

/**
 * @brief The personsui class - Persons User Interface Class
 *                              This class is used to show publishers data.
 *                              You can add, edit or remove publishers in this dialog.
 *                              User interface is done by Qt Creator's visual editor (see file personsui.ui)
 */
class personsui : public QDialog
{
    Q_OBJECT
public:
    personsui(QWidget *parent = 0);
    ~personsui();

    /**
     * @brief setDefaultPerson - Set default publisher to default when opening a dialog
     * @param name - Full name of publisher
     */
    void setDefaultPerson(QString name);
    bool saveChanges();

protected:
    void changeEvent(QEvent *e);

private:
    void applyAuthorizationRules();

    /**
     * @brief updatePersonList - Rebuild list of publishers
     */
    void updatePersonList();

    /**
     * @brief saveChanges - Save the changes made to the selected publisher
     * @return
     */
    bool saveChanges(QListWidgetItem *);

    /**
     * @brief getStudies - This function shows publisher's school studies in the grid list
     */
    void getStudies();

    /**
     * @brief getSchoolHistory - This function shows publisher's school assignment history in the grid list
     */
    void getSchoolHistory();

    /**
     * @brief selectPerson
     * @param personid
     */
    //void selectPerson(int personid);
    void updateDetailsPaneWith(QListWidgetItem const *p);

    std::unique_ptr<person> currentPublisher() const;

    void updateFamiliesList();
    void ClearDetailsPane();
    void SaveFamilySetting(int personId);

    // member variables
    Ui::personsui *m_ui;
    QList<family *> m_families;
    sql_class *sql;
    AccessControl *ac;
    ccongregation c;
    int myCongregationId;
    bool editmode;
    QString m_defaultPersonName;
    int studycolumn;
    int defaultWtConductor;
    int m_studiesGridScrollPosition;

private slots:
    void on_buttonRemoveUnavailable_clicked();
    void on_dateUnavailableStart_dateChanged(QDate date);
    void on_buttonAddUnavailable_clicked();

    /**
     * @brief on_chkNominated_2_toggled - Event when the state of 'Servant' checkbox changed
     * @param checked
     */
    void on_chkNominated_2_toggled(bool checked);

    void on_radBrother_2_toggled(bool checked);

    /**
     * @brief on_buttonAddPerson_clicked - Event when click button to add a new publisher
     */
    void on_buttonAddPerson_clicked();

    /**
     * @brief on_buttonRemovePerson_clicked - Event when click button to remove a selected publisher
     */
    void on_buttonRemovePerson_clicked();

    /**
     * @brief on_lstPublishers_clicked - Event when a publisher has been selected in the list
     * @param index
     */
    void on_lstPublishers_itemClicked(QListWidgetItem *);
    void on_lstPublishers_currentItemChanged(QListWidgetItem *, QListWidgetItem *);

    void on_tableWidget_itemChanged(QTableWidgetItem *item);
    void calendarChanged(int row);
    void on_tableWidget_itemClicked(QTableWidgetItem *item);

    /**
     * @brief addStudyRemoveButton - This adds remove button for study row
     *                               Used when building studies grid list
     * @param table - QTableWidget for studies
     * @param row - Row index
     * @param col - Column index
     */
    void addStudyRemoveButton(QTableWidget *table, int row, int col);

    /**
     * @brief on_buttonStudyRemove_clicked - Event when clicked remove button on studies list
     */
    void on_buttonStudyRemove_clicked();
    void on_buttonWtConductorDefault_toggled(bool checked);

protected:
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *e);

signals:
    void updateScheduledTalks(int speakerId, int oldCongregationId, int newCongregationId, bool removed);
};

#endif // PERSONS_H
